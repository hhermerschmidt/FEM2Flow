% Display and store start and end of current time slab (case 2):
[CalcProtocol, ~] = StoreDispCalcProtocol(CalcProtocol, [], 2, time, []);

% Start of calculation for current time slab:
%------------------------------------------------

% Evaluate time variation in current time slab:
currTimeVar = EvaluateTimeVariation(time.variation,time.curr);


% Get node coordinates at end of time slab due to mesh movement
% -----------------------------------------------------------------

% Extracting node coordinates at beginning of time slab
nodes_t0 = universe.nodes_t1;
nodes_t1 = universe.nodes_t1;

% Calculation of nodes_t1 and adjust connectivity if necessary
if universe.flag.meshMovement == 1
    [ universe, elem, nodes_t1 ] = MeshMovement(universe, time, elem, nodes_t0, nodes_t1);
end

% Modification of Dirichlet b.c.s in case of time-dependence
if universe.flag.bcDirTime == 1
    universe = ModifyBcDirTime( universe, time, nodes_t0, nodes_t1 );
end


% Set indexSystem of each element used to store element matrices in global system:
%-------------------------------------------------------------------------------------
for mm = 1 : universe.meshData.nEle % loop over all elements
    SetIndexSystem(universe.sysIndex, universe.sysIndexMat, elem{mm} );
end

for iteration = 1 : universe.nIterMaxSol

    % Call script to build matrices of all elements:
    %--------------------------------------------------
    BuildElem

    % Call script to assemble global system of equations:
    %-----------------------------------------------------
    Assembling

    % Call script to solve the global system of equations:
    %------------------------------------------------------
    Solver

    % Call script to postprocess the solution:
    %--------------------------------------------
    PostElem

    % Call script to check convergence of solution:
    %-----------------------------------------------
    CheckConvergence

end

% Saving node coordinates at end of current time slab as initial coordinates for next time slab
universe.nodes_t0 = nodes_t0;
universe.nodes_t1 = nodes_t1;


% Store time step and number of iterations for current time slab (case 4):
[~, convData] = StoreDispCalcProtocol([], convData, 4, time, iteration);
