% Display solution increments of individual fields
% (only for free surface flow elements)

if elem{1}.type == 4250 || elem{1}.type == 4251
    [CalcProtocol, ~] = StoreDispCalcProtocol(CalcProtocol, convData, 12, time, [solu, deltaSolu]);
end

% Store the solution of the current time slab in the elements:

for mm = 1 : universe.meshData.nEle  % loop over elements
    
    % Get the position of current element in the solution vector:
    index = elem{mm}.indexSystem;
    
    % Nachlaufrechnung (auf Elementebene)
    % Bei gem-hybr Elementen Berechnung der (internen) ElementFHG
	elem{mm}.Post(solu(index), deltaSolu(index), time.delta);
            
end