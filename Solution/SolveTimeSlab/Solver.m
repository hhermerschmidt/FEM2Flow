% Get the norm of solu:
convData.normSolu      = norm(solu);

switch universe.typeLinearisation
    
    case 1 % Fixed point iteration
        
        switch universe.typeSolver
            
            case 1 % Direct solver
                
                deltaSolu = solu;              % Store solution of last iteration
                solu      = K \ rhs;           % Calculate the current solution
                deltaSolu = solu - deltaSolu;  % Calculate change of solution
                
            case 2 % Iterative solver
                
                error('The chosen combination of linearisation and solver is not yet implemented.')
                
            otherwise
                error('The chosen combination of linearisation and solver is not yet implemented.')
        end
        
    case 2 % Newton-Raphson procedure
        
        switch universe.typeSolver
            
            case 1 % Direct solver
                
                deltaSolu = K \ rhs;           % current solution
                
                solu      = solu + deltaSolu;  % total solution
                                               % L�sung aktueller
                                               % NR-Iteration in aktueller
                                               % RZ-Scheibe

                
            case 2 %Iterative solver
                
                setup.type  = 'nofill';
                [L,U]       = ilu(K, setup);
                clear setup

        %         [L,U] = luinc(K,'0');
                tol = 1e-8;
                [deltaSolu,flag,relres,iter,resvec] = gmres(K, rhs,20,tol,20,L,U);

                if flag ~= 0
                    disp('iterative solver did not converge')
                    convData.stopSol = 1;
                end

                solu = solu + deltaSolu;
                
            case 3 %iterative, BiCG
                
                deltaSolu = bicg(K,rhs);
                
                solu      = solu + deltaSolu;
                
            otherwise
                error('The chosen combination of linearisation and solver is not yet implemented.')  
        end
        
end

% Get the norm of deltaSolu:
convData.normDeltaSolu = norm(deltaSolu);

% Display and store norm of current iteration (case 3):
[CalcProtocol, ~] = StoreDispCalcProtocol(CalcProtocol, convData, 3, time, iteration);

if universe.flag.eigen == 1
    
    if abs( convData.normDeltaSolu/convData.normSolu ) <= universe.precision
        EigVal(:,nn_eigen) = eig(full(K));
    end
    
end

if isreal(deltaSolu) == 0
    disp('Solu contains complex numbers, solution is stopped')
    convData.stopSol = 1;
    break
end

clear K
