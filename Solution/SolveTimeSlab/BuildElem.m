% Initialise the vectors I, J and X used to assemble the sparse matrix:
%----------------------------------------------------------------------            
I    = zeros(universe.sizeX,1);
J    = zeros(universe.sizeX,1);
X    = zeros(universe.sizeX,1);

% Initialise the right hand side:
%-----------------------------------
rhs  = zeros(universe.dimSys, 1);


% Get the element matrices and the positions in the global system of eq.:
%-------------------------------------------------------------------------

cc   = 1; % counter for position in I, J and X

% Get node coordinates at end of time slab due to mesh movement
% -----------------------------------------------------------------
%MeshMovement
 

% Loop over elements:
for mm = 1 : universe.meshData.nEle 
    
    indexSystem = elem{mm}.indexSystem;
   
    
    % Get data for current element:
    eleSolu     = solu(indexSystem);             % L�sung aus letzter NR-Iteration (diese RZ-Scheibe)
    eleSoluDis  = soluDis(indexSystem);          % L�sung aus letzter RZ-Scheibe (letzte Iteration)        
    % Bei Verwendung SSMUM muss hier auf "alten" Systemindex
    % aus der letzten RZ-Scheibe zugegriffen werden.
   
    eleCoords.t0 = nodes_t0(elem{mm}.eleNodes_t0, :);
    eleCoords.t1 = nodes_t1(elem{mm}.eleNodes_t1, :);
    
%     eleMatPara  = universe.matPara(  elem{mm}.matParaSet  );
%     eleLoadPara = universe.loadPara( elem{mm}.loadParaSet );
    
     
    % Call methods of element classes to build the element matrices:
    [ rhsElem, ...
      I_elem , ...
      J_elem , ...
      X_elem , ...
                 ] = elem{mm}.Build( eleSolu    , ...
                                     eleSoluDis , ...
                                     eleCoords  , ...
                                     time       , ...
                                     currTimeVar  ...
                                   );
    
    sizeXElem = size(X_elem,1);
   
	I(cc : cc+sizeXElem-1) = I_elem;
	J(cc : cc+sizeXElem-1) = J_elem;
	X(cc : cc+sizeXElem-1) = X_elem;
    
 	cc = cc + sizeXElem;
    
    % Add current contribution to the right hand side:
    rhs(indexSystem,1) = rhs(indexSystem,1) + rhsElem;
 
end


% Delete surplus entries from I, J and X (if any):
%---------------------------------------------------
if cc - 1 < universe.sizeX
    I = I(1 : cc - 1);
    J = J(1 : cc - 1);
    X = X(1 : cc - 1);
end

clear rhsElem indexSystem I_elem J_elem X_elem cc