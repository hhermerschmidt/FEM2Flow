% Check convergence:
%------------------------------------------------------------------

% Stop iterations if the change of the solution is smaller than the
% required precision:
if ( abs( convData.normDeltaSolu / convData.normSolu ) <= universe.precision )
    
    if universe.flag.dumpStiffnessMatrix > 0
    
        if mod( universe.counter.dumpStiffnessMatrix, universe.flag.dumpStiffnessMatrix ) == 0           
                DumpStiffnessMatrix(I, J, X, universe, time);      
        end
   
    universe.counter.dumpStiffnessMatrix = universe.counter.dumpStiffnessMatrix + 1;

    end

    clear I J X
  
    break 
    
end 

% Break calculation if solution contains complex values                        
if  convData.stopSol == 1 , break , end 