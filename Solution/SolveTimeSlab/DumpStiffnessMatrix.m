function [] = DumpStiffnessMatrix( I, J, X, universe, time )
% Function saving stiffness matrix to h5 file.
%
% Given the stiffness matrix via their rowIndex I, colIndex J and its
% nonzero values X, these data is saved in the h5-file
% "<universe.name>_StiffnessMatrix.h5" for the current time slab.
%
% Input Data:
% --------------
%
% I: vector
%    containing the row indices of the nonzero entries
%
% J: vector
%    containing the column indices of the nonzero entries
%
% X: vector
%    containing the nonzero entries of the stiffness matrix
%
% universe: structure
%           containing general information of the calculation
%
% time: structure
%       containing the time information of the calculation
%
%
% Output Data:
% ---------------
%
% []: None
%
%
% Usage:
% ---------
%
% [] = DumpStiffnessMatrix( I, J, X, universe, time )

% created by    Henning Schippke
% created on    22.10.15
% last modified 24.10.15

% TODO:
% ...

% ================================================================= %
% ================================================================= %

% output path data
fileName  = [universe.name,'_StiffnessMatrix','.h5'];
savePath  = universe.folderResults;

fullPath  = [savePath, fileName];

% current time slab
timeStamp = ['TimeInc_',sprintf('%04d',time.currSlab)];

% vector "conversion
I = uint64(I);
J = uint64(J);

I = I';
J = J';
X = X';

% get vector sizes
sizeI = size(I);
sizeJ = size(J);
sizeX = size(X); 

% initialise (and create if necessary) I, J, X data fiels in h5 file
h5create(fullPath,['/',timeStamp,'/','rowIndex'],sizeI,'datatype','uint64');
h5create(fullPath,['/',timeStamp,'/','colIndex'],sizeJ,'datatype','uint64');
h5create(fullPath,['/',timeStamp,'/','value'   ],sizeX);

% save data to h5 file
h5write(fullPath,['/',timeStamp,'/','rowIndex'],I);
h5write(fullPath,['/',timeStamp,'/','colIndex'],J);
h5write(fullPath,['/',timeStamp,'/','value'   ],X);

% add current time stamp as group attribute
attNameGroup  = 'time [s]';
attValueGroup = time.curr(3);
h5writeatt(fullPath,['/',timeStamp],attNameGroup, attValueGroup)

end %function