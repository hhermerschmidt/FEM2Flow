switch universe.typeLinearisation
    
    case 1 % Fixed point iteration
        
        % Create the stiffness matrix K:
        %---------------------------------------------------
      	K = sparse(I,J,X,universe.dimSys,universe.dimSys); % Create sparse matrix
        
        % Insert the Dirichlet BCs in the global system of eq.:
        %-------------------------------------------------------------

        if ~isempty(universe.bcDir)
            activeBcDir     = find(universe.bcDir(:,6) == 1); % Find active bcDir in current element
            posGlobBcDir    = universe.bcDir(activeBcDir,7);  % Get local position of active bcDir

            K(posGlobBcDir,posGlobBcDir) = speye(length(activeBcDir));
            
            linearIndexCurrTimeVar       = sub2ind(size(currTimeVar), universe.bcDir(activeBcDir,4), (1 + universe.bcDir(activeBcDir,5)));
            rhs(posGlobBcDir)            = universe.bcDir(activeBcDir,3) .* ...
                                            currTimeVar(linearIndexCurrTimeVar);
        end
                                  
    case 2 % Newton-Raphson procedure

        % Create the stiffness matrix K:
        %---------------------------------------------------
        
        K = sparse(I,J,X,universe.dimSys,universe.dimSys);
        
         
                
    	% Insert the Dirichlet BCs in the global system of eq.:
        %-------------------------------------------------------------
        if iteration == 1 % first iteration step

            if ~isempty(universe.bcDir)
                
                activeBcDir     = find(universe.bcDir(:,6) == 1); % Find active bcDir in current element
                posGlobBcDir    = universe.bcDir(activeBcDir,7);  % Get local position of active bcDir
       
                % K(posGlobBcDir,:)            = 0.0;
                % Zu-Null-Setzen der nicht-diagnoal Eintraege geschieht bereits auf Elementebene
                % und wird daher in der Gesamtmatrix nicht erneut wiederholt.
                % Eine Wiederholung des zu-Null-setzens koennte zu Rechenzeitverlusten fuehren,
                % da die Tangentensteifigkeitsmatrix sparse initialisiert wurde und nun nachtraeglich
                % einzelne Eintraege geaendert werden.
                % Bei Sven R. erfolgt das Setzen der Dirichlet-RB ebenfalls auf Elementebene, 
                % so dass diese Zeile bei ih fehlt.

                K(posGlobBcDir,posGlobBcDir) = speye(length(activeBcDir));
                                
                linearIndexCurrTimeVar = sub2ind( size(currTimeVar)                    , ...
                                                       universe.bcDir(activeBcDir,4)   , ...
                                                  (1 + universe.bcDir(activeBcDir,5) )   ...
                                                );
                                            
                rhs(posGlobBcDir)      =    universe.bcDir(activeBcDir,3) ...
                                         .* currTimeVar(linearIndexCurrTimeVar) ...
                                         -  solu(posGlobBcDir,1);
                                 
            end
        
            
    	else % Second and consecutive iteration steps:
            
            if ~isempty(universe.bcDir)
                activeBcDir     = find(universe.bcDir(:,6) == 1); % Find active bcDir in current element
                posGlobBcDir    = universe.bcDir(activeBcDir,7);  % Get local position of active bcDir

                K(posGlobBcDir,posGlobBcDir) = speye(length(activeBcDir));
                rhs(posGlobBcDir)            = 0.0;
                
            end

        end
end


clear posCurrBc activeBcDir posGlobBcDir