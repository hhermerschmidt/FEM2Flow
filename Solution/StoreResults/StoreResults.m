
% Store end of current time slab in time.range:
time.range(1,time.currSlab+1) = time.curr(1,3);

% Store results if calculation did converge:

% Zusammenstellen von Ergebniswerten, Nachlaufrechnung und ausgewaehlten
% Eingabedaten (Materialparameter, Belastung, ...). Schreiben des
% hdf5-files.
if iteration < universe.nIterMaxSol
    for ii = 1 : length(feOutput) % loop over output objects
        feOutput{ii}.StoreOutput(elem, time, universe, solu)
    end
end

% Schreiben des zugehoerigen xmf-files.
universe.countWriteXMF = universe.countWriteXMF + 1;
if universe.countWriteXMF == universe.writeXMF  
    for ii = 1 : length(feOutput) % loop over output objects:
        feOutput{ii}.WriteOutputFile(time.range, time.currSlab)
    end
    universe.countWriteXMF = 0;
end
