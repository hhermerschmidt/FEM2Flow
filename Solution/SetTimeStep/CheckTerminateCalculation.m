% Script is invoked after iterations did not converge and sets user
% dependend reaction or prompts user input.

if universe.promptTerminate == 1
  
    % Prompt wheter to decide to stop calculation or not:
    disp(' ')
    disp('Solution did not converge.')
    disp('Set:')
    disp('1: to reduce time step,')
    disp('2: to reduce time step and do not ask again,')
    flag = input('3: to stop calculations. \n');

    if isempty(flag)
        flag = 1;
    end

    switch flag
        case 2
            universe.promptTerminate = 0;
        case 3
            error('Terminated calculation due to poor convergence')
    end      
            
 
end