
switch universe.flag.timeStep 
    
    case 0 % Time step is kept constant throughout the computation
        
        % Update time data:
        %-----------------------------    
        % Get width of next time slab:
        time.deltaCurr = time.delta;            % store width of current time slab
        time.delta     = time.deltaCurr * 1.0;  % keep width constant for next time slab 

        % Calculate time points of next time slab:    
        time.curr(1,1) = time.curr(1,3);                    % start of next slab               
        time.curr(1,2) = time.curr(1,1) + 0.5 * time.delta; % center of next slab
        time.curr(1,3) = time.curr(1,1) + 1.0 * time.delta; % end of next slab

        % Modify time points and width of next slab if close to time.total:
        if time.curr(1,3) > time.total
           time.curr(1,3) = time.total;
           time.delta     = time.curr(1,3) - time.curr(1,1);
           time.curr(1,2) = time.curr(1,1) + 0.5*time.delta;
        end
        
        if iteration == universe.nIterMaxSol
            error('The calculation has been terminated because maximum number of iterations was reached.')
        end
        
        
    case 1 % Time step is automatically adjusted depending on convergence behaviour
    
    if iteration == universe.nIterMaxSol % poor convergence

        % Current time slab has to be recalculated, time step is reduced

        % Prompt user input if calculation shall be continued:
        CheckTerminateCalculation

        % Set flag for poor convergence:
        convData.poorConv = 1;

        % Get time step for recalculation:
        time.deltaCurr = time.delta;
        time.delta     = time.deltaCurr * time.incRed;

        % Calculate time points of next time slab:
        time.curr(1,1) = time.curr(1,1);      % start of time slab remains the same      
        time.curr(1,2) = time.curr(1,1) + 0.5 * time.delta;
        time.curr(1,3) = time.curr(1,1) + 1.0 * time.delta;

        % Break loop over time slabs if time step below minimum
        if time.delta < time.deltaMin 
            % Display and store that solution stopped (case 5):
            [CalcProtocol, ~] = StoreDispCalcProtocol(CalcProtocol, [], 5, [], []);
            break % Break loop over time slabs:
        end

    elseif iteration <= universe.nIterMinSol % Good convergence, increase time step:    
        
        % Set flag for good convergence:
        convData.goodConv = 1;

        % Update time data:
        %-----------------------------
        % Get time step for next time slab:
        time.deltaCurr = time.delta;
        time.delta     = time.deltaCurr * time.incEnh;

        % Test if time step above user defined maximum:
        if time.delta > time.deltaMax;  time.delta = time.deltaMax; end

        % Calculate time points of next time slab:    
        time.curr(1,1) = time.curr(1,3);
        time.curr(1,2) = time.curr(1,1) + 0.5 * time.delta;
        time.curr(1,3) = time.curr(1,1) + 1.0 * time.delta;

        % Check if end of next time slab exceeds time.total:
        if time.curr(1,3) > time.total
           time.curr(1,3) = time.total;
           time.delta     = time.curr(1,3) - time.curr(1,1);
           time.curr(1,2) = time.curr(1,1) + 0.5 * time.delta;
        end

    else % Keep width of time step constant for next time step

        % Update time data:
        %-----------------------------    
        % Get width of next time slab:
        time.deltaCurr = time.delta;            % store width of current time slab
        time.delta     = time.deltaCurr * 1.0;  % keep width constant for next time slab 

        % Calculate time points of next time slab:    
        time.curr(1,1) = time.curr(1,3);                    % start of next slab               
        time.curr(1,2) = time.curr(1,1) + 0.5 * time.delta; % center of next slab
        time.curr(1,3) = time.curr(1,1) + 1.0 * time.delta; % end of next slab

        % Modify time points and width of next slab if close to time.total:
        if time.curr(1,3) > time.total
           time.curr(1,3) = time.total;
           time.delta     = time.curr(1,3) - time.curr(1,1);
           time.curr(1,2) = time.curr(1,1) + 0.5*time.delta;
        end
    end 
 
    otherwise
        error('The flag universe.flag.timeStep is not set correctly.');
        
end