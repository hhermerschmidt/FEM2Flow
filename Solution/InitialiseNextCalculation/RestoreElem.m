% Restore the elements for recalculation of current time slab with reduced
% time step:

for mm = 1 : universe.meshData.nEle % loop over elements
    % Get solution for current element:
    eleSoluDis = soluDis(elem{mm}.indexSystem);
    
    % Call element method to restore elements:    
	elem{mm}.Restore(eleSoluDis,...
                     time.deltaCurr, ...
                     time.delta, ...
                     universe.typeEleUpdate)
end