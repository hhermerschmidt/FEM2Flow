for mm = 1 : universe.meshData.nEle % loop over elements
    % Get solution vector for current element:
    eleSolu = solu(elem{mm}.indexSystem);
    
    % Call element method to update the elements:
    elem{mm}.Update(eleSolu, ...
                    time.deltaCurr, ...
                    time.delta, ...
                    universe.typeEleUpdate)   
end