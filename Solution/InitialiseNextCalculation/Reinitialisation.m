function [solu, CalcProtocol] = Reinitialisation( universe     , ...
                                                  elem         , ...
                                                  solu         , ...
                                                  CalcProtocol )

nodes_t1 = universe.nodes_t1;

phi_old  = solu(8:8:end);


%% Berechnung diskretes Interface 

% Ermittlung der vom Interface geschnittenen Elemente:
[cutElems] = FindCutElems(elem, phi_old);


% Ermittlung der Interface-Primitiva:
[XA, R, XE] = CreateInterfacePrimitiva( elem        , ...
                                        nodes_t1    , ...
                                        phi_old     , ...
                                        cutElems      ...
                                      );


% ShowLevelSetDegenerated(universe, elem, solu, cutElems, XA, XE );

%% Reinitialisierung - Neuberechnung der kleinsten Abstandswerte
% Abstandsberechnung Punkt Nullniveau-Geradenzug mittels Hilfsebene
% (Normalenvektor der Hilfsebene ist jeweiliger Richtungsvektor der Gerade)
% ueber Schnittpunkt Gerade - Hilfsebene.

% Abstandsberechnung der Knoten zum Interface

[phi_new] = CalcDistInterface(nodes_t1, phi_old, XA, R, XE);

solu(8:8:end) = phi_new;


[CalcProtocol, ~] = StoreDispCalcProtocol(CalcProtocol, [], 13, [], []);
                                   
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Subfunction "findCutElems"
function [cutElems] = FindCutElems(elem, phi)

numEle = size(elem,1);

cutElems = zeros(numEle,1);

parfor mm = 1:numEle
    
    currInzi  = elem{mm}.eleNodes_t1;
    
    currPhi   = phi(currInzi);
    
    SgnCurrPhi = sign(currPhi);
    SgnCurrPhi = unique(SgnCurrPhi);
    SgnCurrPhi = nonzeros(SgnCurrPhi);
    
    if size(SgnCurrPhi,1) > 1
        cutElems(mm) = mm;
    end %if
end %for

cutElems = nonzeros(cutElems);

end %function


%% Subfunction "CreateInterfacePrimitiva"
function [XA, R, XE] = CreateInterfacePrimitiva(elem, nodes, phi, cutElems)

XA = zeros(2,size(cutElems,1));
XE = zeros(size(XA));
R  = zeros(size(XA));

parfor ec = 1:size(cutElems,1) % Schleife ueber alle geschnittenen Elemente

    %Konnektivitaet aktuelles Element
    currInzi = zeros( 1, size( elem{cutElems(ec)}.eleNodes_t1 ,2 )+1 );
    
    currInzi(1:end-1) = elem{cutElems(ec)}.eleNodes_t1;
    currInzi(end)     = currInzi(1);
        
    %LS-Werte aktuelles Element
    currPhi  = phi(currInzi);
    
    %Abschreiten der einzelnen Kanten
    %Annahme: Nur zwei Kanten je Element werden geschnitten
    xPosCutEdgeElem = zeros(2,2);
     
    nn = 1;
    for kk = 1:4 %4 Kanten je Element
        
        %Globale Nr. Kantenknoten
        nodeA = currInzi(kk);
        nodeB = currInzi(kk+1);
        
        %Koordinaten Kantenknoten
        xA    = nodes(nodeA,1:2)';
        xB    = nodes(nodeB,1:2)';
        
        %Kantenvektor
        vecAB = xB - xA;
        
        %Kantenlaenge
        l     = sqrt( vecAB(1)^2 + vecAB(2)^2 );
        
        %LS-Werte Kantenknoten
        phiA  = currPhi(kk);
        phiB  = currPhi(kk+1);
        
        %Wenn Elementkante von LS-Funktion geschnitten wird, dann:
        if sign(phiA) ~= sign(phiB)           
            
            % Geradengleichung LS-Funktion auf geschnittener Kante:
            % phi(s) = (phiB - phiA)/l * s + phiA  , xi = 0...l
            
            %Nulldurchgang auf Elementkante
            s = - phiA * l / (phiB - phiA);
            
            %Globale Postion Nulldurchgang
            xPosCutEdgeElem(:,nn) = xA + s/l * vecAB;
            
            nn = nn +1;
        end %if
    end %for
    
    %Grenzflaechen-Primitivum des Elementes aufbauen  
    XA(:,ec) = xPosCutEdgeElem(:,1);
    XE(:,ec) = xPosCutEdgeElem(:,2);
    R(:,ec)  = xPosCutEdgeElem(:,2) - xPosCutEdgeElem(:,1);

end %for

end %function


%% Subfunction "CalcDistInterface"
function [dVec] = CalcDistInterface(nodes, phi, xAufpunkt, r, xEndpunkt)
% Abstandsberechnung Punkt Nullniveau-Geradenzug mittels Hilfsebene
% (Normalenvektor der Hilfsebene ist jeweiliger Richtungsvektor der Gerade)
% ueber Schnittpunkt Gerade - Hilfsebene.
%
% nodes :    (Anzahl Knoten, 3)
%            Knotenkoordinaten [ x , y , z ]
% phi   :    (Anzahl Knoten, 1)
%            alte LS-Werten an Knotenkoordinaten
% xAufpunkt: (2, Anzahl Aufpunkte)
%            Koordinaten saemtlicher Aufpunkte aller
%            Grenzflaechen-Primitiva
% r :        (2, Anzahl Aufpunkte)
%            Richtungsvektoren aller Grenzflaechen-Primitiva
% xEndpunkt: (2, Anzahl Aufpunkte)
%            Koordinaten saemtlicher Endpunkte aller
%            Grenzflaechen-Primitiva


numNodes = size(nodes,1);

d    = zeros(numNodes, size(xAufpunkt,2)); % Vektor: Abstandswerte Knotenpunkt - GFP
dVec = zeros(numNodes,1);          % Vektor: min. Abstand Knotenpunkt  - GF
kk   = zeros(numNodes,size(xAufpunkt,2));

nodes_x = nodes(:,1);
nodes_y = nodes(:,2);

% DXP = zeros( , ,numNodes)
parfor ii = 1:numNodes; % Schleife ueber alle Knoten
    
    vz = sign(phi(ii));
    
    xP = [nodes_x(ii) , nodes_y(ii)]'; %aktueller Knoten
    
    d_help = d(ii,:);
    kk_help = kk(ii,:);
    
    for jj = 1:size(xAufpunkt,2) % Schleife ueber alle Grenzflaechen-Primitiva
        
        % Schnittpunkt LS-Gerade - Hilfsebene (durch aktuellen Knoten)
        dxPA      = xAufpunkt(:,jj) - xP;
        abstandPA = norm(dxPA,2);
        
        lambda    = - ( dxPA' * r(:,jj) ) / norm(r(:,jj),2)^2;
        
        xS        = xAufpunkt(:,jj) + lambda * r(:,jj);
        dxPS      = xS - xP; % Vektor Schnittpunkt (LS-Gerade) - Knotenpunkt
        abstandPS = norm(dxPS,2);
        
        dxPE      = xEndpunkt(:,jj) - xP;
        abstandPE = norm(dxPE,2);
        
        %dxP     = [dxPS     , dxPA     , dxPE     ];
        abstand = [abstandPS, abstandPA, abstandPE];  
        
        if lambda >= 0 && lambda <= 1 % Direktes Lot existiert
           
           kk_help(jj) = 1;        
        
        else % Lot liegt ausserhalb Definitionsbereich der Geraden
             % Abstand ist Entfernung zum Anfangs- oder Endpunkt der
             % Geraden
                                  
            minAbstand = find(abstand(2:3) == min(abstand(2:3)) );
            
            if minAbstand == 1
               kk_help(jj) = 2; % Abstand zum Anfangspunkt     
            else
               kk_help(jj) = 3; % Abstand zum Endpunkt
            end

        end %if

        %DXP(:,jj,ii) = dxP(:,kk(ii,jj));
        d_help(jj)     = abstand(kk_help(jj));
              
    end %for
    
    % Ermittlung des k�rzesten Abstandes jedes Knotenpunktes   

    k = find( d_help == min(d_help) );
    
    dVec(ii) = vz * d_help(k(1));
    
end %for

end %function


%% Darstellung degenerierte Level-Set Funktion (2D)
function [] = ShowLevelSetDegenerated(universe, elem, solu, cutElems, XA, XE )
    display('Darstellung der degenerierten Level-Set Funktion (2D) ...')

    %f3 = figure('name','Level-Set Funktion 2D (degeneriert)','color',[1 1 1],'visible','off');
    figure(3)
    
    phi = solu(8:8:end);
    
    for ii = 1:size(elem,1)

        fill(universe.nodes_t1(elem{ii}.eleNodes_t1,1), ...
             universe.nodes_t1(elem{ii}.eleNodes_t1,2), ...
             phi(elem{ii}.eleNodes_t1)                  ...
            )
        hold on
    end
    % set(gca,'CLim',[-0.6, 0.6]);
    % set(gca,'CLim',[min(phi), max(phi)]);

    set(gca,'XTick',linspace(0,1,6))
    set(gca,'YTick',linspace(0,1,6))

    set(gca, 'XTicklabel', num2str(get(gca, 'XTick')', '%.1f'))
    set(gca, 'YTicklabel', num2str(get(gca, 'YTick')', '%.1f'))

    set(gca,'XLim',[ 0 1]);
    set(gca,'YLim',[ 0 1]);

 %   title({'Nullniveau degenerierte LS-Funktion';['Zeitpunkt: ',pointInTime]},'FontSize',16)
    xlabel('x'), ylabel('y')
    axis equal tight

    display('Darstellung der degenerierten Level-Set Funktion (2D) ... beendet.')
    display(' ')

    display('Darstellung der vom Interface geschnittenen Elemente ...');
    for ec = 1:size(cutElems,1)
        plot(universe.nodes_t1( elem{ cutElems(ec) }.eleNodes_t1, 1), ...
             universe.nodes_t1( elem{ cutElems(ec) }.eleNodes_t1, 2), ...
             '.k','MarkerSize',15)
        hold on
    end %for
    
    display('Darstellung der vom Interface geschnittenen Elemente ... beendet.');
    display(' ')

    display('Darstellung der Interface-Primitiva ...')
    %Schnittpunkte LS-Funktion - Elementkanten plotten
    for ii = 1:size(XA,2)   
        plot(XA(1,ii),XA(2,ii),'xr','MarkerSize',15)
        hold on
    end
    
    for ii = 1:size(XE,2)   
        plot(XE(1,ii),XE(2,ii),'xr','MarkerSize',15)
        hold on
    end

    %LS-Funktion mittels Grenzflaechenprimitiva
    for ii = 1:size(XA,2)   
        plot([XA(1,ii), XE(1,ii)], [XA(2,ii), XE(2,ii)], '-k','LineWidth',2)
        hold on
    end
    hold off

    display('Darstellung der Interface-Primitiva ... beendet.')
    display(' ')
    
    pause

%     display('Abspeichern Abb. degenerierte Level-Set Funktionen (2D) ...')
% 
%     set(gcf, 'paperunits'   ,'centimeters') %set(f1, 'papertype','A4']);
%     set(gcf, 'papersize'    , [20.0 , 20.0])
%     set(gcf, 'paperposition', [1.0 1.0 18.0 18.0]);
% 
%     name = ['LSFunktion2D_',pointInTime,'_alt'];
%     print(f3, '-dpdf', ['Pictures/',name]);
%     saveas(f3,['./Figures/',name,'.fig'])
%     close(f3)
%     display('Abspeichern Abb. degenerierte Level-Set Funktionen (2D) ... beendet.')
%     display(' ')
end