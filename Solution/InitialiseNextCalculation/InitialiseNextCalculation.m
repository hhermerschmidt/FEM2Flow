% Reinitialisierung
%---------------------------------
if universe.flag.levelSet > 0
    
    if time.currSlab > 1  && ...
       mod( universe.counter.LevelSet, universe.flag.levelSet ) == 0 
            
            [solu, ...
             CalcProtocol ] = Reinitialisation(universe, elem, solu, CalcProtocol);       
    
    end
   
universe.counter.LevelSet = universe.counter.LevelSet + 1;

end



% Check if convergence was poor and update elements accordingly:
%---------------------------------------------------------------------

if convData.poorConv == 1
    % Poor convergence:
    %----------------------
    
    % Restore solution:
    solu = soluDis;
    
    % Restore elements for recalculation:
    RestoreElem 
    
else
    % Stop timer for current time slab:
    time.elapsed = toc(time.start);
    
    % Call function to display and store information on current time slab:
    [CalcProtocol, convData] = StoreDispCalcProtocol(CalcProtocol, convData, 7, time, []);
    
    % Update solution for next time slab:
    %--------------------------------------
    
    soluDis = solu;
     
    
    % Update elements for next time slab:
    %--------------------------------------
    UpdateElem
    
    % Increase counter for eigenstate analysis of linearised system matrix
    if universe.flag.eigen == 1
        nn_eigen = nn_eigen + 1;
    end
    
end


% Start timer for next time slab:
time.start   = tic;

% Update the solution:
%------------------------
UpdateSolution

% Increment time slab if convergence was not poor
if convData.poorConv == 0; time.currSlab  = time.currSlab + 1; end

% Reset / update values of convData:
convData.poorConv       = 0;
convData.goodConv       = 0;
convData.stopSol        = 0;


% Break calculation if end of calculation time is reached:
%----------------------------------------------------------
if convData.endOfTime == 1; break; end

% Save Data of current time slab (needed for restart)
%------------------------------------------------------
save([universe.folderRestartData, 'universe.mat'], 'universe')
save([universe.folderRestartData, 'time.mat'],     'time')

save([universe.folderRestartData, 'convData.mat'], 'convData')

save([universe.folderRestartData, 'elem.mat'],     'elem')
save([universe.folderRestartData, 'feOutput.mat'], 'feOutput')

save([universe.folderRestartData, 'solu.mat'],     'solu')
save([universe.folderRestartData, 'deltaSolu.mat'],'deltaSolu')
save([universe.folderRestartData, 'soluDis.mat'],  'soluDis')

if universe.flag.eigen == 1
    save([universe.folderRestartData,'EigVal.mat'],'EigVal')
end

