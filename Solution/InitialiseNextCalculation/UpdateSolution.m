% Update the solution using element data:
%--------------------------------------------

% Save the current solution:
currSolu = solu;

% Initialisation:
solu = zeros(universe.dimSys,1);

for mm = 1 : universe.meshData.nEle % loop over elements
    % Get the current solution:
    eleSolu = currSolu(elem{mm}.indexSystem);
    
    % Call element method to update the solution:
    [solUpdate, index] = elem{mm}.UpdateSol(eleSolu, ...
                                            time.deltaCurr, ...
                                            time.delta, ...
                                            universe.typeEleUpdate);
    
    %Startwerte fuer naechsten RZ-Scheibe
    solu(index)        = solUpdate;
            
end

clear index SolUpdate currSolu