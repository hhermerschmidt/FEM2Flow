clc, clear all, close all

dbstop if error all

% Add working directories to path:
%------------------------------------
SetPathesDefault();

%-----------------------------------------------------------
% Set main parameter for mesh generation and calculation:
%-----------------------------------------------------------
GetProblemData();

%----------------------------------------------
% Initialise the finite elements:
%----------------------------------------------
GetElem();

% Set and save all data required for calculation of first time slab:
%---------------------------------------------------------------------
SetCalculationData();

% Initialise output:
%---------------------------
InitialiseOutput();

% Load calculation data: 
%------------------------------
LoadCalculationData

%-----------------------------------
% loop over time slabs:
%-----------------------------------
while time.curr(1,3) <= time.total

    % Solve the system of eq. for the current time slab:
    %------------------------------------------------------
    SolveTimeSlab

    % Store results of current time slab:
    StoreResults

    % Set the time step of the next time slab:
    SetTimeStep

    % Store current results and prepare next calculation:
    InitialiseNextCalculation

end

% Save output files related to previous calculations:
MakeOutputFiles

% Store calculation data to allow later restart:
StoreCalculation

% Remove folders from the search path:
RemovePathes