classdef FluidNavierStokes4201 < FluidElemSuperclass
    
    % Description:
    % Testelement zur Herleitung f�r eigenes 2D-Fluidelement
    % verschachtelte Build-Routinen
    
    
    
    properties (SetAccess = protected)
        
        % General properties:
        %-----------------------------------------------------------------
        type        = 4201;          % identification number of element type
        name        = 'FluidNavierStokes4201';  % name of current element type
        sizeXest    = 24*24;        % estimated number of non-zero entries in eleMat
        probDim     = 2;            % problem dimension
        paraOutput  = 1;            % create Paraview output for current element type (1) or not (0)
% %         
% %         
% %         % Material parameter:
% %         %------------------------------------------
% %         typeTimeVarMatPara          % type of time variation of material parameter
        
        
        % Indices to access the element matrix:
        %----------------------------------------------------------------
        
        % Description:
        % Sets the number of dofs used for initialization of the element
        % attributes and the positions of the dof in the element matrix
        % and the element attribute to access them in the proper
        % positons:
        % Note: Since the velocity dof are the first element dof for
        % this type, positions of velocity dof are the same in velo and
        % in eleMat and thus not given seperately. For the other 
        % unknown fields positions are not the same, compare e.g. 
        % ind.pt0 and ind.pt0Sol
        
        % Initialise values in property section to ensure, that 'ind' is
        % created only once for all elements of this type:
        ind = struct('nNodesOut', 4,...         % Number of nodes for output
                     'v'        , (1:16)',...   % pos of all velocity dof in eleMat
                     'p'        , (17:24)',...  % pos of all pressure dof
                     'vx'       , (1:8)',...    % pos of x-velocity in eleMat
                     'vy'       , (9:16)',...   % pos of y-velocity in eleMat
                     'vxt0'     , (1:4)',...    % pos of vx_t0 in eleMat and velo
                     'vyt0'     , (9:12)',...   % pos of vy_t0 in eleMat and velo
                     'vt0'      , [1:4, 9:12]',... % pos of v_t0 in eleMat and velo
                     'vxt1'     , (5:8)',...    % pos of vx_t1 in eleMat and velo
                     'vyt1'     , (13:16)',...  % pos of vy_t1 in eleMat and velo
                     'vt1'      , [5:8, 13:16]',... % pos of v_t1 in eleMat and velo
                     'pt0'      , (17:20)',...  % pos of p_t0 in eleMat
                     'pt1'      , (21:24)',...  % pos of p_t1 in eleMat
                     'pt0Sol'   , (1:4)',...    % pos of p_t0 in pressure
                     'pt1Sol'   , (5:8)',...    % pos of p_t1 in pressure
                     'nDofV'    , 16,...        % total number of velo dof in current element
                     'nDofP'    , 8,...         % total number of pressure dof in current element
                     'nDofEle'  , 24)           % total number of dof for the current element
        

        % further properties are included in superclasses
    end
    
        
    %------------------------------------------------------------------
    
    %------------------------------------------------------------------
    
    
    methods
        
        function elem = FluidNavierStokes4201( eleConnect    , eleCoord      , ...
                                               eleMatPara    , eleLoadPara   , ...
                                               eleBcDir      , eleBcNeu      , ...
                                               mm                              ...
                                              )

                            
            %Call the superclass constructor:
            elem = elem@FluidElemSuperclass( eleConnect    , eleCoord      , ...
                                             eleMatPara    , eleLoadPara   , ...
                                             eleBcDir      , eleBcNeu      , ...
                                             mm                              ...
                                           );

            
            % Initialise element data for dof and related data:
            %--------------------------------------------------
           	elem.vRel           = zeros(elem.ind.nDofV,1);
           	elem.vRel_dis       = zeros(elem.ind.nDofV,1);
        end
        
        
        %------------------------------------------------------------------
        %------------------------------------------------------------------
        
        
        function[eleVec,...
                 I_elem,...
                 J_elem,...
                 X_elem   ] = Build( elem,...
                                     eleSolu, ...
                                     eleSoluDis, ...
                                     eleCoords, ...
                                     time,...
                                     currTimeVar)
                                 
            % Get the position structure to access element matrix & rhs:
            pos = elem.ind;

            %Initialise the element matrix and the right hand side:
            eleMat      = zeros(pos.nDofEle,pos.nDofEle);
eleMat2 = eleMat;
            rhs         = zeros(pos.nDofEle,1);
            
                                 
            % Get material data from eleMatPara:
            %--------------------------------------------------
            rho         = elem.matPara.rho;       % density
            mu          = elem.matPara.mu;        % dynamic viscosity
            
            fx          = elem.loadPara.fx;        % volume force in x-dir
            fy          = elem.loadPara.fy;        % volume force in y-dir
            
            
            % Get unknowns from the solution:
            %---------------------------------
            velo        = eleSolu(pos.v);
            velo_dis    = eleSoluDis;
%             velo_dis(pos.vxt0)
%             velo_dis(pos.vxt1)

            pressure    = eleSolu(pos.p);
            
            deltaT = time.curr(1,3) - time.curr(1,1);     
            
            nodesGlob           = zeros(8,3);
            %nodesGlob(1:4,1:2)  = eleCoords(:,1:2);
            %nodesGlob(5:8,1:2)  = eleCoords(:,1:2);
            nodesGlob(1:4,1:2)  = eleCoords.t0(:,1:2);
            nodesGlob(5:8,1:2)  = eleCoords.t1(:,1:2);
            nodesGlob(5:8,3)    = deltaT;
            
            
            % Local coordinates of nodes:
            nodesXi1 = [-1.0, 1.0, 1.0,-1.0];
            nodesXi2 = [-1.0,-1.0, 1.0, 1.0];


            % Get the matrix of dynamic viscosity parameters:
            muMat = zeros(3,3);
            muMat(1:2, 1:2) = 2.0 * mu * eye(2);
            muMat(3,   3)   = 1.0 * mu * eye(1);
            

            %Berechnung der gemittelten Elementlaenge
            %-------------------------------------------------------------

            geom_abl_xi = 0.25 * [nodesXi1;
                                  nodesXi2];

            jakobi = [(0.5 * geom_abl_xi),(0.5 * geom_abl_xi)] * nodesGlob(:,1:2);

            volume = 4 * det(jakobi);

            delta_h = sqrt(4 * volume / pi); % diameter of cylinder with same height (1) and volume as element
            %-------------------------------------------------------------


            %Zusammenfassung der Freiwerte
            %------------------------------------------------- 
            v_x = velo(pos.vx);     
            v_y = velo(pos.vy);
            %----------------------------------------------------

            %Relativgeschwindigkeit zum FE-Netz
            %----------------------------------------------------
% %             v_x_rel = elem.vRel(pos.vx);
% %             v_y_rel = elem.vRel(pos.vy);
            %----------------------------------------------------
            
            % Set relative velocity for non-moving mesh:
            v_x_rel = v_x;
            v_y_rel = v_y;
                       
            %Gravitationskraft des Fluids
            f_i_time = [fx;
                        fy] * (currTimeVar(elem.loadPara.timeVar, [1, 3]) * rho);
            

            % Initialise matrices and vectors required to build the element
            % matrix:
            shapeFuncDLocST = zeros( 3,  8);
            
            matrix_jumpVV   = zeros( 4,  4);
            matrix_vDivvDiv = zeros(16, 16);
            matrix_pv       = zeros( 8, 16);
            matrix_vDxvDx   = zeros(16, 16);
            
eleVec = zeros( pos.nDofEle, 1           );
lhs    = zeros(pos.nDofEle,pos.nDofEle);

K_inst     = zeros( pos.nDofV, pos.nDofV );
K_conv     = zeros( pos.nDofV, pos.nDofV );
K_visc     = zeros( pos.nDofV, pos.nDofV );
K_p        = zeros( pos.nDofV, pos.nDofP );
K_mass     = zeros( pos.nDofP, pos.nDofV );

R_conv_vx_d_vx = zeros( pos.nDofV/2, pos.nDofV/2);
R_conv_vx_d_vy = zeros( pos.nDofV/2, pos.nDofV/2);
R_conv_vy_d_vx = zeros( pos.nDofV/2, pos.nDofV/2);
R_conv_vy_d_vy = zeros( pos.nDofV/2, pos.nDofV/2);


K_stab_mass = zeros( pos.nDofV, pos.nDofV);

R_SUPG      = zeros(pos.nDofV, 1);
R_SUPG_Impx_d_vx   = zeros(pos.nDofV/2, pos.nDofV/2);
R_SUPG_Impx_d_vy   = zeros(pos.nDofV/2, pos.nDofV/2);
R_SUPG_Impx_d_p    = zeros(pos.nDofV/2, pos.nDofP);
R_SUPG_Impy_d_vx   = zeros(pos.nDofV/2, pos.nDofV/2);
R_SUPG_Impy_d_vy   = zeros(pos.nDofV/2, pos.nDofV/2);
R_SUPG_Impy_d_p    = zeros(pos.nDofV/2, pos.nDofP);

R_PSPG      = zeros(pos.nDofP, 1);
R_PSPG_d_vx         = zeros( pos.nDofP, pos.nDofV/2);
R_PSPG_d_vy         = zeros( pos.nDofP, pos.nDofV/2);
R_PSPG_d_p          = zeros( pos.nDofP, pos.nDofP);

K_jump_vx  = zeros( pos.nDofV/(2*2), pos.nDofV/(2*2) );
K_jump_vy  = zeros( pos.nDofV/(2*2), pos.nDofV/(2*2) );
r_jump_vx  = zeros( pos.nDofV/(2*2), 1);
r_jump_vy  = zeros( pos.nDofV/(2*2), 1);

help = zeros(16,16);
            
            %--------------------------------------------------------------
            
            % Get local coordinates and weight of Gauss points:
            coord_gp = [double(-1/sqrt(3)), double(1/sqrt(3))];   
            weight   = [1, 1];
            
            
            % Loop over Gauss points
            for jj = 1 : 2 % loop over xi_2 values of Gauss points

                % local xi_2 coordinate of current Gauss point
                xi_2 = coord_gp(1,jj); 	

                
                for kk = 1 : 2 % loop over xi_1 values of Gauss points

                    % local xi_1 coordinate of current Gauss point
                    xi_1 = coord_gp(1,kk);
                    

gp_weight = weight;

% Zeit
t0 = time.curr(1,1);
t1 = time.curr(1,3);

v_prev  = eleSoluDis;     
vx_prev = v_prev(pos.vxt1);
vy_prev = v_prev(pos.vyt1);

vx_t0 = velo(pos.vxt0);
vy_t0 = velo(pos.vyt0);

X = [ eleCoords.t0(:,1:2)', eleCoords.t1(:,1:2)'; ...
      t0 t0 t0 t0         , t1 t1 t1 t1           ...
    ];
    
    
xi1 = xi_1;
xi2 = xi_2;
    
% R�uml. Ansatzfunktionen
N_Sp        = 1/4 * [ (1-xi1)*(1-xi2); ...
                      (1+xi1)*(1-xi2); ...
                      (1+xi1)*(1+xi2); ... 
                      (1-xi1)*(1+xi2)  ...
                    ];

% Lokal-abgeleitete r�uml. Ansatzfunktionen
% [ dN/dxi1 | dN/dxi2 ]
N_Sp_d_Xi   = 1/4 * [ (-1)*(1-xi2), (1-xi1)*(-1); ...
                      (+1)*(1-xi2), (1+xi1)*(-1); ...
                      (+1)*(1+xi2), (1+xi1)*(+1); ... 
                      (-1)*(1+xi2), (1-xi1)*(+1)  ...
                    ];

% Jakobi-Matrix (Raum-Gebiet am Zeitscheibenanfang)
J_Sp        = X(1:2,1:4) * N_Sp_d_Xi;
det_J_Sp    = det(J_Sp);

% Integrationsfaktor Raumgebiet Zeitscheibenanfang
intFacOmega = gp_weight(jj) * gp_weight(kk) * det_J_Sp;

% Sprungterme in eleMat
K_jump_vx   = K_jump_vx + rho * (N_Sp * N_Sp') * intFacOmega;
K_jump_vy   = K_jump_vx;

%Sprungterme in eleVec
r_jump_vx   = r_jump_vx + K_jump_vx * ( vx_t0 - vx_prev );
r_jump_vy   = r_jump_vy + K_jump_vy * ( vy_t0 - vy_prev );
        
        
        



                    % Velocity and pressure shape functions (tri-linear in space):
                    shapeFunc = 0.25*((1 + xi_1 * nodesXi1).*(1 + xi_2 * nodesXi2));

                    % Get local derivatives of shape functions:
                    % shapeFuncDLoc = [shapeFunc/dxi_1; shapeFunc/dxi_2]; shapeFunc/dxi_3;
                    shapeFuncDXi12 = 0.25*[nodesXi1.*(1 + xi_2 * nodesXi2);
                                           nodesXi2.*(1 + xi_1 * nodesXi1)];

                    shapeFuncDLocST(3,:) = 0.5 * [-shapeFunc, shapeFunc];

                     
                    
                    % Uebergangsbedingungen
                    jacobit0        = shapeFuncDXi12 * nodesGlob(1:4,1:2);

                    intFacJump     = weight(1,jj) * weight(1,kk) * det(jacobit0);

                    matrix_jumpVV   = matrix_jumpVV + shapeFunc' * (shapeFunc * (rho * intFacJump));

                    
                    for ii = 1 : 2 % loop over local time axis

                        % local time coordinate of current Gauss point:
                        tau = coord_gp(1,ii);

                        % Time shape functions at current Gauss point:
                        t_1 = 0.5 * (1-tau);
                        t_2 = 0.5 * (1+tau);

                        shapeFuncDLocST(1:2,:) = [(t_1 * shapeFuncDXi12) , (t_2 * shapeFuncDXi12)];

                        jacobi = shapeFuncDLocST * nodesGlob;
                        detJacobi = det(jacobi);
                        invJacobi = inv(jacobi);
                        
                        % Es ist sinnvoll hier eine Abfrage bzgl. detJacobi <= 0 einzubauen.
                        % Dies weist auf degenerierte Elemente hin.

                        % Integration factor of current Gauss point:
                        intFac = weight(1,ii) * weight(1,jj) * weight(1,kk)* detJacobi;

                        b = f_i_time * [t_1;
                                        t_2];
                        

                        %--------------------------------------------------
                        shapeFuncST = [ (t_1 * shapeFunc) , (t_2 * shapeFunc) ];

                        shapeFuncDxST = invJacobi(1,1:2) * shapeFuncDLocST(1:2,:);
                        shapeFuncDyST = invJacobi(2,1:2) * shapeFuncDLocST(1:2,:);
                        shapeFuncDtST = invJacobi(3,:)   * shapeFuncDLocST;

                        shapeFuncDiv  = [shapeFuncDxST  shapeFuncDyST];

                        shapeFuncDxyST = [shapeFuncDxST;  shapeFuncDyST];
                        %--------------------------------------------------

                        %--------------------------------------------------
                        V   = (shapeFuncST  * [v_x v_y])';

                        V_T = (shapeFuncDtST * [v_x v_y])';

                        V_Rel = (shapeFuncST  * [v_x_rel v_y_rel])';
                        %--------------------------------------------------

                        %zeitliche und raumliche Ableitung des Geschwindigkeitfeldes
                        %--------------------------------------------------
                        GradV = (shapeFuncDxyST * [v_x  v_y])';

                        %Auswertung des Geschwindigkeitsgradienten
                        GradV_V = GradV * V;

                        H = [GradV(:,1) * shapeFuncST, GradV(:,2) * shapeFuncST];

                        ShapeGradV = V' * shapeFuncDxyST;

                        Vec = shapeFuncDtST + ShapeGradV;

                        H(1, 1:8)  = H(1, 1:8) + Vec;
                        H(2, 9:16) = H(2,9:16) + Vec;

                        H = H * rho;

                        eleMat(pos.vx,pos.v) = eleMat(pos.vx,pos.v) + shapeFuncST' * (H(1,:) * intFac);
                        eleMat(pos.vy,pos.v) = eleMat(pos.vy,pos.v) + shapeFuncST' * (H(2,:) * intFac);


    eleMat2(pos.vx,pos.v) = eleMat2(pos.vx,pos.v) + shapeFuncST' * (H(1,:) * intFac);
    eleMat2(pos.vy,pos.v) = eleMat2(pos.vy,pos.v) + shapeFuncST' * (H(2,:) * intFac);
    
    help(pos.vx,pos.v) = help(pos.vx,pos.v) + shapeFuncST' * (H(1,:) * intFac);
    help(pos.vy,pos.v) = help(pos.vy,pos.v) + shapeFuncST' * (H(2,:) * intFac);
    
                        %--------------------------------------------------


                        %Materialmodell
                        %--------------------------------------------------

                        ShapeD = [shapeFuncDxST, zeros(1,8);
                                  zeros(1,8),    shapeFuncDyST;
                                  shapeFuncDyST, shapeFuncDxST];

                        matrix_vDxvDx   = matrix_vDxvDx + ShapeD'   * (muMat   * intFac) * ShapeD;

                        EE = [shapeFuncDxST * v_x;
                              shapeFuncDyST * v_y;
                              0.5*(shapeFuncDyST * v_x + shapeFuncDxST * v_y)];

                        %--------------------------------------------------

                        %Masseerhaltung
                        %----------------------------------------------
                        matrix_pv = matrix_pv + (shapeFuncST * intFac)' * shapeFuncDiv;
                        %--------------------------------------------------

                        %Stabilisierung
                        %--------------------------------------------------
                        %Stabilisierung der Impulsbilanz
                        %Die Rohdichte wird an dieser Stelle schon mit eingerechnet 
                        tau_m = 1/sqrt( (2/deltaT)^2 + (2*norm(V_Rel)/delta_h)^2 + (4*(mu/rho)/(delta_h^2))^2 ) * (1/rho);
                        %tau_m = 0;
                        %erster Anteil der Linearisierung
                        StabMat = [H, shapeFuncDxyST];

                        Vec = ShapeGradV * rho;

                        StabMat2 = [ Vec,        zeros(1,8), shapeFuncDxST;
                                     zeros(1,8), Vec,        shapeFuncDyST];

                        eleMat = eleMat + StabMat2' * (StabMat * (tau_m * intFac)); 

                        %zweiter Anteil der Linearisierung 
                        %Auswertung des rhsduums der Impulsbilanz

                        V_Tot = (V_T + GradV_V) * rho;

                        P_Diff = shapeFuncDxyST * pressure;

                        L = V_Tot - b + P_Diff;

                        Mat = [shapeFuncDxyST * L(1) , shapeFuncDxyST * L(2)];

                        Vec = shapeFuncST * (rho * tau_m * intFac);

                        eleMat(pos.v,pos.vx) = eleMat(pos.v,pos.vx) + Mat(1,:)' * Vec;
                        eleMat(pos.v,pos.vy) = eleMat(pos.v,pos.vy) + Mat(2,:)' * Vec;

                        %Stabilisierung der Masseerhaltung
                        %Die Rohdichte wird an dieser Stelle schon mit eingerechnet
                        Re = (norm(V_Rel) * delta_h) / (2 * (mu / rho));
                        if Re < 3
                            tau_c = (delta_h * norm(V_Rel) * (Re / 3)) * rho;
                        else
                            tau_c = (delta_h * norm(V_Rel))            * rho;
                        end
                        %tau_c = 0;                       
                        matrix_vDivvDiv = matrix_vDivvDiv + shapeFuncDiv' * (shapeFuncDiv * (tau_c * intFac));

    
    h_el = delta_h;

    

    vx    = velo(pos.vx);
    vy    = velo(pos.vy);
    p     = pressure;

    % Koordinaten


    % Ansatzfunktionen
    N       = 1/8 * [ (1-xi1)*(1-xi2)*(1-tau); ...
                      (1+xi1)*(1-xi2)*(1-tau); ...
                      (1+xi1)*(1+xi2)*(1-tau); ... 
                      (1-xi1)*(1+xi2)*(1-tau); ...
                      (1-xi1)*(1-xi2)*(1+tau); ...
                      (1+xi1)*(1-xi2)*(1+tau); ...
                      (1+xi1)*(1+xi2)*(1+tau); ...
                      (1-xi1)*(1+xi2)*(1+tau)  ...
                    ];
    
   
    % Lokal-Abgeleitete Ansatzfunktionen: [ dN/dxi1 | dN/dxi2 | dN/dtau ]
    N_d_Xi  = 1/8 * [ (-1)*(1-xi2)*(1-tau), (1-xi1)*(-1)*(1-tau), (1-xi1)*(1-xi2)*(-1); ...
                      (+1)*(1-xi2)*(1-tau), (1+xi1)*(-1)*(1-tau), (1+xi1)*(1-xi2)*(-1); ...
                      (+1)*(1+xi2)*(1-tau), (1+xi1)*(+1)*(1-tau), (1+xi1)*(1+xi2)*(-1); ... 
                      (-1)*(1+xi2)*(1-tau), (1-xi1)*(+1)*(1-tau), (1-xi1)*(1+xi2)*(-1); ...
                      (-1)*(1-xi2)*(1+tau), (1-xi1)*(-1)*(1+tau), (1-xi1)*(1-xi2)*(+1); ...
                      (+1)*(1-xi2)*(1+tau), (1+xi1)*(-1)*(1+tau), (1+xi1)*(1-xi2)*(+1); ...
                      (+1)*(1+xi2)*(1+tau), (1+xi1)*(+1)*(1+tau), (1+xi1)*(1+xi2)*(+1); ...
                      (-1)*(1+xi2)*(1+tau), (1-xi1)*(+1)*(1+tau), (1-xi1)*(1+xi2)*(+1)  ...
              ];
    
    % Jakobi-Matrix (Raum-Zeit Gebiet)
    J_SpTi        = X * N_d_Xi;
    %det_J_SpTi    = det(J_SpTi);

    % Integrationsfaktor Raum-Zeit Gebiet
    %intFacQ  = gp_weight(kk) * gp_weight(ll) * gp_weight(nn) * det_J_SpTi;

    % Inverse Jakobi-Matrix
    inv_J_SpTi    = inv(J_SpTi);        
    inv_J_SpTi_x  = inv_J_SpTi(:,1);
    inv_J_SpTi_y  = inv_J_SpTi(:,2);
    inv_J_SpTi_t  = inv_J_SpTi(:,3);
    
    % Global-Abgeleitet Ansatzfunktionen
    N_d_x   = N_d_Xi * inv_J_SpTi_x;
    N_d_y   = N_d_Xi * inv_J_SpTi_y;
    N_d_t   = N_d_Xi * inv_J_SpTi_t;
    
    
    % Hilfsmatrizen
    N_d_xy  = [ N_d_x ; ...
                N_d_y ; ...
              ];
    
    N_d_X   = [ N_d_x , N_d_y ]; 
    
    H       = [     N         , zeros(size(N)); ...
                zeros(size(N)),     N           ...
              ];
    
    H_d_t   = [       N_d_t   , zeros(size(N)); ...
                zeros(size(N)),       N_d_t     ...
              ];
          
    G        = [ N_d_x' * vx , N_d_y' * vx ; ...
                 N_d_x' * vy , N_d_y' * vy   ...
               ];
           
    B        = [ N_d_x         , zeros(size(N)), N_d_y; ...
                 zeros(size(N)), N_d_y         , N_d_x  ...
               ];           
                   
    C        = [ 2*mu,  0 , 0 ; ...
                   0 ,2*mu, 0 ; ...
                   0 ,  0 , mu; ...
               ];
    
    K_inst = K_inst + rho * H * H_d_t' * intFac;
    K_conv = K_conv + rho * H * G * H' * intFac;
    K_visc = K_visc + B * C * B' * intFac;
    K_p    = K_p    + N_d_xy * N' * intFac;
    K_mass = K_mass + N * N_d_xy' * intFac;
    
    
    % Konvektiver Term (Jakobi-Matrix des Residuums)
    R_conv_vx_d_vx = R_conv_vx_d_vx                    ...
                    + (   N * N_d_x' * (N'     * vx) ...
                        + N * N'     * (N_d_x' * vx) ...
                        + N * N_d_y' * (N'     * vy) ...
                      ) * rho * intFac;

    R_conv_vy_d_vy = R_conv_vy_d_vy ...
                    + (   N * N_d_x' * (N'     * vx) ...
                        + N * N_d_y' * (N'     * vy) ...
                        + N * N'     * (N_d_y' * vy) ...
                      ) * rho * intFac;

    R_conv_vx_d_vy = R_conv_vx_d_vy + N * N' * (N_d_y' * vx) * rho * intFac;

    R_conv_vy_d_vx = R_conv_vy_d_vx + N * N' * (N_d_x' * vy) * rho * intFac;
            
    
    % Stabilisierung Massenerhaltung (GLS)
    K_stab_mass = K_stab_mass + tau_c * (N_d_xy * N_d_xy') * intFac;
   
    % Stabilisierung Konvektion (SUPG)
    vx_rel = vx;
    vy_rel = vy;

    V_rel  = N' * [ vx_rel , vy_rel ];
    norm_v = norm(V_rel);

    Re_el = norm_v * h_el / ( 2 * mu/rho );

    if Re_el < 3
        xi_Re_el = Re_el/3;
    else
        xi_Re_el = 1;
    end

    theta = 1;

    nu = mu/rho;

    delta_t = t1 - t0;


    tau_mom = 1/sqrt( (2/delta_t)^2 + (2*norm_v/h_el)^2 + (4*nu/h_el^2)^2 );
    
    
    
    tau_SUPG = tau_mom;


    L_stab_x = rho * (   (N_d_t' * vx)                 ...
                       + (N'     * vx) * (N_d_x' * vx) ...
                       + (N'     * vy) * (N_d_y' * vx) ...
                     )                                 ...
               + (N_d_x' * p);

    L_stab_y = rho * (   (N_d_t' * vy)                 ...
                       + (N'     * vx) * (N_d_x' * vy) ...
                       + (N'     * vy) * (N_d_y' * vy) ...
                     )                                 ...
               + (N_d_y' * p);

    L_stab   = [ L_stab_x ; ...
                 L_stab_y   ...
               ];   

    W_SUPG = N_d_x * (N' * vx) + N_d_y * (N' * vy);

    R_SUPG =   R_SUPG ...
             + tau_SUPG ...
               * [ W_SUPG        , zeros(size(N)) ; ...
                   zeros(size(N)), W_SUPG           ...
                 ]                                  ...
               * ( L_stab ) ... %- f_gp )                  ...
               * intFac;
           
    R_SUPG_Impx_d_vx =   R_SUPG_Impx_d_vx                                      ...
                       + tau_SUPG                                              ...
                         * (                                                   ...
                               (N_d_x * N') * (L_stab_x ) ...% - f_gp(1))             ...
                             +  W_SUPG      * rho * (   N_d_t'                 ...
                                                      + N'     * (N_d_x' * vx) ...
                                                      + N_d_x' * (N'     * vx) ...
                                                      + N_d_y' * (N'     * vy) ...
                                                    )                          ...
                           )                                                   ...
                         * intFac;

    R_SUPG_Impx_d_vy =   R_SUPG_Impx_d_vy                                ...
                       + tau_SUPG                                        ...
                         * (                                             ...
                               (N_d_y * N') * (L_stab_x ) ... % - f_gp(1))       ...
                             +  W_SUPG      * rho * (                    ...
                                                      N' * (N_d_y' * vx) ...
                                                    )                    ...
                           )                                             ...
                         * intFac;

    R_SUPG_Impx_d_p  =   R_SUPG_Impx_d_p ...
                       + tau_SUPG * W_SUPG * N_d_x' * intFac;

    R_SUPG_Impy_d_vx =   R_SUPG_Impy_d_vx                                ...
                       + tau_SUPG                                        ...
                         * (                                             ...
                               (N_d_x * N') * (L_stab_y ) ... %- f_gp(2))       ...
                             +  W_SUPG      * rho * (                    ...
                                                      N' * (N_d_x' * vy) ...
                                                    )                    ...
                           )                                             ...
                         * intFac;

    R_SUPG_Impy_d_vy =   R_SUPG_Impy_d_vy                                      ...
                       + tau_SUPG                                              ...
                         * (                                                   ...
                               (N_d_y * N') * (L_stab_y ) ... %- f_gp(2))             ...
                             +  W_SUPG      * rho * (   N_d_t'                 ...
                                                      + N_d_x' * (N'     * vx) ...
                                                      + N'     * (N_d_y' * vy) ...
                                                      + N_d_y' * (N'     * vy) ...
                                                    )                          ...
                           )                                                   ...
                         * intFac;

    R_SUPG_Impy_d_p  =   R_SUPG_Impy_d_p ...
                       + tau_SUPG * W_SUPG * N_d_y' * intFac;

   % Stabilisierung Druck (PSPG)
   tau_PSPG = tau_mom;
                   
   R_PSPG = R_PSPG + 1/rho * tau_PSPG * N_d_X * (L_stab ...
                                                        ) *intFac;%- f_gp) * intFacQ;
 
   R_PSPG_d_vx = R_PSPG_d_vx                                   ...
                  + 1/rho * tau_PSPG                            ...
                    * N_d_X * rho * [   N_d_t'                  ...
                                      + N'     * (N_d_x' * vx)  ...
                                      + N_d_x' * (N'     * vx)  ...
                                      + N_d_y' * (N'     * vy); ...
                                        N'     * (N_d_x' * vy)  ...
                                    ]                           ...
                    * intFac;

    R_PSPG_d_vy =   R_PSPG_d_vy                                 ...
                  + 1/rho * tau_PSPG                            ...
                    * N_d_X * rho * [   N'     * (N_d_y' * vx); ...
                                        N_d_t'                  ...
                                      + N_d_x' * (N'     * vx)  ...
                                      + N'     * (N_d_y' * vy)  ...
                                      + N_d_y' * (N'     * vy)  ...
                                    ]                           ...
                    * intFac;

    R_PSPG_d_p  =   R_PSPG_d_p         ...
                  + 1/rho * tau_PSPG   ...
                    * (N_d_X * N_d_X') ...
                    * intFac;
                        %--------------------------------------------------

                        
                        
                        %Auswertung des rhsduums
                        %--------------------------------------------------
                        rhs(pos.vx) = rhs(pos.vx) + shapeFuncST' * ((V_Tot(1) - b(1)) * intFac);
                        rhs(pos.vy) = rhs(pos.vy) + shapeFuncST' * ((V_Tot(2) - b(2)) * intFac);
                        
%    help(pos.vx) = help(pos.vx) + shapeFuncST' * ((V_Tot(1) - b(1)) * intFac);
%    help(pos.vy) = help(pos.vy) + shapeFuncST' * ((V_Tot(2) - b(2)) * intFac);
                       
                        rhs(pos.v)  = rhs(pos.v) + ShapeD' * ((2.0 * mu * intFac) * EE); 

%    help(pos.v) = help(pos.v) + ShapeD' * ((2.0 * mu * intFac) * EE);
                        rhs         = rhs          + StabMat2' * (L     * tau_m * intFac);
    

%    help = help + StabMat2' * (L     * tau_m * intFac);

                        %--------------------------------------------------               
                    
             
                    end

%                     % Uebergangsbedingungen
%                     jacobit0        = shapeFuncDXi12 * nodesGlob(1:4,1:2);
% 
%                     intFac          = weight(1,jj) * weight(1,kk) * det(jacobit0);
% 
%                     matrix_jumpVV   = matrix_jumpVV + shapeFunc' * (shapeFunc * (rho * intFac));

                end
            end
            
            
            % Add contributions to eleMat:
            %--------------------------------------------------
            eleMat(pos.p, pos.v) = eleMat(pos.p, pos.v) + matrix_pv;
            eleMat(pos.v, pos.p) = eleMat(pos.v, pos.p) - matrix_pv';
            
eleMat2(pos.p, pos.v) = eleMat2(pos.p, pos.v) + matrix_pv;
eleMat2(pos.v, pos.p) = eleMat2(pos.v, pos.p) - matrix_pv';
            
            %eleMat(pos.v, pos.v) = eleMat(pos.v, pos.v) + matrix_vDivvDiv;
            eleMat(pos.v, pos.v) = eleMat(pos.v, pos.v) + K_stab_mass; %gepr�ft
            eleMat(pos.v, pos.v) = eleMat(pos.v, pos.v) + matrix_vDxvDx;
%eleMat2(pos.v, pos.v) = eleMat2(pos.v, pos.v) + K_stab_mass; %gepr�ft            
eleMat2(pos.v, pos.v) = eleMat2(pos.v, pos.v) + matrix_vDxvDx;

            
% Physikalische Anteile
lhs(pos.v,pos.v) = lhs(pos.v,pos.v) + K_inst;

% Konvektiver Term (linearisiert)
K_conv_lin = [ R_conv_vx_d_vx , R_conv_vx_d_vy ; ...
               R_conv_vy_d_vx , R_conv_vy_d_vy   ...
             ];       
lhs(pos.v,pos.v) = lhs(pos.v,pos.v) + K_conv_lin;

%  if t0 > 0  
%     help(1:8,9:16) - lhs(1:8,9:16)
%     pause
%     help - lhs(1:16,1:16)
%     ((help(1:16,1:16) - lhs(1:16,1:16)) ./ help(1:16,1:16))
%     pause
%  end
 
lhs(pos.v,pos.v) = lhs(pos.v,pos.v) + K_visc;

lhs(pos.v,pos.p) = lhs(pos.v,pos.p) - K_p;

lhs(pos.p,pos.v) = lhs(pos.p,pos.v) + K_mass;

% Sprungterme
lhs(pos.vxt0,pos.vxt0) = lhs(pos.vxt0,pos.vxt0) + K_jump_vx;
lhs(pos.vyt0,pos.vyt0) = lhs(pos.vyt0,pos.vyt0) + K_jump_vy;

lhs2 = lhs;

% Anteile Druck-Stabilisierung
lhs(pos.p,pos.vx) = lhs(pos.p,pos.vx) + R_PSPG_d_vx;
lhs(pos.p,pos.vy) = lhs(pos.p,pos.vy) + R_PSPG_d_vy;
lhs(pos.p,pos.p)  = lhs(pos.p,pos.p)  + R_PSPG_d_p;

% Anteile Konvektions-Stabilisierung
R_SUPG_d_v = [ R_SUPG_Impx_d_vx , R_SUPG_Impx_d_vy ; ...
               R_SUPG_Impy_d_vx , R_SUPG_Impy_d_vy   ...
             ];

R_SUPG_d_p = [ R_SUPG_Impx_d_p ; ...
               R_SUPG_Impy_d_p   ...
             ];
lhs(pos.v,pos.v) = lhs(pos.v,pos.v) + R_SUPG_d_v;
lhs(pos.v,pos.p) = lhs(pos.v,pos.p) + R_SUPG_d_p;

% Anteile Stabilisierung Massenerhaltung
lhs(pos.v,pos.v) = lhs(pos.v,pos.v) + K_stab_mass;
       
            
            % Add contributions to rhs:
            %--------------------------------------------------
            %rhs(pos.v,1) = rhs(pos.v,1) + matrix_vDivvDiv * velo;
            rhs(pos.v,1) = rhs(pos.v,1) + K_stab_mass * velo; %gepr�ft
            rhs(pos.p,1) = rhs(pos.p,1) + matrix_pv * velo;
            rhs(pos.v,1) = rhs(pos.v,1) - matrix_pv' * pressure;
%             rhs(pos.v,1) = rhs(pos.v,1) + matrix_vDxvDx * velo;


eleVec(pos.v)       = eleVec(pos.v) + K_inst    * velo;
eleVec(pos.v)       = eleVec(pos.v) + K_visc    * velo;
eleVec(pos.v)       = eleVec(pos.v) + K_conv    * velo;
eleVec(pos.v)       = eleVec(pos.v) - K_p       * pressure;
%eleVec(pos.v)       = eleVec(pos.v) - r_f;
%eleVec(pos.v)       = eleVec(pos.v) - r_t;

eleVec(pos.p)       = eleVec(pos.p) + K_p'      * velo;

eleVec(pos.v)       = eleVec(pos.v) + R_SUPG;
eleVec(pos.v)       = eleVec(pos.v) + K_stab_mass * velo;
eleVec(pos.p)       = eleVec(pos.p) + R_PSPG;

% Sprungterme
eleVec(pos.vxt0)    = eleVec(pos.vxt0) + r_jump_vx;
eleVec(pos.vyt0)    = eleVec(pos.vyt0) + r_jump_vy;

            
            % Add contributions of jump terms to eleMat:
            %--------------------------------------------------
            eleMat(pos.vxt0,pos.vxt0) = eleMat(pos.vxt0,pos.vxt0) + matrix_jumpVV;
            eleMat(pos.vyt0,pos.vyt0) = eleMat(pos.vyt0,pos.vyt0) + matrix_jumpVV;
            
eleMat2(pos.vxt0,pos.vxt0) = eleMat2(pos.vxt0,pos.vxt0) + matrix_jumpVV;
eleMat2(pos.vyt0,pos.vyt0) = eleMat2(pos.vyt0,pos.vyt0) + matrix_jumpVV;
            
            % Get jump at start of current time slab:
            %--------------------------------------------------
            jumpDeltaVx  = velo(pos.vxt0) - velo_dis(pos.vxt1);
            jumpDeltaVy  = velo(pos.vyt0) - velo_dis(pos.vyt1);
            %             jumpDeltaVx  = velo(pos.vxt0) - velo_dis(pos.vxt0);%velo_dis(pos.vxt1);
            %             jumpDeltaVy  = velo(pos.vyt0) - velo_dis(pos.vyt0);%velo_dis(pos.vyt1);

            % Add contributions of jump terms to rhs:
            %--------------------------------------------------
            rhs(pos.vxt0) = rhs(pos.vxt0) + matrix_jumpVV * jumpDeltaVx;
            rhs(pos.vyt0) = rhs(pos.vyt0) + matrix_jumpVV * jumpDeltaVy;
    
      
 
  
        

    
               % Change sign of rhs:
            %----------------------
            eleVec = -rhs;    
    
            % Set Dirichlet BC:
            %-----------------------------
            if ~isempty(elem.bcDir)
                activeBcDir = find(elem.bcDir(:,6) == 1); % Find active bcDir in current element
                posLocBcDir = elem.bcDir(activeBcDir,7);  % Get local position of active bcDir
                
                eleMat(posLocBcDir, :)          = 0.0;
                eleMat(posLocBcDir,posLocBcDir) = eye(length(activeBcDir));
            end
            
            

 
            
            % Store the element matrix and the indices in vectors:
            %--------------------------------------------------------------
            
            % Get the local indices and the values of the nonzero parts of
            % eleMatHyb:
            [I_Loc, J_Loc, X_elem] = find(eleMat);
            
            % Transfer the local indices to global ones:
            index       = elem.indexSystem;
            I_elem      = index(I_Loc);
            J_elem      = index(J_Loc);
            
            % Get the number of nonzero entries for the current element:
            sizeXElem   = size(I_elem,1);

        end

        
        %------------------------------------------------------------------
        %------------------------------------------------------------------
        
        
        function [ergFluid, sizeErg] = PostOut(elem, eleSolu, ~)
            % Method returns output data of the element at end of current
            % time slab:
            
            
            % Get indices to access solution and element properties:
            pos           = elem.ind;
            
            % Number of nodes for which output is returned:
            sizeErg       = pos.nNodesOut*2;
            
            % Get unknowns from the solution:
            %-----------------------------------
            velo          = eleSolu(pos.v);
            pressure      = eleSolu(pos.p);
            
            
            % Initialisation:
            %---------------------
            ergFluid      = zeros(sizeErg,4);
            
            % Get velocities and pressure:
            %------------------------------------
            ergFluid(1:4,1) = velo(pos.vxt0);  % v_x_t0         
            ergFluid(5:8,1) = velo(pos.vxt1);  % v_x_t1
            
            ergFluid(1:4,2) = velo(pos.vyt0);  % v_y_t0
            ergFluid(5:8,2) = velo(pos.vyt1);  % v_y_t1
            
            ergFluid(1:4,4) = pressure(pos.pt0Sol); % p_t0
            ergFluid(5:8,4) = pressure(pos.pt1Sol); % p_t1
            
        end
        
        
        %------------------------------------------------------------------
        %------------------------------------------------------------------
        

        function Update(elem, ~, deltaTCurr, deltaTNext, typeEleUpdate)
            
            % deltaTCurr: time width of current time  slab
            % deltaTNext: time width of next time slab
            % typeEleUpdate: 1: use gradient of current time slab to
            %                   estimate start and end values of next time
            %                   slab (initial values for first iteration)
            %                else: gradient is not used, initial values for
            %                   start and end of next time slab are thesame)
            
            % Get indices:
            pos = elem.ind;
            
            % Set discontinious values:
            elem.vRel_dis     = elem.vRel;
            
            if typeEleUpdate == 1
                deltaVRelCurr               = elem.vRel(pos.vt1) - elem.vRel(pos.vt0);
                elem.vRel(pos.vt0)          = elem.vRel(pos.vt1); 
                elem.vRel(pos.vt1)          = elem.vRel(pos.vt0) + deltaVRelCurr * (deltaTNext / deltaTCurr);
            else
                elem.vRel(pos.vt0)          = elem.vRel(pos.vt1);
            end
           
        end
        
        
        %------------------------------------------------------------------
        %------------------------------------------------------------------
        
        
        function [solUpdate , index] = UpdateSol(elem, eleSolu, ...
                                                 deltaTCurr, deltaTNext,...
                                                 typeEleUpdate)
            
            % Method called in UpdateSolution:
            
            % Get the current solution, update it for the next time slab
            % and return it as solUpdate:
            %--------------------------------------------------------------
            
            % Get indices:
            pos = elem.ind;
            
            % Get unknowns from eleSolu:
            velo        = eleSolu(pos.v);
            pressure    = eleSolu(pos.p);
            
            % Update unknowns for next time slab:
            if typeEleUpdate == 1
                deltaVeloCurr          = velo(pos.vt1) - velo(pos.vt0);
                velo(pos.vt0)          = velo(pos.vt1); 
                velo(pos.vt1)          = velo(pos.vt0) + deltaVeloCurr * (deltaTNext / deltaTCurr);
                
                deltaPressureCurr      = pressure(pos.pt1Sol) - pressure(pos.pt0Sol);
                pressure(pos.pt0Sol)   = pressure(pos.pt1Sol); 
                pressure(pos.pt1Sol)   = pressure(pos.pt0Sol) + deltaPressureCurr * (deltaTNext / deltaTCurr);
                
            else
                velo(pos.vt0)          = velo(pos.vt1);
                pressure(pos.pt0Sol)   = pressure(pos.pt1Sol);
            end
            
            
            % Initialisation of updated solution:
            solUpdate = zeros(pos.nDofEle, 1);
            
            % Set values:
            solUpdate(pos.v)    = velo;
            solUpdate(pos.p)    = pressure;
                
            index               = elem.indexSystem;     
        end
        
        
        %------------------------------------------------------------------
        %------------------------------------------------------------------

        
        function Restore(elem, ~, deltaTCurr, deltaTNext, typeEleUpdate)
            % Function resets the element properties for the dof using
            % data from previous time slab. Function is invoked by
            % "RestoreElem" if iteration did not converge and width of time
            % step is reduced.
            
            % Set indices:
            pos = elem.ind;
            
            % Restore the element data:
            if typeEleUpdate == 1
                % Use gradient of previous time slab for initial values of
                % the current time slab:
                deltaVRelPrev       = elem.vRel_dis(pos.vt1) - elem.vRel_dis(pos.vt0);
                elem.vRel(pos.vt0)  = elem.vRel_dis(pos.vt1); 
                elem.vRel(pos.vt1)  = elem.vRel(pos.vt0) + deltaVRelPrev * (deltaTNext / deltaTCurr);
                
            else
                % Initial values for start and end of time slab are the
                % same:
                elem.vRel(pos.vt0)  = elem.vRel_dis(pos.vt1);
                elem.vRel(pos.vt1)  = elem.vRel(pos.vt0);
            end
                                        
        end
        
        
        %------------------------------------------------------------------
        %------------------------------------------------------------------
        
        
        function SetEleDofGlob(elem)
            % Sets the property eleDofGlob of the current element:
            % eleDofGlob has as many cols as the maximum local dof id of
            % the element. Col: local dof id, value: global dof id
            
            
            elem.eleDofGlob    = [ 1, 21, 2, 22, 4, 24 ];
            % Aufgrund des gew�hlten Ansatzes (s. Build-Routine)
            % ist die Reihenfolge der globalen FHG hier zwingend.
                      
                
        end

        
    end % end of methods
    
    
    %------------------------------------------------------------------
    
    %------------------------------------------------------------------
    
    
    methods (Static)
        function Post(~, ~, ~)
            % Method setting element data from the solution:
            
            % -- no secondary unknowns stored for this element type --
        end
        
        
        %------------------------------------------------------------------
        %------------------------------------------------------------------
        
        
        function eleNodesDofLoc = GetEleNodesDofLoc
            % returns a matrix containing the local node numbers in the
            % first col and the local dof ids in the second col:
            
            % loc dof: 1: vxt0, 2: vxt1, 3: vyt0, 4: vyt1, 5: pt0, 6: pt1
            eleNodesDofLoc= [1, 1;
                             2, 1;
                             3, 1;
                             4, 1;
                             1, 2;
                             2, 2;
                             3, 2;
                             4, 2;
                             1, 3;
                             2, 3;
                             3, 3;
                             4, 3;
                             1, 4;
                             2, 4;
                             3, 4;
                             4, 4;
                             1, 5;
                             2, 5;
                             3, 5;
                             4, 5;
                             1, 6;
                             2, 6;
                             3, 6;
                             4, 6;];
              
        end
        
    end % end of static methods
end % end of classdefinition