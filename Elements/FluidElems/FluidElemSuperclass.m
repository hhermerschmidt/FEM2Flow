classdef FluidElemSuperclass < ElemSuperclass

    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
properties (SetAccess = protected)
         
    %Physical unknowns
    %------------------------------------------------------------

    vRel            % relative displacement (velocity?) to FE-mesh
    vRel_dis        %
    
end
    
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
       
methods

function elem = FluidElemSuperclass( eleConnect    , ...
                                     eleCoord      , ...
                                     eleMatPara    , ...
                                     eleLoadPara   , ...
                                     eleBcDir      , ...
                                     eleBcNeu      , ...
                                     numGlobInput    ...
                                   )


    %Call superclass constructor:
    elem = elem@ElemSuperclass ( eleConnect    , ...
                                 eleCoord      , ...
                                 eleMatPara    , ...
                                 eleLoadPara   , ...
                                 eleBcDir      , ...
                                 eleBcNeu      , ...
                                 numGlobInput    ...
                               );
        

end
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
         
    
function Output = OutputFieldsInit(elem)
% Function initialises output fields for space-only Paraview output

% Initialising element output fields:
%--------------------------------------

% name of the output file
Output.filename     = elem.name; 

% number of nodes per element
Output.nNodesEle    = size(elem.eleNodes_t1,2); 

if elem.probDim == 2 
    Output.ele_type = 'Quadrilateral';   % 2D element
    Output.outId    = [0 1 2 3];         % Help variable 
elseif elem.probDim == 3
    Output.ele_type = 'Hexahedron';      % 3D element
    Output.outId    = [0 1 2 3 4 5 6 7]; % Help variable 
end

Output.nodesOut     = [];

Output.coord        = []; % coordinates of the nodes
Output.connect      = []; % connectivity of the element

Output.z1           = []; % counter help variable
Output.z2           = []; % counter help variable

if elem.type >= 4250          
    
    Output = elem.FreeSurfaceOutputFieldsInit(Output);           

else

    % 'Cell'-type variables only possess one value per element
    Output.cells.num    = 6; %number of cell-variables occurring in the output
    Output.cells.name   = cell(1,Output.cells.num);
    Output.cells.type   = cell(1,Output.cells.num);
    Output.cells.data   = cell(1,Output.cells.num);

    Output.cells.name{1,1}  = 'Element';
    Output.cells.type{1,1}  = 'Int';
    
    Output.cells.name{1,2}  = 'rho';
    Output.cells.type{1,2}  = 'Float';

    Output.cells.name{1,3}  = 'mu';
    Output.cells.type{1,3}  = 'Float';

    Output.cells.name{1,4}  = 'f_x';
    Output.cells.type{1,4}  = 'Float'; 

    Output.cells.name{1,5}  = 'f_y';
    Output.cells.type{1,5}  = 'Float';

    Output.cells.name{1,6}  = 'f_z';
    Output.cells.type{1,6}  = 'Float';   


    % 'Scalar'-type variables are '1-entry' variables changing in
    % the element
    Output.scalars.num  = 2; %number of scalar-variables occurring in the output
    Output.scalars.name = cell(1,Output.scalars.num);
    Output.scalars.type = cell(1,Output.scalars.num);
    Output.scalars.data = cell(1,Output.scalars.num);

    Output.scalars.name{1,1}  = 'nodeNumber_t1';
    Output.scalars.type{1,1}  = 'Int';
    
    Output.scalars.name{1,2}  = 'pressure';
    Output.scalars.type{1,2}  = 'Float';


    % 'Vector'-type variables are '3-entry' variables; each entry
    % changes in the elment
    Output.vectors.num  = 2; %number of vector-variables occurring in the output
    Output.vectors.name = cell(1,Output.vectors.num);
    Output.vectors.type = cell(1,Output.vectors.num);
    Output.vectors.data = cell(1,Output.vectors.num);

    Output.vectors.name{1,1} = 'coordinates';
    Output.vectors.type{1,1} = 'Float'; 
    
    Output.vectors.name{1,2} = 'velocity';
    Output.vectors.type{1,2} = 'Float'; 

    % 'Tensor'-type varibales are '9-entry' variables; each entry
    % changes in the element
    Output.tensors.num  = 0; %number of tensor-variables occurring in the output
    Output.tensors.name = cell(1,Output.tensors.num);
    Output.tensors.type = cell(1,Output.tensors.num);
    Output.tensors.data = cell(1,Output.tensors.num);

    % --no tensor data--
    
end %if
    
end %function
        
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
function Output = GetOutputData( elem      , ...
                                 Output    , ...
                                 eleSolu   , ...
                                 eleCoords , ...
                                 eleMatPara, ...
                                 eleLoadPara ...
                               )

% Get the output data from postprocessing and store it:

% Get the post-processed data of the current element:
[ergFluid, hh] = elem.PostOut(eleSolu, eleCoords); % hh = Zahl der Ergebnisssaetze je Elem

hh = hh/2;

% Get index for data storage:
index = (Output.z1 : (Output.z1 + hh - 1));

% Store node coordinates
switch elem.probDim
    case 2
        Output.coord(index,1) = eleCoords.t1(1:4,1);
        Output.coord(index,2) = eleCoords.t1(1:4,2);
        Output.coord(index,3) = eleCoords.t1(1:4,3);
    case 3
        Output.coord(index,1) = eleCoords.t1(1:8,1);
        Output.coord(index,2) = eleCoords.t1(1:8,2);
        Output.coord(index,3) = eleCoords.t1(1:8,3);
end

% Store connectivity:   
switch elem.probDim
    case 2
        Output.connect(Output.z2,1:4) = Output.outId;
    case 3
        Output.connect(Output.z2,1:8) = Output.outId;
end

if elem.type >= 4250  

    Output = elem.FreeSurfaceGetOutputData(Output, ergFluid, index, eleLoadPara, eleCoords);           

else

    % Store cell type output data
    Output.cells.data{1,1}(Output.z2,1) = elem.numGlob;

    Output.cells.data{1,2}(Output.z2,1) = eleMatPara.rho;
    Output.cells.data{1,3}(Output.z2,1) = eleMatPara.mu;

    Output.cells.data{1,4}(Output.z2,1) = eleLoadPara.fx;
    Output.cells.data{1,5}(Output.z2,1) = eleLoadPara.fy;
    Output.cells.data{1,6}(Output.z2,1) = eleLoadPara.fz;


    % Store scalar type output data
    Output.scalars.data{1,1}(index,1)           = elem.eleNodes_t1;

    % pressure
    switch elem.probDim
        case 2
            Output.scalars.data{1,2}(index,1)   = ergFluid(5:8,4);
        case 3
            Output.scalars.data{1,2}(index,1)   = ergFluid(9:16,4);
    end


    % Store vector type output data
    %coordinates
    switch elem.probDim
        case 2
            Output.vectors.data{1,1}(index,1) = eleCoords.t1(1:4,1);
            Output.vectors.data{1,1}(index,2) = eleCoords.t1(1:4,2);
            Output.vectors.data{1,1}(index,3) = eleCoords.t1(1:4,3);
        case 3
            Output.vectors.data{1,1}(index,1) = eleCoords.t1(1:8,1);
            Output.vectors.data{1,1}(index,2) = eleCoords.t1(1:8,2);
            Output.vectors.data{1,1}(index,3) = eleCoords.t1(1:8,3);
    end

    %velocity
    switch elem.probDim
        case 2
            Output.vectors.data{1,2}(index,1:3) = ergFluid(5:8,1:3);
        case 3
            Output.vectors.data{1,2}(index,1:3) = ergFluid(9:16,1:3);
    end


    % Store tensor type output data
    % --no tensor data--

end %if

% Update indices for data storage:
Output.z2    = Output.z2    + 1;
Output.z1    = Output.z1    + hh;
Output.outId = Output.outId + hh;
    
end %function
    
        
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%      
        
        function Output = OutputFieldsInitSpaceTime(elem)
        % Function initialises output fields for Paraview output
        
                if elem.probDim ~= 2 
                  
                    error('A space-time display is not possible.')
                
                end
                 
    
                % Initialising element output fields:
                %--------------------------------------
                Output.filename     = [elem.name,'SpaceTime']; % name of the output file

                Output.nNodesEle    = size(elem.eleNodes_t1,2); % number of nodes per element

                
                Output.ele_type = 'Hexahedron';      % 3D element
                Output.outId    = [0 1 2 3 4 5 6 7]; % Help variable 

                
                Output.nodesOut     = [];

                Output.coord        = []; % coordinates of the nodes
                Output.connect      = []; % connectivity of the element

                Output.z1           = []; % counter help variable
                Output.z2           = []; % counter help variable


                % 'Cell'-type variables only possess one value per element
                Output.cells.num    = 6; %number of cell-variables occurring in the output
                Output.cells.name   = cell(2,Output.cells.num);
                Output.cells.type   = cell(2,Output.cells.num);
                Output.cells.data   = cell(2,Output.cells.num);

                % 'Scalar'-type variables are '1-entry' variables changing in
                % the element
                Output.scalars.num  = 2; %number of scalar-variables occurring in the output
                Output.scalars.name = cell(2,Output.scalars.num);
                Output.scalars.type = cell(2,Output.scalars.num);
                Output.scalars.data = cell(2,Output.scalars.num);


                % 'Vector'-type variables are '3-entry' variables; each entry
                % changes in the elment
                Output.vectors.num  = 2; %number of vector-variables occurring in the output
                Output.vectors.name = cell(2,Output.vectors.num);
                Output.vectors.type = cell(2,Output.vectors.num);
                Output.vectors.data = cell(2,Output.vectors.num);

                % 'Tensor'-type varibales are '9-entry' variables; each entry
                % changes in the element
                Output.tensors.num  = 0; %number of tensor-variables occurring in the output
                Output.tensors.name = cell(2,Output.tensors.num);
                Output.tensors.type = cell(2,Output.tensors.num);
                Output.tensors.data = cell(2,Output.tensors.num);


                % Material parameters:
                %Cell-Data:
                Output.cells.name{1,1}   = 'rho';
                Output.cells.type{1,1}   = 'Float';

                Output.cells.name{1,2}   = 'mu';
                Output.cells.type{1,2}   = 'Float';

                Output.cells.name{1,3}   = 'f_x';
                Output.cells.type{1,3}   = 'Float'; 

                Output.cells.name{1,4}   = 'f_y';
                Output.cells.type{1,4}   = 'Float';

                Output.cells.name{1,5}   = 'f_z';
                Output.cells.type{1,5}   = 'Float';  

                Output.cells.name{1,6}   = 'Element';
                Output.cells.type{1,6}   = 'Int';

                %Scalar-Data
                Output.scalars.name{1,1} = 'pressure';
                Output.scalars.type{1,1} = 'Float';

                Output.scalars.name{1,2} = 'nodeNumber_t0t1';
                Output.scalars.type{1,2} = 'Int';

                %Vector-Data
                Output.vectors.name{1,1} = 'velocity';
                Output.vectors.type{1,1} = 'Float'; 

                Output.vectors.name{1,2} = 'coordinates';
                Output.vectors.type{1,2} = 'Float'; 

                %Tensor-Data
                % --no tensor data--
    
        end
        
        %------------------------------------------------------------------
        %------------------------------------------------------------------
 
        
        function Output = GetOutputDataSpaceTime( elem       , ...
                                                  Output     , ...
                                                  eleSolu    , ...
                                                  eleCoords  , ...
                                                  eleMatPara , ...
                                                  eleLoadPara, ...
                                                  time         ...
                                                )
            
            if elem.probDim ~= 2 
                  
                    error('A space-time display is not possible.')
                
            end
                
            
            % Get the output data from postprocessing and store it:
            
            % Get the post-processed data of the current element:
            [ergFluid, hh] = elem.PostOut(eleSolu, eleCoords); % hh = Zahl der Ergebnisssaetze je Elem
            
                       
            % Get index for data storage:
            index = (Output.z1 : (Output.z1 + hh - 1));
           
            % Store node coordinates
            Output.coord(index,1) = [eleCoords.t0(1:4,1);eleCoords.t1(1:4,1)  ];
            Output.coord(index,2) = [eleCoords.t0(1:4,2);eleCoords.t1(1:4,2)  ];
            Output.coord(index,3) = [zeros(4,1)         ;(time.curr(1,3) - time.curr(1,1))*ones(4,1)];
%           Output.coord(index,3) = [eleCoords.t0(1:4,3);eleCoords.t1(1:4,3)+0.01];
             
            
            % Store connectivity:   
            Output.connect(Output.z2,1:8) = Output.outId;

            % Store cell type output data
            Output.cells.data{1,1}(Output.z2,1) = eleMatPara.rho;
            Output.cells.data{1,2}(Output.z2,1) = eleMatPara.mu;
            
            Output.cells.data{1,3}(Output.z2,1) = eleLoadPara.fx;
            Output.cells.data{1,4}(Output.z2,1) = eleLoadPara.fy;
            Output.cells.data{1,5}(Output.z2,1) = eleLoadPara.fz;
            
            Output.cells.data{1,6}(Output.z2,1) = elem.numGlob;
            
            % Store scalar type output data
            Output.scalars.data{1,1}(index,1)   = [ ergFluid(1:4,4)    ; ergFluid(5:8,4)       ];

            Output.scalars.data{1,2}(index,1)   = [ elem.eleNodes_t0     elem.eleNodes_t1      ]; 
                        
            % Store vector type output data
            Output.vectors.data{1,1}(index,1:3) = [ ergFluid(1:4,1:3)  ; ergFluid(5:8,1:3)     ];
            
            Output.vectors.data{1,2}(index,1)   = [ eleCoords.t0(1:4,1); eleCoords.t1(1:4,1)  ];
            Output.vectors.data{1,2}(index,2)   = [ eleCoords.t0(1:4,2); eleCoords.t1(1:4,2)  ];
            Output.vectors.data{1,2}(index,3)   = [ zeros(4,1)         ; (time.curr(1,3) - time.curr(1,1))*ones(4,1)];
%           Output.vectors.data{1,2}(index,3)   = [ eleCoords.t0(1:4,3); eleCoords.t0(1:4,3)+0.01];
            

            
            % Store tensor type output data
            % --no tensor data--

            
            
            % Update indices for data storage:
            Output.z2    = Output.z2    + 1;
            Output.z1    = Output.z1    + hh;
            Output.outId = Output.outId + hh;
        end
        
    end    
end