classdef FluidNavierStokes431 < FluidElemSuperclass
    
    % Description:
    % Based on element type 430 with hopefully improved performance
    
    
    
    properties (SetAccess = protected)
        
        % General properties:
        %-----------------------------------------------------------------
        type        = 431;          % identification number of element type
        name        = 'FluidNavierStokes431';  % name of current element type
        sizeXest    = 64*64;        % estimated number of non-zero entries in eleMat
        probDim     = 3;            % problem dimension
        paraOutput  = 1;            % create Paraview output for current element type (1) or not (0)
        
               
        
        % Indices to access the element matrix:
        %----------------------------------------------------------------
        
        % Description:
        % Sets the number of dofs used for initialization of the element
        % attributes and the positions of the dof in the element matrix
        % and the element attribute to access them in the proper
        % positons:
        % Note: Since the velocity dof are the first element dof for
        % this type, positions of velocity dof are the same in velo and
        % in eleMat and thus not given seperately. For the other 
        % unknown fields positions are not the same, compare e.g. 
        % ind.pt0 and ind.pt0Sol
        
        % Initialise values in property section to ensure, that 'ind' is
        % created only once for all elements of this type:
        ind = struct( 'nNodesOut', 8          , ... % Number of nodes for output
                                                ...
                      'v'        , (  1:48 )' , ... % pos of all velocity dof in eleMat   
                                                ...
                      'vx'       , (  1:16 )' , ... % pos of x-velocity in eleMat
                      'vxt0'     , (  1: 8 )' , ... % pos of vx_t0 in eleMat and velo
                      'vxt1'     , (  9:16 )' , ... % pos of vx_t1 in eleMat and velo
                                                ...
                      'vy'       , ( 17:32 )' , ... % pos of y-velocity in eleMat
                      'vyt0'     , ( 17:24 )' , ... % pos of vy_t0 in eleMat and velo
                      'vyt1'     , ( 25:32 )' , ... % pos of vy_t1 in eleMat and velo
                                                ...
                      'vz'       , ( 33:48 )' , ... % pos of z-velocity in eleMat                                            
                      'vzt0'     , ( 33:40 )' , ... % pos of vz_t0 in eleMat and velo
                      'vzt1'     , ( 41:48 )' , ... % pos of vz_t1 in eleMat and velo
                                                ...
                      'vt0'      , [  1: 8    , ... % pos of v_t0 in eleMat and velo 
                                     17:24    , ...
                                     33:40 ]' , ...                                          
                      'vt1'      , [  9:16    , ... % pos of v_t1 in eleMat and velo
                                     25:32    , ...
                                     41:48 ]' , ... 
                                                ...
                      'p'        , ( 49:64 )' , ... % pos of all pressure dof
                                                ...
                      'pt0'      , ( 49:56 )' , ...  % pos of p_t0 in eleMat
                      'pt1'      , ( 57:64 )' , ...  % pos of p_t1 in eleMat
                                                ...
                      'pt0Sol'   , (  1: 8 )' , ...    % pos of p_t0 in pressure
                      'pt1Sol'   , (  9:16 )' , ...   % pos of p_t1 in pressure
                                                ...
                      'nDofV'    , 48         , ...   % total number of velo dof in current element
                      'nDofP'    , 16         , ...   % total number of pressure dof in current element
                      'nDofEle'  , 64           ...
                     );           % total number of dof for the current element
        

        % further properties are included in superclasses
    end
    
        
    %------------------------------------------------------------------
    
    %------------------------------------------------------------------
    
    
    methods
        
        function elem = FluidNavierStokes431( eleConnect    , eleCoord      , ...
                                              eleMatPara    , eleLoadPara   , ...
                                              eleBcDir      , eleBcNeu      , ...
                                              mm                              ...
                                            )

                            
            %Call the superclass constructor:
            elem = elem@FluidElemSuperclass( eleConnect    , eleCoord      , ...
                                             eleMatPara    , eleLoadPara   , ...
                                             eleBcDir      , eleBcNeu      , ...
                                             mm                              ...
                                           );

            
            % Initialise element data for dof and related data:
            %--------------------------------------------------
           	elem.vRel           = zeros(elem.ind.nDofV,1);
           	elem.vRel_dis       = zeros(elem.ind.nDofV,1);
        end
        
        
        %------------------------------------------------------------------
        %------------------------------------------------------------------
        
        
        function[eleVec,...
                 index,...
                 I_elem,...
                 J_elem,...
                 X_elem,...
                 sizeXElem] = Build( elem,...
                                     eleSolu, ...
                                     eleSoluDis, ...
                                     eleCoords, ...
                                     deltaT,...
                                     currTimeVar)
             
            % Get the position structure to access element matrix & rhs:
            pos         = elem.ind;
            
            %Initialise the element matrix and the right hand side:
            eleMat      = zeros(pos.nDofEle,pos.nDofEle);
            rhs         = zeros(pos.nDofEle,1);
            
            
            % Get material data from eleMatPara:
            %--------------------------------------------------
            rho         = elem.matPara.rho;       % density
            mu          = elem.matPara.mu;        % dynamic viscosity
            
            fx          = elem.loadPara.fx;        % volume force in x-dir
            fy          = elem.loadPara.fy;        % volume force in y-dir
            fz          = elem.loadPara.fz;        % volume force in z-dir
            
            typeTimeVarLoadPara = elem.loadPara.timeVar; % time variation
            
            
            % Get unknowns from the solution:
            %---------------------------------
            velo        = eleSolu(pos.v);  
            velo_dis    = eleSoluDis;      
            pressure    = eleSolu(pos.p);

            %Zusammenfassung der Freiwerte
            %------------------------------------------------- 
            v_x = velo(pos.vx);     
            v_y = velo(pos.vy);      
            v_z = velo(pos.vz);
            %----------------------------------------------------

            %Relativgeschwindigkeit zum FE-Netz
            %----------------------------------------------------
% %             v_x_rel = elem.vRel(pos.vx);
% %             v_y_rel = elem.vRel(pos.vy);   
% %             v_z_rel = elem.vRel(pos.vz);
            v_x_rel = v_x;
            v_y_rel = v_y;   
            v_z_rel = v_z;
            %----------------------------------------------------
            
            
            % Global node coordinates including time             
            nodesGlob              = zeros(16,4);
            %nodesGlob(1:8  , 1:3 ) = eleCoords;
            %nodesGlob(9:16 , 1:3 ) = eleCoords;
            nodesGlob(1:8  , 1:3 ) = eleCoords.t0(:,1:3);
            nodesGlob(9:16 , 1:3 ) = eleCoords.t1(:,1:3);
            nodesGlob(9:16 ,  4  ) = deltaT;
            
            
            % Local node coordinates:
            nodesXi1 = [ -1.0 ,  1.0 ,  1.0 , -1.0 , -1.0 ,  1.0 , 1.0 , -1.0 ];
            nodesXi2 = [ -1.0 , -1.0 ,  1.0 ,  1.0 , -1.0 , -1.0 , 1.0 ,  1.0 ];
            nodesXi3 = [ -1.0 , -1.0 , -1.0 , -1.0 ,  1.0 ,  1.0 , 1.0 ,  1.0 ];


            % Matrix of dynamic viscosity parameter:
            muMat = zeros(6,6);
            muMat(1:3, 1:3) = 2.0 * mu * eye(3);
            muMat(4:6, 4:6) = 1.0 * mu * eye(3);
            

            %Berechnung der gemittelten Elementlaenge
            %-------------------------------------------------------------

            geom_abl_xi = 0.125 * [nodesXi1;
                                   nodesXi2;
                                   nodesXi3];

            jakobi = [(0.5 * geom_abl_xi),(0.5 * geom_abl_xi)] * nodesGlob(:,1:3);

            volume = 8 * det(jakobi);

            delta_h = (6 * volume / pi)^(1/3); % delta_h = diameter of equi-volume sphere
            %-------------------------------------------------------------


            


            %Gravitationskraft des Fluids
            f_i_time = [fx;
                        fy;
                        fz] * (currTimeVar(typeTimeVarLoadPara, [1, 3]) * rho);

            % Initialise matrices and vectors required to build the element
            % matrix:
            shapeFuncDLocST = zeros(4,16);
            
            matrix_jumpVV   = zeros(8,8);
            matrix_vDivvDiv = zeros(48,48);
            matrix_pv       = zeros(16,48);
            matrix_vDxvDx   = zeros(48,48);

            
            
            %--------------------------------------------------------------
            
            % Get local coordinates and weight of Gauss points:
            coord_gp = [ double(-1/sqrt(3)) , double(1/sqrt(3)) ];   
            weight   = [         1          ,        1          ];
            
            
            % Loop over Gauss points
            for ll = 1 : 2 % loop over xi_3 values of Gauss points

                % local xi_3 coordinate of current Gauss point
                xi_3 = coord_gp(1,ll); 	
                
                for jj = 1 : 2 % loop over xi_2 values of Gauss points

                    % local xi_2 coordinate of current Gauss point
                    xi_2 = coord_gp(1,jj); 	

                    for kk = 1 : 2 % loop over xi_1 values of Gauss points

                        % local xi_1 coordinate of current Gauss point
                        xi_1 = coord_gp(1,kk);


                        % Velocity and pressure shape functions (tri-linear in space):
                        shapeFunc = 0.125 * (   (1 + xi_1 * nodesXi1) ...
                                              .*(1 + xi_2 * nodesXi2) ...
                                              .*(1 + xi_3 * nodesXi3) ...
                                            );
                        shapeFuncDLocST(4,:) = 0.5 * [-shapeFunc, shapeFunc];
                        
                        % Get local derivatives of shape functions:
                        % shapeFuncDLoc = [shapeFunc/dxi_1; shapeFunc/dxi_2]; shapeFunc/dxi_3;
                        shapeFuncDXi123 = 0.125 * ...
                                          [       ...
                                          nodesXi1.*(1 + xi_2 * nodesXi2).*(1 + xi_3 * nodesXi3);
                                          nodesXi2.*(1 + xi_1 * nodesXi1).*(1 + xi_3 * nodesXi3);
                                          nodesXi3.*(1 + xi_1 * nodesXi1).*(1 + xi_2 * nodesXi2) ...
                                          ];

                        

                        for ii = 1 : 2 % loop over local time axis

                            % local time coordinate of current Gauss point:
                            tau = coord_gp(1,ii);
                            
                            % Time shape functions at current Gauss point:
                            t_1 = 0.5 * (1-tau);
                            t_2 = 0.5 * (1+tau);

                            shapeFuncDLocST(1:3,:) = [(t_1 * shapeFuncDXi123) , (t_2 * shapeFuncDXi123)];

                            jacobi = shapeFuncDLocST * nodesGlob;
                            detJacobi = det(jacobi);
                            invJacobi = inv(jacobi);
                            
                            % Integration factor of current Gauss point:
                            intFac = weight(1,ll) * weight(1,ii) * weight(1,jj) * weight(1,kk)* detJacobi;
                            
                            b = f_i_time * [t_1;
                                            t_2];
                            
                            %--------------------------------------------------
                            shapeFuncST = [ (t_1 * shapeFunc) , (t_2 * shapeFunc) ];

                            shapeFuncDxST = invJacobi(1,1:3) * shapeFuncDLocST(1:3,:);
                            shapeFuncDyST = invJacobi(2,1:3) * shapeFuncDLocST(1:3,:);
                            shapeFuncDzST = invJacobi(3,1:3) * shapeFuncDLocST(1:3,:);
                            shapeFuncDtST = invJacobi(4,:)   * shapeFuncDLocST;

                            shapeFuncDiv  = [shapeFuncDxST  shapeFuncDyST  shapeFuncDzST];
                            
                            shapeFuncDxyzST = [shapeFuncDxST;  shapeFuncDyST;  shapeFuncDzST];
                            %--------------------------------------------------

                            %--------------------------------------------------
                            V   = (shapeFuncST  * [v_x v_y v_z])';

                            V_T = (shapeFuncDtST * [v_x v_y v_z])';

                            V_Rel = (shapeFuncST  * [v_x_rel v_y_rel v_z_rel])';
                            %--------------------------------------------------

                            %zeitliche und raumliche Ableitung des Geschwindigkeitfeldes
                            %--------------------------------------------------
                            GradV = (shapeFuncDxyzST * [v_x  v_y  v_z])';

                            %Auswertung des Geschwindigkeitsgradienten
                            GradV_V = GradV * V;

                            H = [GradV(:,1) * shapeFuncST, GradV(:,2) * shapeFuncST, GradV(:,3) * shapeFuncST];

                            ShapeGradV = V' * shapeFuncDxyzST;

                            Vec = shapeFuncDtST + ShapeGradV;

                            H(1, 1:16) = H(1, 1:16) + Vec;
                            H(2,17:32) = H(2,17:32) + Vec;
                            H(3,33:48) = H(3,33:48) + Vec;

                            H = H * rho;

                            eleMat(pos.vx,pos.v) = eleMat(pos.vx,pos.v) + shapeFuncST' * (H(1,:) * intFac);
                            eleMat(pos.vy,pos.v) = eleMat(pos.vy,pos.v) + shapeFuncST' * (H(2,:) * intFac);
                            eleMat(pos.vz,pos.v) = eleMat(pos.vz,pos.v) + shapeFuncST' * (H(3,:) * intFac);
                            %--------------------------------------------------


                            %Materialmodell
                            %--------------------------------------------------

                            ShapeD = [shapeFuncDxST , zeros(1,16)   , zeros(1,16)   ;
                                      zeros(1,16)   , shapeFuncDyST , zeros(1,16)   ;
                                      zeros(1,16)   , zeros(1,16)   , shapeFuncDzST ;
                                      shapeFuncDyST , shapeFuncDxST , zeros(1,16)   ;
                                      shapeFuncDzST , zeros(1,16)   , shapeFuncDxST ;
                                      zeros(1,16)   , shapeFuncDzST , shapeFuncDyST ];

                                  
                            matrix_vDxvDx = matrix_vDxvDx + ShapeD' * (muMat * intFac) * ShapeD;
                            
                            
                            EE = [       (shapeFuncDxST * v_x);
                                         (shapeFuncDyST * v_y);
                                         (shapeFuncDzST * v_z);
                                  ( 0.5*((shapeFuncDyST * v_x) + (shapeFuncDxST * v_y)) );
                                  ( 0.5*((shapeFuncDzST * v_x) + (shapeFuncDxST * v_z)) );
                                  ( 0.5*((shapeFuncDzST * v_y) + (shapeFuncDyST * v_z)) )];
                            
                            
                            %--------------------------------------------------

                            %Masseerhaltung
                            %----------------------------------------------
                            matrix_pv = matrix_pv + (shapeFuncST * intFac)' * shapeFuncDiv;
                            %--------------------------------------------------

                            
                            
                            %Stabilisierung
                            %--------------------------------------------------
                            %Stabilisierung der Impulsbilanz
                            %Die Rohdichte wird an dieser Stelle schon mit eingerechnet 
                            tau_m = 1/sqrt( (2/deltaT)^2 + (2*norm(V_Rel)/delta_h)^2 + (4*(mu/rho)/(delta_h^2))^2 ) * (1/rho);

                            %erster Anteil der Linearisierung
                            StabMat = [H, shapeFuncDxyzST];

                            Vec = ShapeGradV * rho;

                            StabMat2 = [ Vec        ,       zeros(1,32), shapeFuncDxST;
                                         zeros(1,16), Vec,  zeros(1,16), shapeFuncDyST;
                                         zeros(1,32),       Vec,         shapeFuncDzST];

                            eleMat = eleMat + StabMat2' * (StabMat * (tau_m * intFac)); 

                            %zweiter Anteil der Linearisierung 
                            %Auswertung des rhsduums der Impulsbilanz

                            V_Tot = (V_T + GradV_V) * rho;

                            P_Diff = shapeFuncDxyzST * pressure;

                            L = V_Tot - b + P_Diff;

                            Mat = [shapeFuncDxyzST * L(1) , shapeFuncDxyzST * L(2) , shapeFuncDxyzST * L(3)];

                            Vec = shapeFuncST * (rho * tau_m * intFac);

                            eleMat(pos.v,pos.vx) = eleMat(pos.v,pos.vx) + Mat(1,:)' * Vec;
                            eleMat(pos.v,pos.vy) = eleMat(pos.v,pos.vy) + Mat(2,:)' * Vec;
                            eleMat(pos.v,pos.vz) = eleMat(pos.v,pos.vz) + Mat(3,:)' * Vec;

                            %Stabilisierung der Masseerhaltung
                            %Die Rohdichte wird an dieser Stelle schon mit eingerechnet
                            Re = (norm(V_Rel) * delta_h) / (2 * (mu / rho));
                            if Re < 3
                                tau_c = (delta_h * norm(V_Rel) * (Re / 3)) * rho;
                            else
                                tau_c = (delta_h * norm(V_Rel))            * rho;
                            end

                            matrix_vDivvDiv = matrix_vDivvDiv + shapeFuncDiv' * (shapeFuncDiv * (tau_c * intFac));
                            
                            
                            %--------------------------------------------------

                            %Auswertung des rhsduums
                            %--------------------------------------------------
                            rhs(pos.vx) = rhs(pos.vx) + shapeFuncST' * ((V_Tot(1) - b(1)) * intFac);
                            rhs(pos.vy) = rhs(pos.vy) + shapeFuncST' * ((V_Tot(2) - b(2)) * intFac);
                            rhs(pos.vz) = rhs(pos.vz) + shapeFuncST' * ((V_Tot(3) - b(3)) * intFac);
                            
                            rhs(pos.v)  = rhs(pos.v)  + ShapeD' * ((2 * intFac) * EE);

                            rhs         = rhs         + StabMat2' * (L * tau_m * intFac);
                            %--------------------------------------------------

                        end
                        
                        % Uebergangsbedingungen
                        jacobit0        = shapeFuncDXi123 * nodesGlob(1:8,1:3);
                        
                        intFac          = weight(1,ll) * weight(1,jj) * weight(1,kk) * det(jacobit0);
                        
                        matrix_jumpVV   = matrix_jumpVV + shapeFunc' * (shapeFunc * (rho * intFac));
                        
                    end
                end
            end
            
            
            % Add contributions to eleMat:
            %--------------------------------------------------
            eleMat(pos.p, pos.v) = eleMat(pos.p, pos.v) + matrix_pv;
            eleMat(pos.v, pos.p) = eleMat(pos.v, pos.p) - matrix_pv';
            
            eleMat(pos.v, pos.v) = eleMat(pos.v, pos.v) + matrix_vDivvDiv;
            eleMat(pos.v, pos.v) = eleMat(pos.v, pos.v) + matrix_vDxvDx;
            
            % Add contributions to rhs:
            %--------------------------------------------------
            rhs(pos.v,1) = rhs(pos.v,1) + matrix_vDivvDiv * velo;
            rhs(pos.p,1) = rhs(pos.p,1) + matrix_pv * velo;
            rhs(pos.v,1) = rhs(pos.v,1) - matrix_pv' * pressure;
            
            % Add contributions of jump terms to eleMat:
            %--------------------------------------------------
            eleMat(pos.vxt0,pos.vxt0) = eleMat(pos.vxt0,pos.vxt0) + matrix_jumpVV;
            eleMat(pos.vyt0,pos.vyt0) = eleMat(pos.vyt0,pos.vyt0) + matrix_jumpVV;
            eleMat(pos.vzt0,pos.vzt0) = eleMat(pos.vzt0,pos.vzt0) + matrix_jumpVV;
            
            % Get jump at start of current time slab:
            %--------------------------------------------------
            jumpDeltaVx  = velo(pos.vxt0) - velo_dis(pos.vxt1);
            jumpDeltaVy  = velo(pos.vyt0) - velo_dis(pos.vyt1);
            jumpDeltaVz  = velo(pos.vzt0) - velo_dis(pos.vzt1);

            % Add contributions of jump terms to rhs:
            %--------------------------------------------------
            rhs(pos.vxt0) = rhs(pos.vxt0) + matrix_jumpVV * jumpDeltaVx;
            rhs(pos.vyt0) = rhs(pos.vyt0) + matrix_jumpVV * jumpDeltaVy;
            rhs(pos.vzt0) = rhs(pos.vzt0) + matrix_jumpVV * jumpDeltaVz;
            
     
            

            % Set Dirichlet BC:
            %-----------------------------
            if ~isempty(elem.bcDir)
                activeBcDir = find(elem.bcDir(:,6) == 1); % Find active bcDir in current element
                posLocBcDir = elem.bcDir(activeBcDir,7);  % Get local position of active bcDir
                
                eleMat(posLocBcDir, :)          = 0.0;
                eleMat(posLocBcDir,posLocBcDir) = eye(length(activeBcDir));
            end
            
            
            % Change sign of rhs:
            %----------------------
            eleVec = -rhs;
 
            
            % Store the element matrix and the indices in vectors:
            %--------------------------------------------------------------
            
            % Get the local indices and the values of the nonzero parts of
            % eleMatHyb:
            [I_Loc, J_Loc, X_elem] = find(eleMat);
            
            % Transfer the local indices to global ones:
            index       = elem.indexSystem;
            I_elem      = index(I_Loc);
            J_elem      = index(J_Loc);
            
            % Get the number of nonzero entries for the current element:
            sizeXElem   = size(I_elem,1);

        end

        
        %------------------------------------------------------------------
        %------------------------------------------------------------------
        
        
        function [ergFluid, sizeErg] = PostOut(elem, eleSolu, ~)
            % Method returns output data of the element at end of current
            % time slab:
            
            % Get indices to access solution and element properties:
            pos         = elem.ind;
            
            % Number of nodes for which output is returned:
            sizeErg     = pos.nNodesOut*2;
            
            % Get unknowns from the solution:
            %-----------------------------------
            velo        = eleSolu(pos.v);
            pressure    = eleSolu(pos.p);
            
            % Initialisation:
            %---------------------
            ergFluid = zeros(sizeErg,4);
            
            % Get velocities and pressure:
            %------------------------------------
           
            ergFluid(1:8,1)  = velo(pos.vxt0);  % v_x_t0         
            ergFluid(9:16,1) = velo(pos.vxt1);  % v_x_t1
                        
            ergFluid(1:8,2)  = velo(pos.vyt0);  % v_y_t0
            ergFluid(9:16,2) = velo(pos.vyt1);  % v_y_t1
            
            ergFluid(1:8,3)  = velo(pos.vzt0);  % v_z_t0
            ergFluid(9:16,3) = velo(pos.vzt1);  % v_z_t1
            
            ergFluid(1:8,4)  = pressure(pos.pt0Sol); % p_t0
            ergFluid(9:16,4) = pressure(pos.pt1Sol); % p_t1
            


        end
        
        
        %------------------------------------------------------------------
        %------------------------------------------------------------------
        

        function Update(elem, ~, deltaTCurr, deltaTNext, typeEleUpdate)
            
            % deltaTCurr: time width of current time  slab
            % deltaTNext: time width of next time slab
            % typeEleUpdate: 1: use gradient of current time slab to
            %                   estimate start and end values of next time
            %                   slab (initial values for first iteration)
            %                else: gradient is not used, initial values for
            %                   start and end of next time slab are thesame)
            
            % Get indices:
            pos = elem.ind;
            
            % Set discontinious values:
            elem.vRel_dis     = elem.vRel;
            
            if typeEleUpdate == 1
                deltaVRelCurr               = elem.vRel(pos.vt1) - elem.vRel(pos.vt0);
                elem.vRel(pos.vt0)          = elem.vRel(pos.vt1); 
                elem.vRel(pos.vt1)          = elem.vRel(pos.vt0) + deltaVRelCurr * (deltaTNext / deltaTCurr);
            else
                elem.vRel(pos.vt0)          = elem.vRel(pos.vt1);
            end
           
        end
        
        
        %------------------------------------------------------------------
        %------------------------------------------------------------------
        
        
        function [solUpdate , index] = UpdateSol(elem, eleSolu, ...
                                                 deltaTCurr, deltaTNext,...
                                                 typeEleUpdate)
            
            % Method called in UpdateSolution:
            
            % Get the current solution, update it for the next time slab
            % and return it as solUpdate:
            %--------------------------------------------------------------
            
            % Get indices:
            pos = elem.ind;
            
            % Get unknowns from eleSolu:
            velo        = eleSolu(pos.v);
            pressure    = eleSolu(pos.p);
            
            % Update unknowns for next time slab:
            if typeEleUpdate == 1
                deltaVeloCurr          = velo(pos.vt1) - velo(pos.vt0);
                velo(pos.vt0)          = velo(pos.vt1); 
                velo(pos.vt1)          = velo(pos.vt0) + deltaVeloCurr * (deltaTNext / deltaTCurr);
                
                deltaPressureCurr      = pressure(pos.pt1Sol) - pressure(pos.pt0Sol);
                pressure(pos.pt0Sol)   = pressure(pos.pt1Sol); 
                pressure(pos.pt1Sol)   = pressure(pos.pt0Sol) + deltaPressureCurr * (deltaTNext / deltaTCurr);
                
            else
                velo(pos.vt0)          = velo(pos.vt1);
                pressure(pos.pt0Sol)   = pressure(pos.pt1Sol);
            end
            
            
            % Initialisation of updated solution:
            solUpdate = zeros(pos.nDofEle, 1);
            
            % Set values:
            solUpdate(pos.v)    = velo;
            solUpdate(pos.p)    = pressure;
                
            index               = elem.indexSystem;     
        end
        
        
        %------------------------------------------------------------------
        %------------------------------------------------------------------

        
        function Restore(elem, ~, deltaTCurr, deltaTNext, typeEleUpdate)
            % Function resets the element properties for the dof using
            % data from previous time slab. Function is invoked by
            % "RestoreElem" if iteration did not converge and width of time
            % step is reduced.
            
            % Set indices:
            pos = elem.ind;
            
            % Restore the element data:
            if typeEleUpdate == 1
                % Use gradient of previous time slab for initial values of
                % the current time slab:
                deltaVRelPrev       = elem.vRel_dis(pos.vt1) - elem.vRel_dis(pos.vt0);
                elem.vRel(pos.vt0)  = elem.vRel_dis(pos.vt1); 
                elem.vRel(pos.vt1)  = elem.vRel(pos.vt0) + deltaVRelPrev * (deltaTNext / deltaTCurr);
                
            else
                % Initial values for start and end of time slab are the
                % same:
                elem.vRel(pos.vt0)  = elem.vRel_dis(pos.vt1);
                elem.vRel(pos.vt1)  = elem.vRel(pos.vt0);
            end
            
        end
        
        
        %------------------------------------------------------------------
        %------------------------------------------------------------------
        
        
        function SetEleDofGlob(elem)
            % Sets the property eleDofGlob of the current element:
            % eleDofGlob has as many cols as the maximum local dof id of
            % the element. Col: local dof id, value: global dof id
            
            
            elem.eleDofGlob = [1, 21, 2, 22, 3, 23, 4, 24];
                
        end

        
    end % end of methods
    
    
    %------------------------------------------------------------------
    
    %------------------------------------------------------------------
    
    
    methods (Static)
        function Post(~, ~, ~)
            % Method setting element data from the solution:
            
            % -- no secondary unknowns stored for this element type --
        end
        
        
        %------------------------------------------------------------------
        %------------------------------------------------------------------
        
        
        function eleNodesDofLoc = GetEleNodesDofLoc
        % Function returns a matrix containing the local node numbers (1st col) and
        % the corresponding local DOF-IDs (2nd col).
        %
        % The local DOF-IDs are
        % 1: vx at t0
        % 2: vx at t1
        %
        % 3: vy at t0
        % 4: vy at t1
        % 
        % 5: vz at t0
        % 6: vz at t1
        %
        % 7: p  at t0
        % 8: p  at t1
            
            eleNodesDofLoc= [1, 1;
                             2, 1;
                             3, 1;
                             4, 1;
                             5, 1;
                             6, 1;
                             7, 1;
                             8, 1; ...
                                   ...
                             1, 2;
                             2, 2;
                             3, 2;
                             4, 2;
                             5, 2;
                             6, 2;
                             7, 2;
                             8, 2; ...
                                   ...
                             1, 3;
                             2, 3;
                             3, 3;
                             4, 3;
                             5, 3;
                             6, 3;
                             7, 3;
                             8, 3; ...
                                   ...
                             1, 4;
                             2, 4;
                             3, 4;
                             4, 4;
                             5, 4;
                             6, 4;
                             7, 4;
                             8, 4; ...
                                   ...
                             1, 5;
                             2, 5;
                             3, 5;
                             4, 5;
                             5, 5;
                             6, 5;
                             7, 5;
                             8, 5; ...
                                   ...
                             1, 6;
                             2, 6;
                             3, 6;
                             4, 6;
                             5, 6;
                             6, 6;
                             7, 6;
                             8, 6; ...
                                   ...
                             1, 7;
                             2, 7;
                             3, 7;
                             4, 7;
                             5, 7;
                             6, 7;
                             7, 7;
                             8, 7; ...
                                   ...
                             1, 8;
                             2, 8;
                             3, 8;
                             4, 8;
                             5, 8;
                             6, 8;
                             7, 8;
                             8, 8];
              
        end
        
    end % end of static methods
end % end of classdefinition