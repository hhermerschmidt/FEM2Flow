classdef FluidNSFreeSurface4250 < FreeSurfaceElem
% 2D Fluidelement (inkomp. Navier-Stokes) mit freier Oberfl�che (Level-Set)

% Description:
% Fluid-Ursprungselement 2D,
% 4 Knoten, rechteckig,
% trilineare Geschwindigkeits-Druck Formulierung,
% kombinierte SUPG/PSPG-Stabilisierung d. Impulsbilant,
% GLS-Stabilisierung d. Massenerhaltung
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
properties (SetAccess = protected)

    % General properties:
    %-----------------------------------------------------------------
    type        = 4250;                     % identification number of element type
    name        = 'FluidNSFreeSurface4250'; % element name
    sizeXest    = 32*32;                    % (estimated) number of non-zero entries in eleMat
    probDim     = 2;                        % problem dimension
    paraOutput  = 1;                        % Paraview output for current element
                                            % 0: no Paraview output
                                            % 1:    Paraview output
                  
                                            
    phiAverage
    density_dis
    


    % Indices to access the element matrix:
    %----------------------------------------------------------------
    
    ind = struct('nNodesOut'  , 4             , ... % Number of nodes for output
                 'v'          , (1:16)'       , ... % pos of all velocity dof in eleMat
                 'p'          , (17:24)'      , ... % pos of all pressure dof
                 'phi'        , (25:32)'      , ...
                 'vx'         , (1:8)'        , ... % pos of x-velocity in eleMat
                 'vy'         , (9:16)'       , ... % pos of y-velocity in eleMat
                 'vxt0'       , (1:4)'        , ... % pos of vx_t0 in eleMat and velo
                 'vyt0'       , (9:12)'       , ... % pos of vy_t0 in eleMat and velo
                 'vt0'        , [1:4, 9:12]'  , ... % pos of v_t0 in eleMat and velo
                 'vxt1'       , (5:8)'        , ... % pos of vx_t1 in eleMat and velo
                 'vyt1'       , (13:16)'      , ... % pos of vy_t1 in eleMat and velo
                 'vt1'        , [5:8, 13:16]' , ... % pos of v_t1 in eleMat and velo
                 'pt0'        , (17:20)'      , ... % pos of p_t0 in eleMat
                 'pt1'        , (21:24)'      , ... % pos of p_t1 in eleMat
                 'pt0Sol'     , (1:4)'        , ... % pos of p_t0 in pressure
                 'pt1Sol'     , (5:8)'        , ... % pos of p_t1 in pressure
                 'phi_t0'     , (25:28)'      , ...
                 'phi_t1'     , (29:32)'      , ...
                 'phi_t0_Sol' , (1:4)'        , ... % pos of phi_t0 in ?
                 'phi_t1_Sol' , (5:8)'        , ... % pos of phi_t1 in ?
                 'nDofV'      , 16            , ... % total number of velo dof in current element
                 'nDofP'      , 8             , ... % total number of pressure dof in current element
                 'nDofPhi'    , 8             , ...
                 'nDofEle'    , 32)                 % total number of dof for the current element



end
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
       
methods
        
function elem = FluidNSFreeSurface4250( eleConnect    , eleCoord      , ...
                                        eleMatPara    , eleLoadPara   , ...
                                        eleBcDir      , eleBcNeu      , ...
                                        mm                              ...
                                      )


    %Call the superclass constructor:
    elem = elem@FreeSurfaceElem( eleConnect    , eleCoord      , ...
                                 eleMatPara    , eleLoadPara   , ...
                                 eleBcDir      , eleBcNeu      , ...
                                 mm                              ...
                               );

    
    % Initialise element data for dof and related data:
    %--------------------------------------------------
    elem.vRel           = zeros(elem.ind.nDofV,1);
    elem.vRel_dis       = zeros(elem.ind.nDofV,1);
    
    % Initialise element density and viscosity (depending on IC of LevelSet fct
    %--------------------------------------------------------------------------
    elem.density   = zeros(elem.ind.nNodesOut*2,1);
    elem.viscosity = zeros(elem.ind.nNodesOut*2,1);
    
    load feMesh.mat
    eleMatPara = feMesh.matPara;
    eleCoords  = feMesh.nodes(eleConnect,:);
    
    load LevelSetIC.mat
    elePhiInitial = initialPhi(eleConnect,1);
    
    PhiInitial_Average = 1/4 * sum(elePhiInitial);
    elem.phiAverage = PhiInitial_Average;

    hx_Average = eleCoords(2,1) - eleCoords(1,1);
    smoothArea = 10*hx_Average;
 
    if PhiInitial_Average <= 0 - smoothArea;
        elem.density(:)   = eleMatPara.rhoLiquid;
        elem.viscosity(:) = eleMatPara.muLiquid;

    elseif PhiInitial_Average >= 0 + smoothArea;
        elem.density(:)   = eleMatPara.rhoGas;
        elem.viscosity(:) = eleMatPara.muGas;

    else
        Heaviside = 0.5 * (1 + PhiInitial_Average/smoothArea + 1/pi * sin(PhiInitial_Average*pi/smoothArea));

        elem.density(:)   = (1-Heaviside) * eleMatPara.rhoLiquid + Heaviside * eleMatPara.rhoGas;
        elem.viscosity(:) = (1-Heaviside) * eleMatPara.muLiquid  + Heaviside * eleMatPara.muGas;

    end
    
    elem.density_dis = elem.density;

%     for ii = 1:size(elePhiInitial,1)
%         if elePhiInitial(2) <= 0
%             elem.density(:)   = eleMatPara.rhoLiquid;
%             elem.viscosity(:) = eleMatPara.muLiquid;
%         else
%             elem.density(:)   = eleMatPara.rhoGas;
%             elem.viscosity(:) = eleMatPara.muGas;
%         end
% 
%     end
    

    clear feMesh eleMatPara eleCoords initialPhi eleInitialPhi PhiInitial_Average hx_Average smoothArea Heaviside
end
        
        
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        
function[ eleVec , ...
          I_elem , ...
          J_elem , ...
          X_elem       ] = Build( elem       , ...
                                  eleSolu    , ...
                                  eleSoluDis , ...
                                  eleCoords  , ...
                                  time       , ...
                                  currTimeVar  ...
                                )

% Get the position structure to access element matrix & rhs:
pos = elem.ind;

%Initialise the element matrix and the right hand side:
eleMat      = zeros(pos.nDofEle,pos.nDofEle);
rhs         = zeros(pos.nDofEle,1);


% Get material data from eleMatPara:
%--------------------------------------------------
rhoGas       = elem.matPara.rhoGas;    % density (gas    phase)
rhoLiquid    = elem.matPara.rhoLiquid; % density (liquid phase)

muGas        = elem.matPara.muGas;     % dynamic viscosity (gas    phase)
muLiquid     = elem.matPara.muLiquid;  % dynamic viscosity (liquid phase)

delta_rho    = rhoGas - rhoLiquid;
delta_mu     = muGas  - muLiquid;

fx          = elem.loadPara.fx;        % volume force in x-dir
fy          = elem.loadPara.fy;        % volume force in y-dir


% Get unknowns from the solution:
%---------------------------------
velo        = eleSolu(pos.v);
velo_dis    = eleSoluDis(pos.vt1);

pressure    = eleSolu(pos.p);


% LevelSet-Werte vorherige Zeitscheibe (Zeitscheibenende)
Phi_prev  = eleSoluDis(pos.phi_t1); 

% Loesungen vorhergehender Iterationsschritt
Phi    = eleSolu(pos.phi); % LevelSet-Werte dieser Zeitscheibe (letzte NR-Iteration)
Phi_t0 = Phi(pos.phi_t0_Sol);

rhoPrev = elem.density_dis(end);

Phi_Average = 1/8 * sum(Phi);
elem.phiAverage = Phi_Average;
hx_Average = eleCoords.t1(2,1) - eleCoords.t1(1,1);
smoothArea = 10*hx_Average;

if Phi_Average <= 0 - smoothArea;
    rho = rhoLiquid;
    mu  = muLiquid;
         
elseif Phi_Average >= 0 + smoothArea;
    rho = rhoGas;
    mu  = muGas;
        
else
    Heaviside = 0.5 * (1 + Phi_Average/smoothArea + 1/pi * sin(Phi_Average*pi/smoothArea));
    
    rho = (1-Heaviside) * rhoLiquid + Heaviside * rhoGas;
    mu  = (1-Heaviside) * muLiquid  + Heaviside * muGas;
    
end

elem.density(:)   = rho;
elem.viscosity(:) = mu;

% rho = rhoLiquid;
% mu  = muLiquid;


Heaviside_d_phi = 1/(2*smoothArea) * ( 1 + cos(Phi_Average*pi/smoothArea));
  
N_rho = 1/8 * ones(8,1);
N_mu  = 1/8 * ones(8,1);




deltaT = time.curr(1,3) - time.curr(1,1);


nodesGlob           = zeros(8,3);
nodesGlob(1:4,1:2)  = eleCoords.t0(:,1:2);
nodesGlob(5:8,1:2)  = eleCoords.t1(:,1:2);
nodesGlob(5:8,3)    = deltaT;


% Local coordinates of nodes:
nodesXi1 = [-1.0, 1.0, 1.0,-1.0];
nodesXi2 = [-1.0,-1.0, 1.0, 1.0];


%Berechnung der gemittelten Elementlaenge
%-------------------------------------------------------------

geom_abl_xi = 0.25 * [nodesXi1;
                      nodesXi2];

jakobi = [(0.5 * geom_abl_xi),(0.5 * geom_abl_xi)] * nodesGlob(:,1:2);

volume = 4 * det(jakobi);

delta_h = sqrt(4 * volume / pi); % diameter of cylinder with same height (1) and volume as element
%-------------------------------------------------------------


%Zusammenfassung der Freiwerte
%------------------------------------------------- 
v_x = velo(pos.vx);     
v_y = velo(pos.vy);
%----------------------------------------------------

%Relativgeschwindigkeit zum FE-Netz
%----------------------------------------------------
% %             v_x_rel = elem.vRel(pos.vx);
% %             v_y_rel = elem.vRel(pos.vy);
%----------------------------------------------------

% Set relative velocity for non-moving mesh:
v_x_rel = v_x;
v_y_rel = v_y;




% Initialise matrices and vectors required to build the element
% matrix:
shapeFuncDLocST = zeros( 3,  8);

matrix_jumpVV   = zeros( 4,  4);
matrix_vDivvDiv = zeros(16, 16);
matrix_pv       = zeros( 8, 16);
matrix_vDxvDx   = zeros(16, 16);

%----------------------------------------------------------------
% Initialisierungen Level-Set Gleichung
%----------------------------------------------------------------

R_LS_jump     = zeros( pos.nDofPhi/2,      1        );
K_LS_jump     = zeros( pos.nDofPhi/2, pos.nDofPhi/2 );

R_LS          = zeros( pos.nDofPhi,      1          );

K_LS_Phi      = zeros( pos.nDofPhi, pos.nDofPhi     );
K_LS_v        = zeros( pos.nDofPhi, pos.nDofV       );

R_LS_stab     = zeros( pos.nDofPhi,      1          );
K_LS_stab_Phi = zeros( pos.nDofPhi, pos.nDofPhi     );
K_LS_stab_v   = zeros( pos.nDofPhi, pos.nDofV       );

%----------------------------------------------------------------
% Initialisierungen Zusatzterme Impulsbilanz infolge Level-Set
%----------------------------------------------------------------

R_v_inst_d_phi = zeros(pos.nDofV  , pos.nDofPhi     );
R_v_conv_d_phi = zeros(pos.nDofV  , pos.nDofPhi     );
R_v_visc_d_phi = zeros(pos.nDofV  , pos.nDofPhi     );

R_stabSUPG_v_d_phi = zeros(pos.nDofV , pos.nDofPhi  );
R_stabPSPG_v_d_phi = zeros(pos.nDofP , pos.nDofPhi  );
R_stabMass_v_d_phi = zeros(pos.nDofV , pos.nDofPhi  );

%----------------------------------------------------------------
% Numerische Integration
%----------------------------------------------------------------

% Get local coordinates and weight of Gauss points:
coord_gp = [double(-1/sqrt(3)), double(1/sqrt(3))];   
weight   = [1, 1];

nn = 1; %lokale Knotennr.

% Loop over Gauss points
for jj = 1 : 2 % loop over xi_2 values of Gauss points

    % local xi_2 coordinate of current Gauss point
    xi_2 = coord_gp(1,jj); 	

    for kk = 1 : 2 % loop over xi_1 values of Gauss points

        % local xi_1 coordinate of current Gauss point
        xi_1 = coord_gp(1,kk);


        % Velocity and pressure shape functions (tri-linear in space):
        shapeFunc = 0.25*((1 + xi_1 * nodesXi1).*(1 + xi_2 * nodesXi2));

        % Get local derivatives of shape functions:
        % shapeFuncDLoc = [shapeFunc/dxi_1; shapeFunc/dxi_2]; shapeFunc/dxi_3;
        shapeFuncDXi12 = 0.25*[nodesXi1.*(1 + xi_2 * nodesXi2);
                               nodesXi2.*(1 + xi_1 * nodesXi1)];

        shapeFuncDLocST(3,:) = 0.5 * [-shapeFunc, shapeFunc];
        

        
        for ii = 1 : 2 % loop over local time axis

            % local time coordinate of current Gauss point:
            tau = coord_gp(1,ii);
            
            % Time shape functions at current Gauss point:
            t_1 = 0.5 * (1-tau);
            t_2 = 0.5 * (1+tau);
            
            shapeFuncST = [ (t_1 * shapeFunc) , (t_2 * shapeFunc) ];
            
            shapeFuncDLocST(1:2,:) = [(t_1 * shapeFuncDXi12) , (t_2 * shapeFuncDXi12)];
            
            jacobi = shapeFuncDLocST * nodesGlob;
            detJacobi = det(jacobi);
            invJacobi = inv(jacobi);
            
            % Integration factor of current Gauss point:
            intFac = weight(1,ii) * weight(1,jj) * weight(1,kk)* detJacobi;
            
            shapeFuncDxST = invJacobi(1,1:2) * shapeFuncDLocST(1:2,:);
            shapeFuncDyST = invJacobi(2,1:2) * shapeFuncDLocST(1:2,:);
            shapeFuncDtST = invJacobi(3,:)   * shapeFuncDLocST;

            shapeFuncDiv   = [shapeFuncDxST, shapeFuncDyST];
            shapeFuncDxyST = [shapeFuncDxST; shapeFuncDyST];
            %--------------------------------------------------
            
            
            % Ermittlung Materialparameter infolge Diskontinuitaet
            % ------------------------------------------------------------------
            
                       
%             if phi < 0
%                 Heaviside = 0;
%             else
%                 Heaviside = 1;
%             end
%             
%             rho = (1-Heaviside) * rhoLiquid + Heaviside * rhoGas;
%             mu  = (1-Heaviside) * muLiquid  + Heaviside * muGas;
            
%             elem.density(nn)   = rho;
%             elem.viscosity(nn) = mu;
%             nn = nn + 1;
           
            
            % Get the matrix of dynamic viscosity parameters:
            muMat = zeros(3,3);
            muMat(1:2, 1:2) = 2.0 * mu * eye(2);
            muMat(3,   3)   = 1.0 * mu * eye(1);

            %Gravitationskraft des Fluids
            f_i_time = [fx;
                        fy] * (currTimeVar(elem.loadPara.timeVar, [1, 3]) * rho);
            
            b = f_i_time * [t_1;
                            t_2];
        
            
            % ------------------------------------------------------------------
            
            
            

            

            %--------------------------------------------------
            V   = (shapeFuncST  * [v_x v_y])';

            V_T = (shapeFuncDtST * [v_x v_y])';

            V_Rel = (shapeFuncST  * [v_x_rel v_y_rel])';
            %--------------------------------------------------

            %zeitliche und raumliche Ableitung des Geschwindigkeitfeldes
            %--------------------------------------------------
            GradV = (shapeFuncDxyST * [v_x  v_y])';

            %Auswertung des Geschwindigkeitsgradienten
            GradV_V = GradV * V;

            H = [GradV(:,1) * shapeFuncST, GradV(:,2) * shapeFuncST];

            ShapeGradV = V' * shapeFuncDxyST;

            Vec = shapeFuncDtST + ShapeGradV;

            H(1, 1:8)  = H(1, 1:8) + Vec;
            H(2, 9:16) = H(2,9:16) + Vec;

            H = H * rho;

            eleMat(pos.vx,pos.v) = eleMat(pos.vx,pos.v) + shapeFuncST' * (H(1,:) * intFac);
            eleMat(pos.vy,pos.v) = eleMat(pos.vy,pos.v) + shapeFuncST' * (H(2,:) * intFac);
            %--------------------------------------------------


            %Materialmodell
            %--------------------------------------------------

            ShapeD = [shapeFuncDxST, zeros(1,8);
                      zeros(1,8),    shapeFuncDyST;
                      shapeFuncDyST, shapeFuncDxST];

            matrix_vDxvDx   = matrix_vDxvDx + ShapeD'   * (muMat   * intFac) * ShapeD;

            EE = [shapeFuncDxST * v_x;
                  shapeFuncDyST * v_y;
                  0.5*(shapeFuncDyST * v_x + shapeFuncDxST * v_y)];

            %--------------------------------------------------

            %Masseerhaltung
            %----------------------------------------------
            matrix_pv = matrix_pv + (shapeFuncST * intFac)' * shapeFuncDiv;
            %--------------------------------------------------

            %Stabilisierung
            %--------------------------------------------------
            %Stabilisierung der Impulsbilanz
            %Die Rohdichte wird an dieser Stelle schon mit eingerechnet 
            tau_m = 1/sqrt( (2/deltaT)^2 + (2*norm(V_Rel)/delta_h)^2 + (4*(mu/rho)/(delta_h^2))^2 ) * (1/rho);

            %tau_m = 0;%1e-10;

            %erster Anteil der Linearisierung
            StabMat = [H, shapeFuncDxyST];

            Vec = ShapeGradV * rho;

            StabMat2 = [ Vec,        zeros(1,8), shapeFuncDxST;
                         zeros(1,8), Vec,        shapeFuncDyST];

            eleMat(1:24,1:24) = eleMat(1:24,1:24) + StabMat2' * (StabMat * (tau_m * intFac)); 

            %zweiter Anteil der Linearisierung 
            %Auswertung des rhsduums der Impulsbilanz

            V_Tot = (V_T + GradV_V) * rho;

            P_Diff = shapeFuncDxyST * pressure;

            L = V_Tot - b + P_Diff;

            Mat = [shapeFuncDxyST * L(1) , shapeFuncDxyST * L(2)];

            Vec = shapeFuncST * (rho * tau_m * intFac);

            eleMat(pos.v,pos.vx) = eleMat(pos.v,pos.vx) + Mat(1,:)' * Vec;
            eleMat(pos.v,pos.vy) = eleMat(pos.v,pos.vy) + Mat(2,:)' * Vec;

            %Stabilisierung der Masseerhaltung
            %Die Rohdichte wird an dieser Stelle schon mit eingerechnet
            Re = (norm(V_Rel) * delta_h) / (2 * (mu / rho));
            if Re < 3
                tau_c = (delta_h * norm(V_Rel) * (Re / 3)) * rho;
            else
                tau_c = (delta_h * norm(V_Rel))            * rho;
            end
            %tau_c = 0;
            matrix_vDivvDiv = matrix_vDivvDiv + shapeFuncDiv' * (shapeFuncDiv * (tau_c * intFac));


            %--------------------------------------------------

            %Auswertung des rhsduums
            %--------------------------------------------------
            rhs(pos.vx) = rhs(pos.vx) + shapeFuncST' * ((V_Tot(1) - b(1)) * intFac);
            rhs(pos.vy) = rhs(pos.vy) + shapeFuncST' * ((V_Tot(2) - b(2)) * intFac);
            rhs(pos.v)  = rhs(pos.v) + ShapeD' * ((2.0 * mu * intFac) * EE); 

            rhs(1:24,1)         = rhs(1:24,1)          + StabMat2' * (L     * tau_m * intFac);

            %--------------------------------------------------

            
            %------------------------------------------------------------------%
            % Level-Set Gleichung (ohne Sprungterm)
            %------------------------------------------------------------------%
            
            % Ansatzfunktionen (Raum-Zeit)
            N        = shapeFuncST';
            
            % Lokal-Abgeleitete Ansatzfunktionen: 
            N_d_Xi   = shapeFuncDLocST';
            
            % Jakobi-Matrix (Raum-Zeit Gebiet)
            J_SpTi   = jacobi';
            
            % Integrationsfaktor Raum-Zeit Gebiet
            intFacQ  = intFac;
            
            % Inverse Jakobi-Matrix
            inv_J_SpTi    = inv(J_SpTi);        
            inv_J_SpTi_x  = inv_J_SpTi(:,1);
            inv_J_SpTi_y  = inv_J_SpTi(:,2);
            inv_J_SpTi_t  = inv_J_SpTi(:,3);

            % Global-Abgeleitet Ansatzfunktionen
            N_d_x   = N_d_Xi * inv_J_SpTi_x;
            N_d_y   = N_d_Xi * inv_J_SpTi_y;
            N_d_t   = N_d_Xi * inv_J_SpTi_t;
            
            % Geschwindigkeiten
            vx      = N'     * v_x;
            vx_d_x  = N_d_x' * v_x;
            vx_d_y  = N_d_y' * v_x;
            vx_d_t  = N_d_t' * v_x;
                       
            vy      = N'     * v_y;
            vy_d_x  = N_d_x' * v_y;
            vy_d_y  = N_d_y' * v_y;
            vy_d_t  = N_d_t' * v_y;
            
            vx_D_t  = vx_d_t + vx * vx_d_x + vy * vx_d_y;
            vy_D_t  = vy_d_t + vx * vy_d_x + vy * vy_d_y;
            
            % Druck
            %p       = N'     * pressure;
            p_d_x   = N_d_x' * pressure;
            p_d_y   = N_d_y' * pressure;
            
            % Level-Set Funktion            
            phi_d_x = N_d_x' * Phi;
            phi_d_y = N_d_y' * Phi;
            phi_d_t = N_d_t' * Phi;
            
            
            % DGL LevelSet -----------------------------------------------%
            
            % Instationaerer Teil                          
            L_LS_inst       = phi_d_t;
            L_LS_inst_d_Phi = N_d_t';

            % Konvektiver Teil
            L_LS_conv_x       = vx * phi_d_x;
            L_LS_conv_x_d_Phi = vx * N_d_x';
            L_LS_conv_x_d_vx  = N' * phi_d_x;

            L_LS_conv_y       = vy * phi_d_y;
            L_LS_conv_y_d_Phi = vy * N_d_y';
            L_LS_conv_y_d_vy  = N' * phi_d_y;
            
            L_LS_conv       =  L_LS_conv_x       + L_LS_conv_y;
            L_LS_conv_d_Phi =  L_LS_conv_x_d_Phi + L_LS_conv_y_d_Phi;
            L_LS_conv_d_v   = [L_LS_conv_x_d_vx  , L_LS_conv_y_d_vy];
            
            % Zusammenfassung
            L_LS       = L_LS_inst       + L_LS_conv;
            L_LS_d_vx  =                   L_LS_conv_x_d_vx;
            L_LS_d_vy  =                   L_LS_conv_y_d_vy;
            L_LS_d_v   =                   L_LS_conv_d_v;
            L_LS_d_Phi = L_LS_inst_d_Phi + L_LS_conv_d_Phi;
            
            % Residuen ---------------------------------------------------%
            
            R_LS = R_LS + N * (L_LS * intFacQ);
            
            % Linearisierungen -------------------------------------------%
            
            K_LS_Phi = K_LS_Phi + N * (L_LS_d_Phi * intFacQ);          
            K_LS_v   = K_LS_v   + N * (L_LS_d_v   * intFacQ);
                  
            
            % Stabilisierung (GLS) ---------------------------------------% 
            
            tau_LS_stab = 1/sqrt( (2/deltaT)^2 + (2*norm(V_Rel)/delta_h)^2 );
           

            W_LS_stab       = N_d_t + N_d_x * vx + N_d_y * vy;
            W_LS_stab_d_vx  =         N_d_x * N';
            W_LS_stab_d_vy  =                      N_d_y * N';           

            R_LS_stab       = R_LS_stab     + W_LS_stab * (L_LS       * tau_LS_stab * intFacQ);

            K_LS_stab_Phi   = K_LS_stab_Phi + W_LS_stab * (L_LS_d_Phi * tau_LS_stab * intFacQ);
        
            K_LS_stab_vx    = W_LS_stab_d_vx * L_LS + W_LS_stab * L_LS_d_vx;
            K_LS_stab_vy    = W_LS_stab_d_vy * L_LS + W_LS_stab * L_LS_d_vy;               
            
            K_LS_stab_v     = K_LS_stab_v + [ K_LS_stab_vx , K_LS_stab_vy ];
            

            
                
            %-------------------------------------------------------------------
            % Zusatzterme Fluidgleichung infolge LevelSet
            %-------------------------------------------------------------------
            
            if Phi_Average > 0 - smoothArea && Phi_Average < 0 + smoothArea
                         
                % Instation�re Terme
                R_vx_inst_d_rho = N * N_rho' * vx_d_t * intFacQ;
                R_vy_inst_d_rho = N * N_rho' * vy_d_t * intFacQ;
                
                R_v_inst_d_rho  = [ R_vx_inst_d_rho ; ...
                                    R_vy_inst_d_rho   ...
                                  ];
                              
                R_v_inst_d_phi  = R_v_inst_d_phi + R_v_inst_d_rho * (Heaviside_d_phi * delta_rho * intFacQ);
                
                % Konvektive Terme
                R_vx_conv_d_rho = N * N_rho' * ( vx * vx_d_x + vy * vx_d_y );
                R_vy_conv_d_rho = N * N_rho' * ( vx * vy_d_x + vy * vy_d_y );
                
                R_v_conv_d_rho  = [ R_vx_conv_d_rho ; ...
                                    R_vy_conv_d_rho   ...
                                  ];
                              
                R_v_conv_d_phi  = R_v_conv_d_phi + R_v_conv_d_rho * (Heaviside_d_phi * delta_rho * intFacQ);
                
                % Viskose Terme
                R_vx_visc_d_mu  =   N_d_x * N_mu' * (2 * vx_d_x)     ...
                                  + N_d_y * N_mu' * (vx_d_y + vy_d_x);
                R_vy_visc_d_mu  =   N_d_y * N_mu' * (2 * vy_d_y)     ...
                                  + N_d_x * N_mu' * (vx_d_y + vy_d_x);
                              
                R_v_visc_d_mu   = [ R_vx_visc_d_mu ; ...
                                    R_vy_visc_d_mu   ...
                                  ];
                              
                R_v_visc_d_phi  = R_v_visc_d_phi + R_v_visc_d_mu  * (Heaviside_d_phi * delta_mu * intFacQ);
                
                % Stabilisierung SUPG
                tau_stab_Imp = 1/sqrt( (2/deltaT)^2 + (2*norm(V_Rel)/delta_h)^2 + (4*(mu/rho)/(delta_h^2))^2 );
                
                R_stabSUPG_vx_d_rho = (N_d_x * vx + N_d_y * vy) * N_rho' * vx_D_t;
                R_stabSUPG_vy_d_rho = (N_d_x * vx + N_d_y * vy) * N_rho' * vy_D_t;
                
                R_stabSUPG_v_d_rho  = [ R_stabSUPG_vx_d_rho ; ...
                                        R_stabSUPG_vy_d_rho   ...
                                      ];
                                  
                R_stabSUPG_v_d_phi  = R_stabSUPG_v_d_phi + R_stabSUPG_v_d_rho * (Heaviside_d_phi * delta_rho * tau_stab_Imp * intFacQ);
                
                %Stabilisierung PSPG
                R_stabPSPG_vx_d_rho = N_d_x * N_rho' * (-1 * rho^(-2)) * (p_d_x - b(1));
                R_stabPSPG_vy_d_rho = N_d_y * N_rho' * (-1 * rho^(-2)) * (p_d_y - b(2));
                
                R_stabPSPG_v_d_rho  = R_stabPSPG_vx_d_rho + R_stabPSPG_vy_d_rho;
                                  
                R_stabPSPG_v_d_phi  = R_stabPSPG_v_d_phi + R_stabPSPG_v_d_rho * (Heaviside_d_phi * delta_rho * tau_stab_Imp * intFacQ);
                
                % Stabilisierung Massenerhaltung (GLS)
                tau_stab_Cont = tau_c/rho;
                
                R_stabMass_vx_d_rho = N_d_x * N_rho' * (vx_d_x + vy_d_y);
                R_stabMass_vy_d_rho = N_d_y * N_rho' * (vx_d_x + vy_d_y);
                
                R_stabMass_v_d_rho  = [ R_stabMass_vx_d_rho ; ...
                                        R_stabMass_vy_d_rho   ...
                                      ];
                                  
                R_stabMass_v_d_phi  = R_stabMass_v_d_phi + R_stabMass_v_d_rho * (Heaviside_d_phi * delta_rho * tau_stab_Cont * intFacQ);
                
            end %if  

        end %tau

        % Uebergangsbedingungen
        jacobit0        = shapeFuncDXi12 * nodesGlob(1:4,1:2);

        intFac          = weight(1,jj) * weight(1,kk) * det(jacobit0);

        %matrix_jumpVV        = matrix_jumpVV + shapeFunc' * (shapeFunc * (rho     * intFac));
        matrix_jumpVV   = matrix_jumpVV + shapeFunc' * (shapeFunc * (rhoPrev * intFac));
        
        %----------------------------------------------
        % Sprungterm Level-Set Gleichung
        %----------------------------------------------
        
        % Raeuml. Ansatzfunktionen
        N_Sp = shapeFunc';
       
        % Jakobi-Matrix (Raum-Gebiet am Zeitscheibenanfang)
        intFacOmega = intFac;
        
        % Sprungterme in eleVec
        R_LS_jump = R_LS_jump + N_Sp * ((N_Sp' * (Phi_t0 - Phi_prev)) * intFacOmega);
        
        % Sprungterme in eleMat
        K_LS_jump = K_LS_jump + N_Sp * ( N_Sp' * intFacOmega);

    end %xi_1
end %xi_2


% Add contributions to eleMat:
%--------------------------------------------------
eleMat(pos.p, pos.v) = eleMat(pos.p, pos.v) + matrix_pv;
eleMat(pos.v, pos.p) = eleMat(pos.v, pos.p) - matrix_pv';

eleMat(pos.v, pos.v) = eleMat(pos.v, pos.v) + matrix_vDivvDiv;
eleMat(pos.v, pos.v) = eleMat(pos.v, pos.v) + matrix_vDxvDx;

% Add contributions to rhs:
%--------------------------------------------------
rhs(pos.v,1) = rhs(pos.v,1) + matrix_vDivvDiv * velo;
rhs(pos.p,1) = rhs(pos.p,1) + matrix_pv * velo;
rhs(pos.v,1) = rhs(pos.v,1) - matrix_pv' * pressure;
%             rhs(pos.v,1) = rhs(pos.v,1) + matrix_vDxvDx * velo;

% Add contributions of jump terms to eleMat:
%--------------------------------------------------
eleMat(pos.vxt0,pos.vxt0) = eleMat(pos.vxt0,pos.vxt0) + matrix_jumpVV;
eleMat(pos.vyt0,pos.vyt0) = eleMat(pos.vyt0,pos.vyt0) + matrix_jumpVV;

% Get jump at start of current time slab:
%--------------------------------------------------
jumpDeltaVx  = velo(pos.vxt0) - velo_dis(1:4);
jumpDeltaVy  = velo(pos.vyt0) - velo_dis(5:8);
%             jumpDeltaVx  = velo(pos.vxt0) - velo_dis(pos.vxt0);%velo_dis(pos.vxt1);
%             jumpDeltaVy  = velo(pos.vyt0) - velo_dis(pos.vyt0);%velo_dis(pos.vyt1);


% Add contributions of jump terms to rhs:
%--------------------------------------------------
rhs(pos.vxt0) = rhs(pos.vxt0) + matrix_jumpVV * jumpDeltaVx;
rhs(pos.vyt0) = rhs(pos.vyt0) + matrix_jumpVV * jumpDeltaVy;

% rhs(pos.vxt0) = rhs(pos.vxt0) + matrix_jumpVV_prev * jumpDeltaVx;
% rhs(pos.vyt0) = rhs(pos.vyt0) + matrix_jumpVV_prev * jumpDeltaVy;


%----------------------------------------------------------------
% Abspeichern Teilmatrizen infolge Level-Set
%----------------------------------------------------------------

% Element-Vektor (Residuum)
%-----------------------------------
rhs(pos.phi)    = rhs(pos.phi)    + R_LS;
rhs(pos.phi)    = rhs(pos.phi)    + R_LS_stab;
rhs(pos.phi_t0) = rhs(pos.phi_t0) + R_LS_jump;


% Element-Matrix (Tangenten-Matrix)
%-----------------------------------
eleMat(pos.phi   ,pos.v     ) = eleMat(pos.phi   ,pos.v     ) + K_LS_v;
eleMat(pos.phi   ,pos.phi   ) = eleMat(pos.phi   ,pos.phi   ) + K_LS_Phi;
eleMat(pos.phi_t0,pos.phi_t0) = eleMat(pos.phi_t0,pos.phi_t0) + K_LS_jump;

eleMat(pos.phi   ,pos.v    )  = eleMat(pos.phi   ,pos.v    )  + K_LS_stab_v;
eleMat(pos.phi   ,pos.phi  )  = eleMat(pos.phi   ,pos.phi  )  + K_LS_stab_Phi;


%----------------------------------------------------------------
% Abspeichern Teilmatrizen Impulsbilanz infolge Level-Set
%----------------------------------------------------------------

% Element-Matrix (Tangenten-Matrix)
%-----------------------------------
eleMat(pos.v   ,pos.phi    )  = eleMat(pos.v   ,pos.phi    )  + R_v_inst_d_phi;
eleMat(pos.v   ,pos.phi    )  = eleMat(pos.v   ,pos.phi    )  + R_v_conv_d_phi;
eleMat(pos.v   ,pos.phi    )  = eleMat(pos.v   ,pos.phi    )  + R_v_visc_d_phi;

eleMat(pos.v   , pos.phi   )  = eleMat(pos.v   ,pos.phi    )  + R_stabSUPG_v_d_phi;
eleMat(pos.p   , pos.phi   )  = eleMat(pos.p   ,pos.phi    )  + R_stabPSPG_v_d_phi;
eleMat(pos.v   , pos.phi   )  = eleMat(pos.v   ,pos.phi    )  + R_stabMass_v_d_phi;


% Set Dirichlet BC:
%-----------------------------
if ~isempty(elem.bcDir)
    activeBcDir = find(elem.bcDir(:,6) == 1); % Find active bcDir in current element
    posLocBcDir = elem.bcDir(activeBcDir,7);  % Get local position of active bcDir

    eleMat(posLocBcDir, :)          = 0.0;
    eleMat(posLocBcDir,posLocBcDir) = eye(length(activeBcDir));
end


% Change sign of rhs:
%----------------------
eleVec = -rhs;


% Store the element matrix and the indices in vectors:
%--------------------------------------------------------------

% Get the local indices and the values of the nonzero parts of
% eleMatHyb:
[I_Loc, J_Loc, X_elem] = find(eleMat);

% Transfer the local indices to global ones:
index       = elem.indexSystem;
I_elem      = index(I_Loc);
J_elem      = index(J_Loc);

end
      
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                
function [ergFluid, sizeErg] = PostOut(elem, eleSolu, ~)
% Auslesen der Loesungswerte des aktuellen Elementes aus dem
% Gesamtloesungsvektor.


% Get indices to access solution and element properties:
pos           = elem.ind;

% Number of nodes for which output is returned:
sizeErg       = pos.nNodesOut*2;

% Get unknowns from the solution:
%-----------------------------------
velo          = eleSolu(pos.v);
pressure      = eleSolu(pos.p);
Phi           = eleSolu(pos.phi);
rho           = elem.density;
mu            = elem.viscosity;

% Initialisation:
%---------------------
ergFluid      = zeros(sizeErg,7);

% Get velocities and pressure:
%------------------------------------
ergFluid(1:4,1) = velo(pos.vxt0);  % v_x_t0         
ergFluid(5:8,1) = velo(pos.vxt1);  % v_x_t1

ergFluid(1:4,2) = velo(pos.vyt0);  % v_y_t0
ergFluid(5:8,2) = velo(pos.vyt1);  % v_y_t1

ergFluid(1:4,4) = pressure(pos.pt0Sol); % p_t0
ergFluid(5:8,4) = pressure(pos.pt1Sol); % p_t1

ergFluid(1:4,5) = Phi(pos.phi_t0_Sol); % phi_t0
ergFluid(5:8,5) = Phi(pos.phi_t1_Sol); % phi_t1

ergFluid(5:8,6) = [rho(2), rho(4), rho(8), rho(6)];
ergFluid(5:8,7) = [mu(2) , mu(4) , mu(8) , mu(6) ];

end
        
        
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
function [solUpdate , index] = UpdateSol( elem          , ...
                                          eleSolu       , ...
                                          deltaTCurr    , ...
                                          deltaTNext    , ...
                                          typeEleUpdate   ...
                                        )
            
% Ermittlung der Startwerte fuer naechste NR-Iteration
% Funktion wird in 'UpdateSolution' aufgerufen
            
% deltaTCurr: time width of current time  slab
% deltaTNext: time width of next time slab
% typeEleUpdate: 1:    use gradient of current time slab to estimate end values
%                      of next time slab which serve as initial values for next
%                      NR iteration
%                else: gradient is not used, initial values for
%                      start and end of next time slab are the same
            
% Get indices:
pos = elem.ind;

% Get unknowns from eleSolu:
velo        = eleSolu(pos.v);
pressure    = eleSolu(pos.p);
Phi         = eleSolu(pos.phi);

% Update unknowns for next time slab:
if typeEleUpdate == 1
    deltaVeloCurr          = velo(pos.vt1) - velo(pos.vt0);
    velo(pos.vt0)          = velo(pos.vt1); 
    velo(pos.vt1)          = velo(pos.vt0) + deltaVeloCurr * (deltaTNext / deltaTCurr);

    deltaPressureCurr      = pressure(pos.pt1Sol) - pressure(pos.pt0Sol);
    pressure(pos.pt0Sol)   = pressure(pos.pt1Sol); 
    pressure(pos.pt1Sol)   = pressure(pos.pt0Sol) + deltaPressureCurr * (deltaTNext / deltaTCurr);
    
    deltaPhiCurr        = Phi(pos.phi_t1_Sol) - Phi(pos.phi_t0_Sol);
    Phi(pos.phi_t0_Sol) = Phi(pos.phi_t1_Sol); 
    Phi(pos.phi_t1_Sol) = Phi(pos.phi_t0_Sol) + deltaPhiCurr * (deltaTNext / deltaTCurr);

else
    velo(pos.vt0)          = velo(pos.vt1);
    pressure(pos.pt0Sol)   = pressure(pos.pt1Sol);
    Phi(pos.phi_t0_Sol)    = Phi(pos.phi_t1_Sol);
end


% Initialisation of updated solution:
solUpdate = zeros(pos.nDofEle, 1);

% Set values:
solUpdate(pos.v)    = velo;
solUpdate(pos.p)    = pressure;
solUpdate(pos.phi)  = Phi;

index               = elem.indexSystem;     
end
  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function Update(elem, ~, deltaTCurr, deltaTNext, typeEleUpdate)
% Ermittlung der Startwerte fuer naechste NR-Iteration der Variablen der
% Nachlaufrechnung
    
% Update der Nachlaufgroessen muss nur bei gemischter Formulierung erfolgen.

% deltaTCurr: time width of current time  slab
% deltaTNext: time width of next time slab
% typeEleUpdate: 1: use gradient of current time slab to
%                   estimate start and end values of next time
%                   slab (initial values for first iteration)
%                else: gradient is not used, initial values for
%                   start and end of next time slab are thesame)

% Get indices:
pos = elem.ind;

% Set discontinious values:
elem.density_dis = elem.density;

elem.vRel_dis     = elem.vRel;

if typeEleUpdate == 1
    deltaVRelCurr               = elem.vRel(pos.vt1) - elem.vRel(pos.vt0);
    elem.vRel(pos.vt0)          = elem.vRel(pos.vt1); 
    elem.vRel(pos.vt1)          = elem.vRel(pos.vt0) + deltaVRelCurr * (deltaTNext / deltaTCurr);
else
    elem.vRel(pos.vt0)          = elem.vRel(pos.vt1);
end

end
        
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   
        
function Restore(elem, ~, deltaTCurr, deltaTNext, typeEleUpdate)
% Funktion wird von 'RestoreElem' aufgerufen im Fall, dass im aktuellen
% Loesungsschritt keine Konvergenz stattgefunden hat und die Zeitschritte
% reduziert werden muss.
% F�r die Zeitscheibe mit nun reduziertem Zeitschritt werden die Startwerte
% der Nachlaufvariablen f�r den ersten NR-Schritt ermittelt.

% Set indices:
pos = elem.ind;

% Restore the element data:
if typeEleUpdate == 1
    % Use gradient of previous time slab for initial values of
    % the current time slab:
    deltaVRelPrev       = elem.vRel_dis(pos.vt1) - elem.vRel_dis(pos.vt0);
    elem.vRel(pos.vt0)  = elem.vRel_dis(pos.vt1); 
    elem.vRel(pos.vt1)  = elem.vRel(pos.vt0) + deltaVRelPrev * (deltaTNext / deltaTCurr);

else
    % Initial values for start and end of time slab are the
    % same:
    elem.vRel(pos.vt0)  = elem.vRel_dis(pos.vt1);
    elem.vRel(pos.vt1)  = elem.vRel(pos.vt0);
end

end   
        
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%     
        
function SetEleDofGlob(elem)
% Funktion stellt den Zusammenhang zwischen lokalen und globalen FHG her.
% Es wird ein Vektor erzeugt, der jedem lokalen FHG den korrespondierenden
% globalen FHG zuweist.
% Die Anzahl an Spalten entspricht der Anzahl an vorhandenen lokalen FHGen.
%
% Spaltennr.: local dof id
% Wert:       global dof id

    
elem.eleDofGlob    = [ 1, 21, 2, 22, 4, 24, 6, 26 ];
% Aufgrund des gew�hlten Ansatzes (s. Build-Routine)
% ist die Reihenfolge der globalen FHG hier zwingend.

        
    
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
end % end of methods
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
methods (Static)
        
function Post(~, ~, ~)
    % Method setting element data from the solution:

    % -- no secondary unknowns stored for this element type --
end
        
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        
function eleNodesDofLoc = GetEleNodesDofLoc
    % returns a matrix containing the local node numbers in the
    % first col and the local dof ids in the second col:

    % loc dof: 1: vxt0   , 2: vxt1   ,
    %          3: vyt0   , 4: vyt1   ,
    %          5: pt0    , 6: pt1    ,
    %          7: phi_t0 , 8: phi_t1
    
    eleNodesDofLoc= [1, 1;
                     2, 1;
                     3, 1;
                     4, 1;
                     1, 2;
                     2, 2;
                     3, 2;
                     4, 2;
                     1, 3;
                     2, 3;
                     3, 3;
                     4, 3;
                     1, 4;
                     2, 4;
                     3, 4;
                     4, 4;
                     1, 5;
                     2, 5;
                     3, 5;
                     4, 5;
                     1, 6;
                     2, 6;
                     3, 6;
                     4, 6;
                     1, 7;
                     2, 7;
                     3, 7;
                     4, 7;
                     1, 8;
                     2, 8;
                     3, 8;
                     4, 8;];

end
  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

end % end of static methods
end % end of classdefinition