classdef FluidNavierStokes4203 < FluidElemSuperclass
    
    % Description:
    % Erstes eigenes 2D-Fluidelement,
    % 4 Knoten, rechteckig,
    % trilineare Geschwindigkeits-Druck Formulierung,
    % separate SUPG/PSPG-Stabilisierung d. Impulsbilant,
    % GLS-Stabilisierung d. Massenerhaltung
    
    
    
    properties (SetAccess = protected)
        
        % General properties:
        %-----------------------------------------------------------------
        type        = 4203;          % identification number of element type
        name        = 'FluidNavierStokes4203';  % name of current element type
        sizeXest    = 24*24;        % estimated number of non-zero entries in eleMat
        probDim     = 2;            % problem dimension
        paraOutput  = 1;            % create Paraview output for current element type (1) or not (0)
        
        
        % Indices to access the element matrix:
        %----------------------------------------------------------------
        
        % Description:
        % Sets the number of dofs used for initialization of the element
        % attributes and the positions of the dof in the element matrix
        % and the element attribute to access them in the proper
        % positons:
        % Note: Since the velocity dof are the first element dof for
        % this type, positions of velocity dof are the same in velo and
        % in eleMat and thus not given seperately. For the other 
        % unknown fields positions are not the same, compare e.g. 
        % ind.pt0 and ind.pt0Sol
        
        % Initialise values in property section to ensure, that 'ind' is
        % created only once for all elements of this type:
        ind = struct('nNodesOut', 4,...         % Number of nodes for output
                     'v'        , (1:16)',...   % pos of all velocity dof in eleMat
                     'p'        , (17:24)',...  % pos of all pressure dof
                     'vx'       , (1:8)',...    % pos of x-velocity in eleMat
                     'vy'       , (9:16)',...   % pos of y-velocity in eleMat
                     'vxt0'     , (1:4)',...    % pos of vx_t0 in eleMat and velo
                     'vyt0'     , (9:12)',...   % pos of vy_t0 in eleMat and velo
                     'vt0'      , [1:4, 9:12]',... % pos of v_t0 in eleMat and velo
                     'vxt1'     , (5:8)',...    % pos of vx_t1 in eleMat and velo
                     'vyt1'     , (13:16)',...  % pos of vy_t1 in eleMat and velo
                     'vt1'      , [5:8, 13:16]',... % pos of v_t1 in eleMat and velo
                     'pt0'      , (17:20)',...  % pos of p_t0 in eleMat
                     'pt1'      , (21:24)',...  % pos of p_t1 in eleMat
                     'pt0Sol'   , (1:4)',...    % pos of p_t0 in pressure
                     'pt1Sol'   , (5:8)',...    % pos of p_t1 in pressure
                     'nDofV'    , 16,...        % total number of velo dof in current element
                     'nDofP'    , 8,...         % total number of pressure dof in current element
                     'nDofEle'  , 24)           % total number of dof for the current element
        

        % further properties are included in superclasses
    end
    
        
    %------------------------------------------------------------------
    
    %------------------------------------------------------------------
    
    
    methods
        
        function elem = FluidNavierStokes4203( eleConnect    , eleCoord      , ...
                                               eleMatPara    , eleLoadPara   , ...
                                               eleBcDir      , eleBcNeu      , ...
                                               mm                              ...
                                             )

                            
            %Call the superclass constructor:
            elem = elem@FluidElemSuperclass( eleConnect    , eleCoord      , ...
                                             eleMatPara    , eleLoadPara   , ...
                                             eleBcDir      , eleBcNeu      , ...
                                             mm                              ...
                                           );

            
            % Initialise element data for dof and related data:
            %--------------------------------------------------
           	elem.vRel           = zeros(elem.ind.nDofV,1);
           	elem.vRel_dis       = zeros(elem.ind.nDofV,1);
        end
        
        
        %------------------------------------------------------------------
        %------------------------------------------------------------------
        
        
        function[ eleVec,   ...
          I_elem,   ...
          J_elem,   ...
          X_elem      ] = Build( elem       , ...
                                 eleSolu    , ...
                                 eleSoluDis , ...
                                 eleCoords  , ...
                                 time       , ...
                                 currTimeVar   )
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Funktionsbeschreibung:
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Eingehende Variablen:
%
% eleVec:
% index:
% I_elem:
% J_elem:
% X_elem:
% sizeXElem:
%
%
% Ausgehende Variablen:
%
% elem:
% eleSolu:
% eleSoluDis:
% eleCoords:
% eleMatPara:
% time:
% currTimeVar:
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Auslesen d. Eingabdaten
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Element-Positionsindex
pos = elem.ind;

% Zeit
t0 = time.curr(1,1);
t1 = time.curr(1,3);


% Materialdaten
rho = elem.matPara.rho; % Rohdichte
mu  = elem.matPara.mu;  % dynamische Viskosit�t

% Volumenlasten
fx  = elem.loadPara.fx;
fy  = elem.loadPara.fy;
f   = [ fx ; ...
        fy   ...
      ];
f_t = f * currTimeVar(elem.loadPara.timeVar, [1,3]); % -> Einf�hren von eleLoadPara.timeVar !

% Geschw. vorherige Zeitscheibe
v_prev  = eleSoluDis;     % Geschw. der letzten Zeitscheibe am Zeitscheiben-Ende
vx_prev = v_prev(pos.vxt1);
vy_prev = v_prev(pos.vyt1);

% L�sungen vorhergehender Iterationsschritt
v     = eleSolu(pos.v); % Geschw. dieser Zeitscheibe aus letztem NR-Iterationsschritt
p     = eleSolu(pos.p); % Druck dieser Zeitscheibe aus letztem NR-Iterationsschritt

vx    = v(pos.vx);
vy    = v(pos.vy);

vx_t0 = v(pos.vxt0);
vy_t0 = v(pos.vyt0);


% Koordinaten (Raum-Zeit, global)
% [ x ]   [ xA xB xC xD | xE xF xG xH ]
% [ y ] = [ yA yB yC yD | yE yF yG yH ]
% [ t ]   [ t0 t0 t0 t0 | t1 t1 t1 t1 ]

X = [ eleCoords.t0(:,1:2)', eleCoords.t1(:,1:2)'; ...
      t0 t0 t0 t0         , t1 t1 t1 t1           ...
    ];


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Lokal-abgeleitete r�uml. Ansatzfunktionen (bei xi1 = xi2 = 0)
% [ dN/dxi1 | dN/dxi2 ]
N_Sp_d_Xi0  = 1/4 * [ (-1)*(1), (1)*(-1); ...
                      (+1)*(1), (1)*(-1); ...
                      (+1)*(1), (1)*(+1); ... 
                      (-1)*(1), (1)*(+1)  ...
                    ];

J_Sp_Xi0     = X(1:2,1:4) * N_Sp_d_Xi0;
det_J_Sp_Xi0 = det(J_Sp_Xi0);

Vol_el       = 4 * det_J_Sp_Xi0;
h_el         = sqrt( 4*Vol_el / pi );
                                                

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Initialisieren der Teil-Matrizen
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

eleMat = zeros( pos.nDofEle, pos.nDofEle ); % Element-Matrix (Tangentenmatrix)
eleVec = zeros( pos.nDofEle, 1           ); % Element-Vektor

K_inst     = zeros( pos.nDofV, pos.nDofV );
K_conv_nl  = zeros( pos.nDofV, pos.nDofV );
K_visc     = zeros( pos.nDofV, pos.nDofV );

K_stab_mass = zeros( pos.nDofV, pos.nDofV);

R_conv_vx_d_vx = zeros( pos.nDofV/2, pos.nDofV/2);
R_conv_vx_d_vy = zeros( pos.nDofV/2, pos.nDofV/2);
R_conv_vy_d_vx = zeros( pos.nDofV/2, pos.nDofV/2);
R_conv_vy_d_vy = zeros( pos.nDofV/2, pos.nDofV/2);

K_mass     = zeros( pos.nDofP, pos.nDofV );

K_p              = zeros( pos.nDofV, pos.nDofP );

R_Mom_x            = zeros(pos.nDofV/2, 1);
R_Mom_y            = zeros(pos.nDofV/2, 1);
R_Mass             = zeros(pos.nDofP  , 1);

R_Mom_x_d_vx       = zeros(pos.nDofV/2, pos.nDofV/2);
R_Mom_x_d_vy       = zeros(pos.nDofV/2, pos.nDofV/2);
R_Mom_x_d_p        = zeros(pos.nDofV/2, pos.nDofV/2);

R_Mom_y_d_vx       = zeros(pos.nDofV/2, pos.nDofV/2);
R_Mom_y_d_vy       = zeros(pos.nDofV/2, pos.nDofV/2);
R_Mom_y_d_p        = zeros(pos.nDofV/2, pos.nDofV/2);

R_Mass_d_vx        = zeros(pos.nDofV/2, pos.nDofV/2);
R_Mass_d_vy        = zeros(pos.nDofV/2, pos.nDofV/2);
R_Mass_d_p         = zeros(pos.nDofV/2, pos.nDofV/2);

R_SUPG             = zeros(pos.nDofV, 1);
R_SUPG_Impx_d_vx   = zeros(pos.nDofV/2, pos.nDofV/2);
R_SUPG_Impx_d_vy   = zeros(pos.nDofV/2, pos.nDofV/2);
R_SUPG_Impx_d_p    = zeros(pos.nDofV/2, pos.nDofP);
R_SUPG_Impy_d_vx   = zeros(pos.nDofV/2, pos.nDofV/2);
R_SUPG_Impy_d_vy   = zeros(pos.nDofV/2, pos.nDofV/2);
R_SUPG_Impy_d_p    = zeros(pos.nDofV/2, pos.nDofP);
% K_SUPG_inst      = zeros( pos.nDofV, pos.nDofV );
% K_SUPG_conv      = zeros( pos.nDofV, pos.nDofV );
% K_SUPG_p         = zeros( pos.nDofV, pos.nDofP );

% R_SUPG_inst_vx_d_vx = zeros( pos.nDofV/2, pos.nDofV/2 );
% R_SUPG_inst_vx_d_vy = zeros( pos.nDofV/2, pos.nDofV/2 );
% R_SUPG_inst_vy_d_vx = zeros( pos.nDofV/2, pos.nDofV/2 );
% R_SUPG_inst_vy_d_vy = zeros( pos.nDofV/2, pos.nDofV/2 );
% 
% R_SUPG_conv_vx_d_vx = zeros( pos.nDofV/2, pos.nDofV/2 );
% R_SUPG_conv_vx_d_vy = zeros( pos.nDofV/2, pos.nDofV/2 );
% R_SUPG_conv_vy_d_vx = zeros( pos.nDofV/2, pos.nDofV/2 );
% R_SUPG_conv_vy_d_vy = zeros( pos.nDofV/2, pos.nDofV/2 );
% 
% R_SUPG_p_vx_d_vx    = zeros( pos.nDofV/2, pos.nDofV/2 );
% R_SUPG_p_vx_d_vy    = zeros( pos.nDofV/2, pos.nDofV/2 );
% R_SUPG_p_vx_d_p     = zeros( pos.nDofV/2, pos.nDofV/2 );
% R_SUPG_p_vy_d_vx    = zeros( pos.nDofV/2, pos.nDofV/2 );
% R_SUPG_p_vy_d_vy    = zeros( pos.nDofV/2, pos.nDofV/2 );
% R_SUPG_p_vy_d_p     = zeros( pos.nDofV/2, pos.nDofV/2 );

R_PSPG              = zeros( pos.nDofP, 1);
R_PSPG_d_vx         = zeros( pos.nDofP, pos.nDofV/2);
R_PSPG_d_vy         = zeros( pos.nDofP, pos.nDofV/2);
R_PSPG_d_p          = zeros( pos.nDofP, pos.nDofP);

% K_PSPG_inst      = zeros( pos.nDofP, pos.nDofV);
% K_PSPG_conv_nl   = zeros( pos.nDofP, pos.nDofV);
% K_PSPG_conv_lin  = zeros( pos.nDofP, pos.nDofV);
% 
% R_PSPG_conv_d_vx = zeros(pos.nDofP, pos.nDofV/2);
% R_PSPG_conv_d_vy = zeros(pos.nDofP, pos.nDofV/2);
% 
% K_PSPG_p        = zeros( pos.nDofP, pos.nDofP);


K_jump_vx  = zeros( pos.nDofV/(2*2), pos.nDofV/(2*2) );
K_jump_vy  = zeros( pos.nDofV/(2*2), pos.nDofV/(2*2) );


r_f        = zeros( pos.nDofV, 1);
r_t        = zeros( pos.nDofV, 1);

r_jump_vx  = zeros( pos.nDofV/(2*2), 1);
r_jump_vy  = zeros( pos.nDofV/(2*2), 1);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Numerische Integration
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Gau�punkte
gp_coord  = [ -1/sqrt(3) , 1/sqrt(3) ];
gp_weight = [     1      ,    1      ];


% Integrationsschleifen
for kk = 1:2 % xi1: lokaler Raum
    xi1 = gp_coord(kk);
    
    for ll = 1:2 % xi2: lokaler Raum
        xi2 = gp_coord(ll);
        
        
        % R�uml. Ansatzfunktionen
        N_Sp        = 1/4 * [ (1-xi1)*(1-xi2); ...
                              (1+xi1)*(1-xi2); ...
                              (1+xi1)*(1+xi2); ... 
                              (1-xi1)*(1+xi2)  ...
                            ];
        
        % Lokal-abgeleitete r�uml. Ansatzfunktionen
        % [ dN/dxi1 | dN/dxi2 ]
        N_Sp_d_Xi   = 1/4 * [ (-1)*(1-xi2), (1-xi1)*(-1); ...
                              (+1)*(1-xi2), (1+xi1)*(-1); ...
                              (+1)*(1+xi2), (1+xi1)*(+1); ... 
                              (-1)*(1+xi2), (1-xi1)*(+1)  ...
                            ];
        
                        
        % Jakobi-Matrix (Raum-Gebiet am Zeitscheibenanfang)
        J_Sp        = X(1:2,1:4) * N_Sp_d_Xi;
        det_J_Sp    = det(J_Sp);
        
        % Integrationsfaktor Raumgebiet Zeitscheibenanfang
        intFacOmega = gp_weight(kk) * gp_weight(ll) * det_J_Sp;
        
        
        % Sprungterme in eleMat
        K_jump_vx   = K_jump_vx + rho * (N_Sp * N_Sp') * intFacOmega;
        K_jump_vy   = K_jump_vx;
        
        %Sprungterme in eleVec
        r_jump_vx   = r_jump_vx + K_jump_vx * ( vx_t0 - vx_prev );
        r_jump_vy   = r_jump_vy + K_jump_vy * ( vy_t0 - vy_prev );
        
        
        for nn = 1:2 % tau: lokale Zeit
            tau = gp_coord(nn);
            
% Ansatzfunktionen
N       = 1/8 * [ (1-xi1)*(1-xi2)*(1-tau); ...
                  (1+xi1)*(1-xi2)*(1-tau); ...
                  (1+xi1)*(1+xi2)*(1-tau); ... 
                  (1-xi1)*(1+xi2)*(1-tau); ...
                  (1-xi1)*(1-xi2)*(1+tau); ...
                  (1+xi1)*(1-xi2)*(1+tau); ...
                  (1+xi1)*(1+xi2)*(1+tau); ...
                  (1-xi1)*(1+xi2)*(1+tau)  ...
                ];
% Lokal-Abgeleitete Ansatzfunktionen: [ dN/dxi1 | dN/dxi2 | dN/dtau ]
N_d_Xi  = 1/8 * [ (-1)*(1-xi2)*(1-tau), (1-xi1)*(-1)*(1-tau), (1-xi1)*(1-xi2)*(-1); ...
                  (+1)*(1-xi2)*(1-tau), (1+xi1)*(-1)*(1-tau), (1+xi1)*(1-xi2)*(-1); ...
                  (+1)*(1+xi2)*(1-tau), (1+xi1)*(+1)*(1-tau), (1+xi1)*(1+xi2)*(-1); ... 
                  (-1)*(1+xi2)*(1-tau), (1-xi1)*(+1)*(1-tau), (1-xi1)*(1+xi2)*(-1); ...
                  (-1)*(1-xi2)*(1+tau), (1-xi1)*(-1)*(1+tau), (1-xi1)*(1-xi2)*(+1); ...
                  (+1)*(1-xi2)*(1+tau), (1+xi1)*(-1)*(1+tau), (1+xi1)*(1-xi2)*(+1); ...
                  (+1)*(1+xi2)*(1+tau), (1+xi1)*(+1)*(1+tau), (1+xi1)*(1+xi2)*(+1); ...
                  (-1)*(1+xi2)*(1+tau), (1-xi1)*(+1)*(1+tau), (1-xi1)*(1+xi2)*(+1)  ...
               ];
            
%             % Ansatzfunktionen
%             N       = 1/8 * [ (1+xi1)*(1+xi2)*(1-tau); ...
%                               (1-xi1)*(1+xi2)*(1-tau); ...
%                               (1-xi1)*(1-xi2)*(1-tau); ... 
%                               (1+xi1)*(1-xi2)*(1-tau); ...
%                               (1+xi1)*(1+xi2)*(1+tau); ...
%                               (1-xi1)*(1+xi2)*(1+tau); ...
%                               (1-xi1)*(1-xi2)*(1+tau); ...
%                               (1+xi1)*(1-xi2)*(1+tau)  ...
%                             ];
%                         
%             % Lokal-Abgeleitete Ansatzfunktionen: [ dN/dxi1 | dN/dxi2 | dN/dtau ]
%             N_d_Xi  = 1/8 * [ (+1)*(1+xi2)*(1-tau), (1+xi1)*(+1)*(1-tau), (1+xi1)*(1+xi2)*(-1); ...
%                               (-1)*(1+xi2)*(1-tau), (1-xi1)*(+1)*(1-tau), (1-xi1)*(1+xi2)*(-1); ...
%                               (-1)*(1-xi2)*(1-tau), (1-xi1)*(-1)*(1-tau), (1-xi1)*(1-xi2)*(-1); ... 
%                               (+1)*(1-xi2)*(1-tau), (1+xi1)*(-1)*(1-tau), (1+xi1)*(1-xi2)*(-1); ...
%                               (+1)*(1+xi2)*(1+tau), (1+xi1)*(+1)*(1+tau), (1+xi1)*(1+xi2)*(+1); ...
%                               (-1)*(1+xi2)*(1+tau), (1-xi1)*(+1)*(1+tau), (1-xi1)*(1+xi2)*(+1); ...
%                               (-1)*(1-xi2)*(1+tau), (1-xi1)*(-1)*(1+tau), (1-xi1)*(1-xi2)*(+1); ...
%                               (+1)*(1-xi2)*(1+tau), (1+xi1)*(-1)*(1+tau), (1+xi1)*(1-xi2)*(+1)  ...
%                       ];
            
                  
            % Jakobi-Matrix (Raum-Zeit Gebiet)
            J_SpTi        = X * N_d_Xi;
            det_J_SpTi    = det(J_SpTi);
            
            % Integrationsfaktor Raum-Zeit Gebiet
            intFacQ  = gp_weight(kk) * gp_weight(ll) * gp_weight(nn) * det_J_SpTi;
            
            % Inverse Jakobi-Matrix
            inv_J_SpTi    = inv(J_SpTi);        
            inv_J_SpTi_x  = inv_J_SpTi(:,1);
            inv_J_SpTi_y  = inv_J_SpTi(:,2);
            inv_J_SpTi_t  = inv_J_SpTi(:,3);
            
            % Global-Abgeleitet Ansatzfunktionen
            N_d_x   = N_d_Xi * inv_J_SpTi_x;
            N_d_y   = N_d_Xi * inv_J_SpTi_y;
            N_d_t   = N_d_Xi * inv_J_SpTi_t;
                  
            % Hilfsmatrizen
            H       = [     N         , zeros(size(N)); ...
                        zeros(size(N)),     N           ...
                      ];
                  
            N_d_xy  = [ N_d_x ; ...
                        N_d_y ; ...
                      ];
                  
            N_d_X   = [ N_d_x , N_d_y ]; 
            
            
            
            % Volumenlasten
            f_gp   = f_t * [ 1/2 * (1-tau) ; ...
                             1/2 * (1+tau)   ...
                           ];                      
                       
            r_f    = r_f    + H * f_gp *intFacQ;
            
            
%           % Randspannungen
%           r_t    = r_t    + ? * intFacP;
            
            % Residuen
            R_Mom_x =   R_Mom_x                                               ...
                      + (   N * (   rho * (N_d_t' * vx)                       ...
                                  + rho * (N_d_x' * vx) * (N' * vx)           ...
                                  + rho * (N_d_y' * vx) * (N' * vy)           ...
                                  - f_gp(1)                                   ...
                                )                                             ...
                          + N_d_x *  2*mu *   (N_d_x' * vx)                   ...
                          + N_d_y *    mu * ( (N_d_y' * vx) + (N_d_x' * vy) ) ...
                          + N_d_x         *   (N'     * p )                   ...
                        ) * intFacQ;
            
            R_Mom_y =   R_Mom_y                                               ...
                      + (   N * (   rho * (N_d_t' * vy)                       ...
                                  + rho * (N_d_x' * vy) * (N' * vx)           ...
                                  + rho * (N_d_y' * vy) * (N' * vy)           ...
                                  - f_gp(2)                                   ...
                                )                                             ...
                          + N_d_y *  2*mu *   (N_d_y' * vy)                   ...
                          + N_d_x *    mu * ( (N_d_y' * vx) + (N_d_x' * vy) ) ...
                          + N_d_y         *   (N'     * p )                   ...
                        ) * intFacQ ;
                  
            R_Mass  =   R_Mass ...
                      + N * ( (N_d_x' * vx) + (N_d_y' * vy) ) * intFacQ;
            
                  
            % Jacobi-Matrix (Tangentensteifigkeitsmatrix)
            
            R_Mom_x_d_vx =   R_Mom_x_d_vx                             ...
                           + (   rho * N * N_d_t'                     ...
                               + rho * N * (   N_d_x' * (N'     * vx) ...
                                             + N'     * (N_d_x' * vx) ...
                                             + N_d_y' * (N'     * vy) ...
                                           )                          ...
                               +       N_d_x * 2*mu * N_d_x'          ...
                               +       N_d_y *   mu * N_d_y'          ...
                             ) * intFacQ;
 
            R_Mom_x_d_vy =   R_Mom_x_d_vy ...
                           + (   rho * N     * N' *   (N_d_y' * vx) ...
                               + mu  * N_d_y * N_d_x'               ...
                             ) * intFacQ;
                         
            R_Mom_x_d_p  = R_Mom_x_d_p - N_d_x * N' * intFacQ;
                         
                         
            
            R_Mom_y_d_vx =   R_Mom_y_d_vx ...
                           + (   rho * N     * N' *   (N_d_x' * vy) ...
                               + mu  * N_d_x * N_d_y'               ...
                             ) * intFacQ;
                         
            R_Mom_y_d_vy =   R_Mom_y_d_vy                             ...
                           + (   rho * N * N_d_t'                     ...
                               + rho * N * (   N_d_x' * (N'     * vx) ...
                                             + N'     * (N_d_y' * vy) ...
                                             + N_d_y' * (N'     * vy) ...
                                           )                          ...
                               +       N_d_y * 2*mu * N_d_y'          ...
                               +       N_d_x *   mu * N_d_x'          ...
                             ) * intFacQ;
                         
            R_Mom_y_d_p  = R_Mom_y_d_p - N_d_y * N' * intFacQ;
            
            
            R_Mass_d_vx  = R_Mass_d_vx + N * N_d_x' * intFacQ;
            
            R_Mass_d_vy  = R_Mass_d_vy + N * N_d_y' * intFacQ;
            
                  
            % ---------------------------------------------------------------- %
            % Stabilisierung
            % ---------------------------------------------------------------- %
            
            % Netzgeschwindigkeit
%             Delta_X = X(1:2,5:8) - X(1:2,1:4);
%             v_mesh  = sqrt( Delta_X(1,:).^2 + Delta_X(2,:).^2 ) / ( t1 - t0 );          
            vx_rel = vx;
            vy_rel = vy;
            
            V_rel  = N' * [ vx_rel , vy_rel ];
            norm_v = norm(V_rel);
            
            Re_el = norm_v * h_el / ( 2 * mu/rho );
            
            if Re_el < 3
                xi_Re_el = Re_el/3;
            else
                xi_Re_el = 1;
            end
            
            theta = 1;

            nu = mu/rho;
            
            delta_t = t1 - t0;
            
            
            tau_mom = 1/sqrt( (2/delta_t)^2 + (2*norm_v/h_el)^2 + (4*nu/h_el^2)^2 );
            %tau_mom = 0;
            
            % Stabilisierung Konvektion (SUPG)
            tau_SUPG = tau_mom;
            %tau_SUPG = 0;
            
            L_stab_x = rho * (   (N_d_t' * vx)                 ...
                               + (N'     * vx) * (N_d_x' * vx) ...
                               + (N'     * vy) * (N_d_y' * vx) ...
                             )                                 ...
                       + (N_d_x' * p);
            
            L_stab_y = rho * (   (N_d_t' * vy)                 ...
                               + (N'     * vx) * (N_d_x' * vy) ...
                               + (N'     * vy) * (N_d_y' * vy) ...
                             )                                 ...
                       + (N_d_y' * p);
                     
            L_stab   = [ L_stab_x ; ...
                         L_stab_y   ...
                       ];
                   
            L_stab_x_d_vx = rho * (   N_d_t'                 ...
                                    + N'     * (N_d_x' * vx) ...
                                    + N_d_x' * (N'     * vx) ...
                                    + N_d_y' * (N'     * vy) ...
                                  );
            
            W_SUPG = N_d_x * (N' * vx) + N_d_y * (N' * vy);
            
            R_SUPG =   R_SUPG ...
                     + tau_SUPG ...
                       * [ W_SUPG        , zeros(size(N)) ; ...
                           zeros(size(N)), W_SUPG           ...
                         ]                                  ...
                       * ( L_stab - f_gp )                  ...
                       * intFacQ;
                   
            R_SUPG_Impx_d_vx =   R_SUPG_Impx_d_vx                                      ...
                               + tau_SUPG                                              ...
                                 * (                                                   ...
                                       (N_d_x * N') * (L_stab_x - f_gp(1))             ...
                                     +  W_SUPG      * rho * (   N_d_t'                 ...
                                                              + N'     * (N_d_x' * vx) ...
                                                              + N_d_x' * (N'     * vx) ...
                                                              + N_d_y' * (N'     * vy) ...
                                                            )                          ...
                                   )                                                   ...
                                 * intFacQ;
                             
            R_SUPG_Impx_d_vy =   R_SUPG_Impx_d_vy                                ...
                               + tau_SUPG                                        ...
                                 * (                                             ...
                                       (N_d_y * N') * (L_stab_x - f_gp(1))       ...
                                     +  W_SUPG      * rho * (                    ...
                                                              N' * (N_d_y' * vy) ...
                                                            )                    ...
                                   )                                             ...
                                 * intFacQ;
                             
            R_SUPG_Impx_d_p  =   R_SUPG_Impx_d_p ...
                               + tau_SUPG * W_SUPG * N_d_x' * intFacQ;
            
            R_SUPG_Impy_d_vx =   R_SUPG_Impy_d_vx                                ...
                   + tau_SUPG                                        ...
                     * (                                             ...
                           (N_d_x * N') * (L_stab_y - f_gp(2))       ...
                         +  W_SUPG      * rho * (                    ...
                                                  N' * (N_d_x' * vy) ...
                                                )                    ...
                       )                                             ...
                     * intFacQ;
                 
            R_SUPG_Impy_d_vy =   R_SUPG_Impy_d_vy                                      ...
                               + tau_SUPG                                              ...
                                 * (                                                   ...
                                       (N_d_y * N') * (L_stab_y - f_gp(2))             ...
                                     +  W_SUPG      * rho * (   N_d_t'                 ...
                                                              + N_d_x' * (N'     * vx) ...
                                                              + N'     * (N_d_y' * vy) ...
                                                              + N_d_y' * (N'     * vy) ...
                                                            )                          ...
                                   )                                                   ...
                                 * intFacQ;
                             
            R_SUPG_Impy_d_p  =   R_SUPG_Impy_d_p ...
                               + tau_SUPG * W_SUPG * N_d_y' * intFacQ;
                           
                      
                 
            
          
            
            
            
            % Stabilisierung Druck (PSPG)
            tau_PSPG = tau_mom;
            

                   
            R_PSPG = R_PSPG + 1/rho * tau_PSPG * N_d_X * (L_stab - f_gp) * intFacQ;
            
            R_PSPG_d_vx = R_PSPG_d_vx                                   ...
                          + 1/rho * tau_PSPG                            ...
                            * N_d_X * rho * [   N_d_t'                  ...
                                              + N'     * (N_d_x' * vx)  ...
                                              + N_d_x' * (N'     * vx)  ...
                                              + N_d_y' * (N'     * vy); ...
                                                N'     * (N_d_x' * vy)  ...
                                            ]                           ...
                            * intFacQ;
                        
            R_PSPG_d_vy =   R_PSPG_d_vy                                 ...
                          + 1/rho * tau_PSPG                            ...
                            * N_d_X * rho * [   N'     * (N_d_y' * vx); ...
                                                N_d_t'                  ...
                                              + N_d_x' * (N'     * vx)  ...
                                              + N'     * (N_d_y' * vy)  ...
                                              + N_d_y' * (N'     * vy)  ...
                                            ]                           ...
                            * intFacQ;
                        
            R_PSPG_d_p  =   R_PSPG_d_p         ...
                          + 1/rho * tau_PSPG   ...
                            * (N_d_X * N_d_X') ...
                            * intFacQ;
            
            

            
            % Stabilisierung Massenerhaltung
            tau_c    = theta * norm_v * h_el * xi_Re_el;
            tau_c = 0;
            
            K_stab_mass = K_stab_mass + rho * tau_c * (N_d_xy * N_d_xy') * intFacQ;
            
            % ---------------------------------------------------------------- %
            
        end % tau  
    end % xi2
end % xi1

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Abspeichern Teilmatrizen
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Agglomeration von Teilmatrizen
% ---------------------------------------------------------------------------- %

% Konvektiver Term (linearisiert)
K_conv_lin = [ R_conv_vx_d_vx , R_conv_vx_d_vy ; ...
               R_conv_vy_d_vx , R_conv_vy_d_vy   ...
             ];
         
% Konvektions-Stabilisierung
R_SUPG_d_v = [ R_SUPG_Impx_d_vx , R_SUPG_Impx_d_vy ; ...
               R_SUPG_Impy_d_vy , R_SUPG_Impy_d_vy   ...
             ];

R_SUPG_d_p = [ R_SUPG_Impx_d_p ; ...
               R_SUPG_Impy_d_p   ...
             ];
         


% Druck-Stabilisierung
%K_PSPG_conv_lin = [ R_PSPG_conv_d_vx , R_PSPG_conv_d_vy ];



% Randspannungen (vorl�ufig!)
r_t = zeros(2*size(N,1),1);


% Element-Matrix (Tangenten-Matrix)
% ---------------------------------------------------------------------------- %

% Physikalische Anteile
eleMat(pos.vx,pos.vx) = eleMat(pos.vx,pos.vx) + R_Mom_x_d_vx;
eleMat(pos.vx,pos.vy) = eleMat(pos.vx,pos.vy) + R_Mom_x_d_vy;
eleMat(pos.vx,pos.p)  = eleMat(pos.vx,pos.p)  + R_Mom_x_d_p;

eleMat(pos.vy,pos.vx) = eleMat(pos.vy,pos.vx) + R_Mom_y_d_vx;
eleMat(pos.vy,pos.vy) = eleMat(pos.vy,pos.vy) + R_Mom_y_d_vy;
eleMat(pos.vy,pos.p)  = eleMat(pos.vy,pos.p)  + R_Mom_y_d_p;

eleMat(pos.p,pos.vx) = eleMat(pos.p,pos.vx) + R_Mass_d_vx;
eleMat(pos.p,pos.vy) = eleMat(pos.p,pos.vy) + R_Mass_d_vy;
eleMat(pos.p,pos.p)  = eleMat(pos.p,pos.p)  + R_Mass_d_p;

% eleMat(pos.v,pos.v) = eleMat(pos.v,pos.v) + K_inst;
% eleMat(pos.v,pos.v) = eleMat(pos.v,pos.v) + K_conv_lin;
% eleMat(pos.v,pos.v) = eleMat(pos.v,pos.v) + K_visc;
% 
% eleMat(pos.v,pos.p) = eleMat(pos.v,pos.p) - K_p;
% 
% eleMat(pos.p,pos.v) = eleMat(pos.p,pos.v) + K_mass;

% Sprungterme
eleMat(pos.vxt0,pos.vxt0) = eleMat(pos.vxt0,pos.vxt0) + K_jump_vx;
eleMat(pos.vyt0,pos.vyt0) = eleMat(pos.vyt0,pos.vyt0) + K_jump_vy;

% Anteile Konvektions-Stabilisierung
eleMat(pos.v,pos.v) = eleMat(pos.v,pos.v) + R_SUPG_d_v;
eleMat(pos.v,pos.p) = eleMat(pos.v,pos.p) + R_SUPG_d_p;

% Anteile Druck-Stabilisierung
eleMat(pos.p,pos.vx) = eleMat(pos.p,pos.vx) + R_PSPG_d_vx;
eleMat(pos.p,pos.vy) = eleMat(pos.p,pos.vy) + R_PSPG_d_vy;
eleMat(pos.p,pos.p)  = eleMat(pos.p,pos.p)  + R_PSPG_d_p;

% Anteile Stabilisierung Massenerhaltung
eleMat(pos.v,pos.v) = eleMat(pos.v,pos.v) + K_stab_mass;



% Element-Vektor (Residuum)
% ---------------------------------------------------------------------------- %

% Physikalische Anteile
eleVec(pos.vx)        = eleVec(pos.vx) + R_Mom_x;
eleVec(pos.vy)        = eleVec(pos.vy) + R_Mom_y;
eleVec(pos.p)         = eleVec(pos.p)  + R_Mass;

% Sprungterme
eleVec(pos.vxt0)    = eleVec(pos.vxt0) + r_jump_vx;
eleVec(pos.vyt0)    = eleVec(pos.vyt0) + r_jump_vy;

% Anteile Konvektions-Stabilisierung
eleVec(pos.v)       = eleVec(pos.v) + R_SUPG;

% Anteile Druck-Stabilisierung
eleVec(pos.p)       = eleVec(pos.p) + R_PSPG;

% Anteile Stabilisierung Massenerhaltung
eleVec(pos.v)       = eleVec(pos.v) + K_stab_mass * v;



eleVec              = - eleVec;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Einbau Dirichlet-Randbedingungen
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
% Ein Einbau der Dirichlet-Rb auf Elementebene ist nur notwendig, wenn
% auf der Systemebene vor L�sung des linearisierten GLS ein Einbau der
% Dirichlet-RB nicht (!) erfolgt.
% Dies kann der Fall sein, wenn die Tangentensteifigkeitsmatrix des
% Gesamtsystems als sparse-Matrix aus Performanzgr�nden nicht mehr
% bearbeitet werden soll.

% if ~isempty(elem.bcDir)
%     activeBcDir = find(elem.bcDir(:,6) == 1); % Aufsuchen von aktiven Dirichlet-RB in diesem Element
%     posLocBcDir = elem.bcDir(activeBcDir,7);  % Lokal Position der aktiven Dirichlet-RBs
% 
%     eleMat(posLocBcDir, :)          = 0.0;    % Einstreichen der entsprechenden Zeilen
%     eleMat(posLocBcDir,posLocBcDir) = eye(length(activeBcDir)); % 1 auf HD-Positionen
% end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Abspeichern (der nicht-Null Eintr�ge)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[ I_loc, J_loc, X_elem ] = find(eleMat);

I_elem = elem.indexSystem(I_loc);
J_elem = elem.indexSystem(J_loc);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        end



        
        %------------------------------------------------------------------
        %------------------------------------------------------------------
        
        
        function [ergFluid, sizeErg] = PostOut(elem, eleSolu, ~)
            % Method returns output data of the element at end of current
            % time slab:
            
            
            % Get indices to access solution and element properties:
            pos           = elem.ind;
            
            % Number of nodes for which output is returned:
            sizeErg       = pos.nNodesOut*2;
            
            % Get unknowns from the solution:
            %-----------------------------------
            velo          = eleSolu(pos.v);
            pressure      = eleSolu(pos.p);
            
            
            % Initialisation:
            %---------------------
            ergFluid      = zeros(sizeErg,4);
            
            % Get velocities and pressure:
            %------------------------------------
            ergFluid(1:4,1) = velo(pos.vxt0);  % v_x_t0         
            ergFluid(5:8,1) = velo(pos.vxt1);  % v_x_t1
            
            ergFluid(1:4,2) = velo(pos.vyt0);  % v_y_t0
            ergFluid(5:8,2) = velo(pos.vyt1);  % v_y_t1
            
            ergFluid(1:4,4) = pressure(pos.pt0Sol); % p_t0
            ergFluid(5:8,4) = pressure(pos.pt1Sol); % p_t1
            
        end
        
        
        %------------------------------------------------------------------
        %------------------------------------------------------------------
        

        function Update(elem, ~, deltaTCurr, deltaTNext, typeEleUpdate)
            
            % deltaTCurr: time width of current time  slab
            % deltaTNext: time width of next time slab
            % typeEleUpdate: 1: use gradient of current time slab to
            %                   estimate start and end values of next time
            %                   slab (initial values for first iteration)
            %                else: gradient is not used, initial values for
            %                   start and end of next time slab are thesame)
            
            % Get indices:
            pos = elem.ind;
            
            % Set discontinious values:
            elem.vRel_dis     = elem.vRel;
            
            if typeEleUpdate == 1
                deltaVRelCurr               = elem.vRel(pos.vt1) - elem.vRel(pos.vt0);
                elem.vRel(pos.vt0)          = elem.vRel(pos.vt1); 
                elem.vRel(pos.vt1)          = elem.vRel(pos.vt0) + deltaVRelCurr * (deltaTNext / deltaTCurr);
            else
                elem.vRel(pos.vt0)          = elem.vRel(pos.vt1);
            end
           
        end
        
        
        %------------------------------------------------------------------
        %------------------------------------------------------------------
        
        
        function [solUpdate , index] = UpdateSol(elem, eleSolu, ...
                                                 deltaTCurr, deltaTNext,...
                                                 typeEleUpdate)
            
            % Method called in UpdateSolution:
            
            % Get the current solution, update it for the next time slab
            % and return it as solUpdate:
            %--------------------------------------------------------------
            
            % Get indices:
            pos = elem.ind;
            
            % Get unknowns from eleSolu:
            velo        = eleSolu(pos.v);
            pressure    = eleSolu(pos.p);
            
            % Update unknowns for next time slab:
            if typeEleUpdate == 1
                deltaVeloCurr          = velo(pos.vt1) - velo(pos.vt0);
                velo(pos.vt0)          = velo(pos.vt1); 
                velo(pos.vt1)          = velo(pos.vt0) + deltaVeloCurr * (deltaTNext / deltaTCurr);
                
                deltaPressureCurr      = pressure(pos.pt1Sol) - pressure(pos.pt0Sol);
                pressure(pos.pt0Sol)   = pressure(pos.pt1Sol); 
                pressure(pos.pt1Sol)   = pressure(pos.pt0Sol) + deltaPressureCurr * (deltaTNext / deltaTCurr);
                
            else
                velo(pos.vt0)          = velo(pos.vt1);
                pressure(pos.pt0Sol)   = pressure(pos.pt1Sol);
            end
            
            
            % Initialisation of updated solution:
            solUpdate = zeros(pos.nDofEle, 1);
            
            % Set values:
            solUpdate(pos.v)    = velo;
            solUpdate(pos.p)    = pressure;
                
            index               = elem.indexSystem;     
        end
        
        
        %------------------------------------------------------------------
        %------------------------------------------------------------------

        
        function Restore(elem, ~, deltaTCurr, deltaTNext, typeEleUpdate)
            % Function resets the element properties for the dof using
            % data from previous time slab. Function is invoked by
            % "RestoreElem" if iteration did not converge and width of time
            % step is reduced.
            
            % Set indices:
            pos = elem.ind;
            
            % Restore the element data:
            if typeEleUpdate == 1
                % Use gradient of previous time slab for initial values of
                % the current time slab:
                deltaVRelPrev       = elem.vRel_dis(pos.vt1) - elem.vRel_dis(pos.vt0);
                elem.vRel(pos.vt0)  = elem.vRel_dis(pos.vt1); 
                elem.vRel(pos.vt1)  = elem.vRel(pos.vt0) + deltaVRelPrev * (deltaTNext / deltaTCurr);
                
            else
                % Initial values for start and end of time slab are the
                % same:
                elem.vRel(pos.vt0)  = elem.vRel_dis(pos.vt1);
                elem.vRel(pos.vt1)  = elem.vRel(pos.vt0);
            end
                                        
        end
        
        
        %------------------------------------------------------------------
        %------------------------------------------------------------------
        
        
        function SetEleDofGlob(elem)
            % Sets the property eleDofGlob of the current element:
            % eleDofGlob has as many cols as the maximum local dof id of
            % the element. Col: local dof id, value: global dof id
            
            
            elem.eleDofGlob    = [ 1, 21, 2, 22, 4, 24 ];
            % Aufgrund des gew�hlten Ansatzes (s. Build-Routine)
            % ist die Reihenfolge der globalen FHG hier zwingend.
                      
                
        end

        
    end % end of methods
    
    
    %------------------------------------------------------------------
    
    %------------------------------------------------------------------
    
    
    methods (Static)
        function Post(~, ~, ~)
            % Method setting element data from the solution:
            
            % -- no secondary unknowns stored for this element type --
        end
        
        
        %------------------------------------------------------------------
        %------------------------------------------------------------------
        
        
        function eleNodesDofLoc = GetEleNodesDofLoc
            % returns a matrix containing the local node numbers in the
            % first col and the local dof ids in the second col:
            
            % loc dof: 1: vxt0, 2: vxt1, 3: vyt0, 4: vyt1, 5: pt0, 6: pt1
            eleNodesDofLoc= [1, 1;
                             2, 1;
                             3, 1;
                             4, 1;
                             1, 2;
                             2, 2;
                             3, 2;
                             4, 2;
                             1, 3;
                             2, 3;
                             3, 3;
                             4, 3;
                             1, 4;
                             2, 4;
                             3, 4;
                             4, 4;
                             1, 5;
                             2, 5;
                             3, 5;
                             4, 5;
                             1, 6;
                             2, 6;
                             3, 6;
                             4, 6;];
              
        end
        
    end % end of static methods
end % end of classdefinition