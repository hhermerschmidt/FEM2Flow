classdef FluidNavierStokes4202 < FluidElemSuperclass
   
    % Description:
    % Testelement zur Herleitung f�r eigenes 2D-Fluidelement
    % separate Build-Routinen
    
    properties (SetAccess = protected)
        
        % General properties:
        %-----------------------------------------------------------------
        type        = 4202;          % identification number of element type
        name        = 'FluidNavierStokes4202';  % name of current element type
        sizeXest    = 24*24;        % estimated number of non-zero entries in eleMat
        probDim     = 2;            % problem dimension
        paraOutput  = 1;            % create Paraview output for current element type (1) or not (0)

        
        
        % Indices to access the element matrix:
        %----------------------------------------------------------------
        
        % Description:
        % Sets the number of dofs used for initialization of the element
        % attributes and the positions of the dof in the element matrix
        % and the element attribute to access them in the proper
        % positons:
        % Note: Since the velocity dof are the first element dof for
        % this type, positions of velocity dof are the same in velo and
        % in eleMat and thus not given seperately. For the other 
        % unknown fields positions are not the same, compare e.g. 
        % ind.pt0 and ind.pt0Sol
        
        % Initialise values in property section to ensure, that 'ind' is
        % created only once for all elements of this type:
        ind = struct('nNodesOut', 4,...         % Number of nodes for output
                     'v'        , (1:16)',...   % pos of all velocity dof in eleMat
                     'p'        , (17:24)',...  % pos of all pressure dof
                     'vx'       , (1:8)',...    % pos of x-velocity in eleMat
                     'vy'       , (9:16)',...   % pos of y-velocity in eleMat
                     'vxt0'     , (1:4)',...    % pos of vx_t0 in eleMat and velo
                     'vyt0'     , (9:12)',...   % pos of vy_t0 in eleMat and velo
                     'vt0'      , [1:4, 9:12]',... % pos of v_t0 in eleMat and velo
                     'vxt1'     , (5:8)',...    % pos of vx_t1 in eleMat and velo
                     'vyt1'     , (13:16)',...  % pos of vy_t1 in eleMat and velo
                     'vt1'      , [5:8, 13:16]',... % pos of v_t1 in eleMat and velo
                     'pt0'      , (17:20)',...  % pos of p_t0 in eleMat
                     'pt1'      , (21:24)',...  % pos of p_t1 in eleMat
                     'pt0Sol'   , (1:4)',...    % pos of p_t0 in pressure
                     'pt1Sol'   , (5:8)',...    % pos of p_t1 in pressure
                     'nDofV'    , 16,...        % total number of velo dof in current element
                     'nDofP'    , 8,...         % total number of pressure dof in current element
                     'nDofEle'  , 24)           % total number of dof for the current element
        

        % further properties are included in superclasses
    end
    
        
    %------------------------------------------------------------------
    
    %------------------------------------------------------------------
    
    
    methods
        
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  

        function elem = FluidNavierStokes4202( eleConnect    , eleCoord      , ...
                                               eleMatPara    , eleLoadPara   , ...
                                               eleBcDir      , eleBcNeu      , ...
                                               mm                              ...
                                             )

                            
            %Call superclass constructor:
            elem = elem@FluidElemSuperclass( eleConnect    , eleCoord      , ...
                                             eleMatPara    , eleLoadPara   , ...
                                             eleBcDir      , eleBcNeu      , ...
                                             mm                              ...
                                           );
            
            
            % Initialise element data for dof and related data:
            %--------------------------------------------------
           	elem.vRel           = zeros(elem.ind.nDofV,1);
           	elem.vRel_dis       = zeros(elem.ind.nDofV,1);
        end
        
        
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%        
        
function[ rhs , ...
          I_elem , ...
          J_elem , ...
          X_elem   ...
                   ] = Build( elem        , ...
                              eleSolu     , ...
                              eleSoluDis  , ...
                              eleCoords   , ...
                              eleMatPara  , ...
                              time        , ...
                              currTimeVar   ...
                            )                      
                        
if elem.numGlob == 20 
   [NewMat, NewVec] = elem.NewBuildRoutine( eleSolu     , ...
                                       eleSoluDis  , ...
                                       eleCoords   , ...
                                       eleMatPara  , ...
                                       time        , ...
                                       currTimeVar   ...
                                      );
end


    % Position structure to access element matrix & rhs:
    pos = elem.ind;


    % Known values:
    %--------------
    velo_dis  = eleSoluDis;     % velocities of last time slab

    pressure  = eleSolu(pos.p); % pressure   of last iteration

    velo      = eleSolu(pos.v); % velocities of last iteration
    v_x       = velo(pos.vx);     
    v_y       = velo(pos.vy);
%            v_x_rel = elem.vRel(pos.vx); % relative velocity to mesh
%            v_y_rel = elem.vRel(pos.vy);
    v_x_rel   = v_x;
    v_y_rel   = v_y;


    % Node coordinates (space-time):
    % ------------------------------
    deltaT = time.curr(1,3) - time.curr(1,1);     

    nodesGlob           = zeros(8,3);
    nodesGlob(1:4,1:2)  = eleCoords.t0(:,1:2);
    nodesGlob(5:8,1:2)  = eleCoords.t1(:,1:2);
    nodesGlob(5:8, 3 )  = deltaT;           

    % Local coordinates of nodes:
    nodesXi1 = [-1.0, 1.0, 1.0,-1.0];
    nodesXi2 = [-1.0,-1.0, 1.0, 1.0];


    % Material data:
    %---------------
    rho         = eleMatPara.rho;       % density
    mu          = eleMatPara.mu;        % dynamic viscosity

    % (Dynamic) viscosity matrix:
    muMat = zeros(3,3);
    muMat(1:2, 1:2) = 2.0 * mu * eye(2);
    muMat(3,   3)   = 1.0 * mu * eye(1);           

    % Volume forces
    fx          = eleMatPara.fx;        % volume force in x-dir
    fy          = eleMatPara.fy;        % volume force in y-dir  

    f_i_time = [fx;
                fy] * (currTimeVar(eleMatPara.timeVar, [1, 3]) * rho);


    % Element volume and diameter
    %-------------------------------
    geom_abl_xi = 0.25 * [nodesXi1;
                          nodesXi2];

    jakobi = [(0.5 * geom_abl_xi),(0.5 * geom_abl_xi)] * nodesGlob(:,1:2);

    volume = 4 * det(jakobi);

    delta_h = sqrt(4 * volume / pi); % diameter of cylinder with same
                                     % height (1) and volume as element


    % Initialise matrices and vectors
    % --------------------------------------
    lhs = zeros(pos.nDofEle,pos.nDofEle); % element (tangent-stiffness) matrix
    rhs = zeros(pos.nDofEle,1);           % element (unbalanced) load vector (residual)

    shapeFuncDLocST = zeros( 3,  8);           
   
    matrix_jumpVV   = zeros( 4,  4);
    matrix_vDivvDiv = zeros(16, 16);
    matrix_pv       = zeros( 8, 16);
    matrix_vDxvDx   = zeros(16, 16);
   
    %---------------------------------------------------------------------------
    %---------------------------------------------------------------------------
    
    % Get local coordinates and weight of Gauss points:
    coord_gp = [double(-1/sqrt(3)), double(1/sqrt(3))];   
    weight   = [1, 1];

    % Loop over Gauss points
    for jj = 1 : 2 % loop over xi_2 values of Gauss points

        xi_2 = coord_gp(1,jj); 	

        
        %-----------------------------------------------------------------------
        for kk = 1 : 2 % loop over xi_1 values of Gauss points

            xi_1 = coord_gp(1,kk);


            % Shape functions (tri-linear in space):
            shapeFunc = 0.25*( (1 + xi_1 * nodesXi1) .* (1 + xi_2 * nodesXi2) );

            % Local derivatives of shape functions:
            shapeFuncDXi12 = 0.25 * [ nodesXi1 .* (1 + xi_2 * nodesXi2) ; ...
                                      nodesXi2 .* (1 + xi_1 * nodesXi1)   ...
                                    ];
 
            shapeFuncDLocST(3,:) = 0.5 * [-shapeFunc, shapeFunc];


            % Jump terms:
            % -----------
            jacobit0      = shapeFuncDXi12 * nodesGlob(1:4,1:2);

            intFacJump    = weight(1,jj) * weight(1,kk) * det(jacobit0);

            matrix_jumpVV =   matrix_jumpVV ...
                            + shapeFunc' * (shapeFunc * (rho * intFacJump));

                        
            % ------------------------------------------------------------------
            for ii = 1 : 2 % loop over local time axis

                % local time coordinate of current Gauss point:
                tau = coord_gp(1,ii);

                
                % Time shape functions at current Gauss point:
                t_1 = 0.5 * (1-tau);
                t_2 = 0.5 * (1+tau);

                b = f_i_time * [t_1;
                                t_2];
                            
                
                % Shape functions and their derivatives
                % --------------------------------------------------------------
                shapeFuncDLocST(1:2,:) = [(t_1 * shapeFuncDXi12) , (t_2 * shapeFuncDXi12)];

                jacobi    = shapeFuncDLocST * nodesGlob;
                detJacobi = det(jacobi);
                invJacobi = inv(jacobi);
                
                % Integration factor of current Gauss point:
                intFac = weight(1,ii) * weight(1,jj) * weight(1,kk)* detJacobi;

                if detJacobi <= 0
                    warning(['Element ', num2str(elem.numGlob), ' is deteriorated!'])
                    display(['det(Jacobi-Matrix) = ', num2str(detJacobi)])
                    pause
                end

                shapeFuncST   = [ (t_1 * shapeFunc) , (t_2 * shapeFunc) ];

                shapeFuncDxST = invJacobi(1,1:2) * shapeFuncDLocST(1:2,:);
                shapeFuncDyST = invJacobi(2,1:2) * shapeFuncDLocST(1:2,:);
                shapeFuncDtST = invJacobi(3, : ) * shapeFuncDLocST;

                shapeFuncDiv   = [ shapeFuncDxST , shapeFuncDyST ];
                shapeFuncDxyST = [ shapeFuncDxST; ...
                                   shapeFuncDyST  ...
                                 ];
                  
                ShapeD = [ shapeFuncDxST ,   zeros(1,8)   ; ...
                             zeros(1,8)  , shapeFuncDyST  ; ...
                           shapeFuncDyST , shapeFuncDxST    ...
                         ];
                                
                % Instationary and convective part:
                % --------------------------------------------------------------
                
                V     = ( shapeFuncST    * [ v_x     , v_y     ] )';
                V_Rel = ( shapeFuncST    * [ v_x_rel , v_y_rel ] )';
                V_T   = ( shapeFuncDtST  * [ v_x     , v_y     ] )';

                GradV = ( shapeFuncDxyST * [ v_x     , v_y     ] )';

                GradV_V = GradV * V;
                
                V_Tot = (V_T + GradV_V) * rho;
                
                % Residual part
                rhs(pos.vx) =   rhs(pos.vx) ...
                              + shapeFuncST' * ((V_Tot(1) - b(1)) * intFac);
                rhs(pos.vy) =   rhs(pos.vy) ...
                              + shapeFuncST' * ((V_Tot(2) - b(2)) * intFac);
                
                % Tangent stiffness matrix part
                H = [ GradV(:,1) * shapeFuncST , GradV(:,2) * shapeFuncST ];

                ShapeGradV = V' * shapeFuncDxyST;

                Vec = shapeFuncDtST + ShapeGradV;             

                H(1, 1:8 ) = H(1,1:8 ) + Vec;
                H(2, 9:16) = H(2,9:16) + Vec;

                H = rho * H;
                
                lhs(pos.vx,pos.v) =   lhs(pos.vx,pos.v) ...
                                    + shapeFuncST' * (H(1,:) * intFac);
                lhs(pos.vy,pos.v) =   lhs(pos.vy,pos.v)  ...
                                    + shapeFuncST' * (H(2,:) * intFac);


                % Viscous part: 
                %---------------------------------------------------------------

                matrix_vDxvDx =   matrix_vDxvDx ...
                                + ShapeD' * muMat * (ShapeD * intFac);
                             

                % Mass conservation:
                % --------------------------------------------------------------
                matrix_pv =   matrix_pv ...
                            + shapeFuncST' * (shapeFuncDiv * intFac);

                
                % Stabilisation - Momentum
                %---------------------------------------------------------------
                tau_m = 1/sqrt(   ( 2               /  deltaT     )^2 ...
                                + ( 2 * norm(V_Rel) /  delta_h    )^2 ...
                                + ( 4 *  (mu/rho)   / (delta_h^2) )^2 ...
                              )                                       ...
                        * (1/rho);
               
                % First part
                StabMat = [H, shapeFuncDxyST];

                Vec = ShapeGradV * rho;

                StabMat2 = [ Vec,        zeros(1,8), shapeFuncDxST;
                             zeros(1,8), Vec,        shapeFuncDyST];

                lhs = lhs + StabMat2' * (StabMat * (tau_m * intFac)); 
                               
                % Second Parts (Residual)
                P_Diff = shapeFuncDxyST * pressure;
                
                L      = V_Tot + P_Diff - b;
  
                rhs = rhs + StabMat2' * (L       * (tau_m * intFac));
                
                
                Mat = [shapeFuncDxyST * L(1) , shapeFuncDxyST * L(2)];

                Vec = shapeFuncST * (rho * tau_m * intFac);

                lhs(pos.v,pos.vx) = lhs(pos.v,pos.vx) + Mat(1,:)' * Vec;
                lhs(pos.v,pos.vy) = lhs(pos.v,pos.vy) + Mat(2,:)' * Vec;

                
                % Stabilisation - Mass
                % --------------------------------------------------------------
                Re = (norm(V_Rel) * delta_h) / (2 * (mu / rho));
                if Re < 3
                    tau_c = (delta_h * norm(V_Rel) * (Re / 3)) * rho;
                else
                    tau_c = (delta_h * norm(V_Rel))            * rho;
                end
                                    
                matrix_vDivvDiv =   matrix_vDivvDiv ...
                                  + shapeFuncDiv' * (shapeFuncDiv * (tau_c * intFac));

            end % tau
        end % xi1
    end % xi2

    % Velocity jump at start of current time slab:
    % --------------------------------------------------------------------------
    jumpDeltaVx  = velo(pos.vxt0) - velo_dis(pos.vxt1);
    jumpDeltaVy  = velo(pos.vyt0) - velo_dis(pos.vyt1);
%    jumpDeltaVx  = velo(pos.vxt0) - velo_dis(pos.vxt0); %velo_dis(pos.vxt1);
%    jumpDeltaVy  = velo(pos.vyt0) - velo_dis(pos.vyt0); %velo_dis(pos.vyt1);
    
    
    % Add contributions to rhs:
    % --------------------------------------------------------------------------
    rhs(pos.v)    = rhs(pos.v)    + matrix_vDxvDx   * velo;
    rhs(pos.v)    = rhs(pos.v)    - matrix_pv'      * pressure;
    rhs(pos.p)    = rhs(pos.p)    + matrix_pv       * velo;
 
    rhs(pos.vxt0) = rhs(pos.vxt0) + matrix_jumpVV   * jumpDeltaVx;
    rhs(pos.vyt0) = rhs(pos.vyt0) + matrix_jumpVV   * jumpDeltaVy; 
       
    rhs(pos.v)    = rhs(pos.v)    + matrix_vDivvDiv * velo; 
    
  
        
    % Add contributions to lhs:
    % --------------------------------------------------------------------------
    lhs(pos.v, pos.v) = lhs(pos.v, pos.v) + matrix_vDxvDx;
    lhs(pos.v, pos.p) = lhs(pos.v, pos.p) - matrix_pv';
    lhs(pos.p, pos.v) = lhs(pos.p, pos.v) + matrix_pv;

    lhs(pos.vxt0,pos.vxt0) = lhs(pos.vxt0,pos.vxt0) + matrix_jumpVV;
    lhs(pos.vyt0,pos.vyt0) = lhs(pos.vyt0,pos.vyt0) + matrix_jumpVV;
    
    lhs(pos.v, pos.v) = lhs(pos.v, pos.v) + matrix_vDivvDiv;
        
    
if elem.numGlob == 20
    display('Residuum')
    %rhs - NewVec
    %pause
    (rhs-NewVec)./rhs
    pause
    
    display('Matrix')
    %lhs - NewMat
    %pause
    (lhs - NewMat) ./ lhs
    pause
      
end
    
    
    % Change sign of rhs:
    % --------------------------------------------------------------------------
     rhs = -rhs; 
     
     
    % Set Dirichlet BC:
    % --------------------------------------------------------------------------
    if ~isempty(elem.bcDir)
        activeBcDir = find(elem.bcDir(:,6) == 1); % Find active bcDir in current element
        posLocBcDir = elem.bcDir(activeBcDir,7);  % Get local position of active bcDir

        lhs(posLocBcDir, :)          = 0.0;
        lhs(posLocBcDir,posLocBcDir) = eye(length(activeBcDir));
    end


    % Store element matrix and indices in vectors:
    % --------------------------------------------------------------------------

    % Nonzero parts of lhs
    [I_Loc, J_Loc, X_elem] = find(lhs);

    % Transfer the local to global indices:
    index       = elem.indexSystem;
    I_elem      = index(I_Loc);
    J_elem      = index(J_Loc);

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
function [testMat, testVec] = NewBuildRoutine( elem        , ...
                                             eleSolu     , ...
                                             eleSoluDis  , ...
                                             eleCoords   , ...
                                             eleMatPara  , ...
                                             time        , ...
                                             currTimeVar   ...
                                            )
                                        
   
% Auslesen d. vorhandenen Daten ------------------------------------------------

% Element-Positionsindex
pos = elem.ind;

% Zeit
t0 = time.curr(1,1);
t1 = time.curr(1,3);


% Materialdaten
rho = eleMatPara.rho; % Rohdichte
mu  = eleMatPara.mu;  % dynamische Viskosit�t

% Volumenlasten
fx  = eleMatPara.fx;
fy  = eleMatPara.fy;
f   = [ fx ; ...
        fy   ...
      ];
f_t = f * currTimeVar(eleMatPara.timeVar, [1,3]); % -> Einf�hren von eleLoadPara.timeVar !

% Geschw. vorherige Zeitscheibe
v_prev = eleSoluDis;
vx_prev = eleSoluDis(pos.vxt1);
vy_prev = eleSoluDis(pos.vyt1);

% L�sungen vorhergehender Iterationsschritt
v     = eleSolu(pos.v); % Geschw. dieser Zeitscheibe aus letztem NR-Iterationsschritt
p     = eleSolu(pos.p); % Druck dieser Zeitscheibe aus letztem NR-Iterationsschritt

vx    = eleSolu(pos.vx);
vy    = eleSolu(pos.vy);

vx_t0 = eleSolu(pos.vxt0);
vy_t0 = eleSolu(pos.vyt0);

vx_rel = vx;
vy_rel = vy;


% Koordinaten (Raum-Zeit, global)
% [ x ]   [ xA xB xC xD | xE xF xG xH ]
% [ y ] = [ yA yB yC yD | yE yF yG yH ]
% [ t ]   [ t0 t0 t0 t0 | t1 t1 t1 t1 ]

X = [ eleCoords.t0(:,1:2)', eleCoords.t1(:,1:2)'; ...
      t0 t0 t0 t0         , t1 t1 t1 t1           ...
    ];      


% Elementvolumen ---------------------------------------------------------------

% Lokal-abgeleitete r�uml. Ansatzfunktionen (bei xi1 = xi2 = 0)
% [ dN/dxi1 | dN/dxi2 ]
N_Sp_d_Xi0  = 1/4 * [ (-1)*(1), (1)*(-1); ...
                      (+1)*(1), (1)*(-1); ...
                      (+1)*(1), (1)*(+1); ... 
                      (-1)*(1), (1)*(+1)  ...
                    ];

J_Sp_Xi0     = X(1:2,1:4) * N_Sp_d_Xi0;
det_J_Sp_Xi0 = det(J_Sp_Xi0);

Vol_el       = 4 * det_J_Sp_Xi0;
h_el         = sqrt( 4*Vol_el / pi );


% Initialisieren der Teil-Matrizen ---------------------------------------------

eleMat = zeros( pos.nDofEle, pos.nDofEle ); % Element-Matrix (Tangentenmatrix)
eleVec = zeros( pos.nDofEle, 1           ); % Element-Vektor

K_inst = zeros( pos.nDofV, pos.nDofV );
K_conv = zeros( pos.nDofV, pos.nDofV );
K_visc = zeros( pos.nDofV, pos.nDofV );

K_p    = zeros( pos.nDofV, pos.nDofP );

K_mass = zeros( pos.nDofP, pos.nDofV );

r_f        = zeros( pos.nDofV, 1);
%r_t        = zeros( pos.nDofV, 1);

K_jump_vx  = zeros( pos.nDofV/(2*2), pos.nDofV/(2*2) );
K_jump_vy  = zeros( pos.nDofV/(2*2), pos.nDofV/(2*2) );

R_conv_vx_d_vx = zeros( pos.nDofV/2, pos.nDofV/2);
R_conv_vx_d_vy = zeros( pos.nDofV/2, pos.nDofV/2);
R_conv_vy_d_vx = zeros( pos.nDofV/2, pos.nDofV/2);
R_conv_vy_d_vy = zeros( pos.nDofV/2, pos.nDofV/2);


R_SUPG             = zeros(pos.nDofV, 1);
R_SUPG_Impx_d_vx   = zeros(pos.nDofV/2, pos.nDofV/2);
R_SUPG_Impx_d_vy   = zeros(pos.nDofV/2, pos.nDofV/2);
R_SUPG_Impx_d_p    = zeros(pos.nDofV/2, pos.nDofP);
R_SUPG_Impy_d_vx   = zeros(pos.nDofV/2, pos.nDofV/2);
R_SUPG_Impy_d_vy   = zeros(pos.nDofV/2, pos.nDofV/2);
R_SUPG_Impy_d_p    = zeros(pos.nDofV/2, pos.nDofP);

R_PSPG             = zeros( pos.nDofP, 1);
R_PSPG_d_vx        = zeros( pos.nDofP, pos.nDofV/2);
R_PSPG_d_vy        = zeros( pos.nDofP, pos.nDofV/2);
R_PSPG_d_p         = zeros( pos.nDofP, pos.nDofP);

K_stab_mass = zeros( pos.nDofV, pos.nDofV);





            
% Gau�punkte
gp_coord  = [ -1/sqrt(3) , 1/sqrt(3) ];
gp_weight = [     1      ,    1      ];

% Integrationsschleifen
for kk = 1:2 % xi1: lokaler Raum
    xi1 = gp_coord(kk);
    
    for ll = 1:2 % xi2: lokaler Raum
        xi2 = gp_coord(ll);

        % R�uml. Ansatzfunktionen
        N_Sp        = 1/4 * [ (1-xi1)*(1-xi2); ...
                              (1+xi1)*(1-xi2); ...
                              (1+xi1)*(1+xi2); ... 
                              (1-xi1)*(1+xi2)  ...
                            ];

        % Lokal-abgeleitete r�uml. Ansatzfunktionen
        % [ dN/dxi1 | dN/dxi2 ]
        N_Sp_d_Xi   = 1/4 * [ (-1)*(1-xi2), (1-xi1)*(-1); ...
                              (+1)*(1-xi2), (1+xi1)*(-1); ...
                              (+1)*(1+xi2), (1+xi1)*(+1); ... 
                              (-1)*(1+xi2), (1-xi1)*(+1)  ...
                            ];

        % Jakobi-Matrix (Raum-Gebiet am Zeitscheibenanfang)
        J_Sp        = X(1:2,1:4) * N_Sp_d_Xi;
        det_J_Sp    = det(J_Sp);

        % Integrationsfaktor Raumgebiet Zeitscheibenanfang
        intFacOmega = gp_weight(kk) * gp_weight(ll) * det_J_Sp;

        % Sprungterme in eleMat
        K_jump_vx   = K_jump_vx + rho * (N_Sp * N_Sp') * intFacOmega;
        K_jump_vy   = K_jump_vx;      
     
        
        for nn = 1:2 % tau: lokale Zeit
            tau = gp_coord(nn);


            % Ansatzfunktionen
            N       = 1/8 * [ (1-xi1)*(1-xi2)*(1-tau); ...
                              (1+xi1)*(1-xi2)*(1-tau); ...
                              (1+xi1)*(1+xi2)*(1-tau); ... 
                              (1-xi1)*(1+xi2)*(1-tau); ...
                              (1-xi1)*(1-xi2)*(1+tau); ...
                              (1+xi1)*(1-xi2)*(1+tau); ...
                              (1+xi1)*(1+xi2)*(1+tau); ...
                              (1-xi1)*(1+xi2)*(1+tau)  ...
                            ];

            % Lokal-Abgeleitete Ansatzfunktionen: [ dN/dxi1 | dN/dxi2 | dN/dtau ]
            N_d_Xi  = 1/8 * [ (-1)*(1-xi2)*(1-tau), (1-xi1)*(-1)*(1-tau), (1-xi1)*(1-xi2)*(-1); ...
                              (+1)*(1-xi2)*(1-tau), (1+xi1)*(-1)*(1-tau), (1+xi1)*(1-xi2)*(-1); ...
                              (+1)*(1+xi2)*(1-tau), (1+xi1)*(+1)*(1-tau), (1+xi1)*(1+xi2)*(-1); ... 
                              (-1)*(1+xi2)*(1-tau), (1-xi1)*(+1)*(1-tau), (1-xi1)*(1+xi2)*(-1); ...
                              (-1)*(1-xi2)*(1+tau), (1-xi1)*(-1)*(1+tau), (1-xi1)*(1-xi2)*(+1); ...
                              (+1)*(1-xi2)*(1+tau), (1+xi1)*(-1)*(1+tau), (1+xi1)*(1-xi2)*(+1); ...
                              (+1)*(1+xi2)*(1+tau), (1+xi1)*(+1)*(1+tau), (1+xi1)*(1+xi2)*(+1); ...
                              (-1)*(1+xi2)*(1+tau), (1-xi1)*(+1)*(1+tau), (1-xi1)*(1+xi2)*(+1)  ...
                      ];


            % Jakobi-Matrix (Raum-Zeit Gebiet)
            J_SpTi        = X * N_d_Xi;
            det_J_SpTi    = det(J_SpTi);

            % Integrationsfaktor Raum-Zeit Gebiet
            intFacQ  = gp_weight(kk) * gp_weight(ll) * gp_weight(nn) * det_J_SpTi;

            % Inverse Jakobi-Matrix
            inv_J_SpTi    = inv(J_SpTi);        
            inv_J_SpTi_x  = inv_J_SpTi(:,1);
            inv_J_SpTi_y  = inv_J_SpTi(:,2);
            inv_J_SpTi_t  = inv_J_SpTi(:,3);

            % Global-Abgeleitet Ansatzfunktionen
            N_d_x   = N_d_Xi * inv_J_SpTi_x;
            N_d_y   = N_d_Xi * inv_J_SpTi_y;
            N_d_t   = N_d_Xi * inv_J_SpTi_t;

    
            % Hilfsmatrizen
            H       = [     N         , zeros(size(N)); ...
                        zeros(size(N)),     N           ...
                      ];
            
            H_d_t   = [       N_d_t   , zeros(size(N)); ...
                        zeros(size(N)),       N_d_t     ...
                      ];
                  
            N_d_xy  = [ N_d_x ; ...
                        N_d_y ; ...
                      ];
                  
            N_d_X   = [ N_d_x , N_d_y ]; 
                  
            B        = [ N_d_x         , zeros(size(N)), N_d_y; ...
                         zeros(size(N)), N_d_y         , N_d_x  ...
                       ];
            
            G        = [ N_d_x' * vx , N_d_y' * vx ; ...
                         N_d_x' * vy , N_d_y' * vy   ...
                       ];
                   
            C        = [ 2*mu,  0 , 0 ; ...
                           0 ,2*mu, 0 ; ...
                           0 ,  0 , mu; ...
                       ];
                   
    
    K_inst = K_inst + rho * H      * H_d_t'  * intFacQ;
    K_conv = K_conv + rho * H      * G * H'  * intFacQ;
    K_visc = K_visc +       B      * C * B'  * intFacQ;
    K_p    = K_p    +       N_d_xy * N'      * intFacQ;
    K_mass = K_mass +       N      * N_d_xy' * intFacQ;
    
    f_gp   = f_t * [ 1/2 * (1-tau) ; ...
                     1/2 * (1+tau)   ...
                   ];


    r_f    = r_f    + H * f_gp *intFacQ;
    
    %           r_t    = r_t    + H * h * intFacP;
    
    % Konvektiver Term (Jakobi-Matrix des Residuums)
    R_conv_vx_d_vx = R_conv_vx_d_vx                    ...
                    + (   N * N_d_x' * (N'     * vx) ...
                        + N * N'     * (N_d_x' * vx) ...
                        + N * N_d_y' * (N'     * vy) ...
                      ) * rho * intFacQ;

    R_conv_vy_d_vy = R_conv_vy_d_vy ...
                    + (   N * N_d_x' * (N'     * vx) ...
                        + N * N_d_y' * (N'     * vy) ...
                        + N * N'     * (N_d_y' * vy) ...
                      ) * rho * intFacQ;

    R_conv_vx_d_vy = R_conv_vx_d_vy + N * N' * (N_d_y' * vx) * rho * intFacQ;

    R_conv_vy_d_vx = R_conv_vy_d_vx + N * N' * (N_d_x' * vy) * rho * intFacQ;
            
    
   
    % Stabilisierung Konvektion (SUPG) -----------------------------------------
    V_rel  = N' * [ vx_rel , vy_rel ];
    norm_v = norm(V_rel);

%     Re_el = norm_v * h_el / ( 2 * mu/rho );
%
%     if Re_el < 3
%         xi_Re_el = Re_el/3;
%     else
%         xi_Re_el = 1;
%     end
% 
%     theta = 1;

    nu = mu/rho;

    delta_t = t1 - t0;


    tau_mom = 1/sqrt( (2/delta_t)^2 + (2*norm_v/h_el)^2 + (4*nu/h_el^2)^2 );
    
    
    
    tau_SUPG = tau_mom;


    L_stab_x = rho * (   (N_d_t' * vx)                 ...
                       + (N'     * vx) * (N_d_x' * vx) ...
                       + (N'     * vy) * (N_d_y' * vx) ...
                     )                                 ...
               + (N_d_x' * p);

    L_stab_y = rho * (   (N_d_t' * vy)                 ...
                       + (N'     * vx) * (N_d_x' * vy) ...
                       + (N'     * vy) * (N_d_y' * vy) ...
                     )                                 ...
               + (N_d_y' * p);

    L_stab   = [ L_stab_x ; ...
                 L_stab_y   ...
               ];   

    W_SUPG = N_d_x * (N' * vx) + N_d_y * (N' * vy);

    R_SUPG =   R_SUPG ...
             + tau_SUPG ...
               * [ W_SUPG        , zeros(size(N)) ; ...
                   zeros(size(N)), W_SUPG           ...
                 ]                                  ...
               * ( L_stab - f_gp )                  ...
               * intFacQ;
           
    R_SUPG_Impx_d_vx =   R_SUPG_Impx_d_vx                                      ...
                       + tau_SUPG                                              ...
                         * (                                                   ...
                               (N_d_x * N') * (L_stab_x - f_gp(1))             ...
                             +  W_SUPG      * rho * (   N_d_t'                 ...
                                                      + N'     * (N_d_x' * vx) ...
                                                      + N_d_x' * (N'     * vx) ...
                                                      + N_d_y' * (N'     * vy) ...
                                                    )                          ...
                           )                                                   ...
                         * intFacQ;

    R_SUPG_Impx_d_vy =   R_SUPG_Impx_d_vy                                ...
                       + tau_SUPG                                        ...
                         * (                                             ...
                               (N_d_y * N') * (L_stab_x - f_gp(1))       ...
                             +  W_SUPG      * rho * (                    ...
                                                      N' * (N_d_y' * vx) ...
                                                    )                    ...
                           )                                             ...
                         * intFacQ;

    R_SUPG_Impx_d_p  =   R_SUPG_Impx_d_p ...
                       + tau_SUPG * W_SUPG * N_d_x' * intFacQ;

    R_SUPG_Impy_d_vx =   R_SUPG_Impy_d_vx                                ...
                       + tau_SUPG                                        ...
                         * (                                             ...
                               (N_d_x * N') * (L_stab_y - f_gp(2))       ...
                             +  W_SUPG      * rho * (                    ...
                                                      N' * (N_d_x' * vy) ...
                                                    )                    ...
                           )                                             ...
                         * intFacQ;

    R_SUPG_Impy_d_vy =   R_SUPG_Impy_d_vy                                      ...
                       + tau_SUPG                                              ...
                         * (                                                   ...
                               (N_d_y * N') * (L_stab_y - f_gp(2))             ...
                             +  W_SUPG      * rho * (   N_d_t'                 ...
                                                      + N_d_x' * (N'     * vx) ...
                                                      + N'     * (N_d_y' * vy) ...
                                                      + N_d_y' * (N'     * vy) ...
                                                    )                          ...
                           )                                                   ...
                         * intFacQ;

    R_SUPG_Impy_d_p  =   R_SUPG_Impy_d_p ...
                       + tau_SUPG * W_SUPG * N_d_y' * intFacQ;

                   
   % Stabilisierung Druck (PSPG) -----------------------------------------------
   tau_PSPG = tau_mom;
                   
   R_PSPG = R_PSPG + 1/rho * tau_PSPG * N_d_X * (L_stab - f_gp) * intFacQ;
 
   R_PSPG_d_vx = R_PSPG_d_vx                                   ...
                  + 1/rho * tau_PSPG                            ...
                    * N_d_X * rho * [   N_d_t'                  ...
                                      + N'     * (N_d_x' * vx)  ...
                                      + N_d_x' * (N'     * vx)  ...
                                      + N_d_y' * (N'     * vy); ...
                                        N'     * (N_d_x' * vy)  ...
                                    ]                           ...
                    * intFacQ;

    R_PSPG_d_vy =   R_PSPG_d_vy                                 ...
                  + 1/rho * tau_PSPG                            ...
                    * N_d_X * rho * [   N'     * (N_d_y' * vx); ...
                                        N_d_t'                  ...
                                      + N_d_x' * (N'     * vx)  ...
                                      + N'     * (N_d_y' * vy)  ...
                                      + N_d_y' * (N'     * vy)  ...
                                    ]                           ...
                    * intFacQ;

    R_PSPG_d_p  =   R_PSPG_d_p         ...
                  + 1/rho * tau_PSPG   ...
                    * (N_d_X * N_d_X') ...
                    * intFacQ;
                
    % Stabilisierung Massenerhaltung (GLS) -------------------------------------
    V_Rel = ( N'    * [ vx_rel , vy_rel ] )';
    Re = (norm(V_Rel) * h_el) / (2 * (mu / rho));
    if Re < 3
        tau_c = (h_el * norm(V_Rel) * (Re / 3)) * rho;
    else
        tau_c = (h_el * norm(V_Rel))            * rho;
    end
    K_stab_mass = K_stab_mass + tau_c * (N_d_xy * N_d_xy') * intFacQ;
        
        end %tau
    end %xi2
end %xi1

eleVec(pos.v) = eleVec(pos.v) + K_inst    * v;
eleVec(pos.v) = eleVec(pos.v) + K_visc    * v;
eleVec(pos.v) = eleVec(pos.v) + K_conv    * v;
eleVec(pos.v) = eleVec(pos.v) - K_p       * p;
eleVec(pos.v) = eleVec(pos.v) - r_f;
%eleVec(pos.v)       = eleVec(pos.v) - r_t;

eleVec(pos.p) = eleVec(pos.p) + K_p' * v;

eleVec(pos.vxt0)    = eleVec(pos.vxt0) + K_jump_vx * ( vx_t0 - vx_prev );
eleVec(pos.vyt0)    = eleVec(pos.vyt0) + K_jump_vy * ( vy_t0 - vy_prev );

eleVec(pos.v)       = eleVec(pos.v) + R_SUPG;
eleVec(pos.p)       = eleVec(pos.p) + R_PSPG;
eleVec(pos.v)       = eleVec(pos.v) + K_stab_mass * v;
 


eleMat(pos.v,pos.v) = eleMat(pos.v,pos.v) + K_inst;
eleMat(pos.v,pos.v) = eleMat(pos.v,pos.v) + K_visc;
eleMat(pos.v,pos.p) = eleMat(pos.v,pos.p) - K_p;
eleMat(pos.p,pos.v) = eleMat(pos.p,pos.v) + K_mass;

% Konvektiver Term (linearisiert)
K_conv_lin = [ R_conv_vx_d_vx , R_conv_vx_d_vy ; ...
               R_conv_vy_d_vx , R_conv_vy_d_vy   ...
             ];       
eleMat(pos.v,pos.v) = eleMat(pos.v,pos.v) + K_conv_lin;

% Sprungterme
eleMat(pos.vxt0,pos.vxt0) = eleMat(pos.vxt0,pos.vxt0) + K_jump_vx;
eleMat(pos.vyt0,pos.vyt0) = eleMat(pos.vyt0,pos.vyt0) + K_jump_vy;


% Anteile Konvektions-Stabilisierung
R_SUPG_d_v = [ R_SUPG_Impx_d_vx , R_SUPG_Impx_d_vy ; ...
               R_SUPG_Impy_d_vx , R_SUPG_Impy_d_vy   ...
             ];

R_SUPG_d_p = [ R_SUPG_Impx_d_p ; ...
               R_SUPG_Impy_d_p   ...
             ];
eleMat(pos.v,pos.v) = eleMat(pos.v,pos.v) + R_SUPG_d_v;
eleMat(pos.v,pos.p) = eleMat(pos.v,pos.p) + R_SUPG_d_p;

% Anteile Druck-Stabilisierung
eleMat(pos.p,pos.vx) = eleMat(pos.p,pos.vx) + R_PSPG_d_vx;
eleMat(pos.p,pos.vy) = eleMat(pos.p,pos.vy) + R_PSPG_d_vy;
eleMat(pos.p,pos.p)  = eleMat(pos.p,pos.p)  + R_PSPG_d_p;

% Anteile Stabilisierung Massenerhaltung
eleMat(pos.v,pos.v) = eleMat(pos.v,pos.v) + K_stab_mass;
       
testMat = eleMat;
testVec = eleVec;

    
%             % Change sign of rhs:
%             %----------------------
%             eleVec = -rhs;    
%     
%             % Set Dirichlet BC:
%             %-----------------------------
%             if ~isempty(elem.bcDir)
%                 activeBcDir = find(elem.bcDir(:,6) == 1); % Find active bcDir in current element
%                 posLocBcDir = elem.bcDir(activeBcDir,7);  % Get local position of active bcDir
%                 
%                 eleMat(posLocBcDir, :)          = 0.0;
%                 eleMat(posLocBcDir,posLocBcDir) = eye(length(activeBcDir));
%             end
%                    
%             
%             % Store the element matrix and the indices in vectors:
%             %--------------------------------------------------------------
%             
%             % Get the local indices and the values of the nonzero parts of
%             % eleMatHyb:
%             [I_Loc, J_Loc, X_elem] = find(eleMat);
%             
%             % Transfer the local indices to global ones:
%             index       = elem.indexSystem;
%             I_elem      = index(I_Loc);
%             J_elem      = index(J_Loc);
%             
%             % Get the number of nonzero entries for the current element:
%             sizeXElem   = size(I_elem,1);
            
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%




        %------------------------------------------------------------------
        %------------------------------------------------------------------
        
        
        function [ergFluid, sizeErg] = PostOut(elem, eleSolu, ~)
            % Method returns output data of the element at end of current
            % time slab:
            
            
            % Get indices to access solution and element properties:
            pos           = elem.ind;
            
            % Number of nodes for which output is returned:
            sizeErg       = pos.nNodesOut*2;
            
            % Get unknowns from the solution:
            %-----------------------------------
            velo          = eleSolu(pos.v);
            pressure      = eleSolu(pos.p);
            
            
            % Initialisation:
            %---------------------
            ergFluid      = zeros(sizeErg,4);
            
            % Get velocities and pressure:
            %------------------------------------
            ergFluid(1:4,1) = velo(pos.vxt0);  % v_x_t0         
            ergFluid(5:8,1) = velo(pos.vxt1);  % v_x_t1
            
            ergFluid(1:4,2) = velo(pos.vyt0);  % v_y_t0
            ergFluid(5:8,2) = velo(pos.vyt1);  % v_y_t1
            
            ergFluid(1:4,4) = pressure(pos.pt0Sol); % p_t0
            ergFluid(5:8,4) = pressure(pos.pt1Sol); % p_t1
            
        end
        
        
        %------------------------------------------------------------------
        %------------------------------------------------------------------
        

        function Update(elem, ~, deltaTCurr, deltaTNext, typeEleUpdate)
            
            % deltaTCurr: time width of current time  slab
            % deltaTNext: time width of next time slab
            % typeEleUpdate: 1: use gradient of current time slab to
            %                   estimate start and end values of next time
            %                   slab (initial values for first iteration)
            %                else: gradient is not used, initial values for
            %                   start and end of next time slab are thesame)
            
            % Get indices:
            pos = elem.ind;
            
            % Set discontinious values:
            elem.vRel_dis     = elem.vRel;
            
            if typeEleUpdate == 1
                deltaVRelCurr               = elem.vRel(pos.vt1) - elem.vRel(pos.vt0);
                elem.vRel(pos.vt0)          = elem.vRel(pos.vt1); 
                elem.vRel(pos.vt1)          = elem.vRel(pos.vt0) + deltaVRelCurr * (deltaTNext / deltaTCurr);
            else
                elem.vRel(pos.vt0)          = elem.vRel(pos.vt1);
            end
           
        end
        
        
        %------------------------------------------------------------------
        %------------------------------------------------------------------
        
        
        function [solUpdate , index] = UpdateSol(elem, eleSolu, ...
                                                 deltaTCurr, deltaTNext,...
                                                 typeEleUpdate)
            
            % Method called in UpdateSolution:
            
            % Get the current solution, update it for the next time slab
            % and return it as solUpdate:
            %--------------------------------------------------------------
            
            % Get indices:
            pos = elem.ind;
            
            % Get unknowns from eleSolu:
            velo        = eleSolu(pos.v);
            pressure    = eleSolu(pos.p);
            
            % Update unknowns for next time slab:
            if typeEleUpdate == 1
                deltaVeloCurr          = velo(pos.vt1) - velo(pos.vt0);
                velo(pos.vt0)          = velo(pos.vt1); 
                velo(pos.vt1)          = velo(pos.vt0) + deltaVeloCurr * (deltaTNext / deltaTCurr);
                
                deltaPressureCurr      = pressure(pos.pt1Sol) - pressure(pos.pt0Sol);
                pressure(pos.pt0Sol)   = pressure(pos.pt1Sol); 
                pressure(pos.pt1Sol)   = pressure(pos.pt0Sol) + deltaPressureCurr * (deltaTNext / deltaTCurr);
                
            else
                velo(pos.vt0)          = velo(pos.vt1);
                pressure(pos.pt0Sol)   = pressure(pos.pt1Sol);
            end
            
            
            % Initialisation of updated solution:
            solUpdate = zeros(pos.nDofEle, 1);
            
            % Set values:
            solUpdate(pos.v)    = velo;
            solUpdate(pos.p)    = pressure;
                
            index               = elem.indexSystem;     
        end
        
        
        %------------------------------------------------------------------
        %------------------------------------------------------------------

        
        function Restore(elem, ~, deltaTCurr, deltaTNext, typeEleUpdate)
            % Function resets the element properties for the dof using
            % data from previous time slab. Function is invoked by
            % "RestoreElem" if iteration did not converge and width of time
            % step is reduced.
            
            % Set indices:
            pos = elem.ind;
            
            % Restore the element data:
            if typeEleUpdate == 1
                % Use gradient of previous time slab for initial values of
                % the current time slab:
                deltaVRelPrev       = elem.vRel_dis(pos.vt1) - elem.vRel_dis(pos.vt0);
                elem.vRel(pos.vt0)  = elem.vRel_dis(pos.vt1); 
                elem.vRel(pos.vt1)  = elem.vRel(pos.vt0) + deltaVRelPrev * (deltaTNext / deltaTCurr);
                
            else
                % Initial values for start and end of time slab are the
                % same:
                elem.vRel(pos.vt0)  = elem.vRel_dis(pos.vt1);
                elem.vRel(pos.vt1)  = elem.vRel(pos.vt0);
            end
                                        
        end
        
        
        %------------------------------------------------------------------
        %------------------------------------------------------------------
        
        
        function SetEleDofGlob(elem)
            % Sets the property eleDofGlob of the current element:
            % eleDofGlob has as many cols as the maximum local dof id of
            % the element. Col: local dof id, value: global dof id
            
           
            elem.eleDofGlob    = [ 1, 21, 2, 22, 4, 24 ];
            % Aufgrund des gew�hlten Ansatzes (s. Build-Routine)
            % ist die Reihenfolge der globalen FHG hier zwingend.
                      
               
        end

        
    end % end of methods
    
    
    %------------------------------------------------------------------
    
    %------------------------------------------------------------------
    
    
    methods (Static)
        function Post(~, ~, ~)
            % Method setting element data from the solution:
            
            % -- no secondary unknowns stored for this element type --
        end
        
        
        %------------------------------------------------------------------
        %------------------------------------------------------------------
        
        
        function eleNodesDofLoc = GetEleNodesDofLoc
            % returns a matrix containing the local node numbers in the
            % first col and the local dof ids in the second col:
            
            % loc dof: 1: vxt0, 2: vxt1, 3: vyt0, 4: vyt1, 5: pt0, 6: pt1
            eleNodesDofLoc= [1, 1;
                             2, 1;
                             3, 1;
                             4, 1;
                             1, 2;
                             2, 2;
                             3, 2;
                             4, 2;
                             1, 3;
                             2, 3;
                             3, 3;
                             4, 3;
                             1, 4;
                             2, 4;
                             3, 4;
                             4, 4;
                             1, 5;
                             2, 5;
                             3, 5;
                             4, 5;
                             1, 6;
                             2, 6;
                             3, 6;
                             4, 6;];
              
        end
        
    end % end of static methods
end % end of classdefinition