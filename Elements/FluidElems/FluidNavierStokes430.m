classdef FluidNavierStokes430 < FluidElemSuperclass
    
    % Description:
    % First conversion of Navier Stokes based fluid element from S.
    % Reinstaedler to toolbox.
    
    
    
    properties (SetAccess = protected)
        
        % General properties:
        %-----------------------------------------------------------------
        type        = 430;          % identification number of element type
        name        = 'FluidNavierStokes430';  % name of current element type
        sizeXest    = 64*64;        % estimated number of non-zero entries in eleMat
        probDim     = 3;            % problem dimension
        paraOutput  = 1;            % create Paraview output for current element type (1) or not (0)
        
        
        % Indices to access the element matrix:
        %----------------------------------------------------------------
        
        % Description:
        % Sets the number of dofs used for initialization of the element
        % attributes and the positions of the dof in the element matrix
        % and the element attribute to access them in the proper
        % positons:
        % Note: Since the velocity dof are the first element dof for
        % this type, positions of velocity dof are the same in velo and
        % in eleMat and thus not given seperately. For the other 
        % unknown fields positions are not the same, compare e.g. 
        % ind.pt0 and ind.pt0Sol
        
        % Initialise values in property section to ensure, that 'ind' is
        % created only once for all elements of this type:
        ind = struct('nNodesOut', 8,...         % Number of nodes for output
                     'v'        , (1:48)',...   % pos of all velocity dof in eleMat
                     'p'        , (49:64)',...  % pos of all pressure dof
                     'vx'       , (1:16)',...   % pos of x-velocity in eleMat
                     'vy'       , (17:32)',...  % pos of y-velocity in eleMat
                     'vz'       , (33:48)',...  % pos of z-velocity in eleMat
                     'vxt0'     , (1:8)',...    % pos of vx_t0 in eleMat and velo
                     'vyt0'     , (17:24)',...  % pos of vy_t0 in eleMat and velo
                     'vzt0'     , (33:40)',...  % pos of vz_t0 in eleMat and velo
                     'vt0'      , [1:8, 17:24, 33:40]',... % pos of v_t0 in eleMat and velo
                     'vxt1'     , (9:16)',...   % pos of vx_t1 in eleMat and velo
                     'vyt1'     , (25:32)',...  % pos of vy_t1 in eleMat and velo
                     'vzt1'     , (41:48)',...  % pos of vz_t1 in eleMat and velo
                     'vt1'      , [9:16, 25:32, 41:48]',... % pos of v_t1 in eleMat and velo
                     'pt0'      , (49:56)',...  % pos of p_t0 in eleMat
                     'pt1'      , (57:64)',...  % pos of p_t1 in eleMat
                     'pt0Sol'   , (1:8)',...    % pos of p_t0 in pressure
                     'pt1Sol'   , (9:16)',...   % pos of p_t1 in pressure
                     'nDofV'    , 48,...        % total number of velo dof in current element
                     'nDofP'    , 16,...        % total number of pressure dof in current element
                     'nDofEle'  , 64)           % total number of dof for the current element
        

        % further properties are included in superclasses
    end
    
        
    %------------------------------------------------------------------
    
    %------------------------------------------------------------------
    
    
    methods
        
        function elem = FluidNavierStokes430( eleConnect    , eleCoord      , ...
                                              eleMatPara    , eleLoadPara   , ...
                                              eleBcDir      , eleBcNeu      , ...
                                              mm                              ...
                                            )

                            
            %Call the superclass constructor:
            elem = elem@FluidElemSuperclass( eleConnect    , eleCoord      , ...
                                             eleMatPara    , eleLoadPara   , ...
                                             eleBcDir      , eleBcNeu      , ...
                                             mm                              ...
                                           );

            
            % Initialise element data for dof and related data:
            %--------------------------------------------------
           	elem.vRel       = zeros(elem.ind.nDofV,1);
           	elem.vRel_dis   = zeros(elem.ind.nDofV,1);
        end
        
        
        %------------------------------------------------------------------
        %------------------------------------------------------------------
        
        
        function[eleVec,...
                 index,...
                 I_elem,...
                 J_elem,...
                 X_elem,...
                 sizeXElem] = Build( elem,...
                                     eleSolu, ...
                                     eleSoluDis, ...
                                     eleCoords, ...
                                     deltaT,...
                                     currTimeVar)
             
            
            ModBc       = currTimeVar(:, [1, 3]);
            
            % Get material data from eleMatPara:
            %--------------------------------------------------
            rho         = elem.matPara.rho;       % density
            mu          = elem.matPara.mu;        % dynamic viscosity
            
            fx          = elem.loadPara.fx;        % volume force in x-dir
            fy          = elem.loadPara.fy;        % volume force in y-dir
            fz          = elem.loadPara.fz;        % volume force in z-dir
            
            typeTimeVarLoadPara = elem.matPara.timeVar; % time variation
            
            
            % Get unknowns from the solution:
            %---------------------------------
            velo        = eleSolu(pos.v);
            velo_dis    = eleSoluDis;
            pressure    = eleSolu(pos.p);


            
            %--------------------------------------------------------------
            %--------------------------------------------------------------
            % Start of element type by Reinstaedler
            %--------------------------------------------------------------
            %--------------------------------------------------------------
            
            
            NodesGlob           = zeros(16,4);
            NodesGlob(1:8,1:3)  = eleCoords;
            NodesGlob(9:16,1:3) = eleCoords;
            NodesGlob(9:16,4)   = deltaT;

            %Gauss-Punkte und Wichtung
            %-------------------------------------------------------------
            % [Gp1D , Weight1D , NumGp1D] = WeightGP(1 , 4);
            NumGp1D     = 2;
            Gp1D        = [double(-1/sqrt(3)), double(1/sqrt(3))];   
            Weight1D    = [1, 1];
            %-------------------------------------------------------------

            M = zeros(6,6);

            %Berechnung der gemittelten Elementlaenge
            %-------------------------------------------------------------

            geom_abl_xi = [-0.125    0.125    0.125   -0.125   -0.125    0.125    0.125   -0.125;
                           -0.125   -0.125    0.125    0.125   -0.125   -0.125    0.125    0.125;
                           -0.125   -0.125   -0.125   -0.125    0.125    0.125    0.125    0.125];

            Jakobi = [(0.5 * geom_abl_xi),(0.5 * geom_abl_xi)] * NodesGlob(:,1:3);

            volume = 8 * det(Jakobi);

            delta_h = (6 * volume / pi)^(1/3);
            %-------------------------------------------------------------

            %Bereiche in der Elementmatrix
            %-------------------------------------------------
            RangeVx0 = 1:8;
            RangeVx1 = 9:16;

            RangeVy0 = 17:24;
            RangeVy1 = 25:32;

            RangeVz0 = 33:40;
            RangeVz1 = 41:48;

            RangeP0  = 49:56;
            RangeP1  = 57:64;

            RangeVx  = [ RangeVx0 RangeVx1 ];
            RangeVy  = [ RangeVy0 RangeVy1 ];
            RangeVz  = [ RangeVz0 RangeVz1 ];

            RangeV   = [ RangeVx  RangeVy  RangeVz ];

            RangeP   = [ RangeP0  RangeP1 ];
            %-------------------------------------------------   

            %Zusammenfassung der Freiwerte
            %------------------------------------------------- 
            v_x = velo(RangeVx);     
            v_y = velo(RangeVy);      
            v_z = velo(RangeVz);

            p   = pressure;
            %----------------------------------------------------

            %Relativgeschwindigkeit zum FE-Netz
            %----------------------------------------------------
% %             v_x_rel = elem.vRel(RangeVx);
% %             v_y_rel = elem.vRel(RangeVy);   
% %             v_z_rel = elem.vRel(RangeVz);
            %----------------------------------------------------
            v_x_rel = v_x;
            v_y_rel = v_y;   
            v_z_rel = v_z;
            

            %Auswertung der Sprungterme
            %----------------------------------------------------
            VxNext  = velo(RangeVx0);
            VyNext  = velo(RangeVy0);
            VzNext  = velo(RangeVz0);

            VxPrev = velo_dis(RangeVx1);
            VyPrev = velo_dis(RangeVy1);
            VzPrev = velo_dis(RangeVz1);      
            %------------------------------------------------- 

            % Get material data:
            gp_mu      = mu;
            gp_rho     = rho;
            gp_f_x     = fx;
            gp_f_y     = fy;
            gp_f_z     = fz;

            %------------------------------------------------------


            %Elementmatrix und Elementlastvektor auf Null setzen
            %---------------------------------------------------
            EleMat = zeros(64,64);
            Resi   = zeros(64,1);

            %Gravitationskraft des Fluids
            f_i_time = [gp_f_x;
                        gp_f_y;
                        gp_f_z] * (ModBc(typeTimeVarLoadPara,:) * gp_rho);

            geometry_abl = zeros(4,16);

            for j = 1 : NumGp1D
                for k = 1 : NumGp1D
                    for l = 1 : NumGp1D

                        h_1 = 1 - Gp1D(k);
                        h_2 = 1 - Gp1D(j);
                        h_3 = 1 - Gp1D(l);
                        h_4 = 1 + Gp1D(k);
                        h_5 = 1 + Gp1D(j);
                        h_6 = 1 + Gp1D(l);

                        geom_space = 0.125* [ h_1*h_2*h_3,... %Node 1
                                              h_4*h_2*h_3,... %Node 2
                                              h_4*h_5*h_3,... %Node 3
                                              h_1*h_5*h_3,... %Node 4
                                              h_1*h_2*h_6,... %Node 5
                                              h_4*h_2*h_6,... %Node 6
                                              h_4*h_5*h_6,... %Node 7
                                              h_1*h_5*h_6];   %Node 8

                        geom_abl_space = 0.125 * [-h_2*h_3,h_2*h_3,h_5*h_3,-h_5*h_3,-h_2*h_6,h_2*h_6,h_5*h_6,-h_5*h_6;                  
                                                  -h_1*h_3,-h_4*h_3,h_4*h_3,h_1*h_3,-h_1*h_6,-h_4*h_6,h_4*h_6,h_1*h_6;                  
                                                  -h_1*h_2,-h_4*h_2,-h_4*h_5,-h_1*h_5,h_1*h_2,h_4*h_2,h_4*h_5,h_1*h_5];

                        geometry_abl(4,:) = 0.5 * [ -geom_space , geom_space ];

                        for i = 1 : NumGp1D %Time

                            t_1 = 0.5 * (1 - Gp1D(i));
                            t_2 = 0.5 * (1 + Gp1D(i));

                            b = f_i_time * [t_1;
                                            t_2];

                            geometry_abl(1:3,:) = [(t_1 * geom_abl_space) , (t_2 * geom_abl_space)];

                            J = geometry_abl * NodesGlob;

                            inv_J = inv(J);
                            det_J = det(J);

                            wichtung = Weight1D(i) * Weight1D(j) * Weight1D(k) * Weight1D(l);
                            IntFac   = wichtung * det_J;

                            %--------------------------------------------------
                            Shape = [ (t_1 * geom_space) , (t_2 * geom_space) ];

                            ShapeX = inv_J(1,1:3) * geometry_abl(1:3,:);
                            ShapeY = inv_J(2,1:3) * geometry_abl(1:3,:);
                            ShapeZ = inv_J(3,1:3) * geometry_abl(1:3,:);
                            ShapeT = inv_J(4,:)   * geometry_abl;

                            ShapeDiv  = [ShapeX  ShapeY  ShapeZ];
                            %--------------------------------------------------

                            %--------------------------------------------------
                            V   = (Shape  * [v_x v_y v_z])';

                            V_T = (ShapeT * [v_x v_y v_z])';

                            V_Rel = (Shape  * [v_x_rel v_y_rel v_z_rel])';

                            V_Div = ShapeDiv * velo;

                            P     = Shape * p;
                            %--------------------------------------------------

                            %zeitliche und raumliche Ableitung des Geschwindigkeitfeldes
                            %--------------------------------------------------
                            GradV = ([ShapeX;
                                      ShapeY;
                                      ShapeZ] * [v_x  v_y  v_z])';

                            %Auswertung des Geschwindigkeitsgradienten
                            GradV_V = GradV * V;

                            H = [(Shape * GradV(1,1)), (Shape * GradV(1,2)), (Shape * GradV(1,3));
                                 (Shape * GradV(2,1)), (Shape * GradV(2,2)), (Shape * GradV(2,3));
                                 (Shape * GradV(3,1)), (Shape * GradV(3,2)), (Shape * GradV(3,3))];

                            ShapeGradV = V' * [ShapeX;
                                               ShapeY;
                                               ShapeZ];

                            Vec = ShapeT + ShapeGradV;

                            H(1, 1:16) = H(1, 1:16) + Vec;
                            H(2,17:32) = H(2,17:32) + Vec;
                            H(3,33:48) = H(3,33:48) + Vec;

                            H = H * gp_rho;

                            EleMat(RangeVx,RangeV) = EleMat(RangeVx,RangeV) + Shape' * (H(1,:) * IntFac);
                            EleMat(RangeVy,RangeV) = EleMat(RangeVy,RangeV) + Shape' * (H(2,:) * IntFac);
                            EleMat(RangeVz,RangeV) = EleMat(RangeVz,RangeV) + Shape' * (H(3,:) * IntFac);
                            %--------------------------------------------------


                            %Materialmodell
                            %--------------------------------------------------

                            ShapeD = [ShapeX ,      zeros(1,16),  zeros(1,16);
                                      zeros(1,16),  ShapeY,       zeros(1,16);
                                      zeros(1,16),  zeros(1,16),  ShapeZ;
                                      ShapeY,       ShapeX,       zeros(1,16);
                                      ShapeZ,       zeros(1,16),  ShapeX;
                                      zeros(1,16),  ShapeZ,       ShapeY];

                            E = [       (ShapeX * v_x);
                                        (ShapeY * v_y);
                                        (ShapeZ * v_z);
                                 ( 0.5*((ShapeY * v_x) + (ShapeX * v_y)) );
                                 ( 0.5*((ShapeZ * v_x) + (ShapeX * v_z)) );
                                 ( 0.5*((ShapeZ * v_y) + (ShapeY * v_z)) )];

% % %                             E_Dev  = M_dev * E(1:3);
% % %                             E_Dev2 = M_dev * E_Dev;
% % % 
% % %                             J_2_D = 0.5 * (E_Dev(1)^2 + E_Dev(2)^2 + E_Dev(3)^2) +...
% % %                                            E(4)^2     + E(5)^2     + E(6)^2;
% % % 
% % %                             if J_2_D == 0
% % %                                 J_2_D = 10^(-12);
% % %                             end

                            M(1,1) = 2 * gp_mu;
                            M(2,2) = 2 * gp_mu;
                            M(3,3) = 2 * gp_mu;
                            M(4,4) = gp_mu;
                            M(5,5) = gp_mu;
                            M(6,6) = gp_mu;

                            EleMat(RangeV,RangeV) = EleMat(RangeV,RangeV) + ShapeD'   * (M       * IntFac) * ShapeD;
                            EleMat(RangeV,RangeP) = EleMat(RangeV,RangeP) - ShapeDiv' * (Shape   * IntFac);
                            %--------------------------------------------------

                            %Masseerhaltung
                            %--------------------------------------------------
                            EleMat(RangeP,RangeV) = EleMat(RangeP,RangeV) + Shape' * (ShapeDiv * IntFac);
                            %--------------------------------------------------

                            %Stabilisierung
                            %--------------------------------------------------
                            %Stabilisierung der Impulsbilanz
                            %Die Rohdichte wird an dieser Stelle schon mit eingerechnet 
                            tau_m = 1/sqrt( (2/deltaT)^2 + (2*norm(V_Rel)/delta_h)^2 + (4*(gp_mu/gp_rho)/(delta_h^2))^2 ) * (1/gp_rho);

                            %erster Anteil der Linearisierung
                            StabMat = [H(1,:) , ShapeX;
                                       H(2,:) , ShapeY;
                                       H(3,:) , ShapeZ];

                            Vec = ShapeGradV * gp_rho;

                            StabMat2 = [ Vec        ,       zeros(1,32), ShapeX;
                                         zeros(1,16), Vec,  zeros(1,16), ShapeY;
                                         zeros(1,32),       Vec,         ShapeZ];

                            EleMat = EleMat + StabMat2' * (StabMat * (tau_m * IntFac)); 

                            %zweiter Anteil der Linearisierung 
                            %Auswertung des Residuums der Impulsbilanz

                            V_Tot = (V_T + GradV_V) * gp_rho;

                            P_Diff = [ShapeX;
                                      ShapeY;
                                      ShapeZ] * p;

                            L = V_Tot - b + P_Diff;

                            Mat = [(ShapeX * L(1)) , (ShapeX * L(2)) , (ShapeX * L(3));
                                   (ShapeY * L(1)) , (ShapeY * L(2)) , (ShapeY * L(3));
                                   (ShapeZ * L(1)) , (ShapeZ * L(2)) , (ShapeZ * L(3))];

                            Vec = Shape * (gp_rho * tau_m * IntFac);

                            EleMat(RangeV,RangeVx) = EleMat(RangeV,RangeVx) + Mat(1,:)' * Vec;
                            EleMat(RangeV,RangeVy) = EleMat(RangeV,RangeVy) + Mat(2,:)' * Vec;
                            EleMat(RangeV,RangeVz) = EleMat(RangeV,RangeVz) + Mat(3,:)' * Vec;

                            %Stabilisierung der Masseerhaltung
                            %Die Rohdichte wird an dieser Stelle schon mit eingerechnet
                            Re = (norm(V_Rel) * delta_h) / (2 * (gp_mu / gp_rho));
                            if Re < 3
                                tau_c = (delta_h * norm(V_Rel) * (Re / 3)) * gp_rho;
                            else
                                tau_c = (delta_h * norm(V_Rel))            * gp_rho;
                            end

                            EleMat(RangeV,RangeV) = EleMat(RangeV,RangeV)+...
                                                    ShapeDiv' * ShapeDiv * (tau_c * IntFac);
                            %--------------------------------------------------

                            %Auswertung des Residuums
                            %--------------------------------------------------
                            Resi(RangeVx) = Resi(RangeVx) + Shape' * ((V_Tot(1) - b(1)) * IntFac);
                            Resi(RangeVy) = Resi(RangeVy) + Shape' * ((V_Tot(2) - b(2)) * IntFac);
                            Resi(RangeVz) = Resi(RangeVz) + Shape' * ((V_Tot(3) - b(3)) * IntFac);

                            Resi(RangeV)  = Resi(RangeV)  + ShapeD'   * (2 * gp_mu * E * IntFac);
                            Resi(RangeV)  = Resi(RangeV)  - ShapeDiv' * (P             * IntFac);
                            Resi(RangeP)  = Resi(RangeP)  + Shape'    * (V_Div         * IntFac);

                            Resi          = Resi          + StabMat2' * (L     * tau_m * IntFac);
                            Resi(RangeV)  = Resi(RangeV)  + ShapeDiv' * (V_Div * tau_c * IntFac);
                            %--------------------------------------------------

                        end
                    end
                end
            end

            %Uebergangsbedingungen


            for j = 1 : NumGp1D
                for k = 1 : NumGp1D
                    for l = 1 : NumGp1D

                        h_1 = 1 - Gp1D(k);
                        h_2 = 1 - Gp1D(j);
                        h_3 = 1 - Gp1D(l);
                        h_4 = 1 + Gp1D(k);
                        h_5 = 1 + Gp1D(j);
                        h_6 = 1 + Gp1D(l);

                        geom_space = 0.125* [ h_1*h_2*h_3,... %Node 1
                                              h_4*h_2*h_3,... %Node 2
                                              h_4*h_5*h_3,... %Node 3
                                              h_1*h_5*h_3,... %Node 4
                                              h_1*h_2*h_6,... %Node 5
                                              h_4*h_2*h_6,... %Node 6
                                              h_4*h_5*h_6,... %Node 7
                                              h_1*h_5*h_6];   %Node 8

                        geom_abl_space = 0.125 * [-h_2*h_3,h_2*h_3,h_5*h_3,-h_5*h_3,-h_2*h_6,h_2*h_6,h_5*h_6,-h_5*h_6;                  
                                                  -h_1*h_3,-h_4*h_3,h_4*h_3,h_1*h_3,-h_1*h_6,-h_4*h_6,h_4*h_6,h_1*h_6;                  
                                                  -h_1*h_2,-h_4*h_2,-h_4*h_5,-h_1*h_5,h_1*h_2,h_4*h_2,h_4*h_5,h_1*h_5];

                        J = geom_abl_space * NodesGlob(1:8,1:3);

                        IntFac = Weight1D(j) * Weight1D(k) * Weight1D(l) * det(J);

                        h_10 = geom_space' * (geom_space * (gp_rho * IntFac));

                        EleMat(RangeVx0,RangeVx0) = EleMat(RangeVx0,RangeVx0) + h_10;
                        EleMat(RangeVy0,RangeVy0) = EleMat(RangeVy0,RangeVy0) + h_10;
                        EleMat(RangeVz0,RangeVz0) = EleMat(RangeVz0,RangeVz0) + h_10;

                        Resi(RangeVx0) = Resi(RangeVx0) + geom_space' * ((geom_space * (VxNext - VxPrev)) * (gp_rho * IntFac));
                        Resi(RangeVy0) = Resi(RangeVy0) + geom_space' * ((geom_space * (VyNext - VyPrev)) * (gp_rho * IntFac));
                        Resi(RangeVz0) = Resi(RangeVz0) + geom_space' * ((geom_space * (VzNext - VzPrev)) * (gp_rho * IntFac));

                    end
                end
            end


            
            %--------------------------------------------------------------
            %--------------------------------------------------------------
            % End of element type by Reinstaedler
            %--------------------------------------------------------------
            %--------------------------------------------------------------
            
            
            
                     
            

            % Set Dirichlet BC:
            %-----------------------------
            if ~isempty(elem.bcDir)
                activeBcDir = find(elem.bcDir(:,6) == 1); % Find active bcDir in current element
                posLocBcDir = elem.bcDir(activeBcDir,7);  % Get local position of active bcDir
                
                EleMat(posLocBcDir, :)          = 0.0;
                EleMat(posLocBcDir,posLocBcDir) = eye(length(activeBcDir));
            end
            
            
            % Change sign of rhs:
            %----------------------
            eleVec = -Resi;
 
            
            % Store the element matrix and the indices in vectors:
            %--------------------------------------------------------------
            
            % Get the local indices and the values of the nonzero parts of
            % eleMatHyb:
            [I_Loc, J_Loc, X_elem] = find(EleMat);
            
            % Transfer the local indices to global ones:
            index       = elem.indexSystem;
            I_elem      = index(I_Loc);
            J_elem      = index(J_Loc);
            
            % Get the number of nonzero entries for the current element:
            sizeXElem   = size(I_elem,1);

        end

        
        %------------------------------------------------------------------
        %------------------------------------------------------------------
        
        
        function [ergFluid, sizeErg] = PostOut(elem, eleSolu, ~)
            % Method returns output data of the element at end of current
            % time slab:
            
            % Get indices to access solution and element properties:
            pos         = elem.ind;
            
            % Number of nodes for which output is returned:
            sizeErg     = pos.nNodesOut;
            
            % Get unknowns from the solution:
            %-----------------------------------
            velo        = eleSolu(pos.v);
            pressure    = eleSolu(pos.p);
            
            % Initialisation:
            %---------------------
            ergFluid = zeros(sizeErg,4);
            
            % Get velocities and pressure:
            %------------------------------------
            ergFluid(:,1) = velo(pos.vxt1);  % v_x_t1
            ergFluid(:,2) = velo(pos.vyt1);  % v_y_t1
            ergFluid(:,3) = velo(pos.vzt1);  % v_z_t1
            
            ergFluid(:,4) = pressure(pos.pt1Sol); % p_t1

        end
        
        
        %------------------------------------------------------------------
        %------------------------------------------------------------------
        

        function Update(elem, ~, deltaTCurr, deltaTNext, typeEleUpdate)
            
            % deltaTCurr: time width of current time  slab
            % deltaTNext: time width of next time slab
            % typeEleUpdate: 1: use gradient of current time slab to
            %                   estimate start and end values of next time
            %                   slab (initial values for first iteration)
            %                else: gradient is not used, initial values for
            %                   start and end of next time slab are thesame)
            
            % Get indices:
            pos = elem.ind;
            
            % Set discontinious values:
            elem.vRel_dis     = elem.vRel;
            
            if typeEleUpdate == 1
                deltaVRelCurr               = elem.vRel(pos.vt1) - elem.vRel(pos.vt0);
                elem.vRel(pos.vt0)          = elem.vRel(pos.vt1); 
                elem.vRel(pos.vt1)          = elem.vRel(pos.vt0) + deltaVRelCurr * (deltaTNext / deltaTCurr);
            else
                elem.vRel(pos.vt0)          = elem.vRel(pos.vt1);
            end
           
        end
        
        
        %------------------------------------------------------------------
        %------------------------------------------------------------------
        
        
        function [solUpdate , index] = UpdateSol(elem, eleSolu, ...
                                                 deltaTCurr, deltaTNext,...
                                                 typeEleUpdate)
            
            % Method called in UpdateSolution:
            
            % Get the current solution, update it for the next time slab
            % and return it as solUpdate:
            %--------------------------------------------------------------
            
            % Get indices:
            pos = elem.ind;
            
            % Get unknowns from eleSolu:
            velo        = eleSolu(pos.v);
            pressure    = eleSolu(pos.p);
            
            % Update unknowns for next time slab:
            if typeEleUpdate == 1
                deltaVeloCurr          = velo(pos.vt1) - velo(pos.vt0);
                velo(pos.vt0)          = velo(pos.vt1); 
                velo(pos.vt1)          = velo(pos.vt0) + deltaVeloCurr * (deltaTNext / deltaTCurr);
                
                deltaPressureCurr      = pressure(pos.pt1Sol) - pressure(pos.pt0Sol);
                pressure(pos.pt0Sol)   = pressure(pos.pt1Sol); 
                pressure(pos.pt1Sol)   = pressure(pos.pt0Sol) + deltaPressureCurr * (deltaTNext / deltaTCurr);
                
            else
                velo(pos.vt0)          = velo(pos.vt1);
                pressure(pos.pt0Sol)   = pressure(pos.pt1Sol);
            end
            
            
            % Initialisation of updated solution:
            solUpdate = zeros(pos.nDofEle, 1);
            
            % Set values:
            solUpdate(pos.v)    = velo;
            solUpdate(pos.p)    = pressure;
                
            index               = elem.indexSystem;     
        end
        
        
        %------------------------------------------------------------------
        %------------------------------------------------------------------

        
        function Restore(elem, ~, deltaTCurr, deltaTNext, typeEleUpdate)
            % Function resets the element properties for the dof using
            % data from previous time slab. Function is invoked by
            % "RestoreElem" if iteration did not converge and width of time
            % step is reduced.
            
            % Set indices:
            pos = elem.ind;
            
            % Restore the element data:
            if typeEleUpdate == 1
                % Use gradient of previous time slab for initial values of
                % the current time slab:
                deltaVRelPrev       = elem.vRel_dis(pos.vt1) - elem.vRel_dis(pos.vt0);
                elem.vRel(pos.vt0)  = elem.vRel_dis(pos.vt1); 
                elem.vRel(pos.vt1)  = elem.vRel(pos.vt0) + deltaVRelPrev * (deltaTNext / deltaTCurr);
                
            else
                % Initial values for start and end of time slab are the
                % same:
                elem.vRel(pos.vt0)  = elem.vRel_dis(pos.vt1);
                elem.vRel(pos.vt1)  = elem.vRel(pos.vt0);
            end
            
        end
        
        
        %------------------------------------------------------------------
        %------------------------------------------------------------------
        
        
        function SetEleDofGlob(elem)
            % Sets the property eleDofGlob of the current element:
            % eleDofGlob has as many cols as the maximum local dof id of
            % the element. Col: local dof id, value: global dof id
            
            elem.eleDofGlob = [1, 21, 2, 22, 3, 23, 4, 24];
                
        end

        
    end % end of methods
    
    
    %------------------------------------------------------------------
    
    %------------------------------------------------------------------
    
    
    methods (Static)
        function Post(~, ~, ~)
            % Method setting element data from the solution:
            
            % -- no secondary unknowns stored for this element type --
        end
        
        
        %------------------------------------------------------------------
        %------------------------------------------------------------------
        
        
        function eleNodesDofLoc = GetEleNodesDofLoc
            % returns a matrix containing the local node numbers in the
            % first col and the local dof ids in the second col:
            
            % loc dof: 1: vxt0, 2: vxt1, 3: vyt0, 4: vyt1, 5: vzt0, 6: vzt1
            %          7: pt0,  8: pt1
            eleNodesDofLoc= [1, 1;
                             2, 1;
                             3, 1;
                             4, 1;
                             5, 1;
                             6, 1;
                             7, 1;
                             8, 1;
                             1, 2;
                             2, 2;
                             3, 2;
                             4, 2;
                             5, 2;
                             6, 2;
                             7, 2;
                             8, 2;
                             1, 3;
                             2, 3;
                             3, 3;
                             4, 3;
                             5, 3;
                             6, 3;
                             7, 3;
                             8, 3;
                             1, 4;
                             2, 4;
                             3, 4;
                             4, 4;
                             5, 4;
                             6, 4;
                             7, 4;
                             8, 4;
                             1, 5;
                             2, 5;
                             3, 5;
                             4, 5;
                             5, 5;
                             6, 5;
                             7, 5;
                             8, 5;
                             1, 6;
                             2, 6;
                             3, 6;
                             4, 6;
                             5, 6;
                             6, 6;
                             7, 6;
                             8, 6;
                             1, 7;
                             2, 7;
                             3, 7;
                             4, 7;
                             5, 7;
                             6, 7;
                             7, 7;
                             8, 7;
                             1, 8;
                             2, 8;
                             3, 8;
                             4, 8;
                             5, 8;
                             6, 8;
                             7, 8;
                             8, 8];
              
        end
        
    end % end of static methods
end % end of classdefinition