classdef LevelSet2205 < LevelSetElemSuperclass
    
% Description:
% ...

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            
properties (SetAccess = protected)

% General properties:
%-----------------------------------------------------------------
type        = 2205;                   % identification number of element type
name        = 'LevelSet2205';         % element name
sizeXest    = 8*8;                    % number of non-zero entries in eleMat
probDim     = 2;                      % problem dimension
paraOutput  = 1;                      % Paraview output for current element
                                      % 0: no Paraview output
                                      % 1:    Paraview output
                                      
density
viscosity

                                
% Indices to access the element matrix:
%----------------------------------------------------------------

ind = struct('nNodesOut', 4      ,... % Number of nodes for output
             'phi'      , (1:8)' ,... % pos of level-set dof in eleMat/eleVec
             'phi_t0'   , (1:4)' ,... % pos of level-set dof at t0
             'phi_t1'   , (5:8)' ,... % pos of level-set dof at t0
             'nDofPhi'  , 8      ,... % total numb of level-set dof
             'nDofEle'  , 8       ... % total number of dof
            );

end
           
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
methods

    
function elem = LevelSet2205( eleConnect    , eleCoord      , ...
                              eleMatPara    , eleLoadPara   , ...
                              eleBcDir      , eleBcNeu      , ...
                              mm                              ...
                            )

    % Call superclass constructor
    elem = elem@LevelSetElemSuperclass( eleConnect    , eleCoord      , ...
                                        eleMatPara    , eleLoadPara   , ...
                                        eleBcDir      , eleBcNeu      , ...
                                        mm                              ...
                                      );
    
    %dbstop in LevelSet2205 at 94
    
    % Initialise element density and viscosity
    % (depending on IC of LevelSet fct)
    %--------------------------------------------------------------------------
    elem.density   = zeros(elem.ind.nNodesOut*2,1);
    elem.viscosity = zeros(elem.ind.nNodesOut*2,1);
    
    
    rhoGas    = elem.matPara.rhoGas;
    rhoLiquid = elem.matPara.rhoLiquid;
    
    muGas     = elem.matPara.muGas;
    muLiquid  = elem.matPara.muLiquid;
    
    epsilonSmoothingArea = elem.matPara.epsilonSmoothingArea;
       
    
    eleSoluDis    = elem.soluDis;
    elePhiInitial = eleSoluDis(elem.ind.phi);
       
    for ii = 1:length(elePhiInitial)
    
        Heaviside   = GetHeavisideRegularised(elePhiInitial(ii), epsilonSmoothingArea );   
        
        densityCP   = CalcElementMaterialParameter(rhoGas, rhoLiquid, Heaviside);
        viscosityCP = CalcElementMaterialParameter(muGas,  muLiquid,  Heaviside);
        
        elem.density(ii)   = densityCP;
        elem.viscosity(ii) = viscosityCP;
               
    end

    clear feMesh eleCoords eleMatPara initialPhi Heaviside
end



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function[ eleVec , ...
          I_elem , ...
          J_elem , ...
          X_elem       ] = Build( elem        , ...
                                  eleSolu     , ...
                                  eleSoluDis  , ...
                                  eleCoords   , ...
                                  time        , ...
                                  currTimeVar   ...
                                )                

                              
%----------------------------------------------------------------
% Auslesen d. Eingabdaten
%----------------------------------------------------------------

% Element-Positionsindex
pos = elem.ind;

% Zeit
t0 = time.curr(1,1);
t1 = time.curr(1,3);
dt = t1 - t0;

% LS-Werte vorherige Zeitscheibe (Zeitscheibenende)
Phi_prev  = eleSoluDis(pos.phi_t1); 

% Loesungen vorhergehender Iterationsschritt
Phi    = eleSolu(pos.phi); % LS-Werte dieser Zeitscheibe (letzte NR-Iteration)
Phi_t0 = Phi(pos.phi_t0);

% Koordinaten (Raum-Zeit, global)
% [ x ]   [ xA xB xC xD | xE xF xG xH ]
% [ y ] = [ yA yB yC yD | yE yF yG yH ]
% [ t ]   [ t0 t0 t0 t0 | t1 t1 t1 t1 ]

X = [ eleCoords.t0(:,1:2)', eleCoords.t1(:,1:2)'; ...
      t0 t0 t0 t0         , t1 t1 t1 t1           ...
    ];

%----------------------------------------------------------------
% Elementvolumen
%----------------------------------------------------------------

% Lokal-abgeleitete r�uml. Ansatzfunktionen (bei xi1 = xi2 = 0)
%                   [ dN/dxi1 | dN/dxi2 ]
N_Sp_d_Xi0  = 1/4 * [ (-1)*(1), (1)*(-1); ...
                      (+1)*(1), (1)*(-1); ...
                      (+1)*(1), (1)*(+1); ... 
                      (-1)*(1), (1)*(+1)  ...
                    ];

J_Sp_Xi0     = X(1:2,1:4) * N_Sp_d_Xi0;
det_J_Sp_Xi0 = det(J_Sp_Xi0);

Vol_el       = 4 * det_J_Sp_Xi0;
h_el         = sqrt( 4*Vol_el / pi );


%----------------------------------------------------------------
% Initialisierungen
%----------------------------------------------------------------

eleMat = zeros( pos.nDofEle, pos.nDofEle ); % Element-Matrix (Tangentenmatrix)
eleVec = zeros( pos.nDofEle, 1           ); % Element-Vektor

R_LS_jump     = zeros( pos.nDofPhi/2,      1        );
K_LS_jump     = zeros( pos.nDofPhi/2, pos.nDofPhi/2 );

R_LS          = zeros( pos.nDofPhi,      1          );

K_LS_Phi      = zeros( pos.nDofPhi, pos.nDofPhi     );
% K_LS_v        = zeros( pos.nDofPhi, pos.nDofV       );

R_LS_stab     = zeros( pos.nDofPhi,      1          );
K_LS_stab_Phi = zeros( pos.nDofPhi, pos.nDofPhi     );
% K_LS_stab_v   = zeros( pos.nDofPhi, pos.nDofV       );

%----------------------------------------------------------------
% Numerische Integration
%----------------------------------------------------------------

% Gau�punkte
gp_coord  = [ -1/sqrt(3) , 1/sqrt(3) ];
gp_weight = [     1      ,    1      ];


% Integrationsschleifen
for kk = 1:2 % xi1: lokaler Raum
    xi1 = gp_coord(kk);
    
    for ll = 1:2 % xi2: lokaler Raum
        xi2 = gp_coord(ll);
      
        % R�uml. Ansatzfunktionen
        N_Sp        = 1/4 * [ (1-xi1)*(1-xi2); ...
                              (1+xi1)*(1-xi2); ...
                              (1+xi1)*(1+xi2); ... 
                              (1-xi1)*(1+xi2)  ...
                            ];
        
        % Lokal-abgeleitete r�uml. Ansatzfunktionen
        %                   [     dN/dxi1 |    dN/dxi2  ]
        N_Sp_d_Xi   = 1/4 * [ (-1)*(1-xi2), (1-xi1)*(-1); ...
                              (+1)*(1-xi2), (1+xi1)*(-1); ...
                              (+1)*(1+xi2), (1+xi1)*(+1); ... 
                              (-1)*(1+xi2), (1-xi1)*(+1)  ...
                            ];     
                        
        % Jakobi-Matrix (Raum-Gebiet am Zeitscheibenanfang)
        J_Sp        = X(1:2,1:4) * N_Sp_d_Xi;
        det_J_Sp    = det(J_Sp);
        
        % Integrationsfaktor Raumgebiet Zeitscheibenanfang
        intFacOmega = gp_weight(kk) * gp_weight(ll) * det_J_Sp;      

        % Sprungterme in eleVec
        R_LS_jump = R_LS_jump + N_Sp * ((N_Sp' * (Phi_t0 - Phi_prev)) * intFacOmega);
        
        % Sprungterme in eleMat
        K_LS_jump = K_LS_jump + N_Sp * ( N_Sp' * intFacOmega);

        
        for nn = 1:2 % tau: lokale Zeit
            tau = gp_coord(nn);
            
                % Ansatzfunktionen
                N       = 1/8 * [ (1-xi1)*(1-xi2)*(1-tau); ...
                                  (1+xi1)*(1-xi2)*(1-tau); ...
                                  (1+xi1)*(1+xi2)*(1-tau); ... 
                                  (1-xi1)*(1+xi2)*(1-tau); ...
                                  (1-xi1)*(1-xi2)*(1+tau); ...
                                  (1+xi1)*(1-xi2)*(1+tau); ...
                                  (1+xi1)*(1+xi2)*(1+tau); ...
                                  (1-xi1)*(1+xi2)*(1+tau)  ...
                                ];
                            
                % Lokal-Abgeleitete Ansatzfunktionen: 
                %               [        dN/dxi1      |       dN/dxi2       |        dN/dtau      ] 
                N_d_Xi  = 1/8 * [ (-1)*(1-xi2)*(1-tau), (1-xi1)*(-1)*(1-tau), (1-xi1)*(1-xi2)*(-1); ...
                                  (+1)*(1-xi2)*(1-tau), (1+xi1)*(-1)*(1-tau), (1+xi1)*(1-xi2)*(-1); ...
                                  (+1)*(1+xi2)*(1-tau), (1+xi1)*(+1)*(1-tau), (1+xi1)*(1+xi2)*(-1); ... 
                                  (-1)*(1+xi2)*(1-tau), (1-xi1)*(+1)*(1-tau), (1-xi1)*(1+xi2)*(-1); ...
                                  (-1)*(1-xi2)*(1+tau), (1-xi1)*(-1)*(1+tau), (1-xi1)*(1-xi2)*(+1); ...
                                  (+1)*(1-xi2)*(1+tau), (1+xi1)*(-1)*(1+tau), (1+xi1)*(1-xi2)*(+1); ...
                                  (+1)*(1+xi2)*(1+tau), (1+xi1)*(+1)*(1+tau), (1+xi1)*(1+xi2)*(+1); ...
                                  (-1)*(1+xi2)*(1+tau), (1-xi1)*(+1)*(1+tau), (1-xi1)*(1+xi2)*(+1)  ...
                               ];

                % Jakobi-Matrix (Raum-Zeit Gebiet)
                J_SpTi        = X * N_d_Xi;
                det_J_SpTi    = det(J_SpTi);

                % Integrationsfaktor Raum-Zeit Gebiet
                intFacQ  = gp_weight(kk) * gp_weight(ll) * gp_weight(nn) * det_J_SpTi;

                % Inverse Jakobi-Matrix
                inv_J_SpTi    = inv(J_SpTi);        
                inv_J_SpTi_x  = inv_J_SpTi(:,1);
                inv_J_SpTi_y  = inv_J_SpTi(:,2);
                inv_J_SpTi_t  = inv_J_SpTi(:,3);

                % Global-Abgeleitet Ansatzfunktionen
                N_d_x   = N_d_Xi * inv_J_SpTi_x;
                N_d_y   = N_d_Xi * inv_J_SpTi_y;
                N_d_t   = N_d_Xi * inv_J_SpTi_t;
    
                % Geschwindigkeiten
                y    = N' * X(2,:)';
                
                vx   = 0.015 * ( 4 * ( y/0.01 - (y/0.01)^2 ) );
                vx_t = vx * currTimeVar(elem.loadPara.timeVar,[1,3]);
                vx   = N' * [ vx_t(1)*ones(4,1) ; ...
                              vx_t(2)*ones(4,1)   ...
                               ];
                vy = 0;
                v  = sqrt( vx^2 + vy^2 );
                
                % Level-Set Funktion            
                phi_d_x = N_d_x' * Phi;
                phi_d_y = N_d_y' * Phi;
                phi_d_t = N_d_t' * Phi;


                % DGL LevelSet -----------------------------------------------%
            
                % Instationaerer Teil                          
                L_LS_inst       = phi_d_t;
                L_LS_inst_d_Phi = N_d_t';

                % Konvektiver Teil
                L_LS_conv_x       = vx * phi_d_x;
                L_LS_conv_x_d_Phi = vx * N_d_x';
%                 L_LS_conv_x_d_vx  = N' * phi_d_x;

                L_LS_conv_y       = vy * phi_d_y;
                L_LS_conv_y_d_Phi = vy * N_d_y';
%                 L_LS_conv_y_d_vy  = N' * phi_d_y;

                L_LS_conv       =  L_LS_conv_x       + L_LS_conv_y;
                L_LS_conv_d_Phi =  L_LS_conv_x_d_Phi + L_LS_conv_y_d_Phi;
%                 L_LS_conv_d_v   = [L_LS_conv_x_d_vx  , L_LS_conv_y_d_vy];

                % Zusammenfassung
                L_LS       = L_LS_inst       + L_LS_conv;
%                 L_LS_d_vx  =                   L_LS_conv_x_d_vx;
%                 L_LS_d_vy  =                   L_LS_conv_y_d_vy;
%                 L_LS_d_v   =                   L_LS_conv_d_v;
                L_LS_d_Phi = L_LS_inst_d_Phi + L_LS_conv_d_Phi;
            
                % Residuen ---------------------------------------------------%

                R_LS = R_LS + N * (L_LS * intFacQ);

                % Linearisierungen -------------------------------------------%

                K_LS_Phi = K_LS_Phi + N * (L_LS_d_Phi * intFacQ);          
%                 K_LS_v   = K_LS_v   + N * (L_LS_d_v   * intFacQ);
                  
            
                % Stabilisierung (GLS) ---------------------------------------% 
                tau_LS_stab = 1/sqrt( (2/dt)^2 + (2*norm(v)/h_el)^2 );


                W_LS_stab       = N_d_t + N_d_x * vx + N_d_y * vy;
%                 W_LS_stab_d_vx  =         N_d_x * N';
%                 W_LS_stab_d_vy  =                      N_d_y * N';           

                R_LS_stab       =   R_LS_stab ...
                                  + W_LS_stab * (L_LS       * tau_LS_stab * intFacQ);

                K_LS_stab_Phi   =   K_LS_stab_Phi ...
                                  + W_LS_stab * (L_LS_d_Phi * tau_LS_stab * intFacQ);

%                 K_LS_stab_vx    = W_LS_stab_d_vx * L_LS + W_LS_stab * L_LS_d_vx;
%                 K_LS_stab_vy    = W_LS_stab_d_vy * L_LS + W_LS_stab * L_LS_d_vy;               
% 
%                 K_LS_stab_v     = K_LS_stab_v + [ K_LS_stab_vx , K_LS_stab_vy ];        

        end % tau  
    end % xi2
end % xi1



%----------------------------------------------------------------
% Abspeichern Teilmatrizen
%----------------------------------------------------------------

% Element-Vektor (Residuum)
%-----------------------------------
eleVec(pos.phi)    = eleVec(pos.phi)    + R_LS;
eleVec(pos.phi)    = eleVec(pos.phi)    + R_LS_stab;
eleVec(pos.phi_t0) = eleVec(pos.phi_t0) + R_LS_jump;

eleVec = - eleVec;


% Element-Matrix (Tangenten-Matrix)
%-----------------------------------
eleMat(pos.phi   ,pos.phi   ) = eleMat(pos.phi   ,pos.phi   ) + K_LS_Phi;
eleMat(pos.phi   ,pos.phi   ) = eleMat(pos.phi   ,pos.phi   ) + K_LS_stab_Phi;
eleMat(pos.phi_t0,pos.phi_t0) = eleMat(pos.phi_t0,pos.phi_t0) + K_LS_jump;


% Abspeichern (der nicht-Null Eintr�ge)
[ I_loc, J_loc, X_elem ] = find(eleMat);

I_elem = elem.indexSystem(I_loc);
J_elem = elem.indexSystem(J_loc);
   

end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function Post(elem, eleSolu, ~, ~ )
% Nachlaufrechnung

pos = elem.ind;

X   = elem.eleCoord_t1;

Phi = eleSolu(pos.phi);

% Gausspunkte
%             xi1, xi2, tau
cp_coord  = [ -1 , -1 , -1 ; ...
               1 , -1 , -1 ; ...
               1 ,  1 , -1 ; ...
              -1 ,  1 , -1 ; ...
              -1 , -1 ,  1 ; ...
               1 , -1 ,  1 ; ...
               1 ,  1 ,  1 ; ...
              -1 ,  1 ,  1 ; ...
            ];

% Integrationsschleife

for ii = 1:size(cp_coord,1)
    
    xi1 = cp_coord(ii,1);
    xi2 = cp_coord(ii,2);
    tau = cp_coord(ii,3);

    % Lokal-abgeleitete r�uml. Ansatzfunktionen
    %                   [     dN/dxi1 |    dN/dxi2  ]
    N_Sp_d_Xi   = 1/4 * [ (-1)*(1-xi2), (1-xi1)*(-1); ...
                          (+1)*(1-xi2), (1+xi1)*(-1); ...
                          (+1)*(1+xi2), (1+xi1)*(+1); ... 
                          (-1)*(1+xi2), (1-xi1)*(+1)  ...
                        ];     
        
    % Lokal-Abgeleitete Ansatzfunktionen: 
    %               [        dN/dxi1      |       dN/dxi2       |        dN/dtau      ] 
    N_d_Xi  = 1/8 * [ (-1)*(1-xi2)*(1-tau), (1-xi1)*(-1)*(1-tau), (1-xi1)*(1-xi2)*(-1); ...
                      (+1)*(1-xi2)*(1-tau), (1+xi1)*(-1)*(1-tau), (1+xi1)*(1-xi2)*(-1); ...
                      (+1)*(1+xi2)*(1-tau), (1+xi1)*(+1)*(1-tau), (1+xi1)*(1+xi2)*(-1); ... 
                      (-1)*(1+xi2)*(1-tau), (1-xi1)*(+1)*(1-tau), (1-xi1)*(1+xi2)*(-1); ...
                      (-1)*(1-xi2)*(1+tau), (1-xi1)*(-1)*(1+tau), (1-xi1)*(1-xi2)*(+1); ...
                      (+1)*(1-xi2)*(1+tau), (1+xi1)*(-1)*(1+tau), (1+xi1)*(1-xi2)*(+1); ...
                      (+1)*(1+xi2)*(1+tau), (1+xi1)*(+1)*(1+tau), (1+xi1)*(1+xi2)*(+1); ...
                      (-1)*(1+xi2)*(1+tau), (1-xi1)*(+1)*(1+tau), (1-xi1)*(1+xi2)*(+1)  ...
                   ];

    % Jakobi-Matrix (Raum-Gebiet am Zeitscheibenanfang)
    J_Sp            = X(1:2,1:4) * N_Sp_d_Xi;
    det_J_Sp        = det(J_Sp);
    elem.detJSp(ii) = det_J_Sp;
    
    % Jakobi-Matrix (Raum-Zeit Gebiet)
    J_SpTi            = X * N_d_Xi;
    det_J_SpTi        = det(J_SpTi);
    elem.detJSpTi(ii) = det_J_SpTi;
    
    
    N  = 1/8 * [ (1-xi1)*(1-xi2)*(1-tau); ...
                 (1+xi1)*(1-xi2)*(1-tau); ...
                 (1+xi1)*(1+xi2)*(1-tau); ... 
                 (1-xi1)*(1+xi2)*(1-tau); ...
                 (1-xi1)*(1-xi2)*(1+tau); ...
                 (1+xi1)*(1-xi2)*(1+tau); ...
                 (1+xi1)*(1+xi2)*(1+tau); ...
                 (1-xi1)*(1+xi2)*(1+tau)  ...
               ];
           
   phi_CP = N' * Phi;
   
   Heaviside = CalcHeavisideRegularised(phi_CP, epsilonSmoothingArea );
               
   rhoGas    = elem.matPara.rhoGas;
   rhoLiquid = elem.matPara.rhoLiquid;
   
   muGas     = elem.matPara.muGas;
   muLiquid  = elem.matPara.muLiquid;
   
   densityCP   = CalcElementMaterialParameter(rhoGas, rhoLiquid, Heaviside);
   viscosityCP = CalcElementMaterialParameter(muGas,  muLiquid,  Heaviside);
   
   elem.density(ii)   = densityCP;
   elem.viscosity(ii) = viscosityCP;
   
end

end
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [ergMat, sizeErg] = PostOut(elem, eleSolu, ~)
% Auslesen der Loesungswerte des aktuellen Elementes aus dem
% Gesamtloesungsvektor.

pos = elem.ind;

% Number of nodes for which output is returned
sizeErg = pos.nNodesOut*2;

% Solution of current element from overall solution vector
Phi_t0   = eleSolu(pos.phi_t0);
Phi_t1   = eleSolu(pos.phi_t1);

rho       = elem.density;
mu        = elem.viscosity;

% Element solution for output
ergMat      = zeros(sizeErg,3);

ergMat(1:4,1) = Phi_t0;      
ergMat(5:8,1) = Phi_t1;

ergMat(1:4,2) = rho(1:4);
ergMat(5:8,2) = rho(5:8);

ergMat(1:4,3) = mu(1:4);
ergMat(5:8,3) = mu(5:8); 

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [solUpdate , index] = UpdateSol( elem          , ...
                                          eleSolu       , ...
                                          deltaTCurr    , ...
                                          deltaTNext    , ...
                                          typeEleUpdate   ...
                                        )
% Ermittlung der Startwerte fuer naechste NR-Iteration
% Funktion wird in 'UpdateSolution' aufgerufen

% deltaTCurr: time width of current time  slab
% deltaTNext: time width of next time slab
% typeEleUpdate: 1:    use gradient of current time slab to estimate end values
%                      of next time slab which serve as initial values for next
%                      NR iteration
%                else: gradient is not used, initial values for
%                      start and end of next time slab are the same

pos = elem.ind;

% Get unknowns from eleSolu:
phi_t0 = eleSolu(pos.phi_t0);
phi_t1 = eleSolu(pos.phi_t1);

% Update unknowns for next time slab:
if typeEleUpdate == 1

    deltaPhiCurr =   phi_t1 - phi_t0;
    phi_t0       =   phi_t1; 
    phi_t1       =   phi_t0 + deltaPhiCurr * (deltaTNext / deltaTCurr);

else
    phi_t0 = phi_t1;

end

% Initialisation of updated solution:
solUpdate = zeros(pos.nDofEle, 1);

% Set values:
solUpdate(pos.phi)    = [phi_t0, phi_t1];

index               = elem.indexSystem;   

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function Update(elem, ~, deltaTCurr, deltaTNext, typeEleUpdate)
% Ermittlung der Startwerte fuer naechste NR-Iteration der Variablen der
% Nachlaufrechnung
    
% Update der Nachlaufgr��en muss nur bei gemischter Formulierung erfolgen.

% deltaTCurr: time width of current time  slab
% deltaTNext: time width of next time slab
% typeEleUpdate: 1:    use gradient of current time slab to estimate end values
%                      of next time slab which serve as initial values for next
%                      NR iteration
%                else: gradient is not used, initial values for
%                      start and end of next time slab are the same


% % Update secondary unknowns for next time slab:
% if typeEleUpdate == 1
% 
%     deltaHeatFlux        =   elem.heatFlux(5:8,:) - elem.heatFlux(1:4,:);
%     elem.heatFlux(1:4,:) =   elem.heatFlux(5:8,:); 
%     elem.heatFlux(5:8,:) =   elem.heatFlux(5:8,:) ...
%                            + deltaHeatFlux * (deltaTNext / deltaTCurr);
% 
% else
%     elem.heatFlux(1:4,:)  = elem.heatFlux(5:8,:);
% 
% end


end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function Restore(elem, ~, deltaTCurr, deltaTNext, typeEleUpdate)
% Funktion wird von 'RestoreElem' aufgerufen im Fall, dass im aktuellen
% Loesungsschritt keine Konvergenz stattgefunden hat und die Zeitschritte
% reduziert werden muss.
% F�r die Zeitscheibe mit nun reduziertem Zeitschritt werden die Startwerte
% der Nachlaufvariablen f�r den ersten NR-Schritt ermittelt.

% -- no secondary unknowns stored for this element type --

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function SetEleDofGlob(elem, ~)
% Funktion stellt den Zusammenhang zwischen lokalen und globalen FHG her.
% Es wird ein Vektor erzeugt, der jedem lokalen FHG den korrespondierenden
% globalen FHG zuweist.
% Die Anzahl an Spalten entspricht der Anzahl an vorhandenen lokalen FHGen.
%
% Spaltennr.: local dof id
% Wert:       global dof id


elem.eleDofGlob    = [ 6, 26 ];
% Aufgrund des gew�hlten Ansatzes (s. Build-Routine)
% ist die Reihenfolge der globalen FHG hier zwingend.


    
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

end % end of methods

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

methods (Static)

function eleNodesDofLoc = GetEleNodesDofLoc
% Function returns a matrix containing local node numbers and local dof ids

%                [ local node numb | local dof id ]
eleNodesDofLoc = [      1          ,    1; ...        % loc dof id 1: phi_t0
                        2          ,    1; ...
                        3          ,    1; ...
                        4          ,    1; ...
                        1          ,    2; ...        % loc dof id 2: phi_t1
                        2          ,    2; ...
                        3          ,    2; ...
                        4          ,    2  ...
                 ];

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [ Heaviside ] = CalcHeavisideRegularised(levelSetValue, epsilonSmoothingArea )
% Function evluating regularised Heaviside function for given level set
% value and smoothing area. The input "levelSetValue" can be either a
% scalar or a vector. The output "Heaviside" will have the same size than
% the input "levelSetValue".
    
    if     levelSetValue <= - epsilonSmoothingArea
        
            Heaviside = 0;
            
    elseif levelSetValue >= epsilonSmoothingArea
        
        Heaviside = 1;
        
    else
        
        Heaviside = 0.5 * ( 1 + levelSetValue/epsilonSmoothingArea             ...
                              + 1/pi                                           ...
                              * sin( levelSetValue * pi/epsilonSmoothingArea ) ...
                          );
                      
    end %if
    
end %function

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function matPara = CalcElementMaterialParameter(matPara1, matPara2, Heaviside)
% Function calculating material parameter at the location for which the
% Heaviside value is given.

        matPara = matPara1 * Heaviside + matPara2 * (1-Heaviside);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

end % end of static methods

end % end of classdefinition