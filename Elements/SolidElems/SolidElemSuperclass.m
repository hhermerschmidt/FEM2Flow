classdef SolidElemSuperclass < ElemSuperclass

    properties (SetAccess = protected)
        
        %Physikalische Beschreibungsgroessen
        %------------------------------------------------------------
      	sigma           % stresses
        sigma_dis       %
        displace        % displacements
       	displace_dis    %
    end
    
    
    %------------------------------------------------------------------
    %------------------------------------------------------------------
    
    
    methods
        
        % Constructor
        function elem = SolidElemSuperclass( eleConnect    , ...
                                             eleCoord      , ...
                                             eleMatPara    , ...
                                             eleLoadPara   , ...
                                             eleBcDir      , ...
                                             eleBcNeu      , ...
                                             numGlobInput    ...
                                           )

                                          
            %Call superclass constructor:
            elem = elem@ElemSuperclass ( eleConnect    , ...
                                         eleCoord      , ...
                                         eleMatPara    , ...
                                         eleLoadPara   , ...
                                         eleBcDir      , ...
                                         eleBcNeu      , ...
                                         numGlobInput    ...
                                       ); 
        end
        
       
    %------------------------------------------------------------------
    %------------------------------------------------------------------
    
    
        function Output = OutputFieldsInit(elem)
            
            % Function initialises output fields for Paraview output
            
            
            % Initialise the output fields for the current element:
            %--------------------------------------------------------------
            Output.filename     = elem.name; % name of the output file
            
            Output.nNodesEle    = size(elem.eleNodes_t0,2); % number of nodes per element
            
            if elem.probDim == 2 
                Output.ele_type = 'Quadrilateral';   % 2D element
                Output.outId    = [0 1 2 3];         % Help variable 
            elseif elem.probDim == 3
                Output.ele_type = 'Hexahedron';      % 3D element
                Output.outId    = [0 1 2 3 4 5 6 7]; % Help variable 
            end
             
            Output.nodesOut     = [];
            
            Output.coord        = []; % coordinates of the nodes
            Output.connect      = []; % connectivity of the element
            
            Output.z1           = []; % counter help variable
            Output.z2           = []; % counter help variable

            
            % 'Cell'-type variables only possess one value per element
            Output.cells.num    = 5; %number of cell-variables occurring in the output
            Output.cells.name   = cell(1,Output.cells.num);
            Output.cells.type   = cell(1,Output.cells.num);
            Output.cells.data   = cell(1,Output.cells.num);
            
            % 'Scalar'-type variables are '1-entry' variables changing in
            % the element
            Output.scalars.num  = 0; %number of scalar-variables occurring in the output
            Output.scalars.name = cell(1,Output.scalars.num);
            Output.scalars.type = cell(1,Output.scalars.num);
            Output.scalars.data = cell(1,Output.scalars.num);
            
            % 'Vector'-type variables are '3-entry' variables; each entry
            % changes in the elment
            Output.vectors.num  = 2; %number of vector-variables occurring in the output
            Output.vectors.name = cell(1,Output.vectors.num);
            Output.vectors.type = cell(1,Output.vectors.num);
            Output.vectors.data = cell(1,Output.vectors.num);
            
            % 'Tensor'-type varibales are '9-entry' variables; each entry
            % changes in the element
            Output.tensors.num  = 1; %number of tensor-variables occurring in the output
            Output.tensors.name = cell(1,Output.tensors.num);
            Output.tensors.type = cell(1,Output.tensors.num);
            Output.tensors.data = cell(1,Output.tensors.num);
            

            % Material parameters:
            %Cell-Data:
            Output.cells.name{1,1}  = 'rho';
            Output.cells.type{1,1}  = 'Float';

            Output.cells.name{1,2}  = 'f_x';
            Output.cells.type{1,2}  = 'Float'; 

            Output.cells.name{1,3}  = 'f_y';
            Output.cells.type{1,3}  = 'Float';

            Output.cells.name{1,4}  = 'f_z';
            Output.cells.type{1,4}  = 'Float'; 

            Output.cells.name{1,5}  = 'Element';
            Output.cells.type{1,5}  = 'Float';


            %Scalar-Data
            % --no scalar data--

            %Vector-Data
            Output.vectors.name{1,1} = 'velocity';
            Output.vectors.type{1,1} = 'Float'; 

            Output.vectors.name{1,2} = 'displacement';
            Output.vectors.type{1,2} = 'Float';  

            %Tensor-Data
            Output.tensors.name{1,1} = 'pk2_stress';
            Output.tensors.type{1,1} = 'Float'; 
        end
        
        
        %------------------------------------------------------------------
        %------------------------------------------------------------------
        
        
        function Output = GetOutputData(elem, ...
                                        Output, ...
                                        eleSolu, ...
                                        eleCoords,...
                                        eleMatPara)
                                    
            % Get the output data from postprocessing and store it:
            
            % Get the post-processed data of the current element:
            [ergSolid, hh] = elem.PostOut(eleSolu, eleCoords); % hh = Zahl der Ergebnisssaetze je Elem

            % Get index for data storage:
            index = (Output.z1 : (Output.z1 + hh - 1));

            % Store node coordinates
            Output.coord(index,1) = eleCoords(:,1);
            Output.coord(index,2) = eleCoords(:,2);
            Output.coord(index,3) = eleCoords(:,3);
            
            % Store connectivity:
            Output.connect(Output.z2,:) = Output.outId;

            % Store cell type output data
            Output.cells.data{1,1}(Output.z2,1) = eleMatPara.rho;
            Output.cells.data{1,2}(Output.z2,1) = eleMatPara.fx;
            Output.cells.data{1,3}(Output.z2,1) = eleMatPara.fy;
            Output.cells.data{1,4}(Output.z2,1) = eleMatPara.fz;
            Output.cells.data{1,5}(Output.z2,1) = elem.numGlob;
            
            % Store scalar type output data
            % --no scalar data--
            
            % Store vector type output data
            Output.vectors.data{1,1}(index,1:3) = ergSolid(:,1:3);
            Output.vectors.data{1,2}(index,1:3) = ergSolid(:,4:6);
            
            % Store tensor type output data
            Output.tensors.data{1,1}(index,1:6) = ergSolid(:,7:12);

            % Update indices for data storage:
            Output.z2    = Output.z2    + 1;
            Output.z1    = Output.z1    + hh;
            Output.outId = Output.outId + hh;
        end
    end    
end