classdef SolidStandard131 < SolidElemSuperclass
    
    properties (SetAccess = protected)
        
        % General properties:
        %-----------------------------------------------------------------
        type        = 131;          % identification number of element type
        name        = 'SolidStandard131';  % name of current element type
        sizeXest    = 48*48;        % estimated number of non-zero entries in eleMat
        probDim     = 3;            % problem dimension
        paraOutput  = 1;            % create Paraview output for current element type (1) or not (0)
        
        
        % Indices to access the element matrix:
        %----------------------------------------------------------------
        
        % Description:
        % Sets the number of dofs used for initialization of the element
        % attributes and the positions of the dof in the element matrix
        % and the element attribute to access them in the proper
        % positons:
        % Note: Since the velocity dof are the first element dof for
        % this type, positions of velocity dof are the same in velo and
        % in eleMat and thus not given seperately. For the other 
        % unknown fields positions are not the same, compare e.g. 
        % ind.St0 and ind.St0Sol
        
        % Initialise values in property section to ensure, that 'ind' is
        % created only once for all elements of this type:
        ind = struct('nNodesOut', 8,...         % Number of nodes for output
                     'v'        , (1:48)',...   % pos of all velocity dof in eleMat
                     'S'        , (49:84)',...  % pos of all stress dof
                     'vx'       , (1:16)',...   % pos of x-velocity in eleMat
                     'vy'       , (17:32)',...  % pos of y-velocity in eleMat
                     'vz'       , (33:48)',...  % pos of z-velocity in eleMat
                     'vxt0'     , (1:8)',...    % pos of vx_t0 in eleMat and elem.velo
                     'vyt0'     , (17:24)',...  % pos of vy_t0 in eleMat and elem.velo
                     'vzt0'     , (33:40)',...  % pos of vz_t0 in eleMat and elem.velo
                     'vt0'      , [1:8, 17:24, 33:40]',... % pos of v_t0 in eleMat and elem.velo
                     'vxt1'     , (9:16)',...   % pos of vx_t1 in eleMat and elem.velo
                     'vyt1'     , (25:32)',...  % pos of vy_t1 in eleMat and elem.velo
                     'vzt1'     , (41:48)',...  % pos of vz_t1 in eleMat and elem.velo
                     'vt1'      , [9:16, 25:32, 41:48]',... % pos of v_t1 in eleMat and elem.velo
                     'St0'      , (49:66)',...  % pos of sigma_t0 in eleMat
                     'St0Sol'   , (1:18)',...   % pos of sigma_t0 in elem.sigma
                     'St1Sol'   , (19:36)',...  % pos of sigma_t1 in elem.sigma
                     'nDofV'    , 48,...        % number of velocity dof
                     'nDofS'    , 36,...        % number of stress dof
                     'nDofMain' , 48,...        % number of element dof in the global system
                     'nDofCond' , 36,...        % number of dof condensed on element level
                     'nDofEle'  , 84,...        % total number of dof for the current element
                     'main'     , ( 1:48)',...  % positions of the main dof in the uncondensed element matrix
                     'cond'     , (49:84)')     % position of dof in the element matrix which are later condensed
        
        % further properties are included in superclasses
    end
    
        
    %------------------------------------------------------------------
    
    %------------------------------------------------------------------
    
    
    methods
        
        function elem = SolidStandard131( eleConnect    , eleCoord      , ...
                                          eleMatPara    , eleLoadPara   , ...
                                          eleBcDir      , eleBcNeu      , ...
                                          mm                              ...
                                        )

                            
            %Call the superclass constructor:
            elem = elem@SolidElemSuperclass( eleConnect    , eleCoord      , ...
                                             eleMatPara    , eleLoadPara   , ...
                                             eleBcDir      , eleBcNeu      , ...
                                             mm                              ...
                                           );
            
            % Initialise element data for dof and related data:
            %--------------------------------------------------
           	elem.sigma          = zeros(elem.ind.nDofS,1);
           	elem.displace       = zeros(elem.ind.nDofV,1);
          	
           	elem.sigma_dis      = zeros(elem.ind.nDofS,1);
           	elem.displace_dis   = zeros(elem.ind.nDofV,1);
        end
        
        
        %------------------------------------------------------------------
        %------------------------------------------------------------------
        
        
        function[eleVecHyb,...
                 index,...
                 I_elem,...
                 J_elem,...
                 X_elem,...
                 sizeXElem] = Build( elem,...
                                     eleSolu, ...
                                     eleSoluDis, ...
                                     eleCoords, ...
                                     deltaT,...
                                     currTimeVar)
             
            % Get the position structure to access element matrix & rhs:
            pos = elem.ind;

            %Initialise the element matrix and the right hand side:
            eleMat      = zeros(pos.nDofEle,pos.nDofEle);
            rhs         = zeros(pos.nDofEle,1);
            
            
            % Get material data from eleMatPara:
            %--------------------------------------------------
            CInvers     = elem.matPara.matCinv;   % inverse stiffness matrix
            alpha       = elem.matPara.alpha;     % damping coefficient
            beta        = elem.matPara.beta;      % damping coefficient
            rho         = elem.matPara.rho;       % density
            
            fx          = elem.loadPara.fx;        % volume force in x-dir
            fy          = elem.loadPara.fy;        % volume force in y-dir
            fz          = elem.loadPara.fz;        % volume force in z-dir
            
            
            % Get unknowns from the solution:
            %---------------------------------
            velo        = eleSolu;
            velo_dis    = eleSoluDis;

            
            % Get transformation data:
           %-------------------------

            % Local coordinates of nodes:
            NodesXi1 = [-1.0, 1.0, 1.0,-1.0,-1.0, 1.0, 1.0,-1.0];
            NodesXi2 = [-1.0,-1.0, 1.0, 1.0,-1.0,-1.0, 1.0, 1.0];
            NodesXi3 = [-1.0,-1.0,-1.0,-1.0, 1.0, 1.0, 1.0, 1.0];
            
            % Jacobian matrix at origin of element coordinate system:
            J = 0.125 * ([NodesXi1; NodesXi2; NodesXi3] * eleCoords);
                                   
            J = J.*(J > eps); % remove numerical impurities

            % Transformation matrix:
            T = [J(1,1)*J(1,1) , J(2,1)*J(2,1) , J(3,1)*J(3,1) ,       2*J(1,1)*J(2,1) ,              2*J(1,1)*J(3,1) ,               2*J(2,1)*J(3,1);
                 J(1,2)*J(1,2) , J(2,2)*J(2,2) , J(3,2)*J(3,2) ,       2*J(1,2)*J(2,2) ,              2*J(1,2)*J(3,2) ,               2*J(2,2)*J(3,2);
                 J(1,3)*J(1,3) , J(2,3)*J(2,3) , J(3,3)*J(3,3) ,       2*J(1,3)*J(2,3) ,              2*J(1,3)*J(3,3) ,               2*J(2,3)*J(3,3);
                 J(1,1)*J(1,2) , J(2,1)*J(2,2) , J(3,1)*J(3,2) ,  J(1,1)*J(2,2)+J(2,1)*J(1,2) ,  J(1,1)*J(3,2)+J(3,1)*J(1,2) ,   J(2,1)*J(3,2)+J(3,1)*J(2,2);
                 J(1,1)*J(1,3) , J(2,1)*J(2,3) , J(3,1)*J(3,3) ,  J(1,1)*J(2,3)+J(2,1)*J(1,3) ,  J(1,1)*J(3,3)+J(3,1)*J(1,3) ,   J(2,1)*J(3,3)+J(3,1)*J(2,3);
                 J(1,2)*J(1,3) , J(2,2)*J(2,3) , J(3,2)*J(3,3) ,  J(1,2)*J(2,3)+J(2,2)*J(1,3) ,  J(1,2)*J(3,3)+J(3,2)*J(1,3) ,   J(2,2)*J(3,3)+J(3,2)*J(2,3)]; 

             
            % Determinant and inverse of Jacobian of time ansatz:
            detJT = 0.5 * deltaT;
            invJT = 1 / detJT;

            %---------------------------------------------------
            
            
            % Build the element matrix and the right hand side:
            %---------------------------------------------------
            
            % Initialise the submatrices:
            matrix_vv           = zeros(pos.nDofV / 3, pos.nDofV / 3);
            matrix_SV           = zeros(pos.nDofS, pos.nDofV);
            matrix_VSdamp       = zeros(pos.nDofV, pos.nDofS);
            matrix_SS           = zeros(pos.nDofS, pos.nDofS);
            matrix_jumpV        = zeros(pos.nDofV / 6, pos.nDofV / 6);
            matrix_jumpS        = zeros(pos.nDofS / 2, pos.nDofS / 2);
            matrix_jumpVSdamp   = zeros(pos.nDofV / 2, pos.nDofS / 2);
            
            % Initialise the subvector for volume forces:
            vector_VF           = zeros(pos.nDofV / 3, 1);
            
            % Get local coordinates and weight of Gauss points:
            coord_gp = [double(-1/sqrt(3)), double(1/sqrt(3))];   
            weight   = [1, 1];

            %Loop over Gauss points:
            for ll = 1 : 2 % loop over xi_3 values of Gauss points

                % local xi_3 coordinate of current Gauss point
                xi_3 = coord_gp(1,ll); 	
                
                for jj = 1 : 2 % loop over xi_2 values of Gauss points

                    % local xi_2 coordinate of current Gauss point
                    xi_2 = coord_gp(1,jj); 	

                    for kk = 1 : 2 % loop over xi_1 values of Gauss points

                        % local xi_1 coordinate of current Gauss point
                        xi_1 = coord_gp(1,kk);

                        % Velocity shape functions (tri-linear in space):
                        vShape = 0.125*((1 + xi_1 * NodesXi1).*(1 + xi_2 * NodesXi2).*(1 + xi_3 * NodesXi3));

                        % Get local derivatives of shape functions:
                        % vShapeDLoc = [dvShape/dxi_1; dvShape/dxi_2]; dvShape/dxi_3;
                        vShapeDLoc = 0.125*[NodesXi1.*(1 + xi_2 * NodesXi2).*(1 + xi_3 * NodesXi3);
                                            NodesXi2.*(1 + xi_1 * NodesXi1).*(1 + xi_3 * NodesXi3);
                                            NodesXi3.*(1 + xi_1 * NodesXi1).*(1 + xi_2 * NodesXi2)];


                        % Calculate the Jacobian (size = 3 x 3) at current integration point and related data: 
                        jacobi      = vShapeDLoc * eleCoords;
                        detJacobi   = det(jacobi);
                        invJSpace   = inv(jacobi);


                        % Get velocity derivatives in the real element:
                        vShapeDx    = invJSpace(1,:) * vShapeDLoc;
                        vShapeDy    = invJSpace(2,:) * vShapeDLoc;
                        vShapeDz    = invJSpace(3,:) * vShapeDLoc;

                        
                        % Shape functions for stresses:
                        matShapeS = [  0    0     0   xi_2    0     0   xi_3    0     0   xi_2*xi_3     0         0    ;
                                     xi_1   0     0     0     0     0     0   xi_3    0       0     xi_3*xi_1     0    ;
                                       0  xi_1    0     0   xi_2    0     0     0     0       0         0     xi_1*xi_2;
                                       0    0     0     0     0     0     0     0   xi_3      0         0         0    ;
                                       0    0     0     0     0   xi_2    0     0     0       0         0         0    ;
                                       0    0   xi_1    0     0     0     0     0     0       0         0         0     ];

                        SShape = [eye(6), T*matShapeS];
                              
                              
                        for ii = 1 : 2 % loop over local time axis

                            % local time coordinate of current Gauss point:
                            tau = coord_gp(1,ii);
                            
                            % Time shape functions at current Gauss point:
                            t_1 = 0.5 * (1-tau);
                            t_2 = 0.5 * (1+tau);

                            % Integration factor of current Gauss point:
                            intFac = weight(1,ll) * weight(1,ii) * weight(1,jj) * weight(1,kk)* detJacobi * detJT;

                            
                            %----------------------------------
                            % Get space-time shape functions:
                            %----------------------------------

                            % Space-time shape functions formed by
                            % multiplication of space and time shape
                            % functions:
                            
                            % Velocity space-time shape functions and their
                            % spatial and time derivatives:
                            vShapeST        = [t_1 * vShape,      t_2 * vShape];

                            vShapeDxST      = [ t_1 * vShapeDx, t_2 * vShapeDx];                           
                            vShapeDyST      = [ t_1 * vShapeDy, t_2 * vShapeDy];
                            vShapeDzST      = [ t_1 * vShapeDz, t_2 * vShapeDz];
                            
                            vShapeDtau      = [-0.5 * vShape,   0.5 * vShape];
                            
                            % Space-time shape functions for sigma and
                            % their time derivatives:
                            SShapeST        = [ t_1 * SShape, t_2 * SShape];
                            SShapeDtau      = [-0.5 * SShape, 0.5 * SShape];

                            
                            %-------------------------------------------
                            % Calculate contributions to eleMat and rhs:
                            %-------------------------------------------
                        
                            
                            % Coupling of stresses and velocities:
                            %-----------------------------------------

                            % StrainMat = [dv/dx      0       0  
                            %                0      dv/dy     0
                            %                0        0     dv/dz
                            %              dv/dy    dv/dx     0
                            %              dv/dz      0     dv/dx 
                            %                0      dv/dz   dv/dy] = dE/dt

                            StrainMat = [ vShapeDxST , zeros(1,16), zeros(1,16);
                                          zeros(1,16), vShapeDyST , zeros(1,16);
                                          zeros(1,16), zeros(1,16), vShapeDzST ;
                                          vShapeDyST , vShapeDxST , zeros(1,16);
                                          vShapeDzST , zeros(1,16), vShapeDxST ;
                                          zeros(1,16), vShapeDzST , vShapeDyST ;]; 

                            matrix_SV = matrix_SV + SShapeST' * (StrainMat * intFac);
                            
                            % Stress rate proportional damping:
                            matrix_VSdamp = matrix_VSdamp + StrainMat' * ((beta * intFac * invJT) * SShapeDtau);
                            
                            
                            % Matrix of stresses:
                            %-----------------------------
                            matrix_SS            = matrix_SS + SShapeST'*((CInvers*invJT*intFac)*SShapeDtau);
                            
                            
                            % Matrices of velocities:
                            %------------------------------------
                            mat_v_1 = vShapeST'   * (vShapeST   * (rho * alpha * intFac));     % damping
                            mat_v_2 = vShapeST'   * (vShapeDtau * (rho * invJT * intFac));     % inertia

                            matrix_vv = matrix_vv + mat_v_1 + mat_v_2;
                            
                            
                            % Volume forces:
                            %-------------------------------------------
                            vector_VF = vector_VF - vShapeST' * intFac;

                        end


                        %--------------------------------------
                        % Jump terms:
                        %--------------------------------------

                        % Get integration factor for jump terms:
                        intFac = weight(1,ll) * weight(1,jj) * weight(1,kk) * detJacobi;
                        
                        % Jump terms of velocities:
                        %--------------------------------------------
                        matrix_jumpV = matrix_jumpV + vShape' * (vShape * (rho * intFac));
                        
                        % Jump terms of stresses:
                        %--------------------------------------------
                        matrix_jumpS = matrix_jumpS + SShape'*((CInvers*intFac)*SShape);
                        
                        % Jump term for stiffness proportional damping:
                        %------------------------------------------------
                        StrainMat = [ vShapeDx  , zeros(1,8), zeros(1,8);
                                      zeros(1,8), vShapeDy  , zeros(1,8);
                                      zeros(1,8), zeros(1,8), vShapeDz  ;
                                      vShapeDy  , vShapeDx  , zeros(1,8);
                                      vShapeDz  , zeros(1,8), vShapeDx  ;
                                      zeros(1,8), vShapeDz  , vShapeDy  ;]; 
                        
                        matrix_jumpVSdamp = matrix_jumpVSdamp + StrainMat' * ((intFac * beta) * SShape);
                        
                    end
                end
            end
            
            
            %--------------------------------------------------------------
            
            % Get jump at start of current time slab:
            %--------------------------------------------------
            jumpDeltaVx  = velo(pos.vxt0) - velo_dis(pos.vxt1);
            jumpDeltaVy  = velo(pos.vyt0) - velo_dis(pos.vyt1);
            jumpDeltaVz  = velo(pos.vzt0) - velo_dis(pos.vzt1);

            jumpDeltaS   = elem.sigma(pos.St0Sol) - elem.sigma_dis(pos.St1Sol);
            
            
            % Store submatrices in eleMat:
            %-------------------------------
            eleMat(pos.v, pos.S)       = eleMat(pos.v, pos.S) + matrix_SV' + matrix_VSdamp;
            eleMat(pos.S, pos.v)       = eleMat(pos.S, pos.v) - matrix_SV;
            
            eleMat(pos.S, pos.S)       = eleMat(pos.S, pos.S) + matrix_SS;
            
            eleMat(pos.vx,pos.vx)      = eleMat(pos.vx,pos.vx) + matrix_vv;
            eleMat(pos.vy,pos.vy)      = eleMat(pos.vy,pos.vy) + matrix_vv;
            eleMat(pos.vz,pos.vz)      = eleMat(pos.vz,pos.vz) + matrix_vv;
            
            % Jump terms:
            eleMat(pos.vxt0, pos.vxt0) = eleMat(pos.vxt0, pos.vxt0) + matrix_jumpV;
            eleMat(pos.vyt0, pos.vyt0) = eleMat(pos.vyt0, pos.vyt0) + matrix_jumpV;
            eleMat(pos.vzt0, pos.vzt0) = eleMat(pos.vzt0, pos.vzt0) + matrix_jumpV;
            
            eleMat(pos.vt0, pos.St0)   = eleMat(pos.vt0, pos.St0) + matrix_jumpVSdamp;
            
            eleMat(pos.St0,pos.St0)    = eleMat(pos.St0,pos.St0) + matrix_jumpS;
            
            
            % Add contributions to rhs:
            %-----------------------------
            
            rhs(pos.v)    = rhs(pos.v) + (matrix_SV' + matrix_VSdamp) * elem.sigma;
            rhs(pos.S)    = rhs(pos.S) - matrix_SV  * velo;
            
            rhs(pos.S)    = rhs(pos.S) + matrix_SS*elem.sigma;
            
            rhs(pos.vx)   = rhs(pos.vx) + matrix_vv * velo(pos.vx);
            rhs(pos.vy)   = rhs(pos.vy) + matrix_vv * velo(pos.vy);
            rhs(pos.vz)   = rhs(pos.vz) + matrix_vv * velo(pos.vz);
            
            % Volume forces:
            rhs(pos.vx) = rhs(pos.vx) - vector_VF * fx; 
            rhs(pos.vy) = rhs(pos.vy) - vector_VF * fy; 
            rhs(pos.vz) = rhs(pos.vz) - vector_VF * fz;
            
            % Jump terms:
            rhs(pos.vxt0) = rhs(pos.vxt0) + matrix_jumpV*jumpDeltaVx;
            rhs(pos.vyt0) = rhs(pos.vyt0) + matrix_jumpV*jumpDeltaVy;
            rhs(pos.vzt0) = rhs(pos.vzt0) + matrix_jumpV*jumpDeltaVz;
            
            rhs(pos.vt0)  = rhs(pos.vt0) + matrix_jumpVSdamp * jumpDeltaS;
            
            rhs(pos.St0)  = rhs(pos.St0) + matrix_jumpS*jumpDeltaS;
            
            %-----------------------------------------------------


            % Set Neummann BC:
            %------------------------
            if ~isempty(elem.bcNeu)
                rhs = elem.SetNeumannBc(eleCoords, currTimeVar, rhs, detJT);
            end           
            

            % Set Dirichlet BC:
            %-----------------------------
            if ~isempty(elem.bcDir)
                activeBcDir = find(elem.bcDir(:,6) == 1); % Find active bcDir in current element
                posLocBcDir = elem.bcDir(activeBcDir,7);  % Get local position of active bcDir
                
                eleMat(posLocBcDir, :)          = 0.0;
                eleMat(posLocBcDir,posLocBcDir) = eye(length(activeBcDir));
            end
            
            
            % Hybridise eleMat and rhs:
            %---------------------------

            % Get submatrices of eleMat:
            K_SV   = eleMat(pos.cond, pos.main);
            K_VS   = eleMat(pos.main, pos.cond);
            K_SS   = eleMat(pos.cond, pos.cond);
            K_VV   = eleMat(pos.main, pos.main);
            K_SS_I = inv(K_SS);
            
            % Change sign of rhs:
            eleVec = -rhs;

            % Calculate the hybridised element matrix and vector:
            eleMatHyb = K_VV - (K_VS  * K_SS_I * K_SV); %#ok<MINV>
            eleVecHyb = eleVec(pos.main) - (K_VS * K_SS_I * eleVec(pos.cond)); %#ok<MINV>

            %Calculate matrix and vector required for recondensation during postprocessing:
            elem.matrix = K_SS_I * K_SV; %#ok<MINV>
            elem.vector = K_SS_I * eleVec(pos.cond,1); %#ok<MINV>            
 
            
            % Store the element matrix and the indices in vectors:
            %--------------------------------------------------------------
            
            % Get the local indices and the values of the nonzero parts of
            % eleMatHyb:
            [I_Loc, J_Loc, X_elem] = find(eleMatHyb);
            
            % Transfer the local indices to global ones:
            index       = elem.indexSystem;
            I_elem      = index(I_Loc);
            J_elem      = index(J_Loc);
            
            % Get the number of nonzero entries for the current element:
            sizeXElem   = size(I_elem,1);

        end
        
        
        %------------------------------------------------------------------
        %------------------------------------------------------------------
        
        
        function Post(elem, soluElem, deltaSoluElem, deltaT)
            % Method setting element data from the solution:
            
            % Get velocities from the solution vector:
            velo   = soluElem;

            % Calculate stresses by recondensation:
            deltaSigma  = elem.vector - elem.matrix * deltaSoluElem;
            elem.sigma  = elem.sigma  + deltaSigma;
            
% % %             elem.sigma  = elem.sigma .*(abs(elem.sigma) > 1.0e-8); % remove numerical impurities

            % Get the displacement at end of time slab by time integration
            % of velocity over the time slab plus the initial displacement:
            elem.displace(elem.ind.vt1) = elem.displace(elem.ind.vt0) +...
                                   (velo(elem.ind.vt0) + velo(elem.ind.vt1)) * 0.5 * deltaT;                
        end

        
        %------------------------------------------------------------------
        %------------------------------------------------------------------
        
        
        function [ergSolid, sizeErg] = PostOut(elem, eleSolu, eleCoords)
            % Method returns output data of the element at end of current
            % time slab:
            
            % Get element data from the solution:
            velo        = eleSolu;
            
            % Get indices to access solution and element properties:
            pos         = elem.ind;
            
            % Number of nodes for which output is returned:
            sizeErg = pos.nNodesOut;
            
            % Initialisation:
            %---------------------
            ergSolid = zeros(sizeErg,12);
            
            % Get velocities and displacements:
            %------------------------------------
            ergSolid(:,1) = velo(pos.vxt1);  % v_x_t1
            ergSolid(:,2) = velo(pos.vyt1);  % v_y_t1
            ergSolid(:,3) = velo(pos.vzt1);  % v_z_t1

            ergSolid(:,4) = elem.displace(pos.vxt1); % u_x_t1
            ergSolid(:,5) = elem.displace(pos.vyt1); % u_y_t1
            ergSolid(:,6) = elem.displace(pos.vzt1); % u_z_t1
            
            
            % Calculate stresses at the nodes:
            %------------------------------------------

            % Local coordinates of nodes:
            NodesXi1 = [-1.0, 1.0, 1.0,-1.0,-1.0, 1.0, 1.0,-1.0];
            NodesXi2 = [-1.0,-1.0, 1.0, 1.0,-1.0,-1.0, 1.0, 1.0];
            NodesXi3 = [-1.0,-1.0,-1.0,-1.0, 1.0, 1.0, 1.0, 1.0];
            
            % Jacobi matrix:
            J = 0.125 * ([NodesXi1; NodesXi2; NodesXi3] * eleCoords);
                                   
            J = J.*(J > eps); % remove numerical impurities
       
            % Transformation matrix:
            T = [J(1,1)*J(1,1) , J(2,1)*J(2,1) , J(3,1)*J(3,1) ,       2*J(1,1)*J(2,1) ,              2*J(1,1)*J(3,1) ,               2*J(2,1)*J(3,1);
                 J(1,2)*J(1,2) , J(2,2)*J(2,2) , J(3,2)*J(3,2) ,       2*J(1,2)*J(2,2) ,              2*J(1,2)*J(3,2) ,               2*J(2,2)*J(3,2);
                 J(1,3)*J(1,3) , J(2,3)*J(2,3) , J(3,3)*J(3,3) ,       2*J(1,3)*J(2,3) ,              2*J(1,3)*J(3,3) ,               2*J(2,3)*J(3,3);
                 J(1,1)*J(1,2) , J(2,1)*J(2,2) , J(3,1)*J(3,2) ,  J(1,1)*J(2,2)+J(2,1)*J(1,2) ,  J(1,1)*J(3,2)+J(3,1)*J(1,2) ,   J(2,1)*J(3,2)+J(3,1)*J(2,2);
                 J(1,1)*J(1,3) , J(2,1)*J(2,3) , J(3,1)*J(3,3) ,  J(1,1)*J(2,3)+J(2,1)*J(1,3) ,  J(1,1)*J(3,3)+J(3,1)*J(1,3) ,   J(2,1)*J(3,3)+J(3,1)*J(2,3);
                 J(1,2)*J(1,3) , J(2,2)*J(2,3) , J(3,2)*J(3,3) ,  J(1,2)*J(2,3)+J(2,2)*J(1,3) ,  J(1,2)*J(3,3)+J(3,2)*J(1,3) ,   J(2,2)*J(3,3)+J(3,2)*J(2,3)]; 
     
            
            % Calculate stresses at the nodes of the current element:
            for node = 1 : sizeErg % loop over element nodes

                % Get local coordinates of current node:
                xi_1 = NodesXi1(1, node);
                xi_2 = NodesXi2(1, node);
                xi_3 = NodesXi3(1, node);
                
                % Get stress shape functions at current node:
                matShapeS = [  0    0     0   xi_2    0     0   xi_3    0     0   xi_2*xi_3     0         0    ;
                             xi_1   0     0     0     0     0     0   xi_3    0       0     xi_3*xi_1     0    ;
                               0  xi_1    0     0   xi_2    0     0     0     0       0         0     xi_1*xi_2;
                               0    0     0     0     0     0     0     0   xi_3      0         0         0    ;
                               0    0     0     0     0   xi_2    0     0     0       0         0         0    ;
                               0    0   xi_1    0     0     0     0     0     0       0         0         0     ];
                
                SShape = [eye(6), T*matShapeS];                

                % Calculate and store the node values:
                ergSolid(node,7:12) = SShape * elem.sigma(pos.St1Sol);                                                                                                                            
            end   
        end
        
        
        %------------------------------------------------------------------
        %------------------------------------------------------------------
        

        function Update(elem, eleSolu, deltaTCurr, deltaTNext, typeEleUpdate)
            
            % Update solution for the next time slab:
            
            % deltaTCurr: time width of current time  slab
            % deltaTNext: time width of next time slab
            % typeEleUpdate: 1: use gradient of current time slab to
            %                   estimate start and end values of next time
            %                   slab (initial values for first iteration)
            %                else: gradient is not used, initial values for
            %                   start and end of next time slab are thesame)
            
            % Set indices:
            pos     = elem.ind;
            
            % Get unknowns from eleSolu:
            velo    = eleSolu;
            
            
            % Set discontinious values:         
            elem.sigma_dis    = elem.sigma;            
            elem.displace_dis = elem.displace;
            
            if typeEleUpdate == 1
                deltaSigmaCurr          = elem.sigma(pos.St1Sol) - elem.sigma(pos.St0Sol);
                elem.sigma(pos.St0Sol)  = elem.sigma(pos.St1Sol); 
                elem.sigma(pos.St1Sol)  = elem.sigma(pos.St0Sol) + deltaSigmaCurr * (deltaTNext / deltaTCurr);
                
            else
                elem.sigma(pos.St0Sol)  = elem.sigma(pos.St1Sol);

            end
            
            elem.displace(pos.vt0)      = elem.displace(pos.vt1); 
            elem.displace(pos.vt1)      = elem.displace(pos.vt0) +...
                                                (velo(pos.vt0) + velo(pos.vt1)) * (0.5 * deltaTNext);
                                        
        end
        
        
        %------------------------------------------------------------------
        %------------------------------------------------------------------
        
        
        function [solUpdate , index] = UpdateSol(elem, eleSolu, ...
                                                 deltaTCurr, deltaTNext,...
                                                 typeEleUpdate)
            
            % Method called in UpdateSolution:
            
            % Get the current solution, update it for the next time slab
            % and return it as solUpdate:
            %--------------------------------------------------------------
            
            % Get unknowns from eleSolu:
            velo        = eleSolu;
            
            % Update unknowns for next time slab:
            if typeEleUpdate == 1
                deltaVeloCurr          = velo(elem.ind.vt1) - velo(elem.ind.vt0);
                velo(elem.ind.vt0)     = velo(elem.ind.vt1); 
                velo(elem.ind.vt1)     = velo(elem.ind.vt0) + deltaVeloCurr * (deltaTNext / deltaTCurr);
            else
                velo(elem.ind.vt0)     = velo(elem.ind.vt1);
            end
            
            % Set values:
            solUpdate = velo;
            index     = elem.indexSystem;     
        end
        
        
        %------------------------------------------------------------------
        %------------------------------------------------------------------

        
        function Restore(elem, eleSoluDis, deltaTCurr, deltaTNext, typeEleUpdate)
            % Function resets the element properties for the dof using
            % data from previous time slab. Function is invoked by
            % "RestoreElem" if iteration did not converge and width of time
            % step is reduced.
            
            % deltaTCurr: time step of current time  slab
            % deltaTNext: time step used for recalculation of time slab
            % typeEleUpdate: 1: use gradient of current time slab to
            %                   estimate end values of recalculation
            %                   slab (initial values for first iteration)
            %                else: gradient is not used, initial values for
            %                   start and end of next time slab are the same)
            
            % Set indices:
            pos  = elem.ind;
            
            % Get unknowns from eleSolu:
            veloDis = eleSoluDis;
            
            % Restore element data:
            %-------------------------
            if typeEleUpdate == 1
                deltaSigmaCurr          = elem.sigma_dis(pos.St1Sol) - elem.sigma_dis(pos.St0Sol);
                elem.sigma(pos.St0Sol)  = elem.sigma_dis(pos.St1Sol); 
                elem.sigma(pos.St1Sol)  = elem.sigma(pos.St0Sol) + deltaSigmaCurr * (deltaTNext / deltaTCurr);
                
            else
                % Restore values at start om time slab:
                elem.sigma(pos.St0Sol)  = elem.sigma_dis(pos.St1Sol);
                
                % Set the same values for the end of the time slab:
                elem.sigma(pos.St1Sol)  = elem.sigma(pos.St0Sol);
            end
            
            elem.displace(pos.vt0)      = elem.displace_dis(pos.vt1); 
            elem.displace(pos.vt1)      = elem.displace(pos.vt0) + ...
                                            (veloDis(pos.vt0) + veloDis(pos.vt1)) * (0.5 * deltaTNext);
                                        
        end
        
        
        %------------------------------------------------------------------
        %------------------------------------------------------------------
        
        
        function SetEleDofGlob(elem)
            % Sets the property eleDofGlob of the current element:
            % eleDofGlob has as many cols as the maximum local dof id of
            % the element. Col: local dof id, value: global dof id
            
            
            elem.eleDofGlob = [1, 21, 2, 22, 3, 23];
                
        end

        
    end % end of methods
    
    
    %------------------------------------------------------------------
    
    %------------------------------------------------------------------
    
    
    methods (Static)
        function eleNodesDofLoc = GetEleNodesDofLoc
            % returns a matrix containing the local node numbers in the
            % first col and the local dof ids in the second col:
            
            % loc dof: 1: vxt0, 2:vxt1, 3: vyt0, 4: vyt1, 5: vzt0, 6: vzt1
            eleNodesDofLoc= [1, 1;
                             2, 1;
                             3, 1;
                             4, 1;
                             5, 1;
                             6, 1;
                             7, 1;
                             8, 1;
                             1, 2;
                             2, 2;
                             3, 2;
                             4, 2;
                             5, 2;
                             6, 2;
                             7, 2;
                             8, 2;
                             1, 3;
                             2, 3;
                             3, 3;
                             4, 3;
                             5, 3;
                             6, 3;
                             7, 3;
                             8, 3;
                             1, 4;
                             2, 4;
                             3, 4;
                             4, 4;
                             5, 4;
                             6, 4;
                             7, 4;
                             8, 4;
                             1, 5;
                             2, 5;
                             3, 5;
                             4, 5;
                             5, 5;
                             6, 5;
                             7, 5;
                             8, 5;
                             1, 6;
                             2, 6;
                             3, 6;
                             4, 6;
                             5, 6;
                             6, 6;
                             7, 6;
                             8, 6];
              
        end
        
    end % end of static methods
end % end of classdefinition