classdef ElemSuperclass < handle

properties (SetAccess = protected)

    numGlob         % global element number of current element     

    eleDofGlob      % global dof IDs of local element dof IDs


    eleNodes_t0     % global node numbers of local nodes (element connectivity)
    eleNodes_t1

    eleCoord_t0     % node coordinates
    eleCoord_t1

    matPara         % material parameters
    loadPara        % load     parameters

    bcDir           % Dirichlet Bc of current element
    bcNeu           % Neumann Bc of current element


    solu         % solution of element dof of last iteration
    soluDelta    % solution incerement of element dof of last iteration
    soluDis      % solution of element dof of end of previous time slab



    % Indices for matrix entries:
    %------------------------------------------------------------
    indexSystem     % Positions of element matrix entries in global 
                    % system of equations

    % Recondensation
    %------------------------------------------------------------
    matrix          % matrix for recondensation
    vector          % vector for recondensation


    % Further properties which are set for each element directly in the
    % element's constructor since they are the same for all objects of
    % one type:
    %------------------------------------------------------------------

    % type          % element type (identification number)
    % name          % name of element type
    % probDim       % dimension of the problem (1: 1D, 2: 2D, 3: 3D)

    % sizeXest      % estimated number of non-zero entries in condensed 
                    % element matrix

    % paraOutput    % switch for paraview output of each element type:
                    % 0: no Paraview output for current element
                    % 1: create Paraview output file for the current
                    % element type

end

% ======================================================================= %
% ======================================================================= %
    
methods

%Constructor
function elem = ElemSuperclass ( eleConnect    , ...
                                 eleCoord      , ...
                                 eleMatPara    , ...
                                 eleLoadPara   , ...
                                 eleBcDir      , ...
                                 eleBcNeu      , ...
                                 numGlobInput    ...
                                )

    % Global element number
    elem.numGlob  = numGlobInput;

    % Set the global dof Ids of the local dof:
    elem.SetEleDofGlob() %#ok<MCNPN> suppress warning due to
                         % call to child class method



    % Global node numbers (element connectivity):
    elem.eleNodes_t0 = eleConnect;
    elem.eleNodes_t1 = eleConnect;

    elem.eleCoord_t0 = eleCoord;
    elem.eleCoord_t1 = eleCoord;

    elem.matPara     = eleMatPara;     % element material parameters          
    elem.loadPara    = eleLoadPara;    % element load parameters 

    elem.bcDir       = eleBcDir;       % element Dirichlet bc
    elem.bcNeu       = eleBcNeu;       % element Neumann   bc


    elem.solu      = [];  % elementwise solution of last iteration
    elem.soluDelta = [];  % elementwise solution increment
    elem.soluDis   = [];  % elementwise solution of end of previous time slab


    % Get local (related to elememt matrix) position of the dof
    % subjected to Dirichlet BCs. Information is stored in
    % elem.bcDir(:,7).
    elem.GetLocPosBcDir();            

    % Position for data storage in global system of eq.
    %--------------------------------------------------
    elem.indexSystem    = [];       

    % Recondensation:
    %------------------------------------------------
    elem.matrix         = [];
    elem.vector         = [];
end

% ======================================================================= %


function [] = SetEleSolu(elem, eleSolu, eleDeltaSolu)


    elem.solu      = eleSolu;
    elem.soluDelta = eleDeltaSolu;
end

% ======================================================================= %


function [] = SetEleSoluDis(elem, eleSoluDis)

    elem.soluDis = eleSoluDis;

end

% ======================================================================= %


% Method for Neumann boundary conditions:
function rhs = SetNeumannBc(elem, eleCoords, ModBC, rhs, detJT)

    ModBCquad   = ModBC;
    ModBC       = ModBC(:, [1 3]);


    GlobCoord   = eleCoords;

    coord_gp    = [double(-1/sqrt(3)), double(1/sqrt(3))];
    weight      = [1, 1];

    coord_gp_quad   = [-0.77459666924148,   0.0             ,   0.77459666924148];
    weight_quad     = [ 0.55555555555556,   0.88888888888889,   0.55555555555556];

    % Loop over current Neumann BCs
    for ii = 1 : size(elem.bcNeu,1)

        switch elem.bcNeu(ii,2)
            case 1      % Concentrated load [N]
                ForcesX = zeros(2,1);
                ForcesY = zeros(2,1);
                ForcesZ = zeros(2,1);

                for kk = 1 : 2  % loop over time
                    tau             = coord_gp(1, kk);
                    IntFacTime      = weight(1,kk)*detJT;
                    %Spannungsfreiwerte
                    StressBoundVal  = elem.bcNeu(ii,8)*ModBC(elem.bcNeu(ii,7),:)';

                    % Shape function:                                       
                    stress_bound_shape = 0.5 * [(1-tau),(1+tau)];

                    %Randspannung ausgewertet am Zeitschritt-Gauss-Punkt:
                    StressBoundGP = stress_bound_shape * StressBoundVal;

                    % Only one shape function i nonzero at node => [1]
                    VeloBound = [1]*[0.5*(1-tau), 0.5*(1+tau)]; %#ok<NBRAK>

                    switch elem.bcNeu(ii,4)
                        case 1 % force in global x-direction
                            ForcesX = ForcesX + VeloBound' * (StressBoundGP  * IntFacTime);                                               
                        case 2 % force in global y-direction
                            ForcesY = ForcesY + VeloBound' * (StressBoundGP  * IntFacTime);
                        case 3 % force in global z-direction
                            ForcesZ = ForcesZ + VeloBound' * (StressBoundGP  * IntFacTime);
                    end                              
                end                     

                % Add the concentrated loads to "rhs":
                % Get the positions in rhs:
                switch elem.probDim
                    case 2 % 2D element
                        PosNodeX = [elem.bcNeu(ii,3), elem.bcNeu(ii,3) + 4];
                        PosNodeY = PosNodeX + 8;
                    case 3 % 3D element
                        PosNodeX = [elem.bcNeu(ii,3), elem.bcNeu(ii,3) + 8];
                        PosNodeY = PosNodeX + 16;
                        PosNodeZ = PosNodeX + 32;
                end

                % Add the contribution at proper positions:
                rhs(PosNodeX) = rhs(PosNodeX) - ForcesX;
                rhs(PosNodeY) = rhs(PosNodeY) - ForcesY;
                if elem.probDim == 3
                    rhs(PosNodeZ) = rhs(PosNodeZ) - ForcesZ; 
                end


            case 2   % Line load
                StressBoundX = zeros(4,1);
                StressBoundY = zeros(4,1);
                StressBoundZ = zeros(4,1);


                switch elem.probDim
                    case 2
                        switch elem.bcNeu(ii,3) % element edge subjected to loading 
                            case 1
                                Nodes = [1, 2];
                            case 2
                                Nodes = [2, 3];
                            case 3
                                Nodes = [3, 4];
                            case 4
                                Nodes = [4, 1];
                        end

                    case 3
                        switch elem.bcNeu(ii,3) % element edge subjected to loading
                            case 1
                                Nodes = [1, 2];
                            case 2
                                Nodes = [2, 3];
                            case 3
                                Nodes = [3, 4];
                            case 4
                                Nodes = [4, 1];
                            case 5
                                Nodes = [1, 5];
                            case 6
                                Nodes = [2, 6];
                            case 7
                                Nodes = [3, 7];
                            case 8
                                Nodes = [4, 8];
                            case 9
                                Nodes = [5, 6];
                            case 10
                                Nodes = [6, 7];
                            case 11
                                Nodes = [7, 8];
                            case 12
                                Nodes = [8, 5];
                        end
                end

                % Global coordinates of loaded nodes:
                CoordBound  = GlobCoord(Nodes,:);


                for k = 1 : 2 % Loop over time
                    for j = 1 : 2 % Loop over Gauss-Points on edge

                        tau     = coord_gp(1,k);

                        switch elem.bcNeu(ii,3) % element edge subjected to loading
                            case 1
                                xi_1            = coord_gp(1,j);
                                vBoundShape     = 0.5*[(1-xi_1), (1+xi_1)];
                                GeomBoundDiff   = [-0.5, 0.5];

                            case 2
                                xi_2            = coord_gp(1,j);
                                vBoundShape     = 0.5*[(1-xi_2), (1+xi_2)];
                                GeomBoundDiff   = [-0.5, 0.5];

                            case 3
                                xi_1            = coord_gp(1,j);
                                vBoundShape     = 0.5*[(1+xi_1), (1-xi_1)];
                                GeomBoundDiff   = [0.5, -0.5];

                            case 4
                                xi_2            = coord_gp(1,j);
                                vBoundShape     = 0.5*[(1+xi_2), (1-xi_2)];
                                GeomBoundDiff   = [0.5, -0.5];

                            case 5
                                xi_3            = coord_gp(1,j);
                                vBoundShape     = 0.5*[(1-xi_3), (1+xi_3)];
                                GeomBoundDiff   = [-0.5, 0.5];

                            case 6
                                xi_3            = coord_gp(1,j);
                                vBoundShape     = 0.5*[(1-xi_3), (1+xi_3)];
                                GeomBoundDiff   = [-0.5, 0.5];

                            case 7
                                xi_3            = coord_gp(1,j);
                                vBoundShape     = 0.5*[(1-xi_3), (1+xi_3)];
                                GeomBoundDiff   = [-0.5, 0.5];

                            case 8
                                xi_3            = coord_gp(1,j);
                                vBoundShape     = 0.5*[(1-xi_3), (1+xi_3)];
                                GeomBoundDiff   = [-0.5, 0.5];

                            case 9
                                xi_1            = coord_gp(1,j);
                                vBoundShape     = 0.5*[(1-xi_1), (1+xi_1)];
                                GeomBoundDiff   = [-0.5, 0.5];

                            case 10
                                xi_2            = coord_gp(1,j);
                                vBoundShape     = 0.5*[(1-xi_2), (1+xi_2)];
                                GeomBoundDiff   = [-0.5, 0.5];

                            case 11
                                xi_1            = coord_gp(1,j);
                                vBoundShape     = 0.5*[(1+xi_1), (1-xi_1)];
                                GeomBoundDiff   = [0.5, -0.5];

                            case 12
                                xi_2            = coord_gp(1,j);
                                vBoundShape     = 0.5*[(1+xi_2), (1-xi_2)];
                                GeomBoundDiff   = [0.5, -0.5];

                            otherwise
                                error('non-existent element edge')
                        end

                        % Calculate space-time shape function
                        vBoundShapeST = [0.5*(1-tau)*vBoundShape, 0.5*(1+tau)*vBoundShape];

                        %Berechnung des Basisvektors
                        vector_basis = GeomBoundDiff * CoordBound;

                        %Berechnung der physikalischen Laenge des Basisvektors
                        detJBound = sqrt(vector_basis(1,1)^2 + vector_basis(1,2)^2 + vector_basis(1,3)^2);

                        IntFac = weight(1,k) * weight(1,j) * detJT * detJBound;


                        switch elem.bcNeu(ii,5) % Spannungsverlauf auf Rand

                            case 0 %konstanter Verlauf der Randspannung
                                %Spannungsfreiwerte
                                StressBoundVal = elem.bcNeu(ii,8)*ModBC(elem.bcNeu(ii,7),:)';

                                %Formfunktion in der Zeit                                     
                                stress_bound_shape = 0.5 * [(1-tau),(1+tau)];

                                %Randspannung ausgewertet am
                                %Gauss-Punkt (ortsunabhaengig weil
                                %konstant)
                                StressBoundGP = stress_bound_shape * StressBoundVal;

                            case 1 %linearer Verlauf der Randspannung                                    
                                %Spannungsfreiwerte  
                                StressBoundVal          = zeros(4,1);
                                StressBoundVal([1 2],1) = elem.bcNeu(ii,[6 7]) * ModBC(elem.bcNeu(ii,5),1); % fuer t_0                                     
                                StressBoundVal([3 4],1) = elem.bcNeu(ii,[6 7]) * ModBC(elem.bcNeu(ii,5),2); % fuer t_1

                                %Randspannung ausgewertet am GaussPkt
                                %interpoliert mit Formfkt
                                StressBoundGP = vBoundShapeST * StressBoundVal;

                            case 2
                                disp('Quadratic stress variation along edge is not yet implemented')
                        end


                        %Auswertung der Randspannungen im jeweiligen Gauss-Punkt
                        switch elem.bcNeu(ii,4)
                            case 1  %Randspannung in globaler x-Richtung
                                StressBoundX = StressBoundX + vBoundShapeST' * (StressBoundGP * IntFac);

                            case 2  %Randspannung in globaler y-Richtung
                                StressBoundY = StressBoundY + vBoundShapeST' * (StressBoundGP * IntFac);

                            case 3
                                StressBoundZ = StressBoundZ + vBoundShapeST' * (StressBoundGP * IntFac);

                            case 4 %Spannungen normal zur Flaeche
                                %Berechnung des Normalenvektors
                                vector_normal = [vector_basis(1,2) , -vector_basis(1,1)];

                                %Normierung des Normalenvektors
                                vector_normal_norm = 1/(sqrt(vector_normal(1,1)^2 + vector_normal(1,2)^2))*vector_normal;

                                switch elem.bcNeu(ii,3)
                                    case {1,4}
                                        vector_normal_norm = -vector_normal_norm;
                                end

                                switch elem.bcNeu(ii,6)
                                    case 1 % Spannungs-RB
                                        %Transformation der Randspannungen in das globale Koordinatensystem
                                        StressBoundGlob = StressBoundGP * vector_normal_norm;

                                        %Aufbau des Elementlastvektors
                                        StressBoundX    = StressBoundX + vBoundShapeST' * (StressBoundGlob(1,1) * IntFac);
                                        StressBoundY    = StressBoundY + vBoundShapeST' * (StressBoundGlob(1,2) * IntFac);
                                        % StressBoundZ bleibt Null, da Linienlast in Normalenrichtung nur im 2D-Fall zulaessig

                                    case 2 %Didisp-RB
                                        % Calculate the density of surface charge
                                        DiDispGlob      = elem.bcNeu(ii,8:9) * vector_normal_norm';

                                        % Use that PsiBoundShape = vBoundShape 
                                        DiDispBound     = DiDispBound + vBoundShapeST' * (DiDispGlob * IntFac);
                                end
                        end
                    end
                end


                switch elem.bcNeu(ii,6)

                    case 1 % Mechanische RB
                        %Steuerdaten zum Einbau der Spannungsrandbedingungen
                        %---------------------------------------------------------------      
                        switch elem.probDim
                            case 2
                                PosBoundX = [Nodes, Nodes + 4];
                                PosBoundY = PosBoundX + 8;
                            case 3
                                PosBoundX = [Nodes, Nodes + 8];
                                PosBoundY = PosBoundX + 16;
                                PosBoundZ = PosBoundX + 32;
                        end

                        %Es wird davon ausgegangen, dass das Element verdreht im Raum
                        %liegt, so dass eine eingepraegte Randspannung in allen
                        %Raumrichtungen Arbeit leisten kann
                        rhs(PosBoundX) = rhs(PosBoundX) - StressBoundX;
                        rhs(PosBoundY) = rhs(PosBoundY) - StressBoundY;

                        if elem.probDim == 3
                            rhs(PosBoundZ) = rhs(PosBoundZ) - StressBoundZ; 
                        end

                    case 2 % Elektrische RB
                        % Steuerdaten fuer Einbau der RB in Resi (nur 2D zulaessig):
                        PosBoundPsi = 16 + [Nodes, Nodes + 4];

                        rhs(PosBoundPsi) = rhs(PosBoundPsi) - DiDispBound;

                    otherwise
                        error('Error adding Neumann type BC to rhs')
                end


            case 3 % Surface load
                StressSurfX = zeros(8,1);
                StressSurfY = zeros(8,1);
                StressSurfZ = zeros(8,1);


                switch elem.bcNeu(ii,3)
                    case 1
                        Nodes = [2, 6, 7, 3];
                        % Global coordinates of nodes:
                        CoordSurf  = GlobCoord(Nodes,[2,3]);
                    case -1
                        Nodes = [1, 5, 8, 4];
                        % Global coordinates of nodes:
                        CoordSurf  = GlobCoord(Nodes,[2,3]);
                    case 2
                        Nodes = [4, 3, 7, 8];
                        % Global coordinates of nodes:
                        CoordSurf  = GlobCoord(Nodes,[1,3]);
                    case -2
                        Nodes = [1, 2, 6, 5];
                        % Global coordinates of nodes:
                        CoordSurf  = GlobCoord(Nodes,[1,3]);
                    case 3
                        Nodes = [5, 6, 7, 8];
                        % Global coordinates of nodes:
                        CoordSurf  = GlobCoord(Nodes,[1,2]);
                    case -3
                        Nodes = [1, 2, 3, 4];
                        % Global coordinates of nodes:
                        CoordSurf  = GlobCoord(Nodes,[1,2]);
                end


                for kk = 1 : 3 % loop over local time coordinate
                    tau     = coord_gp_quad(1,kk);

                    t_1q    = -0.5*tau*(1.0-tau);
                    t_2q    = 1.0-tau*tau;
                    t_3q    = 0.5*tau*(1+tau);

                    for jj = 1 : 2 % Schleifen ueber Gausspkte auf Oberflaeche
                        for ll = 1 : 2 % Schleifen ueber Gausspkte auf Oberflaeche

                            switch elem.bcNeu(ii,3)
                                case {1, -1}
                                    xi_3 = coord_gp(1,jj);
                                    xi_2 = coord_gp(1,ll);

                                    vSurfShape = 0.25*[(1-xi_3)*(1-xi_2),...
                                                       (1+xi_3)*(1-xi_2),...
                                                       (1+xi_3)*(1+xi_2),...
                                                       (1-xi_3)*(1+xi_2)];

                                    GeomBoundDiff = 0.25*[-(1-xi_2), (1-xi_2), (1+xi_2),-(1+xi_2);
                                                          -(1-xi_3),-(1+xi_3), (1+xi_3), (1-xi_3)];

                                case {2, -2}
                                    xi_1 = coord_gp(1,jj);
                                    xi_3 = coord_gp(1,ll);

                                    vSurfShape = 0.25*[(1-xi_1)*(1-xi_3),...
                                                       (1+xi_1)*(1-xi_3),...
                                                       (1+xi_1)*(1+xi_3),...
                                                       (1-xi_1)*(1+xi_3)];

                                    GeomBoundDiff = 0.25*[-(1-xi_3), (1-xi_3), (1+xi_3),-(1+xi_3);
                                                          -(1-xi_1),-(1+xi_1), (1+xi_1), (1-xi_1)];

                                case {3, -3}
                                    xi_1 = coord_gp(1,jj);
                                    xi_2 = coord_gp(1,ll);

                                    vSurfShape = 0.25*[(1-xi_1)*(1-xi_2),...
                                                       (1+xi_1)*(1-xi_2),...
                                                       (1+xi_1)*(1+xi_2),...
                                                       (1-xi_1)*(1+xi_2)];

                                    GeomBoundDiff = 0.25*[-(1-xi_2), (1-xi_2), (1+xi_2),-(1+xi_2);
                                                          -(1-xi_1),-(1+xi_1), (1+xi_1), (1-xi_1)];


                            end

                            % Berechnung der Raum-Zeit-Formfunktionen
                            vSurfShapeST = [0.5*(1-tau)*vSurfShape, 0.5*(1+tau)*vSurfShape];

                            %Berechnung des Basisvektors
                            JSurf = GeomBoundDiff * CoordSurf;

                            %Calculate surface area
                            detJSurf = det(JSurf);

                            IntFac = weight_quad(1,kk) * weight(1,jj) * weight(1,ll) * detJT * detJSurf;


                            switch elem.bcNeu(ii,5) % Spannungsverlauf auf Rand

                                case 0 %konstanter Verlauf der Randspannung
                                    %Spannungsfreiwerte
                                    StressSurfVal = elem.bcNeu(ii,8)*ModBCquad(elem.bcNeu(ii,7),:)';

                                    %Formfunktion                                       
                                    stress_Surf_shape = [t_1q, t_2q, t_3q];

                                    %Randspannung ausgewertet am
                                    %Gauss-Punkt (ortsunabhaengig weil
                                    %konstant)
                                    StressSurfGP = stress_Surf_shape * StressSurfVal;

                                case 1 %linearer Verlauf der Randspannung                                    

                                    % Spannung am Gausspunkt interpoliert
                                    % aus den Werten an den Knoten und mit
                                    % Zeitverlauf multipliziert
                                    StressSurfVal = ((elem.bcNeu(ii,8:11)*vSurfShape)*ModBCquad(elem.bcNeu(ii,7),:))';

                                    %Formfunktionen in der Zeit:                                       
                                    stress_Surf_shape = [t_1q, t_2q, t_3q];

                                    %Randspannung am Raum-Zeit-Gausspunkt:
                                    StressSurfGP = stress_Surf_shape * StressSurfVal;

                                case 2
                                    disp('Quadratischer Verlauf bislang nicht implementiert')
                            end


                            %Auswertung der Randspannungen im jeweiligen Gauss-Punkt
                            switch elem.bcNeu(ii,4)
                                case 1  %Randspannung in globaler x-Richtung
                                    StressSurfX = StressSurfX + vSurfShapeST' * (StressSurfGP * IntFac);

                                case 2  %Randspannung in globaler y-Richtung
                                    StressSurfY = StressSurfY + vSurfShapeST' * (StressSurfGP * IntFac);

                                case 3
                                    StressSurfZ = StressSurfZ + vSurfShapeST' * (StressSurfGP * IntFac);

                                case 4 %Spannungen normal zur Flaeche
                                    %Berechnung des Normalenvektors aus den
                                    %Diagonalen der Flaeche:
                                    Diag1 = GlobCoord(Nodes(3),:) - GlobCoord(Nodes(1),:);
                                    Diag2 = GlobCoord(Nodes(4),:) - GlobCoord(Nodes(2),:);

                                    % Bestimme Normalenvektor mit Kreuzprodukt:
                                    vector_normal = cross(Diag1, Diag2);

                                    %Normierung des Normalenvektors
                                    vector_normal_norm = 1/(sqrt(vector_normal(1,1)^2 + vector_normal(1,2)^2 + vector_normal(1,3)^2))*vector_normal;

                                    switch elem.bcNeu(ii,6)
                                        case 1 % Spannungs-RB
                                            %Transformation der Randspannungen in das globale Koordinatensystem
                                            StressSurfGlob = StressSurfGP * vector_normal_norm;

                                            %Aufbau des Elementlastvektors
                                            StressSurfX    = StressSurfX + vSurfShapeST' * (StressSurfGlob(1,1) * IntFac);
                                            StressSurfY    = StressSurfY + vSurfShapeST' * (StressSurfGlob(1,2) * IntFac);
                                            StressSurfZ    = StressSurfZ + vSurfShapeST' * (StressSurfGlob(1,3) * IntFac);

                                        case 2 %Didisp-RB
                                            % Calculate the density of surface charge
                                            DiDispGlob      = elem.bcNeu(ii,8:10) * vector_normal_norm';

                                            % Use that PsiSurfShape = vSurfShape 
                                            DiDispSurf     = DiDispSurf + vSurfShape' * (DiDispGlob * IntFac);

                                    end
                            end
                        end
                    end
                end


                switch elem.bcNeu(ii,6)
                    case 1 % Mechanische RB
                        %Steuerdaten zum Einbau der Spannungsrandbedingungen
                        %---------------------------------------------------------------      
                        % Nur fuer 3D zulaessig:
                        PosSurfX = [Nodes, Nodes + 8];
                        PosSurfY = PosSurfX + 16;
                        PosSurfZ = PosSurfX + 32;

                        %Es wird davon ausgegangen, dass das Element verdreht im Raum
                        %liegt, so dass eine eingepraegte Randspannung in allen
                        %Raumrichtungen Arbeit leisten kann
% %                                 rhs(PosSurfX) = rhs(PosSurfX) - StressSurfX;
% %                                 rhs(PosSurfY) = rhs(PosSurfY) - StressSurfY;
% %                                 rhs(PosSurfZ) = rhs(PosSurfZ) - StressSurfZ;
                        rhs(PosSurfX) = rhs(PosSurfX) + StressSurfX;
                        rhs(PosSurfY) = rhs(PosSurfY) + StressSurfY;
                        rhs(PosSurfZ) = rhs(PosSurfZ) + StressSurfZ;


                    case 2 % Elektrische RB
                        % Steuerdaten fuer Einbau der RB in Resi (nur 3D zulaessig):
                        PosSurfPsi = 48 + [Nodes, Nodes + 8];

                        rhs(PosSurfPsi) = rhs(PosSurfPsi) - DiDispSurf;

                    otherwise
                        error('Error setting BcNeumann')
                end
        end
    end
end


%------------------------------------------------------------------
%------------------------------------------------------------------


function SetIndexSys(elem , vec) 
    % Set indexSystem which is used to store the element matrix in
    % the global system of equations:
    elem.indexSystem = vec; 
end


%------------------------------------------------------------------
%------------------------------------------------------------------

function GetLocPosBcDir(elem)

    % Get global node numbers and global DOF-IDs
    eleNodesDofGlob = elem.GetNodesDofGlobal;

    bcDirMod    = elem.bcDir;
    nBcDir      = size(bcDirMod,1); % number of bcDir in current element

    if ~isempty(bcDirMod)
        for ii = 1 : nBcDir

            posLocBcDir = find( eleNodesDofGlob(:,1) ...  % Global node numbers have
                                ==                   ...  % to be identical
                                bcDirMod(ii,1)       ...
                                &                    ...  
                                eleNodesDofGlob(:,2) ...  % Global DOF-IDs have
                                ==                   ...  % to be identical
                                bcDirMod(ii,2)       ...
                              );

            if isempty(posLocBcDir) % the current BC cannot be set in the current element
                bcDirMod(ii,:) = 0;
            else % Store the local position of the current BC in col 7:
                bcDirMod(ii,7) = posLocBcDir;
            end

        end

        % Delete BCs which cannot be set in the current element:
        elem.bcDir = bcDirMod(bcDirMod(:,1) > 0, :);

    end

end

%------------------------------------------------------------------
%------------------------------------------------------------------

function eleNodesDofGlob = GetNodesDofGlobal(elem)
% Functions returns the global node numbers and the global DOF-IDs
% of the element nodes.

    % Get assignment of local node IDs with local DOF-IDs
    eleNodesDofLoc   = elem.GetEleNodesDofLoc; %#ok<MCNPN> call to child class method


    % Initialisation        

    % eleNodesDofGlob  = zeros(size(eleNodesDofLoc)); % old

    eleNodesDofGlob      = zeros( size(eleNodesDofLoc)         ); 

    eleNodesGlob_t0      = zeros( size(eleNodesDofGlob,1)/2, 1 );
    eleNodesGlob_t1      = zeros( size(eleNodesDofGlob,1)/2, 1 );


    % Transformation of local node IDs to global node IDs

    % eleNodesDofGlob(:, 1) = elem.eleNodes(eleNodesDofLoc(:, 1));  % old

    numbDofLocT0      = size(eleNodesDofLoc,1) / 2;

    eleNodesGlob_t0(:,1) = elem.eleNodes_t0( eleNodesDofLoc(   1          : numbDofLocT0,1) );
    eleNodesGlob_t1(:,1) = elem.eleNodes_t1( eleNodesDofLoc(numbDofLocT0+1:     end     ,1) );



    numbNodesEle  = elem.ind.nNodesOut; %#ok<MCNPN> call to child class method
    numbDofAtNode = size(eleNodesDofLoc,1) / numbNodesEle;


    for nn = 1:numbDofAtNode/2

    eleNodesDofGlob( 2*(nn-1)*numbNodesEle+1 : 2*nn*numbNodesEle, 1)   ...
    =                                                                  ...
    [ eleNodesGlob_t0( (nn-1)*numbNodesEle+1 : nn*numbNodesEle ,1 ) ;  ...
      eleNodesGlob_t1( (nn-1)*numbNodesEle+1 : nn*numbNodesEle ,1 )    ...
    ];

    end

    % Transformation of local node DOFs to global node DOFs          
    eleNodesDofGlob(:, 2) = elem.eleDofGlob( eleNodesDofLoc(:,2) );



end



%------------------------------------------------------------------
%------------------------------------------------------------------

function SetEleNodes(elem, eleConnect_t0 , eleConnect_t1)

    elem.eleNodes_t0 = eleConnect_t0;
    elem.eleNodes_t1 = eleConnect_t1;
end






end % end of methods
end % end of classdefinition

