classdef HeatElemSuperclass < ElemSuperclass
% ...

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 
properties (SetAccess = protected)

% Properties needed in postprocessing for calculating secondary unknowns
heatFlux        % Waermestrom [W/m^2]

detJSp          % Determinante Jacobi-Matrix (rein r�umlich)
detJSpTi        % Determinante Jacobi-Matrix (Raum-Zeit)

Pe              % Element - P�clet-Zahl

end
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
     
methods

    
function elem = HeatElemSuperclass( eleConnect    , ...
                                    eleCoord      , ...
                                    eleMatPara    , ...
                                    eleLoadPara   , ...
                                    eleBcDir      , ...
                                    eleBcNeu      , ...
                                    numGlobInput    ...
                                  )


    %Call superclass constructor
    elem = elem@ElemSuperclass ( eleConnect    , ...
                                 eleCoord      , ...
                                 eleMatPara    , ...
                                 eleLoadPara   , ...
                                 eleBcDir      , ...
                                 eleBcNeu      , ...
                                 numGlobInput    ...
                               );
                           
    
    % Waermestrom [W/m^2]
    elem.heatFlux = zeros(8,2);     % [ qx_A , qy_A, qz_A ]  t0
                                    % [ qx_B , qy_B, qz_B ]
                                    %         ...
                                    % [ qx_D , qy_D, qz_D ]
                                    % [ qx_E , qy_E, qz_E ]  t1
                                    %         ...
                                    % [ qx_H , qy_H, qz_H ]
                                    
    % Determinante Jacobi-Matrix
    elem.detJSp   = zeros(8,1); % rein raeumlich
    elem.detJSpTi = zeros(8,1); % Raum-Zeit
    
    % Element - P�clet-Zahl
    elem.Pe = 0;
                                    
end

       
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
function Output = OutputFieldsInit(elem)
% Function initialises output fields for space-only Paraview output

    % Initialising element output fields:
    %--------------------------------------

    % name of the output file
    Output.filename     = elem.name; 

    % number of nodes per element
    Output.nNodesEle    = size(elem.eleNodes_t1,2); 

    if elem.probDim == 2 
        Output.ele_type = 'Quadrilateral';   % 2D element
        Output.outId    = [0 1 2 3];         % Help variable 
    elseif elem.probDim == 3
        Output.ele_type = 'Hexahedron';      % 3D element
        Output.outId    = [0 1 2 3 4 5 6 7]; % Help variable 
    end

    Output.nodesOut     = [];

    Output.coord        = []; % coordinates of the nodes
    Output.connect      = []; % connectivity of the element

    Output.z1           = []; % counter help variable
    Output.z2           = []; % counter help variable


    % 'Cell'-type variables only possess one value per element
    Output.cells.num    = 6; %number of cell-variables occurring in the output
    Output.cells.name   = cell(1,Output.cells.num);
    Output.cells.type   = cell(1,Output.cells.num);
    Output.cells.data   = cell(1,Output.cells.num);

    Output.cells.name{1,1}  = 'element number';
    Output.cells.type{1,1}  = 'Int';
    
    Output.cells.name{1,2}  = 'density [kg/m^3]';
    Output.cells.type{1,2}  = 'Float';

    Output.cells.name{1,3}  = 'capacity [J/(kg*K)]';
    Output.cells.type{1,3}  = 'Float'; 

    Output.cells.name{1,4}  = 'conductivity [W/(m*K)]';
    Output.cells.type{1,4}  = 'Float';

    Output.cells.name{1,5}  = 'Peclet number';
    Output.cells.type{1,5}  = 'Float';
    
    Output.cells.name{1,6}  = 'heat source [W/m^3]';
    Output.cells.type{1,6}  = 'Float';

    % 'Scalar'-type variables are '1-entry' variables changing in
    % the element
    Output.scalars.num  = 4; %number of scalar-variables occurring in the output
    Output.scalars.name = cell(1,Output.scalars.num);
    Output.scalars.type = cell(1,Output.scalars.num);
    Output.scalars.data = cell(1,Output.scalars.num);

    Output.scalars.name{1,1}  = 'temperature [C]';
    Output.scalars.type{1,1}  = 'Float';
    
    Output.scalars.name{1,2}  = 'det(J_Sp)';
    Output.scalars.type{1,2}  = 'Float';
    
    Output.scalars.name{1,3}  = 'det(J_SpTi)';
    Output.scalars.type{1,3}  = 'Float';
    
    Output.scalars.name{1,4}  = 'node number';
    Output.scalars.type{1,4}  = 'Int';

    
    % 'Vector'-type variables are '3-entry' variables; each entry
    % changes in the elment
    Output.vectors.num  = 2; %number of vector-variables occurring in the output
    Output.vectors.name = cell(1,Output.vectors.num);
    Output.vectors.type = cell(1,Output.vectors.num);
    Output.vectors.data = cell(1,Output.vectors.num);

    Output.vectors.name{1,1} = 'heat flux [W/m^2]';
    Output.vectors.type{1,1} = 'Float'; 
 
    Output.vectors.name{1,2} = 'coordinates';
    Output.vectors.type{1,2} = 'Float'; 


    % 'Tensor'-type varibales are '9-entry' variables; each entry
    % changes in the element
    Output.tensors.num  = 0; %number of tensor-variables occurring in the output
    Output.tensors.name = cell(1,Output.tensors.num);
    Output.tensors.type = cell(1,Output.tensors.num);
    Output.tensors.data = cell(1,Output.tensors.num);

    % --no tensor data--

end
        
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
function Output = GetOutputData( elem      , ...
                                 Output    , ...
                                 eleSolu   , ...
                                 eleCoords , ...
                                 eleMatPara, ...
                                 eleLoadPara ...
                               )

    % Get the output data from postprocessing and store it:

    % Get the post-processed data of the current element:
    [ergHeat, hh] = elem.PostOut(eleSolu, eleCoords); % hh = Zahl der Ergebnisssaetze je Elem
    
    hh = hh/2;

    % Get index for data storage:
    index = (Output.z1 : (Output.z1 + hh - 1));

    % Store node coordinates
    switch elem.probDim
        case 2
            Output.coord(index,1) = eleCoords.t1(1:4,1);
            Output.coord(index,2) = eleCoords.t1(1:4,2);
            Output.coord(index,3) = eleCoords.t1(1:4,3);
        case 3
            Output.coord(index,1) = eleCoords.t1(1:8,1);
            Output.coord(index,2) = eleCoords.t1(1:8,2);
            Output.coord(index,3) = eleCoords.t1(1:8,3);
    end

    % Store connectivity   
    switch elem.probDim
        case 2
            Output.connect(Output.z2,1:4) = Output.outId;
        case 3
            Output.connect(Output.z2,1:8) = Output.outId;
    end


    % Store cell type output data
    Output.cells.data{1,1}(Output.z2,1) = elem.numGlob;      %'element number'
    
    Output.cells.data{1,2}(Output.z2,1) = eleMatPara.rho;    %'density'
    Output.cells.data{1,3}(Output.z2,1) = eleMatPara.c;      %'capacity'
    Output.cells.data{1,4}(Output.z2,1) = eleMatPara.lambda; %'conductivity'
    Output.cells.data{1,5}(Output.z2,1) = elem.Pe;           %'P�clet number'
    
    Output.cells.data{1,6}(Output.z2,1) = eleLoadPara.source; %'heat source'
    

    % Store scalar type output data
    Output.scalars.data{1,1}(index,1)   = ergHeat(5:8,1);     %'temperature'
 
    Output.scalars.data{1,2}(index,1)   = elem.detJSp(5:8);   %'det(J_Sp)'
    Output.scalars.data{1,3}(index,1)   = elem.detJSpTi(5:8); %'det(J_SpTi)'
    
    Output.scalars.data{1,4}(index,1)   = elem.eleNodes_t1;   %'node number'


    % Store vector type output data
    Output.vectors.data{1,1}(index,1:3) = ergHeat(5:8,2:4);   %'heat flux'


    %'coordinates'
    switch elem.probDim
        case 2
            Output.vectors.data{1,2}(index,1) = eleCoords.t1(1:4,1);
            Output.vectors.data{1,2}(index,2) = eleCoords.t1(1:4,2);
            Output.vectors.data{1,2}(index,3) = eleCoords.t1(1:4,3);
        case 3
            Output.vectors.data{1,2}(index,1) = eleCoords.t1(1:8,1);
            Output.vectors.data{1,2}(index,2) = eleCoords.t1(1:8,2);
            Output.vectors.data{1,2}(index,3) = eleCoords.t1(1:8,3);
    end


    % Store tensor type output data
    % --no tensor data--


    % Update indices for data storage:
    Output.z2    = Output.z2    + 1;
    Output.z1    = Output.z1    + hh;
    Output.outId = Output.outId + hh;

end %GetOutputData 
        
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

end %methods    
end %classdef