classdef HeatCond2202 < HeatElemSuperclass
    
% Description:
% 2D Waermeleitungselement (stationaer, kovektive und diffusive Waermeleitung)
% 8-Knoten Raum-Zeit - Element
% Trilineare Funktionen (Geometrie, Test, Interpolation)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            
properties (SetAccess = protected)

% General properties:
%-----------------------------------------------------------------
type        = 2202;                   % identification number of element type
name        = 'HeatCond2202';         % element name
sizeXest    = 8*8;                    % (estimated) number of non-zero entries in eleMat
probDim     = 2;                      % problem dimension
paraOutput  = 1;                      % Paraview output for current element
                                      % 0: no Paraview output
                                      % 1:    Paraview output

                                      
% Indices to access the element matrix:
%----------------------------------------------------------------

ind = struct('nNodesOut', 4      ,... % Number of nodes for output
             'T'        , (1:8)' ,... % pos of all temperature dof in eleMat
             'T_t0'     , (1:4)' ,... % pos of T_t0 in eleMat and eleVec
             'T_t1'     , (5:8)' ,... % pos of T_t1 in eleMat and eleVec
             'nDofT'    , 8      ,... % total number of temperature dof
             'nDofEle'  , 8       ... % total number of dof
            );

end
           
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
methods

    
function elem = HeatCond2202( eleConnect    , eleCoord      , ...
                              eleMatPara    , eleLoadPara   , ...
                              eleBcDir      , eleBcNeu      , ...
                              mm                              ...
                            )

    % Call superclass constructor
    elem = elem@HeatElemSuperclass( eleConnect    , eleCoord      , ...
                                    eleMatPara    , eleLoadPara   , ...
                                    eleBcDir      , eleBcNeu      , ...
                                    mm                              ...
                                  );

end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function[ eleVec , ...
          I_elem , ...
          J_elem , ...
          X_elem       ] = Build( elem        , ...
                                  eleSolu     , ...
                                  ~           , ... % eleSoluDis
                                  eleCoords   , ...
                                  time        , ...
                                  ~ )               % currTimeVar

                              
%----------------------------------------------------------------
% Auslesen d. Eingabdaten
%----------------------------------------------------------------

% Element-Positionsindex
pos = elem.ind;

% Zeit
t0 = time.curr(1,1);
t1 = time.curr(1,3);
%delta_t = t1 - t0;

% Materialdaten
lambda   = elem.matPara.lambda; % [ W/(m *K)] Leitfaehigkeit
rho      = elem.matPara.rho;    % [kg/ m^3  ] Dichte
capacity = elem.matPara.c;      % [ J/(kg*K)] spez. Waermeleitfaehigkeit

% Quelle
Q   = elem.loadPara.source;
Q   = Q / (rho * capacity);

vx  = elem.matPara.vx; % [m/s]
vy  = elem.matPara.vy; % [m/s]


% L�sungen vorhergehender Iterationsschritt
T    = eleSolu(pos.T); % Temperaturwerte dieser Zeitscheibe aus letztem NR-Iterationsschritt

% Koordinaten (Raum-Zeit, global)
% [ x ]   [ xA xB xC xD | xE xF xG xH ]
% [ y ] = [ yA yB yC yD | yE yF yG yH ]
% [ t ]   [ t0 t0 t0 t0 | t1 t1 t1 t1 ]

X = [ eleCoords.t0(:,1:2)', eleCoords.t1(:,1:2)'; ...
      t0 t0 t0 t0         , t1 t1 t1 t1           ...
    ];

%----------------------------------------------------------------
% Elementvolumen
%----------------------------------------------------------------

% Lokal-abgeleitete r�uml. Ansatzfunktionen (bei xi1 = xi2 = 0)
%                   [ dN/dxi1 | dN/dxi2 ]
N_Sp_d_Xi0  = 1/4 * [ (-1)*(1), (1)*(-1); ...
                      (+1)*(1), (1)*(-1); ...
                      (+1)*(1), (1)*(+1); ... 
                      (-1)*(1), (1)*(+1)  ...
                    ];

J_Sp_Xi0     = X(1:2,1:4) * N_Sp_d_Xi0;
det_J_Sp_Xi0 = det(J_Sp_Xi0);

Vol_el       = 4 * det_J_Sp_Xi0;
h_el         = sqrt( 4*Vol_el / pi );

%----------------------------------------------------------------
% Element - Peclet-Zahl
%----------------------------------------------------------------
kappa  = lambda / ( rho * capacity ); % [m^2/s] Temperaturleitfaehigkeit
norm_v = sqrt(vx^2 + vy^2);           % [m/s]   Betrag Geschwindigkeitsvektor
L_char = h_el/2;                      % [m]     charakterist. Elementlaenge

Pe = L_char * norm_v / kappa;         % [-]     Element - P�clet-Zahl

elem.Pe = Pe;

%----------------------------------------------------------------
% Stabilisierungsparameter
%----------------------------------------------------------------
% Der Stabilisierungsparameter kann hier vorab ermittelt werden, sofern die
% Materialparameter als auch die Geschwindigkeiten konstant sind.

% Brooks, Hughes; 1981: F�r 1D-Probleme raeuml. exakt Lsg.
%tau_stab = ( L_char / norm_v) * (coth(Pe) - 1/Pe);

% Shakib, Hughes; 1991: Stat. Fall aus Raum-Zeit-Formulierung abgeleitet
tau_stab = 1/sqrt( (2*norm_v/h_el)^2 + (12*kappa/h_el)^2 );


%----------------------------------------------------------------
% Initialisierungen
%----------------------------------------------------------------

eleMat = zeros( pos.nDofEle, pos.nDofEle ); % Element-Matrix (Tangentenmatrix)
eleVec = zeros( pos.nDofEle, 1           ); % Element-Vektor

R_conv = zeros( pos.nDofT,      1        );
K_conv = zeros( pos.nDofT, pos.nDofT     );

R_diff = zeros( pos.nDofT,      1        );
K_diff = zeros( pos.nDofT, pos.nDofT     );

R_stab = zeros( pos.nDofT,      1        );
K_stab = zeros( pos.nDofT, pos.nDofT     );

R_Q    = zeros( pos.nDofT,      1        );

%----------------------------------------------------------------
% Numerische Integration
%----------------------------------------------------------------

% Gau�punkte
gp_coord  = [ -1/sqrt(3) , 1/sqrt(3) ];
gp_weight = [     1      ,    1      ];


% Integrationsschleifen
for kk = 1:2 % xi1: lokaler Raum
    xi1 = gp_coord(kk);
    
    for ll = 1:2 % xi2: lokaler Raum
        xi2 = gp_coord(ll);
      
        for nn = 1:2 % tau: lokale Zeit
            tau = gp_coord(nn);
            
                % Ansatzfunktionen
                N       = 1/8 * [ (1-xi1)*(1-xi2)*(1-tau); ...
                                  (1+xi1)*(1-xi2)*(1-tau); ...
                                  (1+xi1)*(1+xi2)*(1-tau); ... 
                                  (1-xi1)*(1+xi2)*(1-tau); ...
                                  (1-xi1)*(1-xi2)*(1+tau); ...
                                  (1+xi1)*(1-xi2)*(1+tau); ...
                                  (1+xi1)*(1+xi2)*(1+tau); ...
                                  (1-xi1)*(1+xi2)*(1+tau)  ...
                                ];
                            
                % Lokal-Abgeleitete Ansatzfunktionen: 
                %               [        dN/dxi1      |       dN/dxi2       |        dN/dtau      ] 
                N_d_Xi  = 1/8 * [ (-1)*(1-xi2)*(1-tau), (1-xi1)*(-1)*(1-tau), (1-xi1)*(1-xi2)*(-1); ...
                                  (+1)*(1-xi2)*(1-tau), (1+xi1)*(-1)*(1-tau), (1+xi1)*(1-xi2)*(-1); ...
                                  (+1)*(1+xi2)*(1-tau), (1+xi1)*(+1)*(1-tau), (1+xi1)*(1+xi2)*(-1); ... 
                                  (-1)*(1+xi2)*(1-tau), (1-xi1)*(+1)*(1-tau), (1-xi1)*(1+xi2)*(-1); ...
                                  (-1)*(1-xi2)*(1+tau), (1-xi1)*(-1)*(1+tau), (1-xi1)*(1-xi2)*(+1); ...
                                  (+1)*(1-xi2)*(1+tau), (1+xi1)*(-1)*(1+tau), (1+xi1)*(1-xi2)*(+1); ...
                                  (+1)*(1+xi2)*(1+tau), (1+xi1)*(+1)*(1+tau), (1+xi1)*(1+xi2)*(+1); ...
                                  (-1)*(1+xi2)*(1+tau), (1-xi1)*(+1)*(1+tau), (1-xi1)*(1+xi2)*(+1)  ...
                               ];

                % Jakobi-Matrix (Raum-Zeit Gebiet)
                J_SpTi        = X * N_d_Xi;
                det_J_SpTi    = det(J_SpTi);

                % Integrationsfaktor Raum-Zeit Gebiet
                intFacQ  = gp_weight(kk) * gp_weight(ll) * gp_weight(nn) * det_J_SpTi;

                % Inverse Jakobi-Matrix
                inv_J_SpTi    = inv(J_SpTi);        
                inv_J_SpTi_x  = inv_J_SpTi(:,1);
                inv_J_SpTi_y  = inv_J_SpTi(:,2);

                % Global-Abgeleitet Ansatzfunktionen
                N_d_x   = N_d_Xi * inv_J_SpTi_x;
                N_d_y   = N_d_Xi * inv_J_SpTi_y;
    
                T_d_x = N_d_x' * T;
                T_d_y = N_d_y' * T;
 
                
                % Quellterm
                L_Q     = Q * intFacQ;
                R_Q     = R_Q + N * L_Q;
                                
                % Konvektiver Teil
                L_conv_x     = T_d_x  * vx;
                L_conv_x_d_T = N_d_x' * vx;
                
                L_conv_y     = T_d_y  * vy;
                L_conv_y_d_T = N_d_y' * vy;
                               
                L_conv     = L_conv_x     + L_conv_y;
                L_conv_d_T = L_conv_x_d_T + L_conv_y_d_T;
                
                R_conv = R_conv + N * (L_conv     * intFacQ);                            
                K_conv = K_conv + N * (L_conv_d_T * intFacQ);

                
                % Diffusiver Teil                
                L_diff_x     = T_d_x  * kappa;
                L_diff_x_d_T = N_d_x' * kappa;
                
                L_diff_y     = T_d_y  * kappa;
                L_diff_y_d_T = N_d_y' * kappa;
                                              
%                 L_diff     = L_diff_x     + L_diff_y;
%                 L_diff_d_T = L_diff_x_d_T + L_diff_y_d_T;
                
                R_diff = R_diff + N_d_x * (L_diff_x     * intFacQ);
                R_diff = R_diff + N_d_y * (L_diff_y     * intFacQ);
                
                K_diff = K_diff + N_d_x * (L_diff_x_d_T * intFacQ);
                K_diff = K_diff + N_d_y * (L_diff_y_d_T * intFacQ);
                
                                            
                % Randquelle
                % r_t = r_t    + ? * intFacP;

                
                % Stabilisierung (GLS, entspricht hier auch SUPG)
                L     = L_conv - L_Q; 
                L_d_T = L_conv_d_T;

                W_stab =          N_d_x * vx;
                W_stab = W_stab + N_d_y * vy;    
               
                R_stab = R_stab + W_stab * (L     * (tau_stab * intFacQ));
                K_stab = K_stab + W_stab * (L_d_T * (tau_stab * intFacQ));
              

        end % tau  
    end % xi2
end % xi1



%----------------------------------------------------------------
% Abspeichern Teilmatrizen
%----------------------------------------------------------------

% Element-Vektor (Residuum)
%-----------------------------------
eleVec(pos.T)    = eleVec(pos.T)    + R_conv;
eleVec(pos.T)    = eleVec(pos.T)    + R_diff;
eleVec(pos.T)    = eleVec(pos.T)    + R_stab;
eleVec(pos.T)    = eleVec(pos.T)    - R_Q;

eleVec = - eleVec;


% Element-Matrix (Tangenten-Matrix)
%-----------------------------------
eleMat(pos.T   ,pos.T   ) = eleMat(pos.T   ,pos.T   ) + K_conv;
eleMat(pos.T   ,pos.T   ) = eleMat(pos.T   ,pos.T   ) + K_diff;
eleMat(pos.T   ,pos.T   ) = eleMat(pos.T   ,pos.T   ) + K_stab;


% Abspeichern (der nicht-Null Eintr�ge)
[ I_loc, J_loc, X_elem ] = find(eleMat);

I_elem = elem.indexSystem(I_loc);
J_elem = elem.indexSystem(J_loc);
   

end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function Post(elem, soluElem, ~, ~ )
% Nachlaufrechnung

temperature = soluElem;
X           = elem.eleCoord_t1;
lambda      = elem.matPara.lambda;

% Gausspunkte
%             xi1, xi2, tau
cp_coord  = [ -1 , -1 , -1 ; ...
               1 , -1 , -1 ; ...
               1 ,  1 , -1 ; ...
              -1 ,  1 , -1 ; ...
              -1 , -1 ,  1 ; ...
               1 , -1 ,  1 ; ...
               1 ,  1 ,  1 ; ...
              -1 ,  1 ,  1 ; ...
            ];

% Integrationsschleife

for ii = 1:size(cp_coord,1)
    
    xi1 = cp_coord(ii,1);
    xi2 = cp_coord(ii,2);
    tau = cp_coord(ii,3);

    % Lokal-abgeleitete r�uml. Ansatzfunktionen
    %                   [     dN/dxi1 |    dN/dxi2  ]
    N_Sp_d_Xi   = 1/4 * [ (-1)*(1-xi2), (1-xi1)*(-1); ...
                          (+1)*(1-xi2), (1+xi1)*(-1); ...
                          (+1)*(1+xi2), (1+xi1)*(+1); ... 
                          (-1)*(1+xi2), (1-xi1)*(+1)  ...
                        ];     
                    
    % Lokal-Abgeleitete Ansatzfunktionen: 
    %               [        dN/dxi1      |       dN/dxi2       |        dN/dtau      ] 
    N_d_Xi  = 1/8 * [ (-1)*(1-xi2)*(1-tau), (1-xi1)*(-1)*(1-tau), (1-xi1)*(1-xi2)*(-1); ...
                      (+1)*(1-xi2)*(1-tau), (1+xi1)*(-1)*(1-tau), (1+xi1)*(1-xi2)*(-1); ...
                      (+1)*(1+xi2)*(1-tau), (1+xi1)*(+1)*(1-tau), (1+xi1)*(1+xi2)*(-1); ... 
                      (-1)*(1+xi2)*(1-tau), (1-xi1)*(+1)*(1-tau), (1-xi1)*(1+xi2)*(-1); ...
                      (-1)*(1-xi2)*(1+tau), (1-xi1)*(-1)*(1+tau), (1-xi1)*(1-xi2)*(+1); ...
                      (+1)*(1-xi2)*(1+tau), (1+xi1)*(-1)*(1+tau), (1+xi1)*(1-xi2)*(+1); ...
                      (+1)*(1+xi2)*(1+tau), (1+xi1)*(+1)*(1+tau), (1+xi1)*(1+xi2)*(+1); ...
                      (-1)*(1+xi2)*(1+tau), (1-xi1)*(+1)*(1+tau), (1-xi1)*(1+xi2)*(+1)  ...
                   ];

    % Jakobi-Matrix (Raum-Gebiet am Zeitscheibenanfang)
    J_Sp            = X(1:2,1:4) * N_Sp_d_Xi;
    det_J_Sp        = det(J_Sp);
    elem.detJSp(ii) = det_J_Sp;
    
    % Jakobi-Matrix (Raum-Zeit Gebiet)
    J_SpTi            = X * N_d_Xi;
    det_J_SpTi        = det(J_SpTi);
    elem.detJSpTi(ii) = det_J_SpTi;
    
    % Inverse Jakobi-Matrix
    inv_J_SpTi    = inv(J_SpTi);        
    inv_J_SpTi_x  = inv_J_SpTi(:,1);
    inv_J_SpTi_y  = inv_J_SpTi(:,2);

    % Global-Abgeleitet Ansatzfunktionen
    N_d_x   = N_d_Xi * inv_J_SpTi_x;
    N_d_y   = N_d_Xi * inv_J_SpTi_y;      
          
    elem.heatFlux(ii,1) = - (lambda * N_d_x') * temperature;
    elem.heatFlux(ii,2) = - (lambda * N_d_y') * temperature;
   
end

end
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [ergHeat, sizeErg] = PostOut(elem, eleSolu, ~)
% Auslesen der Loesungswerte des aktuellen Elementes aus dem
% Gesamtloesungsvektor.

pos = elem.ind;

% Number of nodes for which output is returned
sizeErg = pos.nNodesOut*2;

% Solution of current element from overall solution vector
temperature   = eleSolu(pos.T);
heatFlux_x    = elem.heatFlux(:,1);
heatFlux_y    = elem.heatFlux(:,2);

% Element solution for output
ergHeat      = zeros(sizeErg,4);

ergHeat(1:4,1) = temperature(pos.T_t0);          
ergHeat(5:8,1) = temperature(pos.T_t1);

ergHeat(1:4,2) = heatFlux_x(1:4);
ergHeat(5:8,2) = heatFlux_x(5:8);

ergHeat(1:4,3) = heatFlux_y(1:4);
ergHeat(5:8,3) = heatFlux_y(5:8);

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [solUpdate , index] = UpdateSol( elem          , ...
                                          eleSolu       , ...
                                          deltaTCurr    , ...
                                          deltaTNext    , ...
                                          typeEleUpdate   ...
                                        )
% Ermittlung der Startwerte fuer naechste NR-Iteration
% Funktion wird in 'UpdateSolution' aufgerufen

% deltaTCurr: time width of current time  slab
% deltaTNext: time width of next time slab
% typeEleUpdate: 1:    use gradient of current time slab to estimate end values
%                      of next time slab which serve as initial values for next
%                      NR iteration
%                else: gradient is not used, initial values for
%                      start and end of next time slab are the same

pos = elem.ind;

% Get unknowns from eleSolu:
temperature = eleSolu(pos.T);

% Update unknowns for next time slab:
if typeEleUpdate == 1

    deltaTempCurr          =   temperature(pos.T_t1) - temperature(pos.T_t0);
    temperature(pos.T_t0)  =   temperature(pos.T_t1); 
    temperature(pos.T_t1)  =   temperature(pos.T_t0) ...
                             + deltaTempCurr * (deltaTNext / deltaTCurr);

else
    temperature(pos.T_t0)  = temperature(pos.T_t1);

end

% Initialisation of updated solution:
solUpdate = zeros(pos.nDofEle, 1);

% Set values:
solUpdate(pos.T)    = temperature;

index               = elem.indexSystem;   

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function Update(elem, ~, deltaTCurr, deltaTNext, typeEleUpdate)
% Ermittlung der Startwerte fuer naechste NR-Iteration der Variablen der
% Nachlaufrechnung
    
% Update der Nachlaufgr��en muss nur bei gemischter Formulierung erfolgen.

% deltaTCurr: time width of current time  slab
% deltaTNext: time width of next time slab
% typeEleUpdate: 1:    use gradient of current time slab to estimate end values
%                      of next time slab which serve as initial values for next
%                      NR iteration
%                else: gradient is not used, initial values for
%                      start and end of next time slab are the same


% % Update secondary unknowns for next time slab:
% if typeEleUpdate == 1
% 
%     deltaHeatFlux        =   elem.heatFlux(5:8,:) - elem.heatFlux(1:4,:);
%     elem.heatFlux(1:4,:) =   elem.heatFlux(5:8,:); 
%     elem.heatFlux(5:8,:) =   elem.heatFlux(5:8,:) ...
%                            + deltaHeatFlux * (deltaTNext / deltaTCurr);
% 
% else
%     elem.heatFlux(1:4,:)  = elem.heatFlux(5:8,:);
% 
% end


end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function Restore(elem, ~, deltaTCurr, deltaTNext, typeEleUpdate)
% Funktion wird von 'RestoreElem' aufgerufen im Fall, dass im aktuellen
% Loesungsschritt keine Konvergenz stattgefunden hat und die Zeitschritte
% reduziert werden muss.
% F�r die Zeitscheibe mit nun reduziertem Zeitschritt werden die Startwerte
% der Nachlaufvariablen f�r den ersten NR-Schritt ermittelt.

% -- no secondary unknowns stored for this element type --

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function SetEleDofGlob(elem)
% Funktion stellt den Zusammenhang zwischen lokalen und globalen FHG her.
% Es wird ein Vektor erzeugt, der jedem lokalen FHG den korrespondierenden
% globalen FHG zuweist.
% Die Anzahl an Spalten entspricht der Anzahl an vorhandenen lokalen FHGen.
%
% Spaltennr.: local dof id
% Wert:       global dof id


elem.eleDofGlob    = [ 5, 25 ];
% Aufgrund des gew�hlten Ansatzes (s. Build-Routine)
% ist die Reihenfolge der globalen FHG hier zwingend.

    
    
end

end % end of methods

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

methods (Static)

function eleNodesDofLoc = GetEleNodesDofLoc
% Function returns a matrix containing local node numbers and local dof ids

%                [ local node numb | local dof id ]
eleNodesDofLoc = [      1          ,    1; ...        % loc dof id 1: T_t0
                        2          ,    1; ...
                        3          ,    1; ...
                        4          ,    1; ...
                        1          ,    2; ...        % loc dof id 2: T_t1
                        2          ,    2; ...
                        3          ,    2; ...
                        4          ,    2  ...
                 ];

end

end % end of static methods
end % end of classdefinition