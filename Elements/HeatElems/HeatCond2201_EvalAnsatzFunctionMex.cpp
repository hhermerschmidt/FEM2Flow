#include <iostream>
#include "mex.h"



// function prototypes
void   mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] );

double evalAnsatzFunctionCpp( double xi1, double xi2, double tau);


// mex gateway function
void mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] ){

    double* N_pointer;

    // retrieve input arguments
    double xi1   = mxGetScalar( prhs[0] );
    double xi2   = mxGetScalar( prhs[1] );
    double tau   = mxGetScalar( prhs[2] );

    // perform computation
    double N   = evalAnsatzFunctionCpp(xi1, xi2, tau);

    // allocate memory for output argument
    plhs[0]    = mxCreateDoubleMatrix( 8, 1, mxREAL );

    // retrieve value of the pointer to the allocated memory
    N_pointer  = mxGetPr( plhs[0] );

    // copy solution/output value to the output memory adress
    *N_pointer = N;

    return;

}

// actual function performing the calculation
double evalAnsatzFunctionCpp( double xi1, double xi2, double tau ){

    double N[8];

    N[0] = 1/8 * ( (1-xi1)*(1-xi2)*(1-tau) );
    N[1] = 1/8 * ( (1+xi1)*(1-xi2)*(1-tau) );
    N[2] = 1/8 * ( (1+xi1)*(1+xi2)*(1-tau) );
    N[3] = 1/8 * ( (1-xi1)*(1+xi2)*(1-tau) );
    
    N[4] = 1/8 * ( (1-xi1)*(1-xi2)*(1+tau) );
    N[5] = 1/8 * ( (1+xi1)*(1-xi2)*(1+tau) );
    N[6] = 1/8 * ( (1+xi1)*(1+xi2)*(1+tau) );
    N[7] = 1/8 * ( (1-xi1)*(1+xi2)*(1+tau) );
   
    return N;
}