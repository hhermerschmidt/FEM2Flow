% Restart calculation by loading data from user-specific input data folder:
%--------------------------------------------------------------------------

% Add folders to path for restart: 
%-------------------------------------------
SetPathesDefault();

% Prepare data from previous calculation for restart:
%----------------------------------------------------
PrepareRestartData

%-----------------------------------
% loop over time slabs:
%-----------------------------------

while time.curr(1,3) <= time.total

    % Solve the system of eq. for the current time slab:
    %------------------------------------------------------
    SolveTimeSlab

    % Store results of current time slab:
    StoreResults

    % Set the time step of the next time slab:
    SetTimeStep

    % Store current results and prepare next calculation:
    InitialiseNextCalculation
end

% Save files related to previous calculations:
MakeOutputFiles
    
% Store calculation data to allow later restart:
StoreCalculation

% Remove folders from the search path:
RemovePathes