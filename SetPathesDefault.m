function [] = SetPathesDefault()
% Add folders to the search path (including all subfolders):

path(genpath([pwd,'/PreProcessing']  ),path)

path(genpath([pwd,'/Solution']       ),path)

path(genpath([pwd,'/PostProcessing'] ),path)

path(genpath([pwd,'/Elements']        ),path)

path(genpath([pwd,'/OutputRoutines'] ),path)

path(genpath([pwd,'/Utilities']      ),path)

end


