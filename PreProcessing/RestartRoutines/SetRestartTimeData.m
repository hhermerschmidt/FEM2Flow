function [ time ] = SetRestartTimeData( time )
% Function modifying 'time' structure due to restart of calculation.


% Set time and width of current time slab:
deltaTpending   = time.total - time.curr(1,1);
nIncPending     = time.nInc  - (time.currSlab - 1);

time.delta      = deltaTpending / nIncPending;
time.curr(1,2)  = time.curr(1,1) + 0.5 * time.delta;
time.curr(1,3)  = time.curr(1,1) + 1.0 * time.delta;

end
