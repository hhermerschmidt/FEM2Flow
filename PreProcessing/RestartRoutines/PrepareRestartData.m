% Script loading and if necessary modifying data for restart of calculation
%--------------------------------------------------------------------------

% Add user specific folders to the search path:
%------------------------------------------------
SetPathInputData('../InputData');

% Load calculation data:
%------------------------------
LoadCalculationData

% Set the time step of the first time slab after restart:
[ time ] = SetRestartTimeData( time );