function [ solu, deltaSolu, soluDis, elem ] = InitialiseSol( universe, elem )
% Function initialising the solution vectors. In case initial value
% condition are specified, these are stort in the overall solution vectors.
% In addition, the overall solution are also attached to each finite
% element in order to simplify elementwise postprocessing computations.


% -- Initialising global solution vectors ------------------------------- %

solu        = zeros(universe.dimSys,1);
deltaSolu   = zeros(universe.dimSys,1);
soluDis     = zeros(universe.dimSys,1);

% ----------------------------------------------------------------------- %


% -- Set initial condition globally ------------------------------------- %

if universe.flag.ic == 1
    
    [solu, soluDis ] = SetInitialConditon( solu, soluDis        , ...
                                           universe.ic          , ...
                                           universe.sysIndexMat , ...
                                           universe.sysIndex      ...
                                         );
    
end %if

% ----------------------------------------------------------------------- %


% -- Save initial condition also elementwise ---------------------------- %

[ elem ] = SaveICElementwise(universe, elem, solu, deltaSolu, soluDis);


end %function

% ======================================================================= %
% ======================================================================= %


% ======================================================================= %
% Subfunctions
% ======================================================================= %

function [solu, soluDis ] = SetInitialConditon( solu, soluDis        , ...
                                                initialCondition     , ...
                                                sysIndexMat, sysIndex   )
% Function setting the inital conditions into the overall solution vectors.

for ii = 1:size(initialCondition,1)

    posLocIC = sysIndexMat(initialCondition(ii,1), initialCondition(ii,2));

    if posLocIC == 0 % dof does NOT exist at current node
        disp(['WARNING: Invalid initial condition set at node ', ...
              num2str(initialCondition(ii,1))]                   ...
            )

    else

        posGlobIC = sysIndex(initialCondition(ii,1)) + posLocIC - 1;

        soluDis(posGlobIC) = initialCondition(ii,3); % here: initial condition
        solu(posGlobIC)    = initialCondition(ii,3); % here: start value for 1st iteration
                                                     %       of 1st time slab

    end %if

end %for
    
end

% ======================================================================= %


function [ elem ] = SaveICElementwise(universe, elem, solu, deltaSolu, soluDis)
% Function extracting the solution values belonging to each element out of
% the overall solution vectors and stores them also on elememtlevel.

for mm = 1 : universe.meshData.nEle

    % -- Extract eleSolu, eleDeltaSolu, eleSoluDis ------------------ %

    indexSystem = elem{mm}.indexSystem;

    eleSolu      = solu(indexSystem);
    eleSoluDis   = soluDis(indexSystem);
    eleDeltaSolu = deltaSolu(indexSystem);

    % --------------------------------------------------------------- %


    % -- Save eleSolu, eleDeltaSolu, eleSoluDis --------------------- %

    elem{mm}.SetEleSolu(eleSolu, eleDeltaSolu);
    elem{mm}.SetEleSoluDis(eleSoluDis);


    % --------------------------------------------------------------- %

end %for
    
end %function


% ======================================================================= %
% ======================================================================= %