function [ ] = SetCalculationData()
% This function opens and initialised the calculation protocol as well as
% the solution vectors of the overall system of equations. Furthermore, the
% calculation time of the first time step starts to count.


% -- Load global parameters and mesh data ------------------------------- %

load universe.mat
load time.mat
load elem.mat

% ----------------------------------------------------------------------- %


% Calculation protocol -------------------------------------------------- %
% case1: create calculation output,
%        create convergence data,
%        write loop output.

[~, convData] = StoreDispCalcProtocol([], [], 1, time, universe.folderResults); %#ok<NASGU,NODEF>

% ----------------------------------------------------------------------- %


% -- Initialise solution vectors ---------------------------------------- %

[solu, deltaSolu, soluDis, elem] = InitialiseSol(universe, elem); %#ok<NODEF,ASGLU,NASGU>

% ----------------------------------------------------------------------- %


% -- Set (start) time data ---------------------------------------------- %

time = SetTimeData(time); %#ok<NASGU>

% ----------------------------------------------------------------------- %


% -- Save calculation data ---------------------------------------------- %

save([universe.folderRestartData, 'time.mat'],        'time')
save([universe.folderRestartData, 'elem.mat'],        'elem')


save([universe.folderRestartData, 'convData.mat'],    'convData')

save([universe.folderRestartData, 'solu.mat'],        'solu')
save([universe.folderRestartData, 'deltaSolu.mat'],   'deltaSolu')
save([universe.folderRestartData, 'soluDis.mat'],     'soluDis')

% ----------------------------------------------------------------------- %


clear all

% ----------------------------------------------------------------------- %

end

% ======================================================================= %
% ======================================================================= %