function time = SetTimeData(time)
% Function initialised time data and calculating/setting time values
% for current time slab 

% -- Initialise time slab counter --------------------------------------- %

time.currSlab = 1;

% ----------------------------------------------------------------------- %


% -- Overall time range vector ------------------------------------------ %

time.range = zeros(1,time.nInc+1);
time.range(1,time.currSlab) = 0; % start time of first iteration

% ----------------------------------------------------------------------- %


% -- Time incremets ----------------------------------------------------- %

time.delta = time.total / time.nInc;        % time increment first time slab

time.deltaMin = time.delta * time.incMin;   % min time increment
time.deltaMax = time.delta * time.incMax;   % max time increment

% ----------------------------------------------------------------------- %


% -- Time vector of first iteration ------------------------------------- %

time.curr = [0.0, 0.5*time.delta, time.delta]; % quad in time [t0, tm, t1]

% ----------------------------------------------------------------------- %


% -- Start timer -------------------------------------------------------- %

time.start = tic;

% ----------------------------------------------------------------------- %

end

% ======================================================================= %
% ======================================================================= %