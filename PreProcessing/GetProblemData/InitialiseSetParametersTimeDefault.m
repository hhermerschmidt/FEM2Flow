function [ time ] = InitialiseSetParametersTimeDefault()
% Function initialisiung and setting default values for time parameters.

% -- Problem specific time parameters ----------------------------------- %
% These parameters are problem specific and have to be set in
%
% GetParametersTimeProblemSpecific.m:

time.total  = []; % end point of calculation
time.nInc   = []; % number of time increments


time.variation = []; % structure containing information on time evolution of
                     % time dependent data

% ----------------------------------------------------------------------- %



% -- Time parameters with default values -------------------------------- %

time.compTimeOverall = tic; %  counter overall time measurement

time.incMax = 10;     % [1] maximum ratio of time step to initial time step
time.incMin = 0.0001; % [1] minimal ratio of time step to initial time step

time.incEnh = 1.2;    % [1] factor enhancing time inc. in case of good convergence
time.incRed = 0.8;    % [1] factor reducing  time inc. in case of poor convergence

% ----------------------------------------------------------------------- %


% -- Time parameters calculated during calculation ---------------------- %

time.range    = []; % vector containing all time points
time.curr     = []; % time point vector [t0, tm, t1] for current time slab

time.currSlab = []; % time slab counter

time.delta    = []; % time step during iteration
time.deltaMin = []; % minimal time step
time.deltaMax = []; % maximal time step

time.start    = []; % start timer for time measurement
 
% ----------------------------------------------------------------------- %
    

end % function InitialiseSetParametersTimeDefault

% ======================================================================= %