function [universe] = InitialiseSetParametersGlobalDefault()
% Function initialising and setting default values for global parameters.

% -- Problem specific parameters ---------------------------------------- %
% These parameters are problem specific and have to be set in
%
% GetParametersGlobalProblemSpecific.m:

universe.name              = []; % Name of problem to be calculated     
universe.probDim           = []; % problem dimension

% and
% GetParametersMaterialProblemSpecific.m:

universe.matPara           = []; % material data set(s)
universe.loadPara          = []; % load     data set(s) 

% ----------------------------------------------------------------------- %  

                                 
% -- Mesh related parameters -------------------------------------------- %
% These attributes are handed over in GetElem() from feMesh.
% They are global mesh parameters which are needed for the overall
% computation procedure.

universe.nodes_t0          = []; % node coordinates at begin of time slab
universe.nodes_t1          = []; % node coordinates at end   of time slab

universe.inziConnect       = []; % element connectvity

universe.meshData.nEle     = []; % overall number of elements
universe.meshData.nNodes   = []; % overall number of nodes

universe.sysIndexMat       = []; % system index matrix
universe.sysIndex          = []; % system index vector

universe.nDofNodes         = []; % number of dof at each node
universe.dimSys            = []; % total number of dof
universe.sizeX             = []; % length of vectors used to create system of 
                                 % equations
                                 
universe.bcDir             = []; % Dirichlet BC
universe.bcNeu             = []; % Neumann   BC

universe.ic                = []; % initial condition
                                
% ----------------------------------------------------------------------- %  


% -- Flags -------------------------------------------------------------- %

universe.flag.timeStep       = 1;    % 0: Time step remains fixed
                                     % 1: Time step is adjusted automatically
                                     
universe.flag.remote         = 1;    % 0: Calculation is performed locally
                                     % 1: Calculation is performed remotely

universe.flag.meshMovement   = 0;    % 0: No mesh moving
                                     % 1:    mesh moving
                                     
universe.flag.ssmum          = 0;    % 0: no ssmum
                                     % 1: mesh movement using SSMUM
                                    
universe.flag.eigen          = 0;    % 0: no eigenstates of linearised system matrix are computed
                                     % 1:    eigenstates of linearised system matrix are computed
                                     
universe.flag.bcDirTime      = 0;    % 0: Dirichlet b.c.s are time-invariant
                                     % 1: Dirichlet b.c.s are time-dependent
 
universe.flag.ic             = 0;    % 0: ic are 0 everywhere;
                                     % 1: ic are set via input data file

universe.flag.levelSet       = 0;    % 0: level set fct is not reinitialised
                                     %    (in case one is existing)
                                     % n: level set fct is reinitialised
                                     %    after n time steps
                                     
universe.flag.dumpStiffnessMatrix = 0; % 0: stiffness matrix is not saved in
                                       %    h5-file during calculation
                                       % n: stiffness matrix is saved
                                       %    every nth time slab in external
                                       %    h5-file during calculation                                

                                       
universe.counter.LevelSet            = 1;

universe.counter.dumpStiffnessMatrix = 1; 

% ----------------------------------------------------------------------- %                                  


% -- Solving of global equation system ---------------------------------- %

% Linearisation of the non-linear system of eq.
universe.typeLinearisation   = 2;    % 1: Fixed point iteration         
                                     % 2: Newton Raphson procedure
                
% Solution proceedure for the linear(ised) system of eq.
universe.typeSolver          = 1;    % 1: direct   (Gauss) 
                                     % 2: iterative (gmres)

universe.nIterMaxSol         = 15;   % Max number of iterations of solution
                                     % for current time step
universe.nIterMinSol         = 2;    % Min number of iterations of solution
                                     % for current time step
   
universe.precision           = 1e-8; % requiered precision of convergence  

universe.typeEleUpdate       = 1;    % 0: no extrapolation of current results to
                                     %    next time step in elem.Update
                                     % 1: linear extrapolation of current results
                                     %    to next time step in elem.Update

universe.promptTerminate     = 1;    % 1: in case of poor convergence, it is
                                     %    asked how shall be advanced
                                     
% ----------------------------------------------------------------------- %
  

% -- Output Generation -------------------------------------------------- %

universe.outputType        = 1;  %  1: Paraview (space-only)
                                 %  2: Paraview (space-time)
                                 
universe.writeXMF          = 1; % Number of times steps, after which the
                                % xmf-file is written
universe.countWriteXMF     = 0; 

% ----------------------------------------------------------------------- %
                 

% -- Folders ------------------------------------------------------------ %

% Folder containing user specific routines
universe.folderRoutines  = '../InputData'; 
SetPathInputData(universe.folderRoutines);

universe.folderInputDataFiles = '../InputData/InputDataFiles/';

% Folder from which input data files (mesh etc) are loaded
universe.folderRestartData = '../InputData/RestartData/'; 
CreateRestartDataFolder(universe.folderRestartData);

% Folder used to store the calculation results
universe.folderResults   = '../Results/'; 
CreateResultsFolder(universe.folderResults);

% Matlab path
universe.ansPath             = path; % matlab path

% ----------------------------------------------------------------------- %

end % function InitialiseSetParametersGlobalDefault

% ======================================================================= %


% ======================================================================= %
% Subfunctions
% ======================================================================= %

function [] = CreateResultsFolder(resultsFolder)
%Function creating empty results folder 

% check if the results folder already exists and delete it if necessary
if exist(resultsFolder,'dir') == 7 && ~isempty(dir(resultsFolder))
    userChoice = input(['The results folder does already exist and contains data files.\n', ...
                        'How shall be proceeded?\n', ...
                        '1: delete data files in the results folder and continue\n', ...
                        '2: copy already existing results folder to another location\n', ...
                        '3: abort calculation?\n']);
    
    switch userChoice
        case 1
            rmdir(resultsFolder, 's');
            mkdir(resultsFolder); 
        case 2
            copyPath = input(['Specify as string the path to the new location including the new folder name',...
                              '(relative to /path/to/FEM2Flow/ and without a trailing slash)\n']);
            copyfile(resultsFolder, copyPath);
            rmdir(resultsFolder, 's');
            mkdir(resultsFolder); 
        case 3
            error('!!! Calculation interrupted by user !!!');      
        otherwise
            error('!!! No valid input specified !!!');

    end
    
    
else % results folder does not yet exist

    % create empty results folder:
    mkdir(resultsFolder)

end %if

end % function CreateResultsFolder

% ======================================================================= %


function [] = CreateRestartDataFolder(restartDataFolder)
% Function creating restart data folder (if it does not yet exist).

% check if the folder already exists and create it if not
if isempty(dir(restartDataFolder))
    mkdir(restartDataFolder)
end

% add restart data folder to path:
oldPath = cd(restartDataFolder); % change to restart data folder
path(pwd, path)                  % add restart data folder to path  
cd(oldPath)                      % return to working directory

end % function CreateRestartDataFolder

% ======================================================================= %