function [ ] = GetProblemData()
% Function setting data defining general solution parameters as well as 
% the calculation problem itself.


% -- Initialise and Set/Get Global Parameters --------------------------- %

[ universe ] = InitialiseSetParametersGlobalDefault();
    
[ universe ] = GetParametersGlobalProblemSpecific( universe );

% ----------------------------------------------------------------------- %


% -- Get Material Parameter Sets ---------------------------------------- %

[ universe ] = GetParametersMaterialProblemSpecific( universe );

% ----------------------------------------------------------------------- %


% -- Get Load Parameter Sets -------------------------------------------- %

[ universe ] = GetParametersLoadProblemSpecific( universe );

% ----------------------------------------------------------------------- %


% -- Initialise and Set Time Parameters --------------------------------- %

[ time ] = InitialiseSetParametersTimeDefault();

[ time ] = GetParametersTimeProblemSpecific( time ); %#ok<NASGU>

% ----------------------------------------------------------------------- %


% -- Initialise and Set/Get FE-Mesh Data -------------------------------- %

[ feMesh ] = GetMeshData( universe ); %#ok<NASGU>

% ----------------------------------------------------------------------- %


% -- Store Calculation Data --------------------------------------------- %

save([universe.folderRestartData, 'universe.mat'], 'universe')
save([universe.folderRestartData, 'time.mat'],     'time')
save([universe.folderRestartData, 'feMesh.mat'],   'feMesh')


% ----------------------------------------------------------------------- %


clear all

end % main function


% ======================================================================= %
% ======================================================================= %


% ======================================================================= %
% Subfunctions
% ======================================================================= %

function [ feMesh] = GetMeshData( universe )

% Initialise feMesh structure
feMesh = MeshFromInputData(universe);

% Load mesh data from input files
feMesh.BuildMesh;
  
% Transform feMesh from object to structure
feMesh = feMesh.TransformToStructure(); 

end % function GetMeshData

% ======================================================================= %
% ======================================================================= %