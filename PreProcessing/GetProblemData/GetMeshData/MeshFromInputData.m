classdef MeshFromInputData < MeshSuperclass
    
    
properties (SetAccess = protected)

    % Properties are defined in the superclass
    
end
        
% ======================================================================= %
        

methods
    
function feMesh = MeshFromInputData (universe)

    % superclass constructor
    feMesh = feMesh@MeshSuperclass(universe);

    % mesh type
    feMesh.type     = 'MeshFromInputData';   
    
    % path (relative) to input data files
    feMesh.folderInputDataFiles = universe.folderInputDataFiles;

end

% ======================================================================= %


function BuildMesh(feMesh)


    feMesh.SetCoordinates;
    
    feMesh.SetConnectivity;

    feMesh.SetElementData;

    feMesh.SetNeumannBc;

    feMesh.SetDirichletBc;   
    
    feMesh.SetInitialCondition;

end

% ======================================================================= %


function SetCoordinates(feMesh)            
% Function loading node coordinates from input data folder.

    inputDataCoordinates  = importdata([feMesh.folderInputDataFiles,'nodeCoordinates.csv'],',',7);
    
    nodes           = inputDataCoordinates.data(:,2:end);
    
    feMesh.nodes    = nodes;
    feMesh.nNodes   = size(nodes, 1);

end

% ======================================================================= %


function SetConnectivity(feMesh)            
% Function loading element connectivity from input data folder.

    inputDataConnectivity = importdata([feMesh.folderInputDataFiles,'nodeConnectivity.csv'],',',7);

    connectivity       = inputDataConnectivity.data(:,2:end);

    inziConnect        = num2cell(connectivity,2);
    
    feMesh.inziConnect = inziConnect;
    feMesh.nEle        = size(inziConnect, 1);

end

% ======================================================================= %

end % end of methods   

end % end of classdefinition