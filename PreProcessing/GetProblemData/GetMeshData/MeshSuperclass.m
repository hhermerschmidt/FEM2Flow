classdef MeshSuperclass < handle

properties (SetAccess = protected)
    
    
    type            % type/name of mesh (in general: MeshFromInputData)  

    
    % node coordinates
    nodes           % matrix: nNodes x 3: [xCoord | yCoord | zCoord]
    
    % element connectivity
    inziConnect     % cell: nEle x 1
    
    nNodes          % number of nodes
    nEle            % number of elements

 
    matPara         % structure containing material parameters
    loadPara        % structure containing volume loads

    % element table
    eleSet          % matrix: nEle x 3: [eleTypeID | matParaSetID | loadParaSetID] 

    
    % (initial) Dirichlet boundary conditions
    bcDir
    % matrix: var x 7 
    %
    % [nodeID | dofID | value | timeVariationID | timeSlabID | switchID | sysIndexID]	
    %
    % nodeID:			node id (global)
    % dofID:			dof id (global)
    % value:			value of dirichlet bc
    % timevariationID:	
    % timeSlabID:		time slab id (0: begin, 1: midpoint, 2:end)
    % switchID:			
    % sysIndexID:		position id (global) of bcDir in global/overall system
    
   	% (initial) Neumann boundary conditions			
    bcNeu          
    % matrix: var x 8  
    %
    % [elementID | areaTypeID | areaID | stressDirID | stressType | bcNeuTypeID | timeVariationID | stressValue]	
    %
    % elementID:        element id (global)
    % areaTypeID:       type of loaded area (1: point, 2: line)
    % areaID:           local id of loaded node/edge/surface
    % stressDirID:      stress direction if (1: x, 2: y, 3: z)
    % stressType:       stress variation along stress direction (0: const, )
    % bcNeuTypeID:      1: stress bc
    % timeVariationID 
    % stressValue     = [1, 0, 0, 0] ?
           
    
    % initial condition
    ic
    % matrix: (nNodes x nDofPerNode) x 3
    %
    % [nodeID | dofID | value ]
    %
    % nodeID:			node id (global)
    % dofID:			dof id (global)
    % value:			value of initial condition   
    
    
    % number of dof at each node
    nDofNodes       % vector: nNodes x 1
    
    
    % dimension of global system of eq. (total number of dof)
    dimSys          % scalar
    
    
    % dimension of unassembled system (used to create global sparse matrix)
    sizeX           % scalar
    
    
    % (relative) path to folder with input data files
    folderInputDataFiles % string


end

% ======================================================================= %
% ======================================================================= %


methods

function feMesh = MeshSuperclass(universe) % Constructor

    % material and load parameters
    feMesh.matPara          = universe.matPara;            
    feMesh.loadPara         = universe.loadPara;

    % (Initial) boundary conditions
    feMesh.bcNeu            = [];
    feMesh.bcDir         	= [];            

end

% ======================================================================= %


function SetElementData(feMesh)
% Function loading element type table.
% Array with nEle rows and
% 1st column: element type ID
% 2nd column: material parameter set ID
% 3rd column: load parameter set ID          
    
    inputDataEleSet = importdata([feMesh.folderInputDataFiles,'elementTable.csv'],',',7);
    
    feMesh.eleSet   = inputDataEleSet.data(:,2:end);

end

% ======================================================================= %


function SetNeumannBc(feMesh)
% Function loading array containing Neumann bc data.

try
    inputDataBCNeu = importdata([feMesh.folderInputDataFiles,'condBoundNeu.csv'],',',7);
catch %#ok<CTCH>
    inputDataBCNeu.data = [];
end

    feMesh.bcNeu = inputDataBCNeu.data;

end

% ======================================================================= %


function SetDirichletBc(feMesh)
% Function loading array containing Dirichlet bc data.

try
    inputDataBCDir = importdata([feMesh.folderInputDataFiles,'condBoundDir.csv'],',',7);
catch %#ok<CTCH>
    inputDataBCDir.data = [];
end

    feMesh.bcDir = inputDataBCDir.data;

end

% ======================================================================= %

function SetInitialCondition(feMesh)

try
    inputDataIC = importdata([feMesh.folderInputDataFiles,'condInitial.csv'],',',7);
catch %#ok<CTCH>
    inputDataIC.data = [];
end

    feMesh.ic = inputDataIC.data;

end

% ======================================================================= %


function [ feMeshStruct ] = TransformToStructure(feMesh)
   
% Transform feMesh from object to structure
warning('off', 'MATLAB:structOnObject')

feMeshStruct = struct(feMesh);

warning('on', 'MATLAB:structOnObject')
    
end

% ======================================================================= %

end  % end of methods 

% ======================================================================= %
% ======================================================================= %


end % end of class definitions

% ======================================================================= %
% ======================================================================= %