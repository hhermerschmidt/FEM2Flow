% Load data for calculations:
%-----------------------------------------------
load universe.mat   % load global parameters
load time.mat       % load time parameters

load elem.mat       % load elements

load convData.mat   % load calculation data

load solu.mat       % load solution
load deltaSolu.mat  % load change of calculation
load soluDis.mat    % load soluDis

load feOutput.mat   % load output objects


% Open protocol for calculation data (case 10):
%------------------------------------------------
[CalcProtocol, ~] = StoreDispCalcProtocol([], [], 10, [], ...
                                            universe.folderResults);
   
% Initialise counter in case of eigenstate calculation                                        
if universe.flag.eigen == 1
    nn_eigen = 1;
end