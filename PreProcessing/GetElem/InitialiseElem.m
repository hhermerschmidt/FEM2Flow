function [ elem, feMesh ] = InitialiseElem( feMesh )
% This function initialises the finite elements, creates the system index
% matrices, modifies the Dirichlet bc input data set and calculates the
% dimension of overall global system before condensation, needed to creat
% the vector I, J, and X.


% -- Initialise elements ------------------------------------------------ %
% Initialising the finite elements.

[ elem ] = ElementTypesTable( feMesh );

%------------------------------------------------------------------------ %


% -- Create system index matrices --------------------------------------- %
% Creating the system index matrices sysIndexMat and sysIndex.

[ feMesh ] = CreateSystemIndexMatrices(feMesh, elem );


% Set element index system vector:
% The vector elem{mm}.indexSystem is used to store the element matrices in
% the global system of equations.

for mm = 1 : feMesh.nEle
    SetIndexSystem(feMesh.sysIndex, feMesh.sysIndexMat, elem{mm} )
end

%------------------------------------------------------------------------ %


% -- Modify Dirichlet bc array ------------------------------------------ %
% Modification of the Dirichlet bc input data array by inserting adding the
% information of the position of each nodal Dirichlet bc in the global
% system of equations to feMesh.bcDir(:,7).

[ feMesh ] = ModifyBcDirArray( feMesh );

%------------------------------------------------------------------------ %


% -- Calculate length of uncondensed system vectors --------------------- %
% The length of the uncondensed system vector I, J, and X is calculated.
% These vector are later used to create the global, sparse matrix K
% The length is equal to the sum of the number of entries of all element
% matrices.

[ feMesh ] = CalcSizeX( feMesh, elem );

%------------------------------------------------------------------------ %

end %function InitialiseElem

% ======================================================================= %
% ======================================================================= %


% ======================================================================= %
% Subfunctions
% ======================================================================= %

function [ feMesh ] = CreateSystemIndexMatrices(feMesh, elem )

numNodes    = feMesh.nNodes;    % number of nodes
numEle      = feMesh.nEle;      % number of elements


% -- Create system index matrix ----------------------------------------- %
% Matrix which has as many rows as nodes and 30 columns (maximum number of
% dof at each node; global indexing). If the global dof exists at this node
% the entry in sysIndexMat is set to 1 otherwise it is 0.

sysIndexMat = zeros(numNodes, 30); % Initialisation

for mm = 1 : numEle
    
    % matrix containing global node numbers and associated global DOF-IDs
    eleNodesDofGlobal = elem{mm}.GetNodesDofGlobal;
    
    for ii = 1 : size(eleNodesDofGlobal, 1) % loop over all DOFs of curr elem
        % Set value of each existing dof in sysIndexMat to "1":
        % row: global node number
        % col: global DOF-ID
        % value = 1 , if DOF at node exists
        % value = 0 , if DOF at node does not exist
        sysIndexMat(eleNodesDofGlobal(ii,1), eleNodesDofGlobal(ii,2)) = 1;
    end
end

nDofNodes   = sum(sysIndexMat,2);   % number of dof at each node
dimSys      = sum(nDofNodes);       % total number of dof

feMesh.nDofNodes = nDofNodes;
feMesh.dimSys    = dimSys;

%------------------------------------------------------------------------ %

% -- Modify system index matrix ----------------------------------------- %
% The entries in the system index matrix sysIndexMat are renumbered in the
% way that for each node (in each row) the non-zero entries are numbered in
% ascending order from 1 to as many dof exist at this node.

for nn = 1 : numNodes
    posCurrDof = find(sysIndexMat(nn,:) == 1);
    sysIndexMat(nn, posCurrDof) = (1 : nDofNodes(nn)); %#ok<FNDSB>
end

feMesh.sysIndexMat = sysIndexMat;

%------------------------------------------------------------------------ %


% Create system index vector -------------------------------------------- %
% Vector with as many entries as nodes. Each entry indicates the occurence
% of the first dof of the current node in the global system

sysIndex    = zeros(numNodes,1);
sysIndex(1) = 1;

for nn = 2 : numNodes
    % increase sysIndex by number of dof of previous node
    sysIndex(nn) = sysIndex(nn-1) + nDofNodes(nn-1,1);    
end

feMesh.sysIndex = sysIndex;

end %function CreateSystemIndexMatrices


% ======================================================================= %


function [ feMesh ] = ModifyBcDirArray( feMesh )
% This function identifies the position of each Dirichlet bc in the global
% system of equations and stores it at the 7th position in the
% corresponding row of the Dirichlet bc input data matrix (which was in the
% beginning set to 0).

sysIndexMat = feMesh.sysIndexMat;
sysIndex    = feMesh.sysIndex;

bcDirMod    = feMesh.bcDir;

if ~isempty(bcDirMod)
    
    % loop over all Dirichlet bcs
    for ii = 1 : size(feMesh.bcDir, 1)
        
        % get local position of current bc at its node
        posLocBcDir = sysIndexMat(bcDirMod(ii,1), bcDirMod(ii,2));
                                % bcDirMod(ii,1): globalNodeID
                                % bcDirMod(ii,2): globalDofID
        
        if posLocBcDir == 0 % dof does NOT exist at current node
            disp(['WARNING: Invalid Dirichlet BC deleted at node ', ...
                  num2str(bcDirMod(ii,1))]                          ...
                )
            bcDirMod(ii, :) = 0;
        
        else % dof exists at current node
            % store global postion of current dof in col. 7
            bcDirMod(ii,7) = sysIndex(bcDirMod(ii,1)) + posLocBcDir - 1;
        
        end %if
    
    end %for

    % Delete bcs which cannot be set since dof does not exist at assigned node
    posValidBcDir = find(bcDirMod(:,1) ~= 0);

    % Store modified bcDir in feMesh
    feMesh.bcDir = bcDirMod(posValidBcDir, :); %#ok<FNDSB>
    
end %if

end %function ModiyBcDirArray

% ======================================================================= %


function [ feMesh ] = CalcSizeX( feMesh, elem )

sizeX = 0;
for mm = 1 : feMesh.nEle 
    sizeX = sizeX + elem{mm}.sizeXest;
end

feMesh.sizeX    = sizeX;

end

% ======================================================================= %

% ======================================================================= %
% ======================================================================= %