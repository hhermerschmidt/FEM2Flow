function [ ] = GetElem( )
% The finite elements are initialised and global mesh data is handed over
% to and stored in universe.


% -- Load global parameters and mesh data ------------------------------- %

load universe.mat
load feMesh.mat

% ----------------------------------------------------------------------- %


% -- Initialise finite elements ----------------------------------------- %

[elem, feMesh] = InitialiseElem(feMesh); %#ok<ASGLU,NODEF>

% ----------------------------------------------------------------------- %


% -- Transfer global mesh data ------------------------------------------ %

universe.nodes_t0           = feMesh.nodes;
universe.nodes_t1           = feMesh.nodes;

universe.inziConnect        = feMesh.inziConnect;

universe.meshData.nEle      = feMesh.nEle;
universe.meshData.nNodes    = feMesh.nNodes;

universe.sysIndexMat        = feMesh.sysIndexMat;
universe.sysIndex           = feMesh.sysIndex;

universe.nDofNodes          = feMesh.nDofNodes;
universe.dimSys             = feMesh.dimSys;
universe.sizeX              = feMesh.sizeX;

universe.bcDir              = feMesh.bcDir;
universe.bcNeu              = feMesh.bcNeu;
universe.ic                 = feMesh.ic;

% ----------------------------------------------------------------------- %


% Save global parameters, finite elements and mesh data ----------------- %

save([universe.folderRestartData, 'universe.mat'], 'universe')
save([universe.folderRestartData, 'elem.mat']    , 'elem'    )
save([universe.folderRestartData, 'feMesh.mat']  , 'feMesh'  )

% ----------------------------------------------------------------------- %


clear all

% ----------------------------------------------------------------------- %

end

% ======================================================================= %
% ======================================================================= %