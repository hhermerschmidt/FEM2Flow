function ordinateArray  = EvaluateTimeVariation(timeVariation,tEval)

% Description:
% Each row of ordinateArray contains the ordinates of the time variations
% given in timeVariation at time tEval.
% NOTE: tEval has to be a row vector

% Example:
%
% timeVariation = struct('id',      {1,  1,   10},...
%                       'para',     {[], [],  []},...
%                       'time',     {[], [],  [0,2,3,3.5,5]},...
%                       'ordinate', {[], [],  [0,1,1,2,2]});

% id: 1  ordinate constant = 1
% id: 2  switch: ordinate first = 1 then = 0
% id: 3  switch first = 0 then = 1
% id: 4  linear increasing
% id: 5  creep from 0 to 1
% id: 6  alternating
% id: 10 free time dependence, ('time' and 'ordinate' to be specified and
%        are then evaluated at tEval by interpolation) 


% Initialise output:
ordinateArray = zeros(size(timeVariation,2),size(tEval,2));

% Loop over columns of 'timeVariation'
for jj = 1:size(timeVariation,2) 
    
    % Loop over number of cols of 'tEval'
    for ii = 1:size(tEval,2)
        
        switch timeVariation(jj).id
  
            case 1 % constant 1
                ordinateArray(jj,ii) = 1;

            case 2 %switch erst 1 dann 0
                if tEval(1,ii) < timeVariation(jj).para
                    ordinateArray(jj,ii) = 1;
                else
                    ordinateArray(jj,ii) = 0;
                end
                
            case 3 % switch first 0, then 1
                if tEval(1,ii) < timeVariation(jj).para
                    ordinateArray(jj,ii) = 0;
                else
                    ordinateArray(jj,ii) = 1;
                end
                
            case 4 % linear increasing
                ordinateArray(jj,ii) = tEval(1,ii);

            case 5 %kriechverlauf
                if tEval(1,ii) < timeVariation(jj).para
                    ordinateArray(jj,ii) = tEval(1,ii) / timeVariation(jj).para;
                else
                    ordinateArray(jj,ii) = 1;
                end

            case 6 % alternating

                tt = mod(tEval(1,ii), timeVariation(jj).para);
                if tt <= timeVariation(jj).para / 4.0
                    ordinateArray(jj,ii) = 4 / timeVariation(jj).para * tt;
                elseif (tt > timeVariation(jj).para / 4 && t < 3 * timeVariation(jj).para / 4)
                    ordinateArray(jj,ii) = -4 / timeVariation(jj).para * tt + 2;
                else
                    ordinateArray(jj,ii) = 4 / timeVariation(jj).para * tt - 4;
                end
                
            case 7 % Smooth start-up process for channel flow
                
                t_developed = timeVariation(jj).time;

                if tEval(1,ii) <= t_developed

                    ordinateArray(jj,ii) = 1/t_developed * tEval(1,ii);
                else
                    ordinateArray(jj,ii) = 1;
                end
                
                
            case 10 % general time variation given in 'time' and 'ordinate'                
                ordinateArray(jj,ii) = interp1(timeVariation(jj).time, timeVariation(jj).ordinate, tEval(1,ii));
                         
        end     
    end
end

    