% Open file tree of fe-program

% -- Main File ---------------------------------------------------------- %

open ../../Start.m

% ----------------------------------------------------------------------- %


% -- Get Problem Data --------------------------------------------------- %

open ../../PreProcessing/GetProblemData/GetProblemData.m

open ../../PreProcessing/GetProblemData/InitialiseSetParametersGlobalDefault.m
open ../../PreProcessing/GetProblemData/InitialiseSetParametersTimeDefault.m

open ../../PreProcessing/GetProblemData/GetMeshData/MeshSuperclass.m
open ../../PreProcessing/GetProblemData/GetMeshData/MeshFromInputData.m

% ----------------------------------------------------------------------- %


% -- Initialise Finite Elements ----------------------------------------- %

open ../../PreProcessing/GetElem/GetElem.m

open ../../PreProcessing/GetElem/InitialiseElem.m
open ../../PreProcessing/GetElem/ElementTypesTable.m

% ----------------------------------------------------------------------- %


% -- Set Calculation Data ----------------------------------------------- %

open ../../PreProcessing/SetCalculationData/SetCalculationData.m

% ----------------------------------------------------------------------- %

% -- Initialise Output -------------------------------------------------- %

open ../../OutputRoutines/InitialiseOutput/InitialiseOutput.m

open ../../OutputRoutines/OutputClasses/MakeParaviewOutput.m
open ../../OutputRoutines/OutputClasses/MakeParaviewOutputSpaceTime.m


% -- Load Calculation Data ---------------------------------------------- %

open ../../PreProcessing/LoadCalculationData/LoadCalculationData.m

% ----------------------------------------------------------------------- %


% -- SolveTimeSlab ------------------------------------------------------ %

open ../../Solution/SolveTimeSlab/SolveTimeSlab.m
 
open ../../Solution/SolveTimeSlab/MeshMovement.m
open ../../Solution/SolveTimeSlab/ModifyBcDirTime.m

open ../../Solution/SolveTimeSlab/BuildElem.m
open ../../Solution/SolveTimeSlab/Assembling.m
open ../../Solution/SolveTimeSlab/Solver.m
open ../../Solution/SolveTimeSlab/PostElem.m
open ../../Solution/SolveTimeSlab/CheckConvergence.m

% ----------------------------------------------------------------------- %


% -- Store Results ------------------------------------------------------ %

open ../../Solution/StoreResults/StoreResults.m

% ----------------------------------------------------------------------- %


% -- SetTimeStep -------------------------------------------------------- %

open ../../Solution/SetTimeStep/SetTimeStep.m

open ../../Solution/SetTimeStep/CheckTerminateCalculation.m

% ----------------------------------------------------------------------- %


% -- InitialiseNextCalculation ------------------------------------------ %

open ../../Solution/InitialiseNextCalculation/InitialiseNextCalculation.m

open ../../Solution/InitialiseNextCalculation/Reinitialisation.m

open ../../Solution/InitialiseNextCalculation/RestoreElem.m
open ../../Solution/InitialiseNextCalculation/UpdateElem.m
open ../../Solution/InitialiseNextCalculation/UpdateSolution.m

% ----------------------------------------------------------------------- %


% -- Postprocessing ----------------------------------------------------- %

open ../../PostProcessing/MakeOutputFiles/MakeOutputFiles.m

open ../../PostProcessing/StoreCalculation/StoreCalculation.m

% ----------------------------------------------------------------------- %


% -- Utilities ---------------------------------------------------------- %

open ../../Utilities/StoreDispCalcProtocol/StoreDispCalcProtocol.m

open ../../Utilities/EvaluateTimeVariation/EvaluateTimeVariation.m

open ../../Utilities/SetIndexSystem/SetIndexSystem.m

% open ../../SetPathesDefault.m
% open ../../Utilities/SetRemovePathes/SetPathInputData.m
% 
% open ../../Utilities/SetRemovePathes/RemovePathes.m
% open ../../Utilities/SetRemovePathes/RemovePathInputData.m

% ----------------------------------------------------------------------- %


% -- Elements ----------------------------------------------------------- %

open ../../Elements/ElemSuperclass/ElemSuperclass.m

open ../../Elements/FluidElems/FluidElemSuperclass.m
open ../../Elements/FluidElems/FluidNavierStokes4200.m

open ../../Elements/FluidElems/FreeSurfaceElem.m
open ../../Elements/FluidElems/FluidNSFreeSurface4251.m

% ----------------------------------------------------------------------- %


open ../../Start.m