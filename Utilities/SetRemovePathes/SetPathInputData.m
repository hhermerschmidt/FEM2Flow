function [] = SetPathInputData(userRoutinesFolder)
% Function adding the routines folder '../InputData/' and all its
% subfolders to the path.
% This folder contains all input data files needed to set up the input
% datas in order to run the program.

   
oldPath = pwd;              % get current working directory

cd(userRoutinesFolder);     % go to user routines folder '../InputData/'

path(genpath(pwd), path);   % add user routines folder to path

cd(oldPath);                % go back to previous working directory
    
    
end