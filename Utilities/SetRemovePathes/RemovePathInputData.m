function RemovePathInputData(userRoutinesFolder)

    % Description: Removes the user specific 'userRoutinesFolder' and its
    % subfolders from the path:
    
    oldPath = pwd;
    cd(userRoutinesFolder);
    rmpath(genpath(pwd))
    cd(oldPath)
end