
% Remove folders from the search path:
%--------------------------------------

% Remove user specific routines:
if ~isempty(universe.folderRoutines)
    RemovePathInputData(universe.folderRoutines)
end


rmpath(genpath([pwd,'/PreProcessing']  ))

rmpath(genpath([pwd,'/Solution']       ))

rmpath(genpath([pwd,'/PostProcessing'] ))

rmpath(genpath([pwd,'/Elements']        ))

rmpath(genpath([pwd,'/OutputRoutines'] ))

rmpath(genpath([pwd,'/Utilities']      ))



% End of Computation
%----------------------------------
switch universe.flag.remote
    case 0 
        disp('Image is displayed.')

        % Load the ansPicture and show it:
        figure('color', 'w')
        ansImage = imread('Icon.jpg');
        image(ansImage)
        axis off
        axis image
        title('Computation is finished!', 'FontSize',18)
    
    case 1
        
    otherwise
        error('The flag universe.flag.remote is not set correctly.');
   
end
