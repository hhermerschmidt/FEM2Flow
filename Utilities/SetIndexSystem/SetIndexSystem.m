function [] = SetIndexSystem(sysIndex, sysIndexMat, elem )
   
% Get matrix containing global node numbers and associated global DOF-IDs
eleNodesDofGlobal   = elem.GetNodesDofGlobal;

% Overall number of occurring DOFs 
nEleDof             = size(eleNodesDofGlobal, 1);

indexSystem         = zeros(nEleDof,1); % Initialise indexSystem

for ii = 1 : nEleDof % loop over element DOFs
    currNode    = eleNodesDofGlobal(ii,1); % global node number of current node
    currDof     = eleNodesDofGlobal(ii,2); % global DOF-ID      at current node

    indexSystem(ii,1) =   sysIndex(    currNode , 1       ) ... % Position des 1. FHG des Knotens im globalen GLS
                        + sysIndexMat( currNode , currDof ) ... % Abstand zwischen dem 1. FHG des Knotens und dem jeweils betrachteten FHG
                        - 1;
end

% Store indexSystem for current element
elem.SetIndexSys(indexSystem);


end