    function [CalcProtocol, ...
          convData] = StoreDispCalcProtocol(CalcProtocol,...
                                            convData,...
                                            caseId,...
                                            time,...
                                            inputData)

                                        
    switch caseId
        case 1
            % Create the calculation protocol:
            userResultsFolder = inputData;
            CalcProtocol = fopen([userResultsFolder,'Protocol.txt'],'w');
            if CalcProtocol == -1
                error('The calculation protocol could not be created.')
            end
            
            % Write calculation data:
            fprintf(CalcProtocol,'%s \n\n','Calculation Protocol');
            fprintf(CalcProtocol,'---\n');
            fprintf(CalcProtocol,'%s \n',['timerange: 0 sec < t <= ',num2str(time.total) ,' sec']);
            fprintf(CalcProtocol,'---\n\n');
            
            % Close the protocol: 
            status  = fclose(CalcProtocol);
            if status == -1
                disp('The calculation protocol could not be closed')
            end
            
            % Write data of global loop:
            disp('---')
            disp(['timerange: 0 sec < t <= ' ,num2str(time.total) , ' sec'])
            disp('---')
            disp(' ')
            
            % Initialise convergence data:
            convData.iterHist       = zeros(time.nInc,2);
            convData.stopSol        = 0; % Set to 1 if solution is complex valued
            convData.poorConv       = 0; % Set to 1 if convergence is poor
            convData.goodConv       = 0; % Set to 1 if convergence is good
            convData.normSolu       = 0;
            convData.normDeltaSolu  = 0;
            convData.endOfTime      = 0; % used to break calculation after last time slab (regular end of calculations)
            
        case 2
            % Display and store start and end of current time slab:
            disp(['current timeslab: ',num2str(time.curr(1,1)),' sec', ' < t < ' ,num2str(time.curr(1,3)),' sec'])
            fprintf(CalcProtocol,'%s \n',['current timeslab: ',...
                num2str(time.curr(1,1)),' sec', ' < t < ' ,num2str(time.curr(1,3)),' sec']);
            
        case 3
            % Display and store the norm of the current iteration:
            iteration = inputData;
            relativeSolutionIncrease = abs(convData.normDeltaSolu/convData.normSolu);
            
            disp(['iteration: ',num2str(iteration),'  abs( norm(deltaSolu)   / norm(solu)  ) : ',num2str(relativeSolutionIncrease)])
            disp('              -')
            
            text = 'iteration: %2.0f  abs( norm(deltaSolu)   / norm(solu)  ) : %8.2e \n';
            fprintf(CalcProtocol,text,iteration,relativeSolutionIncrease);
            fprintf(CalcProtocol,'               -\n');            
            
%             fprintf(CalcProtocol,'Relative Solution Increment: %e \n',convData.normDeltaSolu/convData.normSolu);
            
        case 12
            % Display solution increments of individual fields (only for
            % free surface flow elements
            
            solu      = inputData(:,1);
            deltaSolu = inputData(:,2);
            solu      = solu - deltaSolu;
                                
            solu_vx         = solu(1:4:end);
            solu_vy         = solu(2:4:end);
            solu_p          = solu(3:4:end);
            solu_phi        = solu(4:4:end);
            solu_phit0      = solu(4:8:end);
            solu_phit1      = solu(8:8:end);

            deltaSolu_vx    = deltaSolu(1:4:end);
            deltaSolu_vy    = deltaSolu(2:4:end);
            deltaSolu_p     = deltaSolu(3:4:end);                    
            deltaSolu_phi   = deltaSolu(4:4:end);                   
            deltaSolu_phit0 = deltaSolu(4:8:end);
            deltaSolu_phit1 = deltaSolu(8:8:end);

            inc_vx    = abs(norm(deltaSolu_vx)    / norm(solu_vx));
            inc_vy    = abs(norm(deltaSolu_vy)    / norm(solu_vy));
            inc_p     = abs(norm(deltaSolu_p)     / norm(solu_p));
            inc_phi   = abs(norm(deltaSolu_phi)   / norm(solu_phi));
            inc_phit0 = abs(norm(deltaSolu_phit0) / norm(solu_phit0));
            inc_phit1 = abs(norm(deltaSolu_phit1) / norm(solu_phit1)); 
            
                  
            
            disp(['              abs( norm(delta_vx)    / norm(vx)    ) : ', num2str(inc_vx)])
            disp(['              abs( norm(delta_vy)    / norm(vy)    ) : ', num2str(inc_vy)])
            disp(['              abs( norm(delta_p )    / norm(p)     ) : ', num2str(inc_p)])
            disp(['              abs( norm(delta_phi)   / norm(phi)   ) : ', num2str(inc_phi)])
            disp(['              abs( norm(delta_phit0) / norm(phit0) ) : ', num2str(inc_phit0)])
            disp(['              abs( norm(delta_phit1) / norm(phit1) ) : ', num2str(inc_phit1)])
            disp('              -')
            
            
            text = '               abs( norm(delta_vx)    / norm(vx)    ) : %8.2e\n';
            fprintf(CalcProtocol,text,inc_vx);
            text = '               abs( norm(delta_vy)    / norm(vy)    ) : %8.2e\n';
            fprintf(CalcProtocol,text,inc_vy);
            text = '               abs( norm(delta_p)     / norm(p)     ) : %8.2e\n';
            fprintf(CalcProtocol,text,inc_p);
            text = '               abs( norm(delta_phi)   / norm(phi)   ) : %8.2e\n';
            fprintf(CalcProtocol,text,inc_phi);
            text = '               abs( norm(delta_phit0) / norm(phit0) ) : %8.2e\n';
            fprintf(CalcProtocol,text,inc_phit0);
            text = '               abs( norm(delta_phit1) / norm(phit1) ) : %8.2e\n';
            fprintf(CalcProtocol,text,inc_phit1);
            fprintf(CalcProtocol,'               -\n'); 

        case 13
            display(' ')
            display('---> Level-Set function is recalculated.')
            display(' ')
            
            text = '\n ---> Level-Set function is recalculated. \n\n';
            fprintf(CalcProtocol,text);
            
        case 4
            % Store time step and number of iterations for current time slab:
            iteration = inputData;
            convData.iterHist(time.currSlab,:) = [time.delta, iteration];
            
        case 5
            % Display and store that solution stopped:
            disp('------------------------------------------------')
            disp('Solution stopped due to poor convergence')
            disp('------------------------------------------------')
            fprintf(CalcProtocol,'%s \n','solution stopped');
            
        case 6
            % Output at end of calculations:
            
            % Store time step width and number of iterations for each time step:
            fprintf(CalcProtocol,'\n%s\n','delta t - number of iterations');
            
            for ii = 1 : time.currSlab - 1
                fprintf(CalcProtocol,'%f \t %i \n',convData.iterHist(ii,1), convData.iterHist(ii,2));
            end

%             % Close the protocol: 
%             status  = fclose(CalcProtocol);
%             if status == -1
%                 disp('The calculation protocol could not be closed')
%             end

            % Output if mesh generated without any further calculations:
            if time.total == 0
                disp('--------------------------------')
                disp('Mesh generated')
                disp('--------------------------------')
            end
            
        case 7
            % Display calculation time for current time slab and estimated
            % duration of loop over all time slabs:
            
            % Reached end of calculations:
            if time.delta < 1e-8
                disp(['computation time current timeslab: ', num2str(time.elapsed),' sec'])
                fprintf(CalcProtocol,'%s \n\n',['computation time current timeslab: ', num2str(time.elapsed),' sec']);
                disp('-----------------------------')
                disp('end')
                disp('-----------------------------')
                fprintf(CalcProtocol,'%s \n','End');
                convData.endOfTime = 1; % Set break for the while loop over time slabs
                return % return to invoking function
            end
            
            if convData.goodConv == 1
                % Display text for good convergence
                disp('-----------------------------')
                disp(['good convergence - enh delta t: ',num2str(time.delta),' sec'])
                disp('-----------------------------')
                fprintf(CalcProtocol,'%s \n',['delta t: ',num2str(time.delta),' sec']);
            end
            

            % Standard output for current time slab:
            disp(['computation time current timeslab: ', num2str(time.elapsed),' sec'])
            fprintf(CalcProtocol,'%s \n\n',['computation time current timeslab: ', num2str(time.elapsed),' sec']);
            disp(['number of timeslabs to compute: ', int2str((time.total-time.curr(1,1))/time.delta)])
            disp(['estimated total computation time: ', int2str(time.elapsed*(time.total-time.curr(1,1))/(time.delta*60)),' min'])
            disp('-')
            
        case 8
            % Store and display if calculation did not converge:
            
            disp('----------------------------------------------------------')
            disp(['no convergence - reduce delta t: ',num2str(time.delta),' sec'])
            disp('----------------------------------------------------------')
            fprintf(CalcProtocol,'%s \n',['delta t: ',num2str(time.delta),' sec']);
            
        case 9
            % Close the protocol: 
            status  = fclose(CalcProtocol);
            if status == -1
                disp('The calculation protocol could not be closed')
            end
            
            
        case 10
            % Open the calculation protocol in append mode:
            userResultsFolder = inputData;
            CalcProtocol = fopen([userResultsFolder,'Protocol.txt'],'a+');
            if CalcProtocol == -1
                error('The calculation protocol could not be opened.')
            end
            
        case 11
            % Display and Store total computation time
            display(' ')
            display(['Total Computation Time: ' num2str(time.compTimeOverall/60) ' min'])
            display(' ')
            fprintf(CalcProtocol,'\n\n%s',['Total Computation Time: ', num2str(time.compTimeOverall/60),' min']);    
            
        otherwise
            error('The chosen case is not yet defined')
    end




end