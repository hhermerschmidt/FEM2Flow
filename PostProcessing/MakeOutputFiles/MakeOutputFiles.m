% Create output files:
%-----------------------

% Store iteration history and close the protocol (case 6):
%--------------------------------------------------------------------
[~, ~] = StoreDispCalcProtocol(CalcProtocol, convData, 6, time, []);


% Create output file(s):
%------------------------------------------------
for ii = 1 : length(feOutput) % loop over output objects:
    feOutput{ii}.WriteOutputFile(time.range, time.currSlab - 1)
end