%---------------------------------------------------------------
% Store current state of calculation (can be later continued):
%---------------------------------------------------------------

% Set end of time flag to zero again:
%----------------------------------------
convData.endOfTime      = 0;

% Total Calculation Time
%-----------------------------
time.compTimeOverall = toc(time.compTimeOverall);
[~, ~] = StoreDispCalcProtocol(CalcProtocol, [], 11, time, []);

% Close calculation protocol (case 9):
%----------------------------------------
[~, ~] = StoreDispCalcProtocol(CalcProtocol, [], 9, [], []);


% Save the path:
universe.ansPath = path;

% Save variables:
%------------------
save([universe.folderRestartData,'universe.mat'],   'universe')
save([universe.folderRestartData,'time.mat'],       'time')

save([universe.folderRestartData,'convData.mat'],   'convData')

save([universe.folderRestartData,'elem.mat'],       'elem')     
save([universe.folderRestartData,'feOutput.mat'],   'feOutput')    

save([universe.folderRestartData,'solu.mat'],       'solu')     
save([universe.folderRestartData,'deltaSolu.mat'],  'deltaSolu')
save([universe.folderRestartData,'soluDis.mat'],    'soluDis')

if universe.flag.eigen == 1
    save([universe.folderRestartData,'EigVal.mat'], 'EigVal')
end

save([universe.folderRestartData,'iteration.mat'],  'iteration')

