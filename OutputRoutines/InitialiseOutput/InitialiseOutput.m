function [] = InitialiseOutput()
% Creating output objects and storing initial values prior to calculation.

% -- Load  data --------------------------------------------------------- %

load universe.mat   % global parameters
load elem.mat       % element data
load soluDis.mat    % initial condition

% ----------------------------------------------------------------------- %

% -- Initialise output objects ------------------------------------------ %

[ feOutput ] = InitialiseOutputObject( universe );

% ----------------------------------------------------------------------- %


% -- Initialise output fields ------------------------------------------- %

InitialiseOutputFields( elem, feOutput );

% ----------------------------------------------------------------------- %


% -- Save initial values prior ('0') to first time slab ----------------- %

WriteInitialData( universe, elem, soluDis, feOutput );

% ----------------------------------------------------------------------- %


% -- Save output objects ------------------------------------------------ %

save([universe.folderRestartData, 'feOutput.mat'], 'feOutput')

% ----------------------------------------------------------------------- %


clear all

% ----------------------------------------------------------------------- %

end

% ======================================================================= %
% ======================================================================= %


% ======================================================================= %
% Subfunctions
% ======================================================================= %

function [ feOutput ] = InitialiseOutputObject( universe )
% Function initialising the output data object which collects all data that
% is written to the hdf5-file and referenced via the xml-file.


nOutputObj  = length(universe.outputType); % number of output objects
feOutput    = cell(nOutputObj, 1);         % Initialise output objects

for ii = 1 : nOutputObj % loop over output objects

    switch universe.outputType(ii)
        
        case 1 % Paraview output (space-only)
            
            feOutput{ii} = MakeParaviewOutput(universe);
            
        case 2 % Paraview output (space-time)

            feOutput{ii} = MakeParaviewOutputSpaceTime(universe);    

        otherwise
            
            error('The chosen output type is not defined.')  
            
    end %switch
    
end %for

end %function InitialiseOutputObject

% ======================================================================= %


function [ ] = InitialiseOutputFields( elem, feOutput )
% Function initialising the field of the output data objects.


for ii = 1 : size(feOutput,1)

    feOutput{ii}.OutputFieldsInit(elem);

end

end %function InitialiseOutputFields

% ======================================================================= %


function [ ] = WriteInitialData(universe, elem, soluDis, feOutput )
% Function collecting the outputdata from each element, writing it to the
% hdf5-file of each element type and the creating the corresponding
% xml-file to reference the fields of the hdf5-file.

time0.currSlab = 0;

for ii = 1 : size(feOutput,1)
    
    feOutput{ii}.StoreOutput(elem, time0, universe, soluDis)
    
    feOutput{ii}.WriteOutputFile(0, time0.currSlab)

end %for

end %function WriteInitialData

% ======================================================================= %


% ======================================================================= %
% ======================================================================= %