classdef MakeParaviewOutputSpaceTime < handle
    
    properties (SetAccess = protected)
        
        probDim         % Problem dimension (1: 1D, 2: 2D, ...)
        nEle            % Number of elements used for Paraview ouput
        
        eleTypes        % Type numbers of used element types
        nEleTypes       % Number of different used element types
        
        outputEleMat    % Matrix summarizing data for each element type
                        % (one row per element type):
                        % Col 1: element type number (e.g. 101)
                        % Col 2: number of elements of the current type (e.g. 10)
                        % Col 3: global number of first element of current type (e.g. 1)
                        
        outputData      % Vector containing one structure of output related data
                        % for element type                           
        
        
        resultsFolder   % Folder used to store the output
        
        writeErrorLog   % structure used to log errors during writing to 
                        % hdf5-file
                        
    end
    
    
%---------------------------------------------------------------------------------------------------
%---------------------------------------------------------------------------------------------------

    methods
        
        
       function feOutput = MakeParaviewOutputSpaceTime(universe) 
       % Constructur initialises and sets:
       %
       % feOutput.outputSubType
       % feOutput.probDim
       % feOutput.nEle
       % feOutput.resultsFolder
       % feOutput.writeErrorLog
       %
       % Constructor initialises only:
       %
       % feOutput.eleTypes
       % feOutput.nEleTypes
       % feOutput.outputEleMat
       % feOutput.outputData
       
            
            feOutput.probDim        = universe.probDim;
            feOutput.nEle           = universe.meshData.nEle;
                        
            feOutput.eleTypes       = [];
            feOutput.nEleTypes      = [];
            
            feOutput.outputEleMat   = [];
            feOutput.outputData     = [];
            
            feOutput.resultsFolder  = universe.folderResults;
            
            feOutput.writeErrorLog.writeError = 0 ; % Set to 1 if any error occurs while writing
            feOutput.writeErrorLog.timeSlabs  = []; % Used to store time slabs where errors occured
        
        end
        
        
        %-------------------------------------------------------------------------------------------
        %-------------------------------------------------------------------------------------------
        
        
        function OutputFieldsInit(feOutput, elem)
        % Function sets:
        %
        % feOutput.eleTypes
        % feOutput.nEleTypes
        % feOutput.outputEleMat
        % feOutput.outputData (via elem.OutputFieldsInit)
            
            
            % Get used element types:
            %----------------------------
            
            eleTypesHelp = zeros(feOutput.nEle,1);
            makeOutput   = zeros(feOutput.nEle,1);
            
            for ii = 1 : feOutput.nEle % loop over all elements
                
                % Vector containing element type of each elememt
                eleTypesHelp(ii) = elem{ii}.type;
                
                % Vector containing switch if paraview output is created for current element
                makeOutput(ii)   = elem{ii}.paraOutput; 
            
            end
            
            % Except elements for which no paraview output is created:
            eleTypesHelp                 = eleTypesHelp(makeOutput == 1);
            
            % Used element types (only non-macro elements)
            feOutput.eleTypes            = sort( unique(eleTypesHelp) ); 
            
            % Number of used element types
            feOutput.nEleTypes           = length(feOutput.eleTypes);   

            
            % Summarizing data for each element type:
            %-------------------------------------------
            feOutput.outputEleMat        = zeros(feOutput.nEleTypes, 3);
            
            % Get number of element type numbers
            feOutput.outputEleMat(:,1)   = feOutput.eleTypes;
            
            % loop over all element types
            for ii = 1 : feOutput.nEleTypes
                
                % Get number of elements of each element type:
                feOutput.outputEleMat(ii, 2) = size(     ( find( eleTypesHelp            ...
                                                                 ==                      ...
                                                                 feOutput.eleTypes(ii,1) ...
                                                               )                         ...
                                                         )                               ...
                                                     , 1                                 ...
                                                   );
                
                % Get the first element of the current element type:
                feOutput.outputEleMat(ii, 3) = find(    eleTypesHelp                ...
                                                        ==                          ...
                                                        feOutput.outputEleMat(ii,1) ...
                                                    , 1                             ...
                                                   );
            
            end
            
            
            % Get output fields for each element type:
            %--------------------------------------------------------------
            %#ok<*AGROW>  % suppresses the warning due to change of size in each loop
            
            
            
                
                % loop over all element types:                                        
                for ii = 1 : feOutput.nEleTypes

                    % Initialising output fields for each occurring element type
%                    outputHelp([ii ii+1])    = elem{feOutput.outputEleMat(ii,3)}.OutputFieldsInitSpaceTime; 
                
                    outputHelp(ii)    = elem{feOutput.outputEleMat(ii,3)}.OutputFieldsInitSpaceTime; 
                    
                    
                    % Set number of nodes for output of each element type.
                    outputHelp(ii).nodesOut = outputHelp(ii).nNodesEle * feOutput.outputEleMat(ii,2);
                    % To display the results every node belonging to more than
                    % one element is duplicated in the degree how many
                    % elements share this node. This allows displaying
                    % discontinuities at the element boundaries.
                end

                % Store outputHelp in feOutput:
                feOutput.outputData = outputHelp;
           
        end
        
        
        %------------------------------------------------------------------
        %------------------------------------------------------------------
        
        
        function StoreOutput(feOutput, elem, time, universe, solu)
            
            % Help variables:
            outEleMat   = feOutput.outputEleMat;
            outputHelp  = feOutput.outputData;
            timeSlab    = time.currSlab; 
            
            
            % Initialise fields for output data of current timeSlab:
            %----------------------------------------
            
            % loop over all element types
            for ii = 1 : feOutput.nEleTypes 
                
                % Number of elements of current type 
                nEleCurr    = outEleMat(ii,2); 
                
                % Help variables for current time slab:
                outputHelp(ii).z1       = 1;
                outputHelp(ii).z2       = 1;

                % Node coordinates:
                outputHelp(ii).coord    = zeros(outputHelp(ii).nodesOut, 3); % 3 per node

                % Connectivity:
                switch feOutput.probDim
                    
                    case 2 % 2D mesh
                        % 4 nodes per element for output
                        outputHelp(ii).connect  = zeros(nEleCurr, 4);
                        
                    case 3 % 3D mesh
                        % 8 nodes per element for output
                        outputHelp(ii).connect  = zeros(nEleCurr, 8);
                        
                end
                
                % Number of elements of current element type:
                outputHelp(ii).eleNum  = zeros(nEleCurr, 1);
 
                % Cell data:
                for jj = 1 : outputHelp(ii).cells.num
                    outputHelp(ii).cells.data{1,jj} = zeros(nEleCurr, 1);
                end

                % Scalar data:
                for jj = 1 : outputHelp(ii).scalars.num
                    outputHelp(ii).scalars.data{1,jj} = zeros(outputHelp(ii).nodesOut, 1);
                end

                % Vector data:
                for jj = 1 : outputHelp(ii).vectors.num
                    outputHelp(ii).vectors.data{1,jj} = zeros(outputHelp(ii).nodesOut, 3);
                end

                % Tensor data:
                for jj = 1 : outputHelp(ii).tensors.num
                    outputHelp(ii).tensors.data{1,jj} = zeros(outputHelp(ii).nodesOut, 9);
                end
                
            end
            
            
            % Get the data for each element for the current timeSlab:
            %--------------------------------------------------------------
            for mm = 1 : feOutput.nEle   % loop over all elements
                
                % Check if output shall be set for current element: 
                if elem{mm}.paraOutput == 1
                
                    % Get necessary output data set for current element:
                                        
                    eleCoords.t0 = universe.nodes_t0(elem{mm}.eleNodes_t0, :);
                    eleCoords.t1 = universe.nodes_t1(elem{mm}.eleNodes_t1, :);

                    eleMatPara   = elem{mm}.matPara;                   
                    eleLoadPara  = elem{mm}.loadPara;
                    
                    eleSolu      = solu(elem{mm}.indexSystem, 1);
                    
                    ii           = find(outEleMat(:,1) == elem{mm}.type,1);
                   
                    % Get output data of current element:
                    outputHelp(ii) = elem{mm}.GetOutputDataSpaceTime( outputHelp(ii) , ...
                                                                      eleSolu        , ...
                                                                      eleCoords      , ...
                                                                      eleMatPara     , ...
                                                                      eleLoadPara    , ...
                                                                      time             ...            
                                                                    );
                end
            end
            
            
            %Create the hdf5-files:
            %------------------------------------
            data_stream  = ['/Discrete/Data/T_', sprintf('%04.0f', timeSlab),'/'];
            data_stream2 = ['/Discrete/Mesh/T_', sprintf('%04.0f', timeSlab),'/'];
            
            % Initialise flag for write errors for current time slab:
            currWriteError = 0;
            
            % loop over element types:
            for ii = 1 : size(outEleMat,1) 
                
                filename = [feOutput.resultsFolder,outputHelp(ii).filename,'.h5'];

                if timeSlab == 0
                    try      
                        hdf5write( filename                     , ...
                                   [data_stream2,'Coordinates'] , ...
                                    outputHelp(ii).coord'       , ... 
                                   'WriteMode','overwrite'        ...
                                 )
                    catch
                        currWriteError = 1;
                    end
                else
                      try   
                        hdf5write( filename                     , ...
                                   [data_stream2,'Coordinates'] , ...
                                    outputHelp(ii).coord'       , ... 
                                   'WriteMode','append'           ...
                                 )
                    catch
                        currWriteError = 1;
                      end
                    
                end
                                                      
                    try                         
                        hdf5write( filename                                , ...
                                   [data_stream2,'Connectivity']           , ...
                                    cast(outputHelp(ii).connect','uint64') , ...
                                   'WriteMode','append'                      ...
                                 )
                             
                    catch
                        currWriteError = 1;    
                    end
                
                
                % loop over cell data:
                for jj = 1 : outputHelp(ii).cells.num
                    try
                        hdf5write( filename                                        , ... 
                                   [data_stream,outputHelp(ii).cells.name{1,jj}]   , ...
                                   outputHelp(ii).cells.data{1,jj}'                , ...
                                   'WriteMode','append'                              ...
                                 )
                    catch
                        currWriteError = 1;    
                    end                    
                end
                
                % loop over scalar data:
                for jj = 1 : outputHelp(ii).scalars.num
                    try
                        hdf5write( filename , ...
                                   [data_stream,outputHelp(ii).scalars.name{1,jj}] , ...
                                   outputHelp(ii).scalars.data{1,jj}'              , ...
                                   'WriteMode','append'                              ...
                                 )
                    catch
                        currWriteError = 1;    
                    end                    
                end
                
                % loop over vector data:
                for jj = 1 : outputHelp(ii).vectors.num
                    try
                        hdf5write( filename                                        , ...
                                   [data_stream,outputHelp(ii).vectors.name{1,jj}] , ...
                                   outputHelp(ii).vectors.data{1,jj}'              , ...
                                   'WriteMode','append'                              ...
                                 )
                    catch
                        currWriteError = 1;    
                    end                    
                end
                
                % loop over tensor data:
                for jj = 1 : outputHelp(ii).tensors.num
                    try
                        hdf5write( filename                                        , ...
                                   [data_stream,outputHelp(ii).tensors.name{1,jj}] , ...
                                   outputHelp(ii).tensors.data{1,jj}'              , ...
                                   'WriteMode','append'                              ...
                                 )
                    catch
                        currWriteError = 1;   
                    end                    
                end 
            end
            
            if currWriteError == 1
                % Store that error occured:
                feOutput.writeErrorLog.writeError   = 1;
                % Add current time slab to list:
                feOutput.writeErrorLog.timeSlabs    = [feOutput.writeErrorLog.timeSlabs; timeSlab]; 
            end
            
        end
        
        
        %------------------------------------------------------------------
        %------------------------------------------------------------------
        
        
        function WriteOutputFile(feOutput, timeRange, timeSlab)
            
            % Get mesh properties required for output:
            OutputHelp    = feOutput.outputData;
            outEleMat     = feOutput.outputEleMat;
            
            % Get the names and the data types of the data to be stored:
            for ii = 1 : size(outEleMat,1) % loop over all element types
                
                for jj = 1 : size(OutputHelp(ii).filename,1) % loop over files

                    cells   = [];
                    scalars = [];
                    vectors = [];
                    tensors = [];

                    zz = 1;
                    for kk = 1 : OutputHelp(ii).cells.num
                        if ~isempty(OutputHelp(ii).cells)
                            cells(zz).name = OutputHelp(ii).cells.name{jj,kk};
                            cells(zz).type = OutputHelp(ii).cells.type{jj,kk};
                            zz = zz + 1;
                        end
                    end
                    
                    zz = 1;
                    for kk = 1 : OutputHelp(ii).scalars.num
                        if ~isempty(OutputHelp(ii).scalars)
                            scalars(zz).name = OutputHelp(ii).scalars.name{jj,kk};
                            scalars(zz).type = OutputHelp(ii).scalars.type{jj,kk};
                            zz = zz + 1;
                        end
                    end

                    zz = 1;
                    for kk = 1 : OutputHelp(ii).vectors.num
                        if ~isempty(OutputHelp(ii).vectors)
                            vectors(zz).name = OutputHelp(ii).vectors.name{jj,kk};
                            vectors(zz).type = OutputHelp(ii).vectors.type{jj,kk};
                            zz = zz + 1;
                        end
                    end

                    zz = 1;
                    for kk = 1 : OutputHelp(ii).tensors.num
                        if ~isempty(OutputHelp(ii).tensors)
                            tensors(zz).name = OutputHelp(ii).tensors.name{jj,kk};
                            tensors(zz).type = OutputHelp(ii).tensors.type{jj,kk};
                            zz = zz + 1;
                        end
                    end


                    % Call method to write current data in xmf-file:
%                     feOutput.WriteXmfFile(OutputHelp(ii).filename{jj,1},...
%                                         OutputHelp(ii).ele_type,...
%                                         VecElem(ii,2),...
%                                         OutputHelp(ii).NodesOut(jj,1),...
%                                         scalars,vectors,tensors,cells,...
%                                         timeRange(1,1:timeSlab)) 


                    feOutput.WriteXmfFile( OutputHelp(ii).filename       , ...
                                           OutputHelp(ii).ele_type       , ...
                                           outEleMat(ii,2)               , ...
                                           OutputHelp(ii).nodesOut(jj,1) , ...
                                           scalars                       , ...
                                           vectors                       , ...
                                           tensors                       , ...
                                           cells                         , ...
                                           timeRange(1,1:timeSlab+1)       ...
                                         ) 

                end    
            end
        end
        

        %------------------------------------------------------------------
        %------------------------------------------------------------------


        function WriteXmfFile  (feOutput,...
                                name_file,...
                                type_typology,...
                                number_of_elements,size_matrix,...
                                scalars,vectors,tensors,cells,...
                                timeRange)
            
            
            % Get number of nodes for different element typologies:
            switch type_typology
                case 'Quadrilateral'
                    nodes_elem = 4;
                case 'Quad_8'
                    nodes_elem = 8;
                case 'Hexahedron'
                    nodes_elem = 8;
                case 'Hex_20'
                    nodes_elem = 20;
                case 'Triangle'
                    nodes_elem = 3;
                case 'Tri_6'
                    nodes_elem = 6;
                case 'Tetrahedron'
                    nodes_elem = 4;
                case 'Tet_10'
                    nodes_elem = 10;  
            end

            size_matrix = size_matrix * 2;

            timeSlab = size(timeRange,2)-1;

            name = [feOutput.resultsFolder,name_file,'.xmf'];
            fid  = fopen(name,'w');

            if fid ~= -1

                fprintf(fid,'%s \n','<?xml version="1.0" encoding="utf-8"?>');
                fprintf(fid,'%s \n','<!DOCTYPE Xdmf SYSTEM "Xdmf.dtd" [');
                fprintf(fid,'%s \n',['<!ENTITY H5File "',name_file,'.h5">']);
                fprintf(fid,'%s \n',']>');

                fprintf(fid,'%s \n','<Xdmf>');
                fprintf(fid,'%s \n','<Domain Name="Discrete">');
                fprintf(fid,'%s \n',['<Grid Name="' ,name_file, '" GridType="Collection" CollectionType="Temporal">']);
                
                
                %loop over time steps:
                for i = 0 : timeSlab

%                    fprintf(fid,'%s%04.0f%s \n','<Grid name="T_',i,'">');
%                    fprintf(fid,'%s%f%s \n','<Time Value="',timeRange(1,i+1),'"/>');
%                    fprintf(fid,'%s \n',['<Topology TopologyType="',type_typology,'" NumberOfElements="',int2str(number_of_elements),'">']);
%                    fprintf(fid,'%s \n',['<DataItem Format="HDF" DataType="Int" Dimensions="',int2str(number_of_elements),' ',int2str(nodes_elem),'">']);
%                    fprintf(fid,'%s \n','&H5File;:/Discrete/Mesh/Connectivity');
%                    fprintf(fid,'%s \n','</DataItem>');
%                    fprintf(fid,'%s \n','</Topology>');
                    
                    fprintf(fid,'%s%04.0f%s \n','<Grid name="T_',i,'">');
                    fprintf(fid,'%s%f%s \n','<Time Value="',timeRange(1,i+1),'"/>');
                    fprintf(fid,'%s \n',['<Topology TopologyType="',type_typology,'" NumberOfElements="',int2str(number_of_elements),'">']);
                    fprintf(fid,'%s \n',['<DataItem Format="HDF" DataType="Int" Dimensions="',int2str(number_of_elements),' ',int2str(nodes_elem),'">']);
                    fprintf(fid,'%s%04.0f%s \n','&H5File;:/Discrete/Mesh/T_',i,'/Connectivity');
                    fprintf(fid,'%s \n','</DataItem>');
                    fprintf(fid,'%s \n','</Topology>');

%                     fprintf(fid,'%s \n','<Geometry Type="XYZ">');
%                     fprintf(fid,'%s \n',['<DataItem DataType="Float" Precision="4" Format="HDF" Dimensions="',int2str(size_matrix),' ',int2str(3),'">']);
%                     fprintf(fid,'%s \n','&H5File;:/Discrete/Mesh/Coordinates');
%                     fprintf(fid,'%s \n','</DataItem>');
%                     fprintf(fid,'%s \n','</Geometry>');
                    
                   	fprintf(fid,'%s \n','<Geometry Type="XYZ">');
                    fprintf(fid,'%s \n',['<DataItem DataType="Float" Precision="4" Format="HDF" Dimensions="',int2str(size_matrix),' ',int2str(3),'">']);
                    fprintf(fid,'%s%04.0f%s \n','&H5File;:/Discrete/Mesh/T_',i,'/Coordinates');
                    fprintf(fid,'%s \n','</DataItem>');
                    fprintf(fid,'%s \n','</Geometry>');

                    
                    
                    if ~isempty(cells)
                        % Cell data:
                        for j=1:size(cells,2)

                            fprintf(fid,'%s         \n',['<Attribute Name="',   cells(j).name,'" Center="Cell" Type="Scalar">']);
                            fprintf(fid,'%s         \n',['<DataItem DataType="',cells(j).type,'" Precision="4" Format="HDF" Dimensions="',int2str(number_of_elements),' ',int2str(1),'">']);
                            fprintf(fid,'%s%04.0f%s \n','&H5File;:/Discrete/Data/T_',i,['/',cells(j).name]);
                            fprintf(fid,'%s \n','</DataItem>');
                            fprintf(fid,'%s \n','</Attribute>');

                        end
                    end

                    if ~isempty(scalars)
                        % Scalar data:
                        for j=1:size(scalars,2)

                            fprintf(fid,'       %s \n',['<Attribute Name="',scalars(j).name,'" Center="Node" Type="Scalar">']);
                            fprintf(fid,'         %s \n',['<DataItem DataType="',scalars(j).type,'" Precision="4" Format="HDF" Dimensions="',int2str(size_matrix),' ',int2str(1),'">']);
                            fprintf(fid,'           %s%04.0f%s \n','&H5File;:/Discrete/Data/T_',i,['/',scalars(j).name]);
                            fprintf(fid,'         %s \n','</DataItem>');
                            fprintf(fid,'       %s \n','</Attribute>');

                        end
                    end

                    if ~isempty(vectors)
                        % Vector data:
                        for j=1:size(vectors,2)

                            fprintf(fid,'       %s \n',['<Attribute Name="',vectors(j).name,'" Center="Node" Type="Vector">']);
                            fprintf(fid,'         %s \n',['<DataItem DataType="',vectors(j).type,'" Precision="4" Format="HDF" Dimensions="',int2str(size_matrix),' ',int2str(3),'">']);
                            fprintf(fid,'           %s%04.0f%s \n','&H5File;:/Discrete/Data/T_',i,['/',vectors(j).name]);
                            fprintf(fid,'         %s \n','</DataItem>');
                            fprintf(fid,'       %s \n','</Attribute>');

                        end
                    end
                    
                    if ~isempty(tensors)
                        % Tensor data:
                        for j=1:size(tensors,2)

                            fprintf(fid,'       %s \n',['<Attribute Name="',tensors(j).name,'" Center="Node" Type="Tensor">']);
                            fprintf(fid,'         %s \n',['<DataItem DataType="',tensors(j).type,'" Precision="4" Format="HDF" Dimensions="',int2str(size_matrix),' ',int2str(9),'">']);
                            fprintf(fid,'           %s%04.0f%s \n','&H5File;:/Discrete/Data/T_',i,['/',tensors(j).name]);
                            fprintf(fid,'         %s \n','</DataItem>');
                            fprintf(fid,'       %s \n','</Attribute>');

                        end
                    end

                    fprintf(fid,'     %s \n','</Grid>');

                end

                fprintf(fid,'   %s \n','</Grid>');
                fprintf(fid,' %s \n','</Domain>');
                fprintf(fid,'%s \n','</Xdmf>');

                status=fclose(fid);
                if status == -1
                    disp(['The file ',name,' could not be closed.'])
                end
            else
                disp(['The file ',name,' could not be created.'])
            end  
        end
        
        
    end % end of method definitions
    
    
    %------------------------------------------------------------------
    %------------------------------------------------------------------
    
    
    methods (Static)
        function ShowOutput
            % No Paraview output shown in Matlab. 
            % Method might be used to start Paraview and show output there.
        end
    end
    
    
end % end of class definition